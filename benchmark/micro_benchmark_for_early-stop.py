import sys
import re
import functools
import operator
from collections import Counter
import random
import numpy as np
# from scipy.stats import norm, zipf

prefix = r"http://bm.org/"
rdf_type_property = r"http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
fact_set = r"fs_1"
fact_infix = r"f_"
dimension_infix = r"d_"
measure_infix = r"m_"
value_infix = r"v_"


def measures_distributions_with_counts(measures_distributions):
    return "_".join(
        [
            str(count) + distribution
            for (distribution, count) in Counter(
                measures_distributions
            ).items()
        ]
    )


"""def generate_data_triples(
    fact,
    fact2group,
    measures,
    measures_distributions,
    output_file,
):
    average_in_group = random_spread(10, combination)

    for i, measure in enumerate(measures):
        values_distribution = measures_distributions[i]
        if values_distribution == "uniform":
            value = int(np.random.uniform(0, 10000)) % 1000
        elif values_distribution == "geometric":
            value = np.random.geometric(0.35)
        elif values_distribution == "exponential":
            value = np.random.exponential(1)
        elif values_distribution == "laplace":
            value = np.random.laplace(10000, 1)
        elif values_distribution == "logistic":
            value = np.random.logistic(10000, 1)
        elif values_distribution == "normal":
            value = np.random.normal(-norm.ppf(0.01), 1000)
        elif values_distribution == "zipf":
            value = np.random.zipf(2.0)
        elif values_distribution == "spread1":
            value = random_spread(1, i)
        elif values_distribution == "spread10":
            value = random_spread(10, i)
        elif values_distribution == "spread100":
            value = random_spread(100, i)
        else:
            raise NotImplementedError("Distribution not supported")

        print(
            "<" + fact + "> <" + measure + "> " + str(value) + " .",
            file=output_file,
        )"""


def generate_dimension_triples(
    fact, dimensions, number_of_distinct_values, fact2group, output_file,
):
    combination = 0
    representation_base = 1
    for dimension in dimensions:
        value = int(random.uniform(0, number_of_distinct_values))
        print(
            "<"
            + fact
            + "> <"
            + dimension
            + "> <"
            + value_infix
            + str(value)
            + "> .",
            file=output_file,
        )
        combination += value * representation_base
        representation_base *= number_of_distinct_values

    fact2group[fact] = combination


def random_spread(spread_coefficient, i):
    random_value = np.random.uniform(0, 1)
    epsilon = 0.1
    return spread_coefficient * i + (random_value - 0.5) * epsilon


def generate_measure_triples(
    fact, measure, mean, output_file,
):
    value = np.random.normal(mean, 1)
    print(
        "<" + fact + "> <" + measure + "> " + str(value) + " .",
        file=output_file,
    )


def generate_URIs(collection, number_of_values, infix):
    for i in range(number_of_values):
        URI = prefix + infix + str(i)
        collection.append(URI)


def generate_triples(
    number_of_facts,
    number_of_dimensions,
    number_of_measures,
    measures_distributions,
    random_seed,
    output_file_name,
):
    if number_of_measures != len(measures_distributions):
        raise ValueError(
            "Mismatch between the number of measures and the\
                         number of measures distributions"
        )

    random.seed(random_seed)  # fixed for testing and reproducibility purposes

    # generate facts
    facts = []
    generate_URIs(facts, number_of_facts, fact_infix)

    # generate dimensions
    dimensions = []
    generate_URIs(dimensions, number_of_dimensions, dimension_infix)
    number_of_distinct_values = 100

    # generate measures
    measures = []
    generate_URIs(measures, number_of_measures, measure_infix)

    fact2group = {}

    with open(output_file_name, "w") as output_file:
        # generate triples
        for fact in facts:
            # add type to the fact
            print(
                "<"
                + fact
                + "> <"
                + rdf_type_property
                + "> <"
                + prefix
                + fact_set
                + "> .",
                file=output_file,
            )

            # generate dimension triples
            generate_dimension_triples(
                fact,
                dimensions,
                number_of_distinct_values,
                fact2group,
                output_file,
            )

        for measure in range(number_of_measures):
            groups = set(fact2group.values())
            # spread = 1
            # if measures_distributions[measure] != "uniform":
            #    spread = 10
            # groups_with_averages = {
            #    group: random_spread(spread, group) for group in groups
            # }
            values_distribution = measures_distributions[measure]
            if values_distribution == "uniform":
                groups_with_averages = {
                    group: int(np.random.uniform(0, 10000)) % 1000
                    for group in groups
                }
            elif values_distribution == "zipf":
                groups_with_averages = {
                    group: 100 * np.random.zipf(2.0) for group in groups
                }
            else:
                raise NotImplementedError("Distribution not supported")
            # print(groups_with_averages)

            for fact in facts:
                # generate measure triples
                generate_measure_triples(
                    fact,
                    measures[measure],
                    groups_with_averages[fact2group[fact]],
                    output_file,
                )


if __name__ == "__main__":
    # Example:
    """python3 micro_benchmark_for_early-stop.py 1000000 1
    'uniform:45, zipf:5' /data/datasets/Spade_benchmark/"""

    number_of_facts = int(sys.argv[1])
    number_of_dimensions = int(sys.argv[2])
    pat = re.compile(r"\s+")

    def measures_distributions_list(measure_distribution_repetitions):
        return [measure_distribution_repetitions[0]] * int(
            measure_distribution_repetitions[1]
        )

    measures_distributions = functools.reduce(
        operator.iconcat,
        [
            measures_distributions_list(measures_distribution_pair.split(":"))
            for measures_distribution_pair in pat.sub("", sys.argv[3]).split(
                ","
            )
        ],
        [],
    )
    path = sys.argv[4]

    generate_triples(
        number_of_facts=number_of_facts,
        number_of_dimensions=number_of_dimensions,
        number_of_measures=len(measures_distributions),
        measures_distributions=measures_distributions,
        random_seed=456,
        output_file_name=path
        + "micro_benchmark_"
        + str(
            "%g" % (number_of_facts / 1000000.0) + "M"
            if number_of_facts >= 1000000.0
            else number_of_facts
        )
        + "fac_"
        + str(number_of_dimensions)
        + "dim_"
        + str(len(measures_distributions))
        + "mea_"
        + measures_distributions_with_counts(measures_distributions)
        + ".nt",
    )
