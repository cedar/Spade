We aim at creating a graph that exhibits the following constraints (including some simplifications without the loss of generality).

1. Only 1 fact set FS of size F.
2. Only 1 lattice L.
3. Each fact f in FS should have D dimensions and M measures.
4. Each property of FS is subject to Spade-specific constraints
	(a) [frequent property] support > threshold (> 0.3)
	(b) [categorical property] proportion of distinct values < threshold (< 0.4)
	(c) number of facts that have the property == support
	(d) number of occurences of a property per fact == number of occurences
5. Dimensions
	(a) are categorical variables
	(b) have the number of distinct values >= 2 and <= 100
6. Measures
	(a) are numerical
	(b) are not multi-value (we don't need that because we pre-aggregate anyway)
	(c) have the number of distinct values > 100
	(d) follow their specified value distributions (potentially different for each one of them)
7. Lattice L
	(a) has D dimensions and M measures
	(b) the ratio between the number of distinct values of dimensions is set by user, e.g., 1:2:4:8.

For tests in Spade using the benchmark datasets, we disable all the derivations.

Input
- F: the total number of facts.
- D: the total number of dimensions.
- M: the total number of measures.
- d: a degree of sparsity of the data.
- r: a ratio of the number od distinct values of dimensions.
- measure_dimensions: a list of value distributions for measures.

To be implemented later
- fixed Spade thresholds: support threshold, frequency distribution, proportion/categorical-property threshold.