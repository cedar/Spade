#!/bin/bash

for NUMBER_OF_FACTS in 1000000 5000000 10000000; do
#NUMBER_OF_FACTS=1000000
	#for RATIO in '1:1:1:1:1' '1:2:4:20:50'; do
	#for RATIO in '1:1:1' '1:2:4'; do
	RATIO='1:20:50'
		for DENSITY in 0.1 0.5; do
			#python3 ./benchmark/micro_benchmark.py $NUMBER_OF_FACTS 5 $RATIO $DENSITY 10 '["uniform", "uniform", "uniform", "uniform", "uniform", "zipf", "zipf", "zipf", "zipf", "zipf"]' '/data/datasets/Spade_benchmark/'
			python3 ./benchmark/micro_benchmark.py $NUMBER_OF_FACTS 3 $RATIO $DENSITY 10 '["uniform", "uniform", "uniform", "uniform", "uniform", "zipf", "zipf", "zipf", "zipf", "zipf"]' '/data/datasets/Spade_benchmark/'
			#python3 ./benchmark/micro_benchmark.py $NUMBER_OF_FACTS 3 $RATIO $DENSITY 3 '["uniform", "zipf", "zipf"]' '/data/datasets/Spade_benchmark/'
			#python3 ./benchmark/micro_benchmark.py $NUMBER_OF_FACTS 3 $RATIO $DENSITY 5 '["uniform", "uniform", "zipf", "zipf", "zipf"]' '/data/datasets/Spade_benchmark/'
		done
	#done
done
