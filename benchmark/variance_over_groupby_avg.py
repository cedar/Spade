import sys
import pandas as pd
import itertools


def all_subsets(dimension_labels, max_number_of_dimensions):
    return [
        list(combination)
        for i in range(1, max_number_of_dimensions + 1)
        for combination in itertools.combinations(dimension_labels, i)
    ]


def join_dimensions(dimensions_by_label_in_combination):
    dimensions_iterator = iter(dimensions_by_label_in_combination)
    dimension_label = next(dimensions_iterator)
    joined_dimensions = dimensions_by_label_in_combination[dimension_label]
    joined_dimensions = joined_dimensions.rename(
        columns={"o": dimension_label[dimension_label.find("d_"): -1]}
    )
    while True:
        try:
            dimension_label = next(dimensions_iterator)
            dimension_to_join = dimensions_by_label_in_combination[
                dimension_label
            ]
            joined_dimensions = joined_dimensions.merge(
                dimension_to_join, on="s", suffixes=[None, "_m"]
            ).copy()
            joined_dimensions = joined_dimensions.rename(
                columns={"o": dimension_label[dimension_label.find("d_"): -1]}
            )
        except StopIteration:
            return joined_dimensions


if __name__ == "__main__":
    filename = sys.argv[1]

    graph = pd.read_csv(filename, names=["s", "p", "o", "dot"], delimiter=" ")[
        ["s", "p", "o"]
    ]
    graph = graph[
        graph["p"] != "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
    ]

    dimension_prefix = "<http://bm.org/d_"
    dimensions = graph[graph["p"].str.contains(dimension_prefix)].copy()
    dimension_labels = dimensions["p"].unique()
    measures = graph[~graph["p"].str.contains(dimension_prefix)].copy()
    measure_labels = measures["p"].unique()

    max_number_of_dimensions = 3
    dimension_labels_combinations_in_all_aggregates = all_subsets(
        dimension_labels, max_number_of_dimensions
    )

    for combination in dimension_labels_combinations_in_all_aggregates:
        dimensions_by_label_in_combination = {
            dimension_label: graph[graph["p"].str.contains(dimension_label)][
                ["s", "o"]
            ].copy()
            for dimension_label in combination
        }

        dimensions_in_combination_joined = join_dimensions(
            dimensions_by_label_in_combination
        )

        dimension_labels_to_group_by = list(
            filter(
                lambda x: x != "s",
                dimensions_in_combination_joined.columns.values.tolist(),
            )
        )

        variances = []
        variances_norm_mean = []

        for measure_label in measure_labels:
            measure = measures[measures["p"] == measure_label][
                ["s", "o"]
            ].copy()

            all_joined = dimensions_in_combination_joined.merge(
                measure, on="s", suffixes=[None, "_m"]
            ).copy()
            all_joined = all_joined.rename(columns={"o": "m"})
            all_joined["m"] = all_joined["m"].astype(float)

            grouped_by = all_joined.groupby(
                dimension_labels_to_group_by
            ).mean()

            var_over_groupby_avg = float(grouped_by.var()["m"])
            mean_over_groupby_avg = float(grouped_by.mean()["m"])
            variances += [
                (dimension_labels_to_group_by, measure, var_over_groupby_avg)
            ]
            variances_norm_mean += [
                (
                    dimension_labels_to_group_by,
                    measure_label,
                    var_over_groupby_avg / mean_over_groupby_avg ** 2,
                )
            ]

            # if var_over_groupby_avg / mean_over_groupby_avg ** 2 > 0.1:
            #    variances_in_groups = joined.groupby("o_d").agg(
            #        ["mean", "var", "count"]
            #    )
            #    print(column)
            #    print(variances_in_groups.to_string())

        # print(variances)
        print("Variance")
        for triple in sorted(variances, key=lambda x: -x[2]):
            print(triple)
        print("*" * 80)
        print("Variance normalized by mean")
        for triple in sorted(variances_norm_mean, key=lambda x: -x[2]):
            print(triple)
        print("*" * 80)
        print("*" * 80)
        print("*" * 80)
