import sys
import json
from functools import reduce
from collections import Counter
import random
import numpy as np
from scipy.stats import norm, zipf

prefix = r'http://bm.org/'
rdf_type_property = r'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
fact_set = r'fs_1'
fact_infix = r'f_'
dimension_infix = r'd_'
measure_infix = r'm_'
value_infix = r'v_'


def measures_distributions_with_counts(measures_distributions):
    return '_'.join([str(count) + distribution for
                    (distribution, count) in
                    Counter(measures_distributions).items()])


def generate_data_triples(
    fact,
    dimensions_with_number_of_distinct_values,
    measures,
    degree_of_sparsity,
    measures_distributions,
    output_file
):
    for (dimension, number_of_distinct_values) in\
         dimensions_with_number_of_distinct_values:
        value = int(random.uniform(1, number_of_distinct_values))
        print('<' + fact + '> <' + dimension + '> <' + value_infix
              + str(value) + '> .', file=output_file)

    for i, measure in enumerate(measures):
        values_distribution = measures_distributions[i]
        if values_distribution == 'uniform':
            value = np.random.uniform(0, 10000)
        elif values_distribution == 'geometric':
            value = np.random.geometric(0.35)
        elif values_distribution == 'exponential':
            value = np.random.exponential(1)
        elif values_distribution == 'laplace':
            value = np.random.laplace(10000, 1)
        elif values_distribution == 'logistic':
            value = np.random.logistic(10000, 1)
        elif values_distribution == 'normal':
            value = np.random.normal(-norm.ppf(0.01), 1)
        elif values_distribution == 'zipf':
            value = np.random.zipf(2.0)
        else:
            raise NotImplmentedError('Distribution not supported')

        print('<' + fact + '> <' + measure + '> ' + str(int(value) % 1000)
              + ' .', file=output_file)


def generate_URIs(collection, number_of_values, infix):
    for i in range(number_of_values):
        URI = prefix + infix + str(i)
        collection.append(URI)


def generate_triples(
    number_of_facts,
    number_of_dimensions,
    ratio_of_distinct_values_of_dimensions,
    degree_of_sparsity,
    number_of_measures,
    measures_distributions,
    random_seed,
    output_file_name
):
    if number_of_dimensions != len(ratio_of_distinct_values_of_dimensions):
        raise ValueError('Mismatch between the number of dimensions and the\
                         number of values in the ratio of distinct values of\
                         dimensions')

    if degree_of_sparsity < 0 or degree_of_sparsity > 1:
        raise ValueError('Degree of sparsity should fall in [0, 1]')

    if number_of_measures != len(measures_distributions):
        raise ValueError('Mismatch between the number of measures and the\
                         number of measures distributions')

    random.seed(random_seed)  # fixed for testing and reproducibility purposes

    # generate facts
    facts = []
    generate_URIs(facts, number_of_facts, fact_infix)

    # generate dimensions
    dimensions = []
    generate_URIs(dimensions, number_of_dimensions, dimension_infix)

    denominator = reduce(lambda x, y: float(x) * float(y),
                         ratio_of_distinct_values_of_dimensions) ** (
                         1. / number_of_dimensions)
    dimensions_with_number_of_distinct_values = []
    for (dimension, ratio_coefficient)\
            in zip(dimensions, ratio_of_distinct_values_of_dimensions):
        number_of_distinct_values = (number_of_facts / degree_of_sparsity) ** (
            1. / number_of_dimensions) * float(ratio_coefficient) / denominator
        if number_of_distinct_values > 100:
            number_of_distinct_values = 100
        dimensions_with_number_of_distinct_values\
            .append((dimension, number_of_distinct_values))
    # print(dimensions_with_number_of_distinct_values)
    '''print(reduce(lambda a, b: a * b, map(lambda x_y: x_y[1],
          dimensions_with_number_of_distinct_values)))'''

    # generate measures
    measures = []
    generate_URIs(measures, number_of_measures, measure_infix)

    with open(output_file_name, 'w') as output_file:
        # generate triples
        for fact in facts:
            # add type to the fact
            print('<' + fact + '> <' + rdf_type_property + '> <' + prefix
                  + fact_set + '> .', file=output_file)

            # generate triples with dimensions
            generate_data_triples(fact,
                                  dimensions_with_number_of_distinct_values,
                                  measures,
                                  degree_of_sparsity, measures_distributions,
                                  output_file)


if __name__ == '__main__':
    # Example:
    """python3 micro_benchmark.py 1000000 5 '1:2:4:20:50' 0.1 10 '["uniform",
    "uniform", "uniform", "uniform", "uniform", "zipf", "zipf", "zipf", "zipf",
    "zipf"]' /data/datasets/Spade_benchmark/"""

    number_of_facts = int(sys.argv[1])
    number_of_dimensions = int(sys.argv[2])
    ratio = sys.argv[3].split(':')
    degree_of_sparsity = float(sys.argv[4])
    number_of_measures = int(sys.argv[5])
    measures_distributions = json.loads(sys.argv[6])
    path = sys.argv[7]

    generate_triples(
        number_of_facts=number_of_facts,
        number_of_dimensions=number_of_dimensions,
        ratio_of_distinct_values_of_dimensions=ratio,
        degree_of_sparsity=degree_of_sparsity,
        number_of_measures=number_of_measures,
        measures_distributions=measures_distributions,
        random_seed=456,
        output_file_name=path + 'micro_benchmark_'
                         + str('%g' % (number_of_facts / 1000000.))
                         + 'M_' + sys.argv[2] + 'dim_'
                         + sys.argv[3].replace(':', '_')
                         + 'ratio_' + sys.argv[4] + 'sparsity_' + sys.argv[5]
                         + 'mea_'
                         + measures_distributions_with_counts(
                            measures_distributions)
                         + '.nt'
    )
