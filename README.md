# Spade project

## External libraries
The spmf library is needed to run Frequent Itemset Mining. It can be downloaded form `http://www.philippe-fournier-viger.com/spmf/index.php?link=download.php`

The list of all the algorithms is available at `https://www.philippe-fournier-viger.com/spmf/index.php?link=algorithms.php`

## Maven compilation
To compile the project run

`$ mvn clean install`

## Scanning for bugs in the code

### ErrorProne
To find some bugs, we can run ErrorProne (Google), a check during the compilation with annotations about bug

`$ mvn clean install -P error-prone`

### Infer
To find more bugs, we can run Infer (Facebook), which lists potential bugs

`$ infer -- mvn clean install`

To analyze the output of Infer, we can run

`$ infer-explore`

If we want to sort the potential bug by file name, we can run

`$ infer-explore --only-show | tr '\n' '$' | sed 's/$\$/@/g' | tr '@' '\n' | sort -k2 | sed \;G | tr '$' '\n'`

If we want to sort the potential bug by file name and skip some old code and potential NPE-related bugs, we can run

`$ infer-explore --only-show | tr '\n' '$' | sed 's/$\$/@/g' | tr '@' '\n' | grep -v benchmarks | grep -v old | grep -v 'molap/' | grep -v "NULL_DEREFERENCE" | sort -k2 | sed \;G | tr '$' '\n'`

### Spotbugs
To run Spotbugs

`mvn spotbugs:spotbugs`

Then, check `target/spotbugsXml.xml` file