//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.*;
import static fr.inria.cedar.spade.datastructures.Utils.generateSHA1;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class PostgresCubeEvaluator {
    private static final Logger LOGGER = Logger.getLogger(PostgresCubeEvaluator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    private final CandidateFactSet cfs;
    private final String decodedCF;
    private final ArrayList<AnalyzedAttribute> dimensions;
    private final ArrayList<AnalyzedAttribute> measures;
    private final HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> afsPerMeasure;
    private final DatabaseHandler handler;
    private final AggregateResultManager aggregateResultManager;

    protected HashMap<String, Pair<String, int[]>> countStarFromHashDimsToSHA1OfFilenameAndIdxNextTuple;
    protected HashMap<String, HashMap<AnalyzedAttribute, ArrayList<Pair<String, int[]>>>> allMeasuresFromHashDimsToMeasureToPairsOfSHA1OfFilenameAndIdxNextTuple;
    protected HashMap<String, String> dictionaryFromFilenameToSHA1;
    protected HashMap<String, String> dictionaryFromSHA1ToFilename;

    protected String directoryName;
    protected String filename;

    protected int howManyAggregatedMeasuresInQuery;

    private final int globalMMSTnumber;

    public PostgresCubeEvaluator(int globalMMSTnumber, CandidateFactSet cfs, String decodedCF,
                                 ArrayList<AnalyzedAttribute> dimensions,
                                 ArrayList<AnalyzedAttribute> measures,
                                 HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> afsPerMeasure,
                                 DatabaseHandler handler,
                                 AggregateResultManager aggregateResultManager) {
        this.cfs = cfs;
        this.decodedCF = decodedCF;
        this.dimensions = dimensions;
        this.measures = measures;
        this.handler = handler;
        this.aggregateResultManager = aggregateResultManager;

        this.globalMMSTnumber = globalMMSTnumber;

        this.afsPerMeasure = new HashMap<>();
        for (Map.Entry<AnalyzedAttribute, LinkedList<AggregationFunction>> entry: afsPerMeasure.entrySet()) {
            LinkedList<AggregationFunction> tempList = new LinkedList<>();
            for (AggregationFunction af: entry.getValue()) {
                if (af != AggregationFunction.RAW_VALUES_LIST) {
                    tempList.add(af);
                }
            }
            this.afsPerMeasure.put(entry.getKey(), tempList);
        }

        howManyAggregatedMeasuresInQuery = 1;
        for (AnalyzedAttribute m: measures) {
            for (AggregationFunction af: this.afsPerMeasure.get(m)) {
                howManyAggregatedMeasuresInQuery++;
            }
        }

        /*directoryName = GlobalSettings.FLASK_FOLDER_PATH + GlobalSettings.DATABASE_NAME + "/" + decodedCF + "/" + GlobalSettings.INTERESTINGNESS_FUNCTION + "/";
        File directory = new File(directoryName);
        if (!directory.exists()) {
            directory.mkdirs();
        }*/
        // directory creation moved to NewSystemInterface class
        directoryName = GlobalSettings.FLASK_FOLDER_PATH;
    }

    public void evaluate(boolean allowUseOfPreaggregatedMeasuresTables) {
        setUpWriters();
        String sqlQuery = buildQueryForCube(allowUseOfPreaggregatedMeasuresTables);
        //System.out.println(sqlQuery);

        ArrayList<String> previousValuesOfDimensions = new ArrayList<>();
        ArrayList<String> currentValuesOfDimensions = new ArrayList<>();

        double[] valuesOfAggregatedMeasures = new double[howManyAggregatedMeasuresInQuery];
        for (int i = 0; i < valuesOfAggregatedMeasures.length; i++) {
            valuesOfAggregatedMeasures[i] = Double.NaN;
        }

        addHeaderToAggregateResults();
        String dimVal;

        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                currentValuesOfDimensions.clear();
                int currentColumn = 1;

                for (AnalyzedAttribute aa: dimensions) {
                    dimVal = rs.getString(currentColumn);
                    if(dimVal!=null){
                        dimVal = dimVal.replace(","," ");
                    }
                    currentValuesOfDimensions.add(dimVal);
                    currentColumn++;
                }

                if (previousValuesOfDimensions.isEmpty()) { // first tuple in the resultset
                    valuesOfAggregatedMeasures[0] = rs.getDouble(currentColumn); // count star
                    currentColumn++;
                    int j = 1;
                    for (AnalyzedAttribute measure: measures) {
                        for (AggregationFunction af: afsPerMeasure.get(measure)) {
                            valuesOfAggregatedMeasures[j] = rs.getDouble(currentColumn);
                            if (rs.wasNull()) {
                                valuesOfAggregatedMeasures[j] = Double.NaN;
                            }
                            currentColumn++;
                            j++;
                        }
                    }
                }
                else {
                    if (dimensionValuesAreTheSame(previousValuesOfDimensions, currentValuesOfDimensions)) {
                        double previousCountStar = valuesOfAggregatedMeasures[0];
                        double currentCountStar = rs.getDouble(currentColumn); // count star
                        currentColumn++;

                        if (currentCountStar > previousCountStar) {
                            valuesOfAggregatedMeasures[0] = currentCountStar;
                            int j = 1;
                            for (AnalyzedAttribute measure: measures) {
                                for (AggregationFunction af: afsPerMeasure.get(measure)) {
                                    valuesOfAggregatedMeasures[j] = rs.getDouble(currentColumn);
                                    if (rs.wasNull()) {
                                        valuesOfAggregatedMeasures[j] = Double.NaN;
                                    }
                                    currentColumn++;
                                    j++;
                                }
                            }
                        }
                        else {
                            for (AnalyzedAttribute measure: measures) {
                                for (AggregationFunction af: afsPerMeasure.get(measure)) {
                                    //rs.getDouble(currentColumn);
                                    currentColumn++;
                                }
                            }
                        }
                    }
                    else {
                        storeTupleIntoAggregateResultManager(previousValuesOfDimensions, valuesOfAggregatedMeasures);
                        valuesOfAggregatedMeasures[0] = rs.getDouble(currentColumn);
                        currentColumn++;
                        int j = 1;
                        for (AnalyzedAttribute measure: measures) {
                            for (AggregationFunction af: afsPerMeasure.get(measure)) {
                                valuesOfAggregatedMeasures[j] = rs.getDouble(currentColumn);
                                if (rs.wasNull()) {
                                    valuesOfAggregatedMeasures[j] = Double.NaN;
                                }
                                currentColumn++;
                                j++;
                            }
                        }
                    }
                }

                previousValuesOfDimensions.clear();
                for (String s: currentValuesOfDimensions) {
                    previousValuesOfDimensions.add(s);
                }

            }
            rs.close();
            aggregateResultManager.commit();

            //long time = System.currentTimeMillis();
            //exportDataInKeyValueStoreToCSVFiles();
            //PerformanceMeasurements.totalTimeToExportAggregateResultsToCSVFiles[0] = Utils.calculateRunTimeInMilliseconds(time);
            //LOGGER.info("Time to export Redis data to CSV files: " + PerformanceMeasurements.totalTimeToExportAggregateResultsToCSVFiles[0] + " ms");
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private void storeTupleIntoAggregateResultManager (ArrayList<String> valuesOfDimensions,
                                                      double[] valuesOfAggregatedMeasures) {
        String nonNullDimensions = "";
        StringBuilder finalValuesOfDimensions;
        String finalValuesOfDims, hashOfDimensions;

        try {
            for (int ii = 0; ii < valuesOfDimensions.size(); ii++) {
                if (valuesOfDimensions.get(ii) != null && !valuesOfDimensions.get(ii).equals("null") && !valuesOfDimensions.get(ii).equals("NaN")) {
                    nonNullDimensions = nonNullDimensions + ii;
                }
            }

            if (!nonNullDimensions.isEmpty()) {
                finalValuesOfDimensions = new StringBuilder();
                for (String s: valuesOfDimensions) {
                    if (s != null && !s.equals("null")) {
                        finalValuesOfDimensions.append(s).append(", ");
                    }
                }
                //finalValuesOfDims = finalValuesOfDimensions.toString();

                hashOfDimensions = generateSHA1(nonNullDimensions);
                countStarFromHashDimsToSHA1OfFilenameAndIdxNextTuple.get(hashOfDimensions).getValue()[0]++;
                aggregateResultManager.addResultToAggregateResults(countStarFromHashDimsToSHA1OfFilenameAndIdxNextTuple.get(hashOfDimensions).getKey(), finalValuesOfDimensions, valuesOfAggregatedMeasures[0]);

                int posInValues = 1;
                for (AnalyzedAttribute measure: measures) {
                    int kk = 0;
                    for (AggregationFunction af: afsPerMeasure.get(measure)) {
                        if (!Double.isNaN(valuesOfAggregatedMeasures[posInValues])) {
                            if ((af != AggregationFunction.COUNT /*&& valuesOfAggregatedMeasures[posInValues] != 0*/) || (af == AggregationFunction.COUNT && valuesOfAggregatedMeasures[posInValues] != 0)) {
                                allMeasuresFromHashDimsToMeasureToPairsOfSHA1OfFilenameAndIdxNextTuple.get(hashOfDimensions).get(measure).get(kk).getValue()[0]++;
                                aggregateResultManager.addResultToAggregateResults(allMeasuresFromHashDimsToMeasureToPairsOfSHA1OfFilenameAndIdxNextTuple.get(hashOfDimensions).get(measure).get(kk).getKey(),  finalValuesOfDimensions, valuesOfAggregatedMeasures[posInValues]);
                            }
                        }
                        kk++;
                        posInValues++;
                    }
                }
            }

        }
        catch (NoSuchAlgorithmException ex) {
            throw new IllegalStateException(ex);
        }

    }

    protected void exportDataInKeyValueStoreToCSVFiles() {
        ArrayList<String> SHA1sOfFilenamesToAskFor = new ArrayList<>(GlobalSettings.NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME);
        String filename, filenameSHA1;
        int currentNumberOfTuplesToAskFor = 0, nrOfTuplesInCurrentFile;
        boolean fileIsInTheBackpack;

        for (Map.Entry<String, String> entry: dictionaryFromFilenameToSHA1.entrySet()) {
            filename = entry.getKey();
            filenameSHA1 = entry.getValue();

            if (GlobalSettings.NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME == 0) {
                aggregateResultManager.storeAggregateResultToCSV(filenameSHA1, filename);
            }
            else {
                nrOfTuplesInCurrentFile = findNumberOfTupleInFile(filenameSHA1);

                fileIsInTheBackpack = false;
                if ((currentNumberOfTuplesToAskFor + nrOfTuplesInCurrentFile <= GlobalSettings.NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME) || (nrOfTuplesInCurrentFile >= GlobalSettings.NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME && SHA1sOfFilenamesToAskFor.isEmpty())) {
                    SHA1sOfFilenamesToAskFor.add(filenameSHA1);
                    currentNumberOfTuplesToAskFor += nrOfTuplesInCurrentFile;
                    fileIsInTheBackpack = true;
                }

                if (!fileIsInTheBackpack || currentNumberOfTuplesToAskFor >= GlobalSettings.NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME) {
                    aggregateResultManager.storeAggregatesResultsToCSV(SHA1sOfFilenamesToAskFor, dictionaryFromSHA1ToFilename);
                    SHA1sOfFilenamesToAskFor.clear();
                    currentNumberOfTuplesToAskFor = 0;
                }

                if (!fileIsInTheBackpack) {
                    SHA1sOfFilenamesToAskFor.add(filenameSHA1);
                    currentNumberOfTuplesToAskFor = nrOfTuplesInCurrentFile;
                }
            }
        }

        if (!SHA1sOfFilenamesToAskFor.isEmpty()) {
            aggregateResultManager.storeAggregatesResultsToCSV(SHA1sOfFilenamesToAskFor, dictionaryFromSHA1ToFilename);
            SHA1sOfFilenamesToAskFor.clear();
        }
    }

    private int findNumberOfTupleInFile(String filenameSHA1) {
        for (Map.Entry<String, HashMap<AnalyzedAttribute, ArrayList<Pair<String, int[]>>>> entry: allMeasuresFromHashDimsToMeasureToPairsOfSHA1OfFilenameAndIdxNextTuple.entrySet()) {
            for (Map.Entry<AnalyzedAttribute, ArrayList<Pair<String, int[]>>> entry1: entry.getValue().entrySet()) {
                for (Pair<String, int[]> pairFileIndex: entry1.getValue()) {
                    if (pairFileIndex.getKey().equals(filenameSHA1)) {
                        return pairFileIndex.getValue()[0];
                    }
                }
            }
        }
        return -1;
    }

    private boolean dimensionValuesAreTheSame(
            ArrayList<String> previousValuesOfDimensions,
            ArrayList<String> currentValuesOfDimensions) {
        boolean sameDimensions = true;
        int k = 0;
        while (sameDimensions == true && k < currentValuesOfDimensions.size()) {
            if (currentValuesOfDimensions.get(k) == null && previousValuesOfDimensions.get(k) != null) {
                sameDimensions = false;
            }
            if (currentValuesOfDimensions.get(k) != null && previousValuesOfDimensions.get(k) == null) {
                sameDimensions = false;
            }
            if (currentValuesOfDimensions.get(k) != null && previousValuesOfDimensions.get(k) != null && !currentValuesOfDimensions.get(k).equals(previousValuesOfDimensions.get(k))) {
                sameDimensions = false;
            }
            k++;
        }
        return sameDimensions;
    }

    private void setUpWriters() {
        countStarFromHashDimsToSHA1OfFilenameAndIdxNextTuple = new HashMap<>();
        allMeasuresFromHashDimsToMeasureToPairsOfSHA1OfFilenameAndIdxNextTuple = new HashMap<>();
        dictionaryFromFilenameToSHA1 = new HashMap<>();
        dictionaryFromSHA1ToFilename = new HashMap<>();

        String combinationOfDimensions, hashOfCombinationsOfDimensions;
        int maxDimensions = dimensions.size();

        for (int k = 1; k <= maxDimensions; k++) {
            Iterator<int[]> iterator = CombinatoricsUtils.combinationsIterator(maxDimensions, k);
            while (iterator.hasNext()) {
                final int[] combination = iterator.next();

                combinationOfDimensions = String.valueOf(combination[0]);
                for (int i = 1; i < combination.length; i++) {
                    combinationOfDimensions = combinationOfDimensions + combination[i];
                }
                try {
                    hashOfCombinationsOfDimensions = generateSHA1(combinationOfDimensions);
                }
                catch (NoSuchAlgorithmException ex) {
                    throw new IllegalStateException(ex);
                }

                filename ="";
                if(globalMMSTnumber != 0){
                    filename = "L" + globalMMSTnumber +"___";
                }
                filename = filename + "CFS_" + decodedCF + "_Dimensions";

                for (int i = 0; i < combination.length; i++) {
                    filename += "_" + dimensions.get(combination[i]).getEncoding();
                }

                try {
                    String finalFilename = directoryName + filename + "__COUNT_STAR.csv";
                    String finalFilenameSHA1 = generateSHA1(finalFilename);
                    countStarFromHashDimsToSHA1OfFilenameAndIdxNextTuple.put(hashOfCombinationsOfDimensions, new Pair(finalFilenameSHA1, new int[1]));
                    countStarFromHashDimsToSHA1OfFilenameAndIdxNextTuple.get(hashOfCombinationsOfDimensions).getValue()[0] = 1;
                    dictionaryFromFilenameToSHA1.put(finalFilename, finalFilenameSHA1);
                    dictionaryFromSHA1ToFilename.put(finalFilenameSHA1, finalFilename);
                }
                catch (NoSuchAlgorithmException ex) {
                    throw new IllegalStateException(ex);
                }
                HashMap<AnalyzedAttribute, ArrayList<Pair<String, int[]>>> tempMeasure = new HashMap<>();
                for (AnalyzedAttribute measure: measures) {
                    ArrayList<Pair<String, int[]>> tempArray = new ArrayList<>();
                    for (AggregationFunction af: afsPerMeasure.get(measure)) {
                        if (af != AggregationFunction.RAW_VALUES_LIST) {
                            try {
                                String finalFilename = directoryName + filename + "__" + af + "_" + measure.getEncoding() + ".csv";
                                String finalFilenameSHA1 = generateSHA1(finalFilename);
                                tempArray.add(new Pair(finalFilenameSHA1, new int[1]));
                                tempArray.get(tempArray.size() - 1).getValue()[0] = 1;
                                dictionaryFromFilenameToSHA1.put(finalFilename, finalFilenameSHA1);
                                dictionaryFromSHA1ToFilename.put(finalFilenameSHA1, finalFilename);
                                //tempArray.add(new BufferedWriter(new FileWriter(directoryName + filename + "__" + af + "_" + measure.getEncoding() + ".csv")));
                            }
                            catch (NoSuchAlgorithmException ex) {
                                throw new IllegalStateException(ex);
                            }
                        }
                    }
                    tempMeasure.put(measure, tempArray);
                }
                allMeasuresFromHashDimsToMeasureToPairsOfSHA1OfFilenameAndIdxNextTuple.put(hashOfCombinationsOfDimensions, tempMeasure);
            }
        }
    }

    private void addHeaderToAggregateResults() {
        String filenameSHA1, filename;
        StringBuilder stringBuilderAggrgationFunction = new StringBuilder();
        StringBuilder decodedDimensions = new StringBuilder();
        for (AnalyzedAttribute dim: dimensions) {
            decodedDimensions.append(handler.decodeAttributeName(dim)).append(", ");
        }

        for (Map.Entry<String, Pair<String, int[]>> entry: countStarFromHashDimsToSHA1OfFilenameAndIdxNextTuple.entrySet()) {
            filenameSHA1 = entry.getValue().getKey();
            filename = dictionaryFromSHA1ToFilename.get(filenameSHA1);
            aggregateResultManager.addHeaderToAggregateResults(filename, filenameSHA1, decodedDimensions,   "count(*)");
        }

        for (Map.Entry<String, HashMap<AnalyzedAttribute, ArrayList<Pair<String, int[]>>>> entry: allMeasuresFromHashDimsToMeasureToPairsOfSHA1OfFilenameAndIdxNextTuple.entrySet()) {
            for (Map.Entry<AnalyzedAttribute, ArrayList<Pair<String, int[]>>> entry1: entry.getValue().entrySet()) {
                AnalyzedAttribute measure = entry1.getKey();
                String decodedMeasureName = handler.decodeAttributeName(measure);

                for (int i = 0; i < afsPerMeasure.get(measure).size(); i++) {
                    AggregationFunction af = afsPerMeasure.get(measure).get(i);
                    if(af != AggregationFunction.RAW_VALUES_LIST) {
                        filenameSHA1 = entry1.getValue().get(i).getKey();
                        filename = dictionaryFromSHA1ToFilename.get(filenameSHA1);
                        stringBuilderAggrgationFunction.setLength(0);
                        switch (af) {
                            case COUNT:
                                stringBuilderAggrgationFunction.append("count(").append(decodedMeasureName).append(")");
                                break;
                            case SUM:
                                stringBuilderAggrgationFunction.append("sum(").append(decodedMeasureName).append(")");
                                break;
                            case AVERAGE:
                                stringBuilderAggrgationFunction.append("avg(").append(decodedMeasureName).append(")");
                                break;
                            case MIN:
                                stringBuilderAggrgationFunction.append("min(").append(decodedMeasureName).append(")");
                                break;
                            case MAX:
                                stringBuilderAggrgationFunction.append("max(").append(decodedMeasureName).append(")");
                                break;
                            default:
                                break;
                        }
                        aggregateResultManager.addHeaderToAggregateResults(filename, filenameSHA1, decodedDimensions, stringBuilderAggrgationFunction.toString());
                    }
                }
            }
        }
    }

    private void appendToBufferWriter(BufferedWriter bw, ArrayList<String> tuple,
                                      Double aggregatedValue) {
        try {
            for (String t: tuple) {
                if (t != null && !t.equals("null")) {
                    bw.write(t);
                    bw.write(", ");
                }
            }
            bw.write(String.valueOf(aggregatedValue));
            bw.write("\n");
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private String buildQueryForCube(
            boolean allowUseOfPreaggregatedMeasuresTables) {
        StringBuilder sqlQuery = new StringBuilder("select ");
        String prefix = "";

        int i = 1;
        for (AnalyzedAttribute dim: dimensions) {
            if (dim.getDerivedFrom() != null && dim.getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
                sqlQuery.append(" rescube.dd").append(i);
            }
            else {
                sqlQuery.append("dictd").append(i).append(".cleanvalue");
            }
            sqlQuery.append(", ");
            i++;
        }
        sqlQuery.append("rescube.howmany"); // count(*)
        i = 1;
        for (AnalyzedAttribute measure: measures) {
            int j = 1;
            for (AggregationFunction af: afsPerMeasure.get(measure)) {
                sqlQuery.append(", mm").append(i).append("_").append(j);
                j++;
            }
            i++;
        }

        sqlQuery.append(" from ( select ");

        i = 1;
        for (AnalyzedAttribute dim: dimensions) {
            sqlQuery.append("d").append(i).append(".o as dd").append(i).append(", ");
            i++;
        }
        switch(GlobalSettings.CUBE_TYPE_OF_COUNT){
            case "count_star":
                sqlQuery.append("count(*) as howmany");
                break;
            case "count_distinct":
                sqlQuery.append("count(distinct cfs.s) as howmany");
                break;
            default:
                throw new IllegalStateException("Unsupported CUBE_TYPE_OF_COUNT");
        }

        i = 1;
        for (AnalyzedAttribute measure: measures) {
            int j = 1;
            for (AggregationFunction af: afsPerMeasure.get(measure)) {
                sqlQuery.append(", ");
                switch (af) {
                    case COUNT:
                        sqlQuery.append("sum(m").append(i).append(".count_o) as mm").append(i).append("_").append(j);
                        break;
                    case SUM:
                        sqlQuery.append("sum(m").append(i).append(".sum_o) as mm").append(i).append("_").append(j);
                        break;
                    case AVERAGE:
                        sqlQuery.append("sum(m").append(i).append(".sum_o)/");
                        if (afsPerMeasure.get(measure).contains(AggregationFunction.COUNT)) {
                            sqlQuery.append("sum(m").append(i).append(".count_o)");
                        }
                        else { // reconstructing count(m) from sum(m) in case the measure is atmostone
                            sqlQuery.append("count(m").append(i).append(".sum_o)");
                        }
                        sqlQuery.append(" as mm").append(i).append("_").append(j);
                        break;
                    case MIN:
                        sqlQuery.append("min(m").append(i).append(".min_o) as mm").append(i).append("_").append(j);
                        break;
                    case MAX:
                        sqlQuery.append("max(m").append(i).append(".max_o) as mm").append(i).append("_").append(j);
                        break;
                    default:
                        break;
                }
                j++;
            }
            i++;
        }

        sqlQuery.append(" from ").append(cfs.getTableName()).append(" as cfs ");
        i = 1;
        for (AnalyzedAttribute dim: dimensions) {
            sqlQuery.append(" left join t_").append(dim.getEncoding()).append(" d").append(i)
                    .append(" on ").append("d").append(i).append(".s=").append("cfs.s");
            i++;
        }

        if (allowUseOfPreaggregatedMeasuresTables) {
            i = 1;
            for (AnalyzedAttribute measure: measures) {
                sqlQuery.append(" left join t_").append(measure.getEncoding()).append("_preaggregated as m").append(i)
                        .append(" on m").append(i).append(".s=cfs.s");
                i++;
            }
        }
        else {
            i = 1;
            for (AnalyzedAttribute measure: measures) {
                if (measure.getDerivedFrom() != null && measure.getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
                    sqlQuery.append(" left join ( select s ");
                    for (AggregationFunction af: afsPerMeasure.get(measure)) {
                        switch (af) {
                            case COUNT:
                                sqlQuery.append(", count(o) as count_o");
                                break;
                            case SUM:
                                sqlQuery.append(", sum(o) as sum_o");
                                break;
                            case MIN:
                                sqlQuery.append(", min(o) as min_o");
                                break;
                            case MAX:
                                sqlQuery.append(", max(o) as max_o");
                                break;
                            default:
                                break;
                        }
                    }
                    sqlQuery.append(" from t_").append(measure.getEncoding()).append(" group by s ) as m").append(i)
                            .append(" on m").append(i).append(".s=cfs.s");
                }
                else {
                    sqlQuery.append(" left join ( select s ");
                    for (AggregationFunction af: afsPerMeasure.get(measure)) {
                        switch (af) {
                            case COUNT:
                                sqlQuery.append(", count(o) as count_o");
                                break;
                            case SUM:
                                sqlQuery.append(", sum(o) as sum_o");
                                break;
                            case MIN:
                                sqlQuery.append(", min(o) as min_o");
                                break;
                            case MAX:
                                sqlQuery.append(", max(o) as max_o");
                                break;
                            default:
                                break;
                        }
                    }
                    sqlQuery.append(" from t_").append(measure.getEncoding()).append(" join dictionary on key=s group by s ) as m").append(i)
                            .append(" on m").append(i).append(".s=cfs.s");
                }
                i++;
            }
        }

        sqlQuery.append(" group by cube(");
        StringBuilder listOfDims = new StringBuilder();
        prefix = "";
        i = 1;
        for (AnalyzedAttribute dim: dimensions) {
            listOfDims.append(prefix).append("d").append(i).append(".o");
            prefix = ", ";
            i++;
        }
        //sqlQuery.append(listOfDims.toString()).append(")").append(" order by ").append(listOfDims.toString()).append(" ) as rescube");
        sqlQuery.append(listOfDims.toString()).append(") ) as rescube");
        i = 1;
        for (AnalyzedAttribute dim: dimensions) {
            if (dim.getDerivedFrom() == null || (dim.getDerivedFrom() != null && dim.getDerivationType() != SuggestedDerivation.COUNT_ATTR)) {
                sqlQuery.append(" left join dictionary dictd").append(i).append(" on rescube.dd").append(i).append("=dictd").append(i).append(".key");
            }
            i++;
        }

        i = 1;
        sqlQuery.append(" order by ");
        prefix = "";
        for (AnalyzedAttribute dim: dimensions) {
            if (dim.getDerivedFrom() != null && dim.getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
                sqlQuery.append(prefix).append(" rescube.dd").append(i);
            }
            else {
                sqlQuery.append(prefix).append("dictd").append(i).append(".cleanvalue");
            }
            prefix = ", ";
            i++;
        }

        return sqlQuery.toString();
    }
}
