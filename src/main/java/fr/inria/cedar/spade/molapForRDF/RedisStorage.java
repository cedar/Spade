//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.configuration.PerformanceMeasurements;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.datastructures.Pair;
import fr.inria.cedar.spade.datastructures.Utils;
import java.io.*;
import java.util.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

public class RedisStorage extends AggregateResultStorage {
    private static final Logger LOGGER = Logger.getLogger(RedisStorage.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    private static final String LUAScriptToComputeVarianceOverAggregatesResultsDividedByMean = "" +
            "for j=1,#ARGV do " +
                "local filesha1=ARGV[j]; " +
                "local filesha1stats=filesha1..':stats' " +
                "local values = redis.call('HMGET', filesha1stats, 'count', 'avg'); " +
                "local count = tonumber(values[1]); " +
                "local avg = tonumber(values[2]); " +
                "local xi = 0.0; " +
                "local variance = 0.0; " +
                "local vals=redis.call('LRANGE',filesha1..':values',0,-1); " +
                "for i=1,#vals do " +
                    "xi=vals[i]-avg " +
                    "variance = variance + (xi*xi); " +
                "end; " +
                "if avg==0 then " +
                    "if count>1 then " +
                        "redis.call('HSET', filesha1stats, 'varianceoverdatadividedbymean', variance/((count-1))); " +
                    "else " +
                        "redis.call('HSET', filesha1stats, 'varianceoverdatadividedbymean', variance); " +
                    "end; " +
                "else " +
                    "if count>1 then " +
                        "redis.call('HSET', filesha1stats, 'varianceoverdatadividedbymean', variance/((count-1)*avg*avg)); " +
                    "else " +
                        "redis.call('HSET', filesha1stats, 'varianceoverdatadividedbymean', variance/(avg*avg)); " +
                    "end; " +
                "end; " +
            "end;";

    private static final String LUAScriptToComputeVarianceOverAggregatesResultsDividedBySum = "" +
            "for j=1,#ARGV do " +
                "local filesha1=ARGV[j]; " +
                "local filesha1stats=filesha1..':stats' " +
                "local values = redis.call('HMGET', filesha1stats, 'count', 'sum', 'avg'); " +
                "local count = tonumber(values[1]); " +
                "local sum = tonumber(values[2]); " +
                "local avg = tonumber(values[3]); " +
                "local xi = 0.0; " +
                "local variance = 0.0; " +
                "local vals=redis.call('LRANGE',filesha1..':values',0,-1); " +
                "for i=1,#vals do " +
                    "xi=vals[i]-avg " +
                    "variance = variance + (xi*xi); " +
                "end; " +
                "if sum==0 then " +
                    "if count>1 then " +
                        "redis.call('HSET', filesha1stats, 'varianceoverdatadividedbysum', variance/((count-1))); " +
                    "else " +
                        "redis.call('HSET', filesha1stats, 'varianceoverdatadividedbysum', variance); " +
                    "end; " +
                "else " +
                    "if count>1 then " +
                        "redis.call('HSET', filesha1stats, 'varianceoverdatadividedbysum', variance/((count-1)*sum*sum)); " +
                    "else " +
                        "redis.call('HSET', filesha1stats, 'varianceoverdatadividedbysum', variance/(sum*sum)); " +
                    "end; " +
                "end; " +
            "end;";

    private static final String LUAScriptToComputeVarianceOverAggregatesResultsWithFeatureScaling = "" +
            "for j=1,#ARGV do " +
                "local filesha1=ARGV[j]; " +
                "local filesha1stats=filesha1..':stats' " +
                "local values = redis.call('HMGET', filesha1stats, 'count', 'min', 'max', 'avg'); " +
                "local count = tonumber(values[1]); " +
                "local min = tonumber(values[2]); " +
                "local max = tonumber(values[3]); " +
                "local avg = tonumber(values[4]); " +
                "local xi = 0.0; " +
                "local variance = 0.0; " +
                "local vals=redis.call('LRANGE',filesha1..':values',0,-1); " +
                "for i=1,#vals do " +
                    "if min==max then " +
                        "xi=vals[i]-avg; " +
                    "else " +
                        "xi=((vals[i]-min)/(max-min))-avg " +
                    "end; " +
                    "variance = variance + (xi*xi); " +
                "end; " +
                "if count>1 then " +
                    "redis.call('HSET', filesha1stats, 'varianceoverdatawithfeaturescaling', variance/(count-1)); " +
                "else " +
                    "redis.call('HSET', filesha1stats, 'varianceoverdatawithfeaturescaling', variance); " +
                "end; " +
            "end;";

    private static final String LUAScriptToRetrieveVarianceMeanOfAggregatesResults = "" +
            "local result={}; " +
            "for i=1,#ARGV do " +
                "result[ARGV[i]]=redis.call('HGET',ARGV[i]..':stats','varianceoverdatadividedbymean'); " +
            "end;" +
            "return cjson.encode(result); ";

    private static final String LUAScriptToRetrieveVarianceSumOfAggregatesResults = "" +
            "local result={}; " +
            "for i=1,#ARGV do " +
                "result[ARGV[i]]=redis.call('HGET',ARGV[i]..':stats','varianceoverdatadividedbysum'); " +
            "end;" +
            "return cjson.encode(result); ";

    private static final String LUAScriptToRetrieveVarianceFeatureScalingOfAggregatesResults = "" +
            "local result={}; " +
            "for i=1,#ARGV do " +
                "result[ARGV[i]]=redis.call('HGET',ARGV[i]..':stats','varianceoverdatawithfeaturescaling'); " +
            "end;" +
            "return cjson.encode(result); ";

    private static final String LUAScriptToRetrieveAggregatesResults = "" +
            "local finalres={}; " +
            "for j=1,#ARGV do " +
                "local filesha1=ARGV[j]; " +
                "local nrOfTuplesInFile=redis.call('SCARD',filesha1); " +
                "if nrOfTuplesInFile>0 then " +
                    "local myres=redis.call('SMEMBERS',filesha1); " +
                    "table.insert(myres,1,redis.call('GET','header:'..filesha1)); " +
                    "finalres[ARGV[j]] = myres; " +
                "end; " +
            "end; " +
            "return cjson.encode(finalres)";


    private static final String LUAScriptToRetrieveTuples = "local myres={}; local myargs=unpack(ARGV); local mycard=redis.call('SCARD',myargs); if mycard>0 then myres=redis.call('SMEMBERS',myargs); local myh=redis.call('GET','header:'..myargs); table.insert(myres,1,myh); end; return myres";

    private final Jedis jedis;
    private Pipeline jedisPipeline;
    private int currentPipelineSize;
    private InterestingnessFunction interestingnessFunction;

    private String SHA1ofLUAScriptToRetrieveTuples;
    private String SHA1ofLUAScriptToRetrieveWholeFiles;
    private String SHA1OfLUAScriptToComputeIM;
    private String SHA1OfLUAScriptToRetrieveIMOfAggregatesResults;

    private StringBuilder stringBuilder;

    public RedisStorage(InterestingnessFunction interestingnessFunction) {
        jedis = new Jedis("localhost", 6379, 10000000);
        jedisPipeline = jedis.pipelined();
        currentPipelineSize = 0;
        this.interestingnessFunction = interestingnessFunction;

        SHA1ofLUAScriptToRetrieveTuples = jedis.scriptLoad(LUAScriptToRetrieveTuples);
        SHA1ofLUAScriptToRetrieveWholeFiles = jedis.scriptLoad(LUAScriptToRetrieveAggregatesResults);

        switch (interestingnessFunction){
            case VAR_MEAN:
                SHA1OfLUAScriptToComputeIM = jedis.scriptLoad(LUAScriptToComputeVarianceOverAggregatesResultsDividedByMean);
                SHA1OfLUAScriptToRetrieveIMOfAggregatesResults = jedis.scriptLoad(LUAScriptToRetrieveVarianceMeanOfAggregatesResults);
                break;
            case VAR_SUM:
                SHA1OfLUAScriptToComputeIM = jedis.scriptLoad(LUAScriptToComputeVarianceOverAggregatesResultsDividedBySum);
                SHA1OfLUAScriptToRetrieveIMOfAggregatesResults = jedis.scriptLoad(LUAScriptToRetrieveVarianceSumOfAggregatesResults);
                break;
            case VAR_FEATURE_SCALING:
                SHA1OfLUAScriptToComputeIM = jedis.scriptLoad(LUAScriptToComputeVarianceOverAggregatesResultsWithFeatureScaling);
                SHA1OfLUAScriptToRetrieveIMOfAggregatesResults = jedis.scriptLoad(LUAScriptToRetrieveVarianceFeatureScalingOfAggregatesResults);
                break;
        }
        stringBuilder = new StringBuilder();
    }

    @Override
    public boolean supportsInterestingnessMeasureComputation(){
        return interestingnessFunction == InterestingnessFunction.VAR_MEAN || interestingnessFunction == InterestingnessFunction.VAR_SUM || interestingnessFunction == InterestingnessFunction.VAR_FEATURE_SCALING;
    }

    @Override
    public void addHeaderToAggregateResults(String aggregateFilename, String aggregateSHA1, StringBuilder dimensionsHeader, String aggregatedMeasureHeader){
        stringBuilder.setLength(0);
        stringBuilder.append("header:").append(aggregateSHA1);
        addPairKeyValue(stringBuilder.toString(), dimensionsHeader.toString() + aggregatedMeasureHeader);
    }

    @Override
    public void addResultToAggregateResults(String aggregateSHA1, StringBuilder dimensions, double aggregatedMeasure){
        addValueToSet(aggregateSHA1, dimensions.toString() + aggregatedMeasure);
        addValueToList(aggregateSHA1 + ":values", String.valueOf(aggregatedMeasure));
    }

    @Override
    public List<String> retrieveAggregateResults(String SHA1OfFilename){
        return getTuples(SHA1OfFilename);
    }

    @Override
    public void storeAggregateResultToCSV(String SHA1OfFilename, String filename){
        ArrayList<String> tuples;
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
            tuples = getTuples(SHA1OfFilename);

            for (String t: tuples) {
                bw.write(t + "\n");
            }
            bw.close();
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public HashMap<String, ArrayList<String>> retrieveAggregatesResults(ArrayList<String> SHA1OfFilenames){
        HashMap<String, ArrayList<String>> result = new HashMap<>();
        for(String SHA1OfFilename: SHA1OfFilenames){
            result.put(SHA1OfFilename, getTuples(SHA1OfFilename));
        }
        return result;
    }

    @Override
    public void storeAggregatesResultsToCSV(ArrayList<String> SHA1OfFilenames,
                                            HashMap<String, String> dictionaryFromSHA1ToFilename) {
        String luaOutput = getFiles(SHA1OfFilenames.toArray(new String[0]));
        String fileSHA1;
        int i;
        JSONObject jsonObject = new JSONObject(luaOutput.trim());
        Iterator<String> keys = jsonObject.keys();

        while (keys.hasNext()) {
            fileSHA1 = keys.next();

            try (BufferedWriter bw = new BufferedWriter(new FileWriter(dictionaryFromSHA1ToFilename.get(fileSHA1)))) {
                JSONArray fileTuples = jsonObject.getJSONArray(fileSHA1);
                for (i = 0; i < fileTuples.length(); i++) {
                    String tup = fileTuples.getString(i) + "\n";
                    bw.write(tup);
                }
            }
            catch (IOException ex) {
                throw new IllegalStateException(ex);
            }
        }
    }

    @Override
    public void removeData() {
        jedis.flushDB();
    }

    @Override
    public void commit(){
    }

    protected void jedisSync() {
        long time = System.currentTimeMillis();
        jedisPipeline.sync();
        PerformanceMeasurements.totalTimeToExportAggregateResultsToCSVFiles[0] += Utils.calculateRunTimeInMilliseconds(time);
    }

    @Override
    public void commit(HashMap<String, double[]> aggregatesProfiles) {
        addAggregateStatisticsToPipeline(aggregatesProfiles);
        jedisSync();
        currentPipelineSize = 0;

        switch (interestingnessFunction){
            case VAR_MEAN:
            case VAR_SUM:
            case VAR_FEATURE_SCALING:
                jedis.evalsha(SHA1OfLUAScriptToComputeIM, 0, aggregatesProfiles.keySet().toArray(new String[0]));
                break;
            case SKEWNESS:
            case KURTOSIS:
            default:
                throw new IllegalStateException("Unsopported interestingness functions");
        }
    }

    private void addAggregateStatisticsToPipeline(HashMap<String, double[]> aggregatesProfiles){
        for(Map.Entry<String,double[]> entry: aggregatesProfiles.entrySet()){
            double[] values = entry.getValue();
            values[MOLAPMeasures.AVERAGE_ORDINAL] =values[MOLAPMeasures.SUM_ORDINAL] / values[MOLAPMeasures.COUNT_ORDINAL];
            HashMap statsToAdd = new HashMap();
            statsToAdd.put("count", String.valueOf(values[MOLAPMeasures.COUNT_ORDINAL]));
            statsToAdd.put("sum", String.valueOf(values[MOLAPMeasures.SUM_ORDINAL]));
            statsToAdd.put("min", String.valueOf(values[MOLAPMeasures.MIN_ORDINAL]));
            statsToAdd.put("max", String.valueOf(values[MOLAPMeasures.MAX_ORDINAL]));
            statsToAdd.put("avg", String.valueOf(values[MOLAPMeasures.AVERAGE_ORDINAL]));

            jedisPipeline.hmset(entry.getKey() + ":stats", statsToAdd);
            currentPipelineSize++;

            if(currentPipelineSize == GlobalSettings.REDIS_MAX_PIPELINE_SIZE){
                jedisSync();
                currentPipelineSize=0;
            }
        }
    }

    @Override
    public void closeConnection() {
        jedis.close();
    }


    private void addPairKeyValue(String key, String value) {
        if(currentPipelineSize == GlobalSettings.REDIS_MAX_PIPELINE_SIZE){
            jedisSync();
            currentPipelineSize = 0;
        }
        jedisPipeline.set(key, value);
        currentPipelineSize++;
    }

    private void addValueToSet(String setKey, String value) {
        if(currentPipelineSize == GlobalSettings.REDIS_MAX_PIPELINE_SIZE){
            jedisSync();
            currentPipelineSize = 0;
        }
        jedisPipeline.sadd(setKey, value);
        currentPipelineSize++;
    }

    private void addValueToList(String listKey, String value){
        if(currentPipelineSize == GlobalSettings.REDIS_MAX_PIPELINE_SIZE){
            jedisSync();
            currentPipelineSize = 0;
        }
        jedisPipeline.lpush(listKey, value);
        currentPipelineSize++;
    }

    private ArrayList<String> getTuples(String SHA1OfFileToRetrieve){
        return (ArrayList<String>) jedis.evalsha(SHA1ofLUAScriptToRetrieveTuples,0,SHA1OfFileToRetrieve);
    }

    private String getFiles(String[] SHA1OfFilesToRetrieve){
        return (String) jedis.evalsha(SHA1ofLUAScriptToRetrieveWholeFiles, 0, SHA1OfFilesToRetrieve);
    }

    @Override
    public ArrayList<Pair<String, Double>> storeTopKToCSV(int k, HashMap<String, String> dictionaryFromSHA1ToFilename){
        ArrayList<Pair<String,Double>> allResults = new ArrayList<>();
        ArrayList<Pair<String,Double>> topKFilesResults = new ArrayList<>();
        ArrayList<String> topKFiles = new ArrayList<>();

        LinkedList<String> paramsForLUAScript = new LinkedList<>(dictionaryFromSHA1ToFilename.keySet());
        String luaOutput = (String) jedis.evalsha(SHA1OfLUAScriptToRetrieveIMOfAggregatesResults, 0, paramsForLUAScript.toArray(new String[0]));

        String fileSHA1;
        Double imOfFile;
        Pair<String,Double> currentPair;
        int i;

        JSONObject jsonObject = new JSONObject(luaOutput.trim());
        Iterator<String> keys = jsonObject.keys();
        while(keys.hasNext()) {
            fileSHA1 = keys.next();
            imOfFile = jsonObject.getDouble(fileSHA1);
            allResults.add(new Pair<>(fileSHA1,imOfFile));
        }
        sortResults(allResults);

        int maxNrOfResults = allResults.size();
        for (i = 0; i < k && i < maxNrOfResults; i++){
            currentPair = allResults.get(i);
            topKFilesResults.add(new Pair<>(dictionaryFromSHA1ToFilename.get(currentPair.getKey()),currentPair.getValue()));
            topKFiles.add(currentPair.getKey());
        }

        storeAggregatesResultsToCSV(topKFiles, dictionaryFromSHA1ToFilename);

        return topKFilesResults;

    }

    private void sortResults(ArrayList<Pair<String,Double>> results){
        Collections.sort(results, new ResultsComparator());
    }

    private static class ResultsComparator implements
            Comparator<Pair<String, Double>> {
        @Override
        public int compare(Pair<String, Double> agg1, Pair<String, Double> agg2) {
            Double score1 = agg1.getValue();
            Double score2 = agg2.getValue();

            if (score1 == null) {
                return -1;
            }
            if (score2 == null) {
                return -1;
            }

            return -score1.compareTo(score2);
        }
    }
}
