//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import com.sleepycat.je.*;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.configuration.PerformanceMeasurements;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.datastructures.Pair;
import fr.inria.cedar.spade.datastructures.Utils;
import java.io.*;
import java.util.*;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class BerkeleyDBStorage extends AggregateResultStorage {
    private static final Logger LOGGER = Logger.getLogger(BerkeleyDBStorage.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    @Entity
    static class AggregateResult {

        @PrimaryKey
        String filenameSHA1;

        ArrayList<String> results;

        AggregateResult(String filenameSHA1) {
            this.filenameSHA1 = filenameSHA1;
            results = new ArrayList<>();
        }

        public void addResult(String result){
            results.add(result);
        }

        public ArrayList<String> getResults(){
            return results;
        }

        private AggregateResult() {} // For deserialization
    }

    static class AggregateResultAccessor {
        /* AggregateResult accessors */
        PrimaryIndex<String,AggregateResult> aggregateResultBySHA1;

        /* Opens all primary and secondary indices. */
        public AggregateResultAccessor(EntityStore store)
                throws DatabaseException {

            aggregateResultBySHA1 = store.getPrimaryIndex(
                    String.class, AggregateResult.class);
        }
    }

    private Environment myDbEnvironment = null;
    private Database myDatabase = null;
    private EntityStore store = null;
    private AggregateResultAccessor dao = null;
    File BDBfile;

    private HashMap<String, double[]> aggregatesProfiles;
    InterestingnessFunction interestingnessFunction;

    StringBuilder stringBuilder;

    public BerkeleyDBStorage(InterestingnessFunction interestingnessFunction) {
        try {
            // Open the environment, creating one if it does not exist
            EnvironmentConfig envConfig = new EnvironmentConfig();
            envConfig.setAllowCreate(true);
            envConfig.setTransactional(true); //check
            BDBfile = new File(GlobalSettings.BERKELEYDB_ENVIRONMEMT);
            // Either the file exists or mkdirs is successful
            if (BDBfile.exists() || BDBfile.mkdirs()) {
                myDbEnvironment = new Environment(BDBfile, envConfig);
            }

            switch (GlobalSettings.BERKELEYDB_FEATURES) {
                case "baseapi":
                    // Open the database, creating one if it does not exist
                    DatabaseConfig dbConfig = new DatabaseConfig();
                    dbConfig.setAllowCreate(true);
                    dbConfig.setSortedDuplicates(true);
                    myDatabase = myDbEnvironment.openDatabase(null, GlobalSettings.DATABASE_NAME, dbConfig);

                    stringBuilder = new StringBuilder();
                    break;
                case "dpl":
                    StoreConfig storeConfig = new StoreConfig();
                    storeConfig.setAllowCreate(true);
                    storeConfig.setTransactional(true);
                    store = new EntityStore(myDbEnvironment, "AggregateResultsStore", storeConfig);

                    /* Initialize the data access object. */
                    dao = new AggregateResultAccessor(store);
                    break;
                default:
                    throw new IllegalStateException("feature " + GlobalSettings.BERKELEYDB_FEATURES + " not supported");
            }
        } catch (DatabaseException dbe) {
            throw new IllegalStateException(dbe);
        }

        aggregatesProfiles = new HashMap<>();
        this.interestingnessFunction = interestingnessFunction;
    }

    @Override
    public boolean supportsInterestingnessMeasureComputation(){
        return GlobalSettings.BERKELEYDB_FEATURES.equals("baseapi");
    }

    @Override
    public void addHeaderToAggregateResults(String aggregateFilename, String aggregateSHA1, StringBuilder dimensionsHeader, String aggregatedMeasureHeader){
        switch (GlobalSettings.BERKELEYDB_FEATURES){
            case "baseapi":
                stringBuilder.setLength(0);
                stringBuilder.append("header:").append(aggregateSHA1);
                addPairKeyValue(stringBuilder.toString(), dimensionsHeader.toString() + aggregatedMeasureHeader);
                break;
            case "dpl":
                AggregateResult ar = new AggregateResult(aggregateSHA1);
                ar.addResult(dimensionsHeader + aggregatedMeasureHeader);
                dao.aggregateResultBySHA1.put(ar);
                break;
            default:
                throw new IllegalStateException("feature " + GlobalSettings.BERKELEYDB_FEATURES + " not supported");
        }
    }

    @Override
    public void addResultToAggregateResults(String aggregateSHA1, StringBuilder dimensions, double aggregatedMeasure){
        switch (GlobalSettings.BERKELEYDB_FEATURES){
            case "baseapi":
                dimensions.append(aggregatedMeasure);
                addPairKeyValue(aggregateSHA1, dimensions.toString());
                break;
            case "dpl":
                dimensions.append(aggregatedMeasure);
                AggregateResult res = dao.aggregateResultBySHA1.get(aggregateSHA1);
                res.addResult(dimensions.toString());
                dao.aggregateResultBySHA1.put(res);
                break;
            default:
                throw new IllegalStateException("feature " + GlobalSettings.BERKELEYDB_FEATURES + " not supported");
        }
    }

    private ArrayList<String> getTuples(String SHA1OfFileToRetrieve){
        switch (GlobalSettings.BERKELEYDB_FEATURES){
            case "baseapi":
                return getTuplesUsingBaseAPI(SHA1OfFileToRetrieve);
            case "dpl":
                return getTuplesUsingDPL(SHA1OfFileToRetrieve);
            default:
                throw new IllegalStateException("feature " + GlobalSettings.BERKELEYDB_FEATURES + " not supported");
        }
    }

    private ArrayList<String> getTuplesUsingDPL(String SHA1OfFileToRetrieve){
        return dao.aggregateResultBySHA1.get(SHA1OfFileToRetrieve).getResults();
    }

    private ArrayList<String> getTuplesUsingBaseAPI(String SHA1OfFileToRetrieve){
        Cursor myCursor = null;
        ArrayList<String> result = new ArrayList<>();
        String headerKey;

        try {
            headerKey = "header:" + SHA1OfFileToRetrieve;
            DatabaseEntry theKey = new DatabaseEntry(headerKey.getBytes("UTF-8"));
            DatabaseEntry theData = new DatabaseEntry();

            if (myDatabase.get(null, theKey, theData, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
                byte[] retData = theData.getData();
                String foundData = new String(retData, "UTF-8");
                result.add(foundData);
            }

            theKey = new DatabaseEntry(SHA1OfFileToRetrieve.getBytes("UTF-8"));
            theData = new DatabaseEntry();

            myCursor = myDatabase.openCursor(null, null);
            OperationStatus retVal = myCursor.getSearchKey(theKey, theData, LockMode.DEFAULT);
            if (retVal==OperationStatus.SUCCESS && myCursor.count() > 1) {
                while (retVal == OperationStatus.SUCCESS) {
                    byte[] retData = theData.getData();
                    String foundData = new String(retData, "UTF-8");
                    result.add(foundData);
                    retVal = myCursor.getNextDup(theKey, theData, LockMode.DEFAULT);
                }
            }
            else{
                result.clear();
            }
            myCursor.close();

        }
        catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }

        return result;
    }


    private void addPairKeyValue(String key, String value) {
        try {
            DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
            DatabaseEntry theData = new DatabaseEntry(value.getBytes("UTF-8"));
            myDatabase.put(null, theKey, theData);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void removeData() {
        try {
            FileUtils.deleteDirectory(BDBfile);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void commit() {

    }

    @Override
    public void commit(HashMap<String, double[]> aggregatesProfiles) {
        for(Map.Entry<String,double[]> agg: aggregatesProfiles.entrySet()){
            this.aggregatesProfiles.put(agg.getKey(), agg.getValue());
        }
    }

    @Override
    public HashMap<String, ArrayList<String>> retrieveAggregatesResults(ArrayList<String> SHA1OfFilenames){
        HashMap<String, ArrayList<String>> result = new HashMap<>();
        for(String SHA1OfFilename: SHA1OfFilenames){
            result.put(SHA1OfFilename, getTuples(SHA1OfFilename));
        }
        return result;
    }

    @Override
    public void storeAggregatesResultsToCSV(ArrayList<String> SHA1OfFilenames, HashMap<String,String> dictionaryFromSHA1ToFilename) {
        for(String SHA1: SHA1OfFilenames){
            storeAggregateResultToCSV(SHA1, dictionaryFromSHA1ToFilename.get(SHA1));
        }

    }

    @Override
    public List<String> retrieveAggregateResults(String SHA1OfFilename){
        return getTuples(SHA1OfFilename);
    }

    @Override
    public void storeAggregateResultToCSV(String SHA1OfFilename, String filename) {
        List<String> tuples;
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
            tuples = getTuples(SHA1OfFilename);

            for (String t: tuples) {
                bw.write(t + "\n");
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public ArrayList<Pair<String, Double>> storeTopKToCSV (int k, HashMap<String, String> dictionaryFromSHA1ToFilename){
        switch (GlobalSettings.BERKELEYDB_FEATURES){
            case "baseapi":
                return storeTopKToCSVUsingBaseAPI(k,dictionaryFromSHA1ToFilename);
            case "dpl":
            default:
                throw new IllegalStateException("Unsupported functionality");
        }
    }


    private ArrayList<Pair<String, Double>> storeTopKToCSVUsingBaseAPI (int k, HashMap<String, String> dictionaryFromSHA1ToFilename) {
        String filename, filenameSHA1;
        List<String> tuplesInFile;
        Double[] aggregatedMeasureValues;
        int idxOfTuple=0;
        boolean readHeader;
        Double score;
        ArrayList<Pair<String,Double>> allResults = new ArrayList<>();
        ArrayList<Pair<String,Double>> topKFilenames = new ArrayList<>();
        ArrayList<String> topKFilenamesSHA1 = new ArrayList<>();
        Pair<String,Double> currentPair;

        for(Map.Entry<String,String> aggregate: dictionaryFromSHA1ToFilename.entrySet()){
            filenameSHA1 = aggregate.getKey();
            filename = aggregate.getValue();

            tuplesInFile = getTuples(filenameSHA1);

            aggregatedMeasureValues = new Double[tuplesInFile.size()-1]; // -1 because tuplesInFile contains also the header
            readHeader = true;
            idxOfTuple = 0;
            for(String tuple:tuplesInFile){
                if(!readHeader) {
                    String[] values = tuple.split(", ");
                    aggregatedMeasureValues[idxOfTuple] = Double.parseDouble(values[values.length-1]);
                    idxOfTuple++;
                }
                readHeader = false;
            }

            double[] filenameStatistics = aggregatesProfiles.get(filenameSHA1);
            switch (interestingnessFunction) {
                case VAR_FEATURE_SCALING:
                    score = EfficientComputations.varianceWithKnownMean(EfficientComputations.normalizeByFeatureScaling(aggregatedMeasureValues, filenameStatistics[MOLAPMeasures.MIN_ORDINAL], filenameStatistics[MOLAPMeasures.MAX_ORDINAL]), filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL]);
                    break;
                case VAR_SUM:
                    score = EfficientComputations.varianceOnRawValuesNormalizedByDivisionBySumWithKnownMean(aggregatedMeasureValues, filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL], filenameStatistics[MOLAPMeasures.SUM_ORDINAL]);
                    if (score == null) {
                        if (filenameStatistics[MOLAPMeasures.SUM_ORDINAL] == 0D) {
                            score = EfficientComputations.varianceWithKnownMean(aggregatedMeasureValues, filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL]);
                        }
                    }
                    break;
                case VAR_MEAN:
                    score = EfficientComputations.varianceOnRawValuesNormalizedByDivisionByMeanWithKnownMean(aggregatedMeasureValues, filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL]);
                    if (score == null) {
                        if (filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL] == 0D) {
                            score = EfficientComputations.varianceWithKnownMean(aggregatedMeasureValues, filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL]);
                        }
                    }
                    break;
                case SKEWNESS:
                    score = EfficientComputations.skewness(aggregatedMeasureValues);
                    break;
                case KURTOSIS:
                    score = EfficientComputations.kurtosis(aggregatedMeasureValues);
                    break;
                default:
                    throw new IllegalStateException(interestingnessFunction + " not supported");
            }
            allResults.add(new Pair<>(filenameSHA1,score));
        }

        sortResults(allResults);

        int maxNrOfResults = allResults.size();
        for (int i = 0; i < k && i < maxNrOfResults; i++){
            currentPair = allResults.get(i);
            topKFilenames.add(new Pair<>(dictionaryFromSHA1ToFilename.get(currentPair.getKey()),currentPair.getValue()));
            topKFilenamesSHA1.add(currentPair.getKey());
        }

        long time = System.currentTimeMillis();
        storeAggregatesResultsToCSV(topKFilenamesSHA1, dictionaryFromSHA1ToFilename);
        PerformanceMeasurements.totalTimeToExportAggregateResultsToCSVFiles[0] += Utils.calculateRunTimeInMilliseconds(time);

        return topKFilenames;
    }

    @Override
    public void closeConnection() {
        try {
            if (myDatabase != null) {
                myDatabase.close();
            }

            if (store != null) {
                store.close();
            }

            if (myDbEnvironment != null) {
                myDbEnvironment.close();
            }
        } catch (DatabaseException dbe) {
            throw new IllegalStateException(dbe);
        }
    }

    private void sortResults(ArrayList<Pair<String,Double>> results){
        Collections.sort(results, new ResultsComparator());
    }

    private static class ResultsComparator implements
            Comparator<Pair<String, Double>> {
        @Override
        public int compare(Pair<String, Double> agg1, Pair<String, Double> agg2) {
            Double score1 = agg1.getValue();
            Double score2 = agg2.getValue();

            if (score1 == null) {
                return -1;
            }
            if (score2 == null) {
                return -1;
            }

            return -score1.compareTo(score2);
        }
    }

}
