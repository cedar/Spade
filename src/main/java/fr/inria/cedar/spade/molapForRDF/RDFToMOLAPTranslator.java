//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.configuration.PerformanceMeasurements;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.*;
import fr.inria.cedar.spade.datastructures.estimation.EarlyStopSample;
import fr.inria.cedar.spade.datastructures.estimation.EarlyStopSampleUsingAlgorithmRReservoirSampling;
import fr.inria.cedar.spade.operations.MultidimensionalAggregateEvaluationWithEarlyStop;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class RDFToMOLAPTranslator {
    private static final Logger LOGGER = Logger.getLogger(RDFToMOLAPTranslator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected CandidateFactSet candidateFactSet;
    protected String nameOfTableContainingTheData;

    protected ArrayList<AnalyzedAttribute> dimensions;
    protected ArrayList<AnalyzedAttribute> measures;

    protected HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesInChunkPerDimension;
    protected HashMap<AnalyzedAttribute, ArrayList<String>> distinctValuesOfDimensions;
    protected HashMap<AnalyzedAttribute, Integer> nrOfGroupsPerDimension;

    protected HashMap<AnalyzedAttribute, HashMap<String, Integer>> positionsOfDistinctValuesOfDimensions;
    protected DatabaseHandler handler;

    protected int nrOfChunks;
    protected int chunkSize;
    protected HashMap<Integer, BufferedWriter> referencesToChunks;
    protected HashMap<Integer, String> chunksFileNames;
    protected String directoryName;

    protected EarlyStopSample sample;
    private MultidimensionalAggregateEvaluationWithEarlyStop earlyStopEvaluator;

    protected String tableName;

    public RDFToMOLAPTranslator(ArrayList<AnalyzedAttribute> dimensions,
                                ArrayList<AnalyzedAttribute> measures,
                                CandidateFactSet candidateFactSet,
                                DatabaseHandler handler,
                                HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesInChunkPerDimension) {
        this.dimensions = dimensions;
        this.measures = measures;
        this.candidateFactSet = candidateFactSet;
        this.handler = handler;
        this.nrOfDistinctValuesInChunkPerDimension = nrOfDistinctValuesInChunkPerDimension;

        chunkSize = 1;
        for (AnalyzedAttribute d: this.dimensions) {
            chunkSize = chunkSize * this.nrOfDistinctValuesInChunkPerDimension.get(d);
        }
    }

    public String collectData(boolean materialize) {
        if (!materialize) {
            return null; // do nothing
        }

        try {
            handler.executeUpdate(constructQueryToCollectData(true));
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }

        return nameOfTableContainingTheData;
    }

    /*
     * chunk the data, store the chunks in the db and return a Catalog with all the information
     * */
    public void translate(
            boolean useMaterializedTableWithCollectedData,
            int nrOfSubjectsToSampleInEachGroup,
            boolean inMemory, AggregateInLattice root) {
        long time = System.currentTimeMillis();
        setUp(inMemory);
        PerformanceMeasurements.timeToSetUp[0] = Utils.calculateRunTimeInMilliseconds(time);
        LOGGER.info("\tTime to set up: " + PerformanceMeasurements.timeToSetUp[0] + " ms");

        time = System.currentTimeMillis();
        chunkData(useMaterializedTableWithCollectedData, inMemory, root, nrOfSubjectsToSampleInEachGroup);
        PerformanceMeasurements.timeToChunkDataAndSample[0] = Utils.calculateRunTimeInMilliseconds(time);
        LOGGER.info("\tTime to chunk data and sample: " + PerformanceMeasurements.timeToChunkDataAndSample[0] + " ms");

        tableName = null;
        if (!inMemory) {
            time = System.currentTimeMillis();
            tableName = dumpChunksToDB();
            PerformanceMeasurements.timeToDumpChunksToDB[0] = Utils.calculateRunTimeInMilliseconds(time);
            LOGGER.info("\tTime to dump chunks to db: " + PerformanceMeasurements.timeToDumpChunksToDB[0] + " ms");
        }
    }

    public String getDirectoryWithData() {
        return directoryName;
    }

    public String getTableName() {
        return tableName;
    }

    public int getNumberOfChunks() {
        return nrOfChunks;
    }

    public HashMap<AnalyzedAttribute, ArrayList<String>> getDistinctValuesOfDimensions() {
        return distinctValuesOfDimensions;
    }

    public HashMap<AnalyzedAttribute, Integer> getNumberOfGroupsPerDimension() {
        return nrOfGroupsPerDimension;
    }

    public EarlyStopSample getSample() {
        return sample;
    }

    public void setEarlyStopEvaluator(
            MultidimensionalAggregateEvaluationWithEarlyStop earlyStopEvaluator) {
        this.earlyStopEvaluator = earlyStopEvaluator;
    }

    public MultidimensionalAggregateEvaluationWithEarlyStop getEarlyStopEvaluator() {
        return earlyStopEvaluator;
    }

    private void setUp(boolean inMemory) {
        distinctValuesOfDimensions = new HashMap<>();
        positionsOfDistinctValuesOfDimensions = new HashMap<>();

        orderDimensionsAccordingToNrOfDistinctValues();
        computeDistinctValuesOfDimensions();

        this.nrOfGroupsPerDimension = new HashMap<>();
        nrOfChunks = 1;
        for (AnalyzedAttribute ap: dimensions) {
            int nrDisVals = distinctValuesOfDimensions.get(ap).size();
            int nrDisValsPerChunk = nrOfDistinctValuesInChunkPerDimension.get(ap);
            int nrOfGroups = nrDisVals / nrDisValsPerChunk;
            if ((nrDisVals % nrDisValsPerChunk) != 0) {
                nrOfGroups = nrOfGroups + 1;
            }
            nrOfChunks = nrOfChunks * nrOfGroups;
            nrOfGroupsPerDimension.put(ap, nrOfGroups);
        }

        if (!inMemory) {
            referencesToChunks = new HashMap<>();
            chunksFileNames = new HashMap<>();

            //directoryName = GlobalSettings.MOLAP_FOLDER_PATH + GlobalSettings.DATABASE_NAME + "/data/chunked_" + candidateFactSet.getTableName() + "_using";
            directoryName = GlobalSettings.MOLAP_FOLDER_PATH + "/chunked_" + candidateFactSet.getTableName() + "_using";
            for (AnalyzedAttribute d: dimensions) {
                directoryName = directoryName + "_" + d.getEncoding();
            }
            //directoryName = directoryName + "/chunks/";
            directoryName = directoryName + "/";
            File directory = new File(directoryName);
            if (!directory.exists()) {
                directory.mkdirs();
            }
        }
        else {
            referencesToChunks = null;
            chunksFileNames = null;
            directoryName = null;
        }
    }

    /*
     * get the data from the database, assign each retrieved tuple to the correct chunk
     * */
    private void chunkData(boolean useMaterializedTableWithCollectedData,
                           boolean inMemory, AggregateInLattice root,
                           int numberOfSubjectsToSampleInEachGroup) {
        String sqlQuery;
        try {
            handler.executeUpdate("set enable_hashjoin=off;");
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }

        if (useMaterializedTableWithCollectedData) {
            sqlQuery = "select ";
            for (int i = 0; i < dimensions.size(); i++) {
                sqlQuery = sqlQuery + " Dim" + dimensions.get(i).getEncoding() + ", ";
            }
            sqlQuery = sqlQuery + "subject_id from " + nameOfTableContainingTheData;
        }
        else {
            sqlQuery = constructQueryToCollectData(false);
        }
        long time = System.currentTimeMillis();
        ResultSet rs = handler.getResultSet(sqlQuery);
        PerformanceMeasurements.timeToRetrieveData[0] = Utils.calculateRunTimeInMilliseconds(time);
        LOGGER.info("\t\tTime to retrieve data: " + PerformanceMeasurements.timeToRetrieveData[0] + " ms");

        if (inMemory) {
            root.setGlobalChunkID(0);
        }

        ArrayList<String> tuple;
        int i;
        String s;
        int idxOfCurrentSubject;
        long tupleChunkingTime = 0;
        long reservoirSamplingTime = 0;
        try {
            if (numberOfSubjectsToSampleInEachGroup <= 0) { // without early-stop sampling
                while (rs.next()) {
                    time = System.currentTimeMillis();
                    tuple = new ArrayList<>();
                    // read the values of the dimensions
                    for (i = 1; i <= dimensions.size(); i++) {
                        s = rs.getString(i);
                        if (s == null) {
                            s = "null";
                        }
                        tuple.add(s);
                    }

                    idxOfCurrentSubject = rs.getInt(i); // read the subject_id
                    addTupleToChunk(tuple, idxOfCurrentSubject, inMemory, root);
                    tupleChunkingTime += Utils.calculateRunTimeInMilliseconds(time);
                }
            }
            else { // with early-stop sampling
                long globalIndexOfTuple;
                sample = new EarlyStopSampleUsingAlgorithmRReservoirSampling(root, numberOfSubjectsToSampleInEachGroup);

                while (rs.next()) {
                    time = System.currentTimeMillis();
                    tuple = new ArrayList<>();
                    // read the values of the dimensions
                    for (i = 1; i <= dimensions.size(); i++) {
                        s = rs.getString(i);
                        if (s == null) {
                            s = "null";
                        }
                        tuple.add(s);
                    }

                    idxOfCurrentSubject = rs.getInt(i); // read the subject_id
                    globalIndexOfTuple = addTupleToChunk(tuple, idxOfCurrentSubject, inMemory, root);
                    tupleChunkingTime += Utils.calculateRunTimeInMilliseconds(time);

                    time = System.currentTimeMillis();
                    sample.addTupleToSample(globalIndexOfTuple, idxOfCurrentSubject);
                    reservoirSamplingTime += Utils.calculateRunTimeInMilliseconds(time);
                }
            }
            if (!inMemory) {
                for (Map.Entry<Integer, BufferedWriter> entry: referencesToChunks.entrySet()) {
                    entry.getValue().close();
                }
            }
            rs.close();

            handler.executeUpdate("set enable_hashjoin=on;");
        }
        catch (SQLException | IOException ex) {
            throw new IllegalStateException(ex);
        }
        PerformanceMeasurements.timeToChunkData[0] = tupleChunkingTime;
        LOGGER.info("\t\tTime to chunk data: " + tupleChunkingTime + " ms");

        if (numberOfSubjectsToSampleInEachGroup > 0) {
            PerformanceMeasurements.timeToPerformReservoirSampling[0] = reservoirSamplingTime;
            LOGGER.info("\t\tTime to perform reservoir sampling: " + reservoirSamplingTime + " ms");

            time = System.currentTimeMillis();
            sample.postprocess();
            PerformanceMeasurements.timeToPostprocessSample[0] = Utils.calculateRunTimeInMilliseconds(time);
            LOGGER.info("\t\tTime to postprocess the sample: " + PerformanceMeasurements.timeToPostprocessSample[0] + " ms");
        }
    }

    private String dumpChunksToDB() {
        String chunkTableName = "chunked_" + candidateFactSet.getTableName() + "_using";
        for (AnalyzedAttribute d: dimensions) {
            chunkTableName = chunkTableName + "_" + d.getEncoding();
        }
        String sqlQuery = "CREATE TABLE IF NOT EXISTS " + chunkTableName + " ( chunkid INTEGER PRIMARY KEY, chunkdata BYTEA );";
        try {
            handler.executeUpdate(sqlQuery);

            sqlQuery = "INSERT INTO " + chunkTableName + " VALUES (?, ?)";
            try (PreparedStatement ps = handler.getPreparedStatement(sqlQuery)) {
                for (int i = 0; i < nrOfChunks; i++) {
                    if (chunksFileNames.containsKey(i)) {
                        ps.setInt(1, i);
                        File file = new File(chunksFileNames.get(i));
                        ps.setBinaryStream(2, new FileInputStream(file), (int) file.length());
                        ps.executeUpdate();
                    }
                }
                if (!GlobalSettings.DATABASE_AUTOCOMMIT) {
                    handler.commit();
                }
            }
        }
        catch (SQLException | FileNotFoundException ex) {
            throw new IllegalStateException(ex);
        }
        return chunkTableName;
    }

    private long addTupleToChunk(ArrayList<String> dims, int idxOfSubject,
                                boolean inMemory, AggregateInLattice root) {
        /*
         * given the values of the dimensions find:
         * - the position of each value in the global ordered list of the distinct values of the considered dimension
         * - the group to which each value belongs
         * - the position of the value inside the group
         * */
        ArrayList<Integer> groupOrder = new ArrayList<>();
        ArrayList<Integer> localOrder = new ArrayList<>();
        int globalIdx;
        int groupIdx;
        int localIdx;
        for (int i = 0; i < dims.size(); i++) {
            globalIdx = (positionsOfDistinctValuesOfDimensions.get(dimensions.get(i))).get(dims.get(i));
            groupIdx = globalIdx / nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));
            localIdx = globalIdx % nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));

            groupOrder.add(groupIdx);
            localOrder.add(localIdx);
        }

        int chunkId = getChunkId(groupOrder);
        long offset = getOffsetInChunk(localOrder);

        String strTuple = "" + offset + " " + idxOfSubject + "\n";

        if (inMemory) {
            root.addFact(offset, idxOfSubject);
        }
        else {
            if (referencesToChunks.containsKey(chunkId)) {
                try {
                    referencesToChunks.get(chunkId).write(strTuple);
                }
                catch (IOException ex) {
                    throw new IllegalStateException(ex);
                }
            }
            else {
                String chunkFileName = directoryName + "CHUNK_" + chunkId;
                chunksFileNames.put(chunkId, chunkFileName);
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(chunkFileName))) {
                    writer.write(strTuple);
                    referencesToChunks.put(chunkId, writer);
                }
                catch (IOException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }
        // the global id of a tuple is the chunk id multiplied by the dimension of the chunk plus the offset
        return (chunkId * chunkSize) + offset;
    }

    /*
     * ordering ABC here means that A is the one that changes more frequently while C is the one that changes slowly.
     * */
    private int getChunkId(ArrayList<Integer> groupOrderMultiIdx) {
        int currentDenom = 1, chunkId = 0;

        for (int i = 0; i < dimensions.size() - 1; i++) {
            currentDenom = currentDenom * nrOfGroupsPerDimension.get(dimensions.get(i));
        }

        for (int i = groupOrderMultiIdx.size() - 1; i >= 0; i--) {
            chunkId = chunkId + (groupOrderMultiIdx.get(i) * currentDenom);
            if (i > 0) {
                currentDenom = currentDenom / nrOfGroupsPerDimension.get(dimensions.get(i - 1));
            }
        }
        return chunkId;
    }

    private long getOffsetInChunk(ArrayList<Integer> multiDimIdx) {
        long currentDenom = 1;
        long offset = 0;

        for (int i = 0; i < dimensions.size() - 1; i++) {
            currentDenom = currentDenom * nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));
        }

        for (int i = multiDimIdx.size() - 1; i >= 0; i--) {
            offset = offset + (multiDimIdx.get(i) * currentDenom);
            if (i > 0) {
                currentDenom = currentDenom / nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i - 1));
            }
        }
        return offset;
    }

    private String constructQueryToCollectData(boolean materialize) {
        StringBuilder dimensionsStringBuilder = new StringBuilder();
        StringBuilder sqlQuery = new StringBuilder();
        String prefix = "";
        for (AnalyzedAttribute dim: dimensions) {
            long dimensionEncoding = dim.getEncoding();
            dimensionsStringBuilder.append(prefix).append(dimensionEncoding);
            prefix = ", ";
        }
        String dimensionsString = dimensionsStringBuilder.toString();

        try {
            if (materialize) {
                nameOfTableContainingTheData = candidateFactSet.getTableName() + "_" + generateTableNameAsHash(dimensionsString);
                sqlQuery.append("create table if not exists ").append(nameOfTableContainingTheData).append(" as ");
            }

            sqlQuery.append("select ");

            // select clause: first consider the dimensions, then the subject
            for (int i = 0; i < dimensions.size(); i++) {
                //if (dimensions.get(i).getDerivedFrom() != null && dimensions.get(i).getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
                sqlQuery.append("d").append(i + 1).append(".o as Dim").append(dimensions.get(i).getEncoding()).append(", ");
                /*}
                else {
                    sqlQuery.append("DD").append(i + 1).append(".value as Dimension").append(i+1).append(", ");
                }*/
            }
            sqlQuery.append("cfs.subject_id from ").append(candidateFactSet.getTableName()).append(" as cfs ");

            // dimensions are not decoded
            for (int i = 0; i < dimensions.size(); i++) {
                sqlQuery.append(" left join t_").append(dimensions.get(i).getEncoding()).append(" d").append(i + 1).append(" on cfs.s=d").append(i + 1).append(".s ");
            }
            return sqlQuery.toString();
        }
        catch (NoSuchAlgorithmException ex) {
            throw new IllegalStateException(sqlQuery + " " + ex);
        }
    }

    protected static String generateTableNameAsHash(String tableName) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(tableName.getBytes());
        byte[] bytes = messageDigest.digest();

        return bytesToHex(bytes);
    }

    // reference: https://www.baeldung.com/sha-256-hashing-java
    protected static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        String hex;

        for (int i = 0; i < hash.length / 2; i++) {
            hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) { // only one digit
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }

    private static class AttributeComparator implements
            Comparator<AnalyzedAttribute> {
        @Override
        public int compare(AnalyzedAttribute attribute1,
                           AnalyzedAttribute attribute2) {
            Integer value1 = attribute1.getNumberOfDistinctObjects();
            Integer value2 = attribute2.getNumberOfDistinctObjects();

            if (value1 == null && value2 == null) {
                return 0;
            }
            if (value1 == null) {
                return 1;
            }
            if (value2 == null) {
                return -1;
            }

            return value1.compareTo(value2);
        }
    }

    public void orderDimensionsAccordingToNrOfDistinctValues() {
        Collections.sort(dimensions, new AttributeComparator());
    }

    private void computeDistinctValuesOfDimensions() {
        int i;
        ArrayList<String> result;
        ResultSet rs;
        StringBuilder sqlQuery = new StringBuilder();
        HashMap<String, Integer> positionsOfDistinctValues;
        String cleanValue;
        boolean hasNull;
        for (AnalyzedAttribute dimension: dimensions) {
            positionsOfDistinctValues = new HashMap<>();
            hasNull = false;
            try {
                if (dimension.getDerivationType() != null && dimension.getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
                    sqlQuery.setLength(0);
                    sqlQuery.append("select distinct o from ").append(candidateFactSet.getTableName()).append(" as cfs join t_").append(dimension.getEncoding()).append(" as T on cfs.s=T.s order by o asc");
                    result = handler.getResultSelectFromObjectColumn(sqlQuery.toString());
                    i = 0;
                    for (String r: result) {
                        if (r.equals("null")) {
                            hasNull = true;
                        }
                        positionsOfDistinctValues.put(r, i);
                        i++;
                    }
                }
                else {
                    result = new ArrayList<>();
                    sqlQuery.setLength(0);
                    //sqlQuery = "select distinct key, value from " + candidateFactSet.getTableName() + " as cfs join t_" + dimension.getEncoding() + " as T on cfs.s=T.s join dictionary as D on T.o=D.key order by key asc";
                    sqlQuery.append("select key, cleanvalue from dictionary D where key in ( select T.o from ").append(candidateFactSet.getTableName()).append(" as cfs join t_").append(dimension.getEncoding()).append(" as T on cfs.s=T.s ) order by key asc");
                    rs = handler.getResultSet(sqlQuery.toString());
                    i = 0;
                    while (rs.next()) {
                        positionsOfDistinctValues.put(rs.getString(1), i);
                        cleanValue = rs.getString(2);
                        if (cleanValue.equals("null")) {
                            hasNull = true;
                        }
                        result.add(cleanValue);
                        i++;
                    }
                    rs.close();
                }
            }
            catch (SQLException ex) {
                throw new IllegalStateException(sqlQuery + "\n" + ex);
            }

            if (!hasNull) {
                result.add("null");
            }
            if (!positionsOfDistinctValues.containsKey("null")) {
                positionsOfDistinctValues.put("null", i);
            }

            distinctValuesOfDimensions.put(dimension, result);
            positionsOfDistinctValuesOfDimensions.put(dimension, positionsOfDistinctValues);
        }
    }
}
