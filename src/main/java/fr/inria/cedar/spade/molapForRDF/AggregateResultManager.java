//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.datastructures.Pair;
import fr.inria.cedar.spade.operations.AggregateInterestingnessScorer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AggregateResultManager {
    private static final Logger LOGGER = Logger.getLogger(AggregateResultManager.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    private AggregateResultStorage aggregateResultStorage;
    private InterestingnessFunction interestingnessFunction;

    private HashMap<String, double[]> aggregatesProfiles;
    private HashSet<String> aggregatesProfilesInStorage;

    private HashMap<String,String> dictionaryFromSHA1ToFilename;
    private HashMap<String,String> dictionaryFromFilenameToSHA1;


    public AggregateResultManager(AggregateResultStorage aggregateResultStorage, InterestingnessFunction interestingnessFunction){
        this.aggregateResultStorage = aggregateResultStorage;
        this.interestingnessFunction = interestingnessFunction;

        aggregatesProfiles = new HashMap<>();
        aggregatesProfilesInStorage = new HashSet<>();
        dictionaryFromSHA1ToFilename = new HashMap<>();
        dictionaryFromFilenameToSHA1 = new HashMap<>();

    }

    public void addHeaderToAggregateResults(String aggregateFilename, String aggregateSHA1, StringBuilder dimensionsHeader, String aggregatedMeasureHeader){
        aggregatesProfiles.put(aggregateSHA1, new double[]{0,0,Double.NaN,Double.MAX_VALUE, Double.MIN_VALUE}); // count 0, sum 0, avg undefined, initial min, initial max
        dictionaryFromSHA1ToFilename.put(aggregateSHA1, aggregateFilename);
        dictionaryFromFilenameToSHA1.put(aggregateFilename, aggregateSHA1);

        aggregateResultStorage.addHeaderToAggregateResults(aggregateFilename, aggregateSHA1, dimensionsHeader, aggregatedMeasureHeader);
    }

    public void addResultToAggregateResults(String aggregateSHA1, StringBuilder dimensions, double aggregatedMeasure){
        //update profile
        double[] aggProfile = aggregatesProfiles.get(aggregateSHA1);
        aggProfile[MOLAPMeasures.COUNT_ORDINAL]++;
        aggProfile[MOLAPMeasures.SUM_ORDINAL] += aggregatedMeasure;
        if(aggregatedMeasure < aggProfile[MOLAPMeasures.MIN_ORDINAL]){
            aggProfile[MOLAPMeasures.MIN_ORDINAL] = aggregatedMeasure;
        }
        if(aggregatedMeasure > aggProfile[MOLAPMeasures.MAX_ORDINAL]){
            aggProfile[MOLAPMeasures.MAX_ORDINAL] = aggregatedMeasure;
        }
        aggProfile[MOLAPMeasures.AVERAGE_ORDINAL] = aggProfile[MOLAPMeasures.SUM_ORDINAL] / aggProfile[MOLAPMeasures.COUNT_ORDINAL];

        aggregateResultStorage.addResultToAggregateResults(aggregateSHA1, dimensions, aggregatedMeasure);
    }

    public void computeAndPrintRankedAggregatesAndOutputTopK(InterestingnessFunction interestingnessFunction){
        AggregateInterestingnessScorer aggregateInterestingnessScorer = new AggregateInterestingnessScorer(interestingnessFunction);
        ArrayList<Pair<String,Double>> rankedListOfFiles;
        String filename;

        Iterator<Map.Entry<String, String>> iterator = dictionaryFromSHA1ToFilename.entrySet().iterator();
        while (iterator.hasNext()) { // remove empty aggregates
            Map.Entry<String, String> entry = iterator.next();

            if (aggregatesProfiles.get(entry.getKey())[MOLAPMeasures.COUNT_ORDINAL] == 0) {
                filename = entry.getValue();
                dictionaryFromFilenameToSHA1.remove(filename);
                iterator.remove();
            }
        }

        if(aggregateResultStorage.supportsInterestingnessMeasureComputation()) {
            rankedListOfFiles = aggregateResultStorage.storeTopKToCSV(GlobalSettings.K_FOR_TOP_K, dictionaryFromSHA1ToFilename);
            aggregateInterestingnessScorer.renameResults(rankedListOfFiles);
        }
        else{
            if(!(aggregateResultStorage instanceof CSVStorage)){
                exportAllResultsToCSV();
            }
            rankedListOfFiles = aggregateInterestingnessScorer.score(aggregatesProfiles, dictionaryFromFilenameToSHA1);
        }

        try (FileWriter writer = new FileWriter(GlobalSettings.TOP_K_RESULTS_FILE_NAME); BufferedWriter topKResults = new BufferedWriter(writer)) {
            topKResults.write("aggregate,score\n");
            int i = 1;
            String key;
            Double value;
            for (Pair<String, Double> aggregateResult: rankedListOfFiles) {
                key = aggregateResult.getKey();
                value = aggregateResult.getValue();
                System.out.println(i + ". " + key + "\n\t" + value);
                if (i <= GlobalSettings.K_FOR_TOP_K) {
                    topKResults.write(key + "," + value + "\n");
                }
                i++;
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }


    protected void exportAllResultsToCSV() {
        ArrayList<String> SHA1sOfFilenamesToAskFor = new ArrayList<>(GlobalSettings.NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME);
        int currentNumberOfTuplesToAskFor = 0, nrOfTuplesInCurrentFile;
        boolean fileIsInTheBackpack;
        String filename, filenameSHA1;

        for(Map.Entry<String,String> currentFile: dictionaryFromSHA1ToFilename.entrySet()){
            filenameSHA1 = currentFile.getKey();
            filename = currentFile.getValue();

            if (GlobalSettings.NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME==0){
                aggregateResultStorage.storeAggregateResultToCSV(filenameSHA1, filename);
            }
            else{
                nrOfTuplesInCurrentFile = (int)aggregatesProfiles.get(filenameSHA1)[MOLAPMeasures.COUNT_ORDINAL];

                fileIsInTheBackpack = false;
                if ((currentNumberOfTuplesToAskFor + nrOfTuplesInCurrentFile <= GlobalSettings.NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME) || (nrOfTuplesInCurrentFile >= GlobalSettings.NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME && SHA1sOfFilenamesToAskFor.isEmpty())) {
                    SHA1sOfFilenamesToAskFor.add(filenameSHA1);
                    currentNumberOfTuplesToAskFor += nrOfTuplesInCurrentFile;
                    fileIsInTheBackpack = true;
                }

                if (!fileIsInTheBackpack || currentNumberOfTuplesToAskFor >= GlobalSettings.NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME) {
                    aggregateResultStorage.storeAggregatesResultsToCSV(SHA1sOfFilenamesToAskFor, dictionaryFromSHA1ToFilename);
                    SHA1sOfFilenamesToAskFor.clear();
                    currentNumberOfTuplesToAskFor = 0;
                }

                if (!fileIsInTheBackpack) {
                    SHA1sOfFilenamesToAskFor.add(filenameSHA1);
                    currentNumberOfTuplesToAskFor = nrOfTuplesInCurrentFile;
                }

            }
        }

        if(!SHA1sOfFilenamesToAskFor.isEmpty()){
            aggregateResultStorage.storeAggregatesResultsToCSV(SHA1sOfFilenamesToAskFor, dictionaryFromSHA1ToFilename);
            SHA1sOfFilenamesToAskFor.clear();
        }
    }

    public void storeAggregateResultToCSV(String SHA1OfFilename, String filename){
        aggregateResultStorage.storeAggregateResultToCSV(SHA1OfFilename, filename);
    }

    public void storeAggregatesResultsToCSV(ArrayList<String> SHA1OfFilenames, HashMap<String, String> dictionaryFromSHA1ToFilename){
        aggregateResultStorage.storeAggregatesResultsToCSV(SHA1OfFilenames, dictionaryFromSHA1ToFilename);
    }

    public void commit(){
        if(aggregateResultStorage.supportsInterestingnessMeasureComputation()) {
            HashMap<String, double[]> aggregateProfilesToSend = new HashMap();
            for (Map.Entry<String, double[]> entry : aggregatesProfiles.entrySet()) {
                String currentAggregate = entry.getKey();
                if (!aggregatesProfilesInStorage.contains(currentAggregate)) {
                    double[] currentAggregateStatistics = entry.getValue();
                    if (currentAggregateStatistics[MOLAPMeasures.COUNT_ORDINAL] > 0) {
                        aggregateProfilesToSend.put(currentAggregate, currentAggregateStatistics);
                        aggregatesProfilesInStorage.add(currentAggregate);
                    }
                }
            }
            aggregateResultStorage.commit(aggregateProfilesToSend);
        }
        else{
            aggregateResultStorage.commit();
        }
    }

    public void removeData(){
        aggregateResultStorage.removeData();
    }

    public void closeConnection(){
        aggregateResultStorage.closeConnection();
    }

}
