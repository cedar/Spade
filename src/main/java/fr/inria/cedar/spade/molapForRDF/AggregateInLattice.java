//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.configuration.PerformanceMeasurements;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.*;
import static fr.inria.cedar.spade.datastructures.Utils.generateSHA1;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.roaringbitmap.RoaringBitmap;

public class AggregateInLattice {
    private static final org.apache.log4j.Logger LOGGER = Logger.getLogger(AggregateInLattice.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected DatabaseHandler handler;

    protected ArrayList<AnalyzedAttribute> dimensions;
    protected AnalyzedAttribute dimensionMissingFromParent;
    protected HashMap<AnalyzedAttribute, Integer> numberOfDistinctValuesInChunkPerDimension;
    protected HashMap<AnalyzedAttribute, Integer> numberOfGroupsPerDimension;
    protected String decodedCF;

    protected boolean isRoot;
    protected AggregateInLattice parentAggregate;
    protected LinkedList<AggregateInLattice> childrenAggregates;

    protected HashMap<AnalyzedAttribute, ArrayList<String>> distinctValuesPerDimension;

    protected String directoryName;
    protected HashMap<AnalyzedAttribute, HashMap<AggregationFunction, String>> aggregateResultsFilenames;
    protected HashMap<String, String> dictionaryFromFilenameToSHA1;
    //protected HashMap<String, String> dictionaryFromSHA1ToFilename;
    protected AggregateResultManager aggregateResultManager;

    protected int minimumMemoryCost;
    protected HashMap<AnalyzedAttribute, Integer> minimumMemoryCostPerDimension;

    // chunkID -> offset -> facts bitmap
    protected HashMap<Integer, HashMap<Long, RoaringBitmap>> bitmaps;

    protected int flushToDiskRate;
    protected int howManyTimesHaveFlushed;
    protected int globalChunkID;
    protected int idOfNextTupleToFlush;

    protected int numberOfChunks;
    protected long chunkSize;
    protected boolean printHeader;

    // used to avoid allocating memory for ArrayLists
    protected long[] parentMultiIdx;
    protected long[] multiIdxChunk;
    protected long[] multiIdxOffset;
    protected int[] idxForDecoding;

    protected HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsForEachMeasure;
    protected HashMap<AnalyzedAttribute, double[]> computedValuesForEachMeasure;

    public AggregateInLattice(ArrayList<AnalyzedAttribute> dimensions,
                              HashMap<AnalyzedAttribute, Integer> numberOfDistinctValuesInChunkPerDimension,
                              HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsForEachMeasure,
                              String decodedCF) {
        this.dimensions = dimensions;
        this.numberOfDistinctValuesInChunkPerDimension = numberOfDistinctValuesInChunkPerDimension;
        idOfNextTupleToFlush = 1;

        this.aggregationFunctionsForEachMeasure = new HashMap<>();
        this.computedValuesForEachMeasure = new HashMap<>();
        for (Map.Entry<AnalyzedAttribute, LinkedList<AggregationFunction>> entry: aggregationFunctionsForEachMeasure.entrySet()) {
            this.aggregationFunctionsForEachMeasure.put(entry.getKey(), new LinkedList<>(entry.getValue()));
            this.computedValuesForEachMeasure.put(entry.getKey(), new double[5]); // 5 reflects all possible aggregation functions
        }

        this.decodedCF = decodedCF;
        childrenAggregates = new LinkedList<>();
        howManyTimesHaveFlushed = 0;
        printHeader = true;
        globalChunkID = -1;
        isRoot = false;
        flushToDiskRate = -1;
        numberOfChunks = 1;
        minimumMemoryCost = -1;
        chunkSize = 1;
        for (AnalyzedAttribute d: dimensions) {
            chunkSize = chunkSize * numberOfDistinctValuesInChunkPerDimension.get(d);
        }

        bitmaps = new HashMap<>();
        multiIdxChunk = new long[dimensions.size()];
        multiIdxOffset = new long[dimensions.size()];
        idxForDecoding = new int[dimensions.size()];
    }

    public void setRoot() {
        isRoot = true;
    }

    public void setMinimumMemoryCost(int cost) {
        minimumMemoryCost = cost;
    }

    public int getMinimumMemoryCost() {
        return minimumMemoryCost;
    }

    /**
     * The function computes how many chunks the aggregate needs.
     * The root always needs only one chunk.
     * The need of the remaining aggregates depends on the minimum memory need (found while computing the MMST) and on the size of the chunk.
     */
    protected void computeNumberOfChunks() {
        numberOfChunks = 1;
        if (!isRoot) {
            for (Map.Entry<AnalyzedAttribute, Integer> entry: minimumMemoryCostPerDimension.entrySet()) {
                AnalyzedAttribute key = entry.getKey();
                Integer value = entry.getValue();
                int numberOfChunksPerDimension = value / numberOfDistinctValuesInChunkPerDimension.get(key);
                if (value % numberOfDistinctValuesInChunkPerDimension.get(key) != 0) {
                    numberOfChunksPerDimension = numberOfChunksPerDimension + 1;
                }
                numberOfChunks = numberOfChunks * numberOfChunksPerDimension;
            }
        }
    }

    public void setMinimumMemoryCostPerDimension(
            HashMap<AnalyzedAttribute, Integer> cost) {
        this.minimumMemoryCostPerDimension = cost;
        computeNumberOfChunks();
    }

    /**
     * Once the aggregate knows who his parent is, it can determine the dimension that it is missing from the parent.
     *
     * @param parent the aggregate in the MMST that is used to compute this aggregate
     */
    public void setParentAggregate(AggregateInLattice parent) {
        this.parentAggregate = parent;
        if (parentAggregate != null) {
            for (AnalyzedAttribute a: parent.dimensions) {
                if (!dimensions.contains(a)) {
                    dimensionMissingFromParent = a;
                }
            }
            parentMultiIdx = new long[parent.dimensions.size()];
        }
    }

    protected void addChildAggregate(AggregateInLattice child) {
        childrenAggregates.add(child);
    }

    protected void setFlushToDiskRate(int flushRate) {
        this.flushToDiskRate = flushRate;
    }

    protected void emptyBitmaps() {
        long time = System.currentTimeMillis();
        for (HashMap<Long, RoaringBitmap> chunk: bitmaps.values()) {
            for (RoaringBitmap bitmap: chunk.values()) {
                bitmap.clear();
            }
        }
        PerformanceMeasurements.timeToEmptyMemoryAllocation[0] += Utils.calculateRunTimeInMilliseconds(time);
    }

    public void setGlobalChunkID(int globalChunkID) {
        this.globalChunkID = globalChunkID;
    }

    public void addFact(long offset, int subjectID) {
        HashMap<Long, RoaringBitmap> chunk;
        if (!bitmaps.containsKey(0)) {
            chunk = new HashMap<>();
            bitmaps.put(0, chunk);
        }
        else {
            chunk = bitmaps.get(0);
        }

        RoaringBitmap bitmap;
        if (!chunk.containsKey(offset)) {
            bitmap = new RoaringBitmap();
            chunk.put(offset, bitmap);
        }
        else {
            bitmap = chunk.get(offset);
        }

        bitmap.add(subjectID);
    }

    public void removeNonAverageAggregationFunctions() {
        for (Map.Entry<AnalyzedAttribute, LinkedList<AggregationFunction>> entry: aggregationFunctionsForEachMeasure.entrySet()) {
            entry.getValue().removeIf(aggregationFunction -> aggregationFunction != AggregationFunction.AVERAGE);
        }
        /*for (Map.Entry<AnalyzedAttribute, HashMap<AggregationFunction, String>> entry: aggregateResultsFilenames.entrySet()) {
            entry.getValue().entrySet().removeIf(entries -> entries.getKey() != AggregationFunction.AVERAGE);
        }*/

        for (AggregateInLattice child: childrenAggregates) {
            child.removeNonAverageAggregationFunctions();
        }
    }

    public LinkedList<AggregateInLattice> getChildrenAggregates() {
        return childrenAggregates;
    }

    public AnalyzedAttribute getDimensionMissingFromParent() {
        return dimensionMissingFromParent;
    }

    public ArrayList<AnalyzedAttribute> getDimensions() {
        return dimensions;
    }

    public HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> getAggregationFunctionsForEachMeasure() {
        return aggregationFunctionsForEachMeasure;
    }

    public void removeMeasureAggregationFunctionCombination(
            AnalyzedAttribute measure, AggregationFunction af) {
        if (aggregateResultsFilenames.containsKey(measure)) {
            if (aggregateResultsFilenames.get(measure).containsKey(af)) {
                aggregateResultsFilenames.get(measure).remove(af);
                aggregationFunctionsForEachMeasure.get(measure).remove(af);
                if (aggregateResultsFilenames.get(measure).isEmpty()) {
                    aggregateResultsFilenames.remove(measure);
                    aggregationFunctionsForEachMeasure.remove(measure);
                }
            }
        }
    }

    public String getAggregateResultsFilename(AnalyzedAttribute measure,
                                              AggregationFunction aggregationFunction) {
        return aggregateResultsFilenames.get(measure).get(aggregationFunction);
    }

    public String getDecodedCandidateFactSet() {
        return decodedCF;
    }

    protected void setFields(DatabaseHandler handler,
                             HashMap<AnalyzedAttribute, ArrayList<String>> distinctValuesPerDimension,
                             HashMap<AnalyzedAttribute, Integer> numberOfGroupsPerDimension,
                             HashMap<String, String> dictionaryFromFilenameToSHA1,
                             //HashMap<String, String> dictionaryFromSHA1ToFilename,
                             AggregateResultManager aggregateResultManager) {
        this.handler = handler;
        this.distinctValuesPerDimension = distinctValuesPerDimension;
        this.numberOfGroupsPerDimension = numberOfGroupsPerDimension;
        this.dictionaryFromFilenameToSHA1 = dictionaryFromFilenameToSHA1;
        //this.dictionaryFromSHA1ToFilename = dictionaryFromSHA1ToFilename;
        this.aggregateResultManager = aggregateResultManager;
    }

    public StringBuilder getFilenameByAggregationFunctionAndMeasurePrefix() {
        /*directoryName = GlobalSettings.FLASK_FOLDER_PATH + GlobalSettings.DATABASE_NAME + "/" + decodedCF + "/" + GlobalSettings.INTERESTINGNESS_FUNCTION + "/";
        File directory = new File(directoryName);
        if (!directory.exists()) {
            directory.mkdirs();
        }*/
        // directory creation moved to NewSystemInterface class
        directoryName = GlobalSettings.FLASK_FOLDER_PATH;

        StringBuilder filenameByAggregationFunctionAndMeasure = new StringBuilder(directoryName);
        filenameByAggregationFunctionAndMeasure.append("CFS_").append(decodedCF).append("_Dimensions");
        for (AnalyzedAttribute ap: dimensions) {
            filenameByAggregationFunctionAndMeasure.append("_").append(ap.getEncoding());
        }

        return filenameByAggregationFunctionAndMeasure;
    }

    public String getFilenameByAggregationFunctionAndMeasure(
            StringBuilder filenameByAggregationFunctionAndMeasure,
            AggregationFunction aggregationFunction, AnalyzedAttribute measure) {
        if (aggregationFunction == null) {
            filenameByAggregationFunctionAndMeasure.append("__COUNT_STAR.csv");
            return filenameByAggregationFunctionAndMeasure.toString();
        }
        else {
            filenameByAggregationFunctionAndMeasure.append("__").append(aggregationFunction).append("_").append(measure.getEncoding()).append(".csv");
            return filenameByAggregationFunctionAndMeasure.toString();
        }
    }

    protected void setUpOutput() {
        try {
            StringBuilder filenameByAggregationFunctionAndMeasure = getFilenameByAggregationFunctionAndMeasurePrefix();
            int prefixLength = filenameByAggregationFunctionAndMeasure.length();
            String filename = getFilenameByAggregationFunctionAndMeasure(filenameByAggregationFunctionAndMeasure, null, null);

            String sha1;
            HashMap<AggregationFunction, String> filenamesByAggregationFunction;
            aggregateResultsFilenames = new HashMap<>();

            if (!dictionaryFromFilenameToSHA1.containsKey(filename)) { // doesn't exists in the dictionary => never computed before
                sha1 = generateSHA1(filename);
                dictionaryFromFilenameToSHA1.put(filename, sha1);
                //dictionaryFromSHA1ToFilename.put(sha1, filename);
                filenamesByAggregationFunction = new HashMap<>();
                filenamesByAggregationFunction.put(AggregationFunction.COUNT_STAR, filename);
                aggregateResultsFilenames.put(null, filenamesByAggregationFunction); // no measure -> COUNT_STAR -> filename
            }
            else {
                PerformanceMeasurements.numberOfAggregates[0]--;
            }

            Iterator<Map.Entry<AnalyzedAttribute, LinkedList<AggregationFunction>>> aggregationFunctionsForEachMeasureIterator = aggregationFunctionsForEachMeasure.entrySet().iterator();
            Map.Entry<AnalyzedAttribute, LinkedList<AggregationFunction>> entry;
            AnalyzedAttribute measure;
            LinkedList<AggregationFunction> aggregationFunctions;
            ListIterator<AggregationFunction> aggregationFunctionsIterator;
            AggregationFunction aggregationFunction;

            while (aggregationFunctionsForEachMeasureIterator.hasNext()) {
                entry = aggregationFunctionsForEachMeasureIterator.next();
                measure = entry.getKey();
                aggregationFunctions = entry.getValue();
                filenamesByAggregationFunction = new HashMap<>();

                aggregationFunctionsIterator = aggregationFunctions.listIterator();
                while (aggregationFunctionsIterator.hasNext()) {
                    aggregationFunction = aggregationFunctionsIterator.next();
                    if (aggregationFunction != AggregationFunction.RAW_VALUES_LIST) {
                        filenameByAggregationFunctionAndMeasure.setLength(prefixLength);
                        filename = getFilenameByAggregationFunctionAndMeasure(filenameByAggregationFunctionAndMeasure, aggregationFunction, measure);

                        if (dictionaryFromFilenameToSHA1.containsKey(filename)) { // already exists in the dictionary => already computed in another lattice
                            aggregationFunctionsIterator.remove();
                            PerformanceMeasurements.numberOfAggregates[0]--;
                        }
                        else {
                            sha1 = generateSHA1(filename);
                            filenamesByAggregationFunction.put(aggregationFunction, filename);
                            dictionaryFromFilenameToSHA1.put(filename, sha1);
                            //dictionaryFromSHA1ToFilename.put(sha1, filename);
                        }
                    }
                }

                if (aggregationFunctions.isEmpty()) {
                    aggregationFunctionsForEachMeasureIterator.remove();
                }
                else {
                    if (!filenamesByAggregationFunction.isEmpty()) {
                        aggregateResultsFilenames.put(measure, filenamesByAggregationFunction);
                    }
                }
            }
        }
        catch (NoSuchAlgorithmException ex) {
            throw new IllegalStateException(ex);
        }
    }

    protected void setUpSubtree() {
        setUpOutput();

        for (AggregateInLattice child: childrenAggregates) {
            child.setUpSubtree();
        }
    }

    public void loadChunkToRoot(BufferedReader chunk) {
        long time = System.currentTimeMillis();
        try {
            emptyBitmaps();

            if (chunk != null) {
                String line;
                String[] tokens;
                long offset;
                int subjectID;
                while ((line = chunk.readLine()) != null) {
                    tokens = line.split(" ");
                    offset = Long.parseLong(tokens[0]);
                    subjectID = Integer.parseInt(tokens[1]);

                    // add subject to memory
                    addFact(offset, subjectID);
                }
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
        PerformanceMeasurements.timeToLoadChunkToRoot[0] += Utils.calculateRunTimeInMilliseconds(time);
    }

    public int computeChunkId(int parentChunkId) {
        return (parentChunkId) % numberOfChunks;
    }

    /**
     * Given the monodimensional index of the parent, the function computes, for each dimension of the parent, the index
     * that identifies the distinct value of such dimension
     *
     * @param parentUniIdx monodimensional index of the parent
     */
    protected void computeParentMultiIdx(long parentUniIdx) {
        long denominator = 1;

        for (int i = 0; i < parentAggregate.dimensions.size() - 1; i++) {
            denominator = denominator * numberOfDistinctValuesInChunkPerDimension.get(parentAggregate.dimensions.get(i));
        }

        for (int i = parentAggregate.dimensions.size() - 1; i >= 0; i--) {
            parentMultiIdx[i] = parentUniIdx / denominator;
            parentUniIdx = parentUniIdx % denominator;
            if (i > 0) {
                denominator = denominator / numberOfDistinctValuesInChunkPerDimension.get(parentAggregate.dimensions.get((i - 1)));
            }
        }
    }

    /**
     * Given the monodimensional index of the parent, first find the indices related to every dimension of the parent
     * then find myoffset by only considering the dimensions the current aggregate has
     *
     * @param parentOffset monodimensional index of the parent
     *
     * @return monodimensional index of the aggregate
     */
    public long computeOffset(long parentOffset) {
        computeParentMultiIdx(parentOffset);
        long denominator = 1, myOffset = 0;
        for (int i = 0; i < dimensions.size() - 1; i++) {
            denominator = denominator * numberOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));
        }

        long value;
        for (int i = dimensions.size() - 1; i >= 0; i--) {
            value = parentMultiIdx[parentAggregate.dimensions.indexOf(dimensions.get(i))];
            myOffset = myOffset + value * denominator;
            if (i > 0) {
                denominator = denominator / numberOfDistinctValuesInChunkPerDimension.get(dimensions.get(i - 1));
            }
        }
        return myOffset;
    }

    protected void updateChildBitmapUsingParentBitmap(int parentChunkId, long parentOffset) {
        long time = System.currentTimeMillis();

        RoaringBitmap parentBitmap;
        if (parentAggregate.isRoot) {
            parentBitmap = parentAggregate.bitmaps.get(0).get(parentOffset);
        }
        else {
            parentBitmap = parentAggregate.bitmaps.get(parentChunkId).get(parentOffset);
        }

        if (parentBitmap != null) { // a bitmap at this offset
            int childChunkId = computeChunkId(parentChunkId);
            long childOffset = computeOffset(parentOffset);
            RoaringBitmap childBitmap;
            if (!bitmaps.containsKey(childChunkId)) {
                bitmaps.put(childChunkId, new HashMap<>());
            }
            HashMap<Long, RoaringBitmap> childChunk = bitmaps.get(childChunkId);
            if (!childChunk.containsKey(childOffset)) {
                childBitmap = new RoaringBitmap();
                childChunk.put(childOffset, childBitmap);
            }
            else {
                childBitmap = childChunk.get(childOffset);
            }

            childBitmap.or(parentBitmap);
        }

        PerformanceMeasurements.timeToUpdateBitmaps[0] += Utils.calculateRunTimeInMilliseconds(time);
    }

    protected boolean timeToFlush(int parentChunkId, boolean isLastParentOffset) {
        return (parentChunkId + 1) % flushToDiskRate == 0 && isLastParentOffset;
    }

    public void triggerPropagationInTreeLevelBelow(MOLAPMeasures preaggregatedMeasures) {
        for (int i = 0; i < numberOfChunks; i++) {
            updateSubtree(i, preaggregatedMeasures);
        }
    }

    protected void storeDecodedHeaderToAggregateResultManager() {
        StringBuilder stringBuilderAggregationFunction = new StringBuilder();

        StringBuilder stringBuilderDimensions = new StringBuilder();
        for (AnalyzedAttribute dim: dimensions) {
            stringBuilderDimensions.append(handler.decodeAttributeName(dim)).append(",");
        }

        AnalyzedAttribute measure;
        HashMap<AggregationFunction, String> filenameByAggregationFunction;
        String decodedAttributeName, SHA1OfFilename, filename;
        AggregationFunction currentAggregationFunction;

        for (Map.Entry<AnalyzedAttribute, HashMap<AggregationFunction, String>> entry: aggregateResultsFilenames.entrySet()) {
            measure = entry.getKey();
            filenameByAggregationFunction = entry.getValue();
            decodedAttributeName = handler.decodeAttributeName(measure);

            for (Map.Entry<AggregationFunction, String> entry1: filenameByAggregationFunction.entrySet()) {
                currentAggregationFunction = entry1.getKey();
                filename = entry1.getValue();
                if (currentAggregationFunction != AggregationFunction.RAW_VALUES_LIST) {
                    SHA1OfFilename = dictionaryFromFilenameToSHA1.get(filename);
                    stringBuilderAggregationFunction.setLength(0);
                    switch (currentAggregationFunction) {
                        case COUNT_STAR:
                            stringBuilderAggregationFunction.append("count(*)");
                            break;
                        case COUNT:
                            stringBuilderAggregationFunction.append("count(").append(decodedAttributeName).append(")");
                            break;
                        case SUM:
                            stringBuilderAggregationFunction.append("sum(").append(decodedAttributeName).append(")");
                            break;
                        case AVERAGE:
                            stringBuilderAggregationFunction.append("avg(").append(decodedAttributeName).append(")");
                            break;
                        case MIN:
                            stringBuilderAggregationFunction.append("min(").append(decodedAttributeName).append(")");
                            break;
                        case MAX:
                            stringBuilderAggregationFunction.append("max(").append(decodedAttributeName).append(")");
                            break;
                        default:
                            break;
                    }
                    aggregateResultManager.addHeaderToAggregateResults(filename, SHA1OfFilename, stringBuilderDimensions, stringBuilderAggregationFunction.toString());
                }
            }
        }
    }

    /**
     * The function takes an offset in the multidimensional space and translates it into the corresponding values of each dimension
     * (the idxForDecoding array contains, for each dimension, the index of the value in the array of distinct values).
     * Since data is chunked and each chunk contains an a-priori defined number of distinct values, the last chunk might contain cells
     * that are not valid. For example, consider a dimension having a total of 4 distinct values chunked into chunks of 6 distinct values.
     * There will be only one chunk of size 6, however, the last two cells of this chunk do not contain any meaningful data.
     * The function returns false if the provided offset corresponds to a not valid cell; it returns true otherwise.
     *
     * @param chunkID
     * @param offset
     *
     * @return
     */
    public boolean serializeDecodedValue(int chunkID, long offset) {
        if (this.isRoot) {
            chunkID = globalChunkID;
        }

        long denominator = 1;
        for (int i = 0; i < dimensions.size() - 1; i++) {
            denominator = denominator * numberOfGroupsPerDimension.get(dimensions.get(i));
        }
        for (int i = multiIdxChunk.length - 1; i >= 0; i--) {
            multiIdxChunk[i] = chunkID / denominator;
            chunkID = (int) (chunkID % denominator);
            if (i > 0) {
                denominator = denominator / numberOfGroupsPerDimension.get(dimensions.get(i - 1));
            }
        }

        denominator = 1;
        for (int i = 0; i < dimensions.size() - 1; i++) {
            denominator = denominator * numberOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));
        }
        for (int i = multiIdxOffset.length - 1; i >= 0; i--) {
            multiIdxOffset[i] = offset / denominator;
            offset = offset % denominator;
            if (i > 0) {
                denominator = denominator / numberOfDistinctValuesInChunkPerDimension.get(dimensions.get(i - 1));
            }
        }

        long idx;
        for (int i = 0; i < dimensions.size(); i++) {
            idx = multiIdxChunk[i] * numberOfDistinctValuesInChunkPerDimension.get(dimensions.get(i)) + multiIdxOffset[i];
            if (idx >= distinctValuesPerDimension.get(dimensions.get(i)).size()) {
                return false;
            }
            idxForDecoding[i] = (int) idx;
        }

        return true;
    }

    protected boolean dimensionsDifferFromNull() {
        for (int i = 0; i < dimensions.size(); i++) {
            if (distinctValuesPerDimension.get(dimensions.get(i)).get(idxForDecoding[i]).equals("null")) {
                return false;
            }
        }
        return true;
    }

    public static String getCleanValue(String val) {
        if (val.equals("null")) {
            return "empty value, ";
        }
        else {
            return val.replaceAll(",", " ") + ", ";
        }
    }

    protected void computeAggregatedValuesForEachMeasure(
            RoaringBitmap currentBitmap,
            MOLAPMeasures preaggregatedMeasures) {
        AnalyzedAttribute measure;
        LinkedList<AggregationFunction> aggregationFunctions;

        double[][] preaggregatedValuesArrayPerMeasure;
        boolean isAtMostOne;
        boolean hasCount;
        boolean hasSum;

        int minOrdinal;
        int maxOrdinal;

        for (Map.Entry<AnalyzedAttribute, LinkedList<AggregationFunction>> entry: aggregationFunctionsForEachMeasure.entrySet()) {
            measure = entry.getKey();
            aggregationFunctions = entry.getValue();

            final double[] computedValues = computedValuesForEachMeasure.get(measure);
            preaggregatedValuesArrayPerMeasure = preaggregatedMeasures.getPreaggregatedValuesArrayPerMeasure(measure);

            isAtMostOne = measure.isAtMostOne();
            hasCount = false;
            hasSum = false;

            if (isAtMostOne) {
                minOrdinal = MOLAPMeasures.SUM_ORDINAL;
                maxOrdinal = MOLAPMeasures.SUM_ORDINAL;
            }
            else {
                minOrdinal = MOLAPMeasures.MIN_ORDINAL;
                maxOrdinal = MOLAPMeasures.MAX_ORDINAL;
            }

            for (AggregationFunction aggregationFunction: aggregationFunctions) {
                switch (aggregationFunction) {
                    case COUNT: {
                        hasCount = true;
                        break;
                    }
                    case SUM: {
                        hasSum = true;
                        final double[] preaggregatedValuesArrayPerMeasurePerAggregationFunction = preaggregatedValuesArrayPerMeasure[MOLAPMeasures.SUM_ORDINAL];
                        currentBitmap.forEach((int subjectid) -> {
                            double subjectMeasure = preaggregatedValuesArrayPerMeasurePerAggregationFunction[subjectid - 1];
                            // compute COUNT, it will be overwritten in the end if necessary (if the measure is not at-most-one)
                            if (Double.isNaN(computedValues[MOLAPMeasures.SUM_ORDINAL])) {
                                computedValues[MOLAPMeasures.SUM_ORDINAL] = subjectMeasure;
                                computedValues[MOLAPMeasures.COUNT_ORDINAL] = 1;
                            }
                            else if (!Double.isNaN(subjectMeasure)) {
                                computedValues[MOLAPMeasures.SUM_ORDINAL] += subjectMeasure;
                                computedValues[MOLAPMeasures.COUNT_ORDINAL] += 1;
                            }
                        });
                        break;
                    }
                    case AVERAGE:
                        break;
                    case MIN: {
                        final double[] preaggregatedValuesArrayPerMeasurePerAggregationFunction = preaggregatedValuesArrayPerMeasure[minOrdinal];
                        currentBitmap.forEach((int subjectid) -> {
                            double subjectMeasure = preaggregatedValuesArrayPerMeasurePerAggregationFunction[subjectid - 1];
                            if (Double.isNaN(computedValues[MOLAPMeasures.MIN_ORDINAL])) {
                                computedValues[MOLAPMeasures.MIN_ORDINAL] = subjectMeasure;
                            }
                            else if (subjectMeasure < computedValues[MOLAPMeasures.MIN_ORDINAL]) {
                                computedValues[MOLAPMeasures.MIN_ORDINAL] = subjectMeasure;
                            }
                        });
                        break;
                    }
                    case MAX: {
                        final double[] preaggregatedValuesArrayPerMeasurePerAggregationFunction = preaggregatedValuesArrayPerMeasure[maxOrdinal];
                        currentBitmap.forEach((int subjectid) -> {
                            double subjectMeasure = preaggregatedValuesArrayPerMeasurePerAggregationFunction[subjectid - 1];
                            if (Double.isNaN(computedValues[MOLAPMeasures.MAX_ORDINAL])) {
                                computedValues[MOLAPMeasures.MAX_ORDINAL] = subjectMeasure;
                            }
                            else if (subjectMeasure > computedValues[MOLAPMeasures.MAX_ORDINAL]) {
                                computedValues[MOLAPMeasures.MAX_ORDINAL] = subjectMeasure;
                            }
                        });
                        break;
                    }
                    default:
                        break;
                }
            }

            // if we have to compute COUNT and we can't reconstruct its value from SUM
            if (hasCount && !(isAtMostOne && hasSum)) {
                final double[] preaggregatedValuesArrayPerMeasurePerAggregationFunction = preaggregatedValuesArrayPerMeasure[MOLAPMeasures.COUNT_ORDINAL];
                currentBitmap.forEach((int subjectid) -> {
                    double subjectMeasure = preaggregatedValuesArrayPerMeasurePerAggregationFunction[subjectid - 1];
                    if (Double.isNaN(computedValues[MOLAPMeasures.COUNT_ORDINAL])) {
                        // No need to test whether subjectMeasure is NaN or not. If it's NaN, computedValues[COUNT_ORDINAL] will remain NaN. As in the case for SUM.
                        // Such test would be necessary, if we were to assign computedValues[COUNT_ORDINAL] = 1.
                        //if (!Double.isNaN(subjectMeasure)) {
                        computedValues[MOLAPMeasures.COUNT_ORDINAL] = subjectMeasure;
                        //}
                    }
                    else if (!Double.isNaN(subjectMeasure)) {
                        computedValues[MOLAPMeasures.COUNT_ORDINAL] += subjectMeasure;
                    }
                });
            }
        }
    }

    protected void computeSumAggregatedValuesForThisMeasure(
            double[] computedValues,
            MOLAPMeasures preaggregatedMeasures,
            AnalyzedAttribute measure,
            RoaringBitmap currentBitmap,
            boolean computeCountToo) {
        if (computeCountToo) {
            final double[] preaggregatedValuesArrayPerMeasurePerAggregationFunction = preaggregatedMeasures.getPreaggregatedValuesArrayPerMeasure(measure)[MOLAPMeasures.SUM_ORDINAL];
            currentBitmap.forEach((int subjectid) -> {
                double subjectMeasure = preaggregatedValuesArrayPerMeasurePerAggregationFunction[subjectid - 1];
                if (Double.isNaN(computedValues[MOLAPMeasures.SUM_ORDINAL])) {
                    computedValues[MOLAPMeasures.SUM_ORDINAL] = subjectMeasure;
                    computedValues[MOLAPMeasures.COUNT_ORDINAL] = 1;
                }
                else if (!Double.isNaN(subjectMeasure)) {
                    computedValues[MOLAPMeasures.SUM_ORDINAL] += subjectMeasure;
                    computedValues[MOLAPMeasures.COUNT_ORDINAL] += 1;
                }
            });
        }
        else {
            final double[] preaggregatedValuesArrayPerMeasurePerAggregationFunction = preaggregatedMeasures.getPreaggregatedValuesArrayPerMeasure(measure)[MOLAPMeasures.SUM_ORDINAL];
            currentBitmap.forEach((int subjectid) -> {
                double subjectMeasure = preaggregatedValuesArrayPerMeasurePerAggregationFunction[subjectid - 1];
                if (Double.isNaN(computedValues[MOLAPMeasures.SUM_ORDINAL])) {
                    computedValues[MOLAPMeasures.SUM_ORDINAL] = subjectMeasure;
                }
                else if (!Double.isNaN(subjectMeasure)) {
                    computedValues[MOLAPMeasures.SUM_ORDINAL] += subjectMeasure;
                }
            });
        }
    }

    protected void computeCountAggregatedValuesForThisMeasure(
            double[] computedValues,
            MOLAPMeasures preaggregatedMeasures,
            AnalyzedAttribute measure,
            RoaringBitmap currentBitmap) {
        final double[] preaggregatedValuesArrayPerMeasurePerAggregationFunction = preaggregatedMeasures.getPreaggregatedValuesArrayPerMeasure(measure)[MOLAPMeasures.COUNT_ORDINAL];
        currentBitmap.forEach((int subjectid) -> {
            double subjectMeasure = preaggregatedValuesArrayPerMeasurePerAggregationFunction[subjectid - 1];
            if (Double.isNaN(computedValues[MOLAPMeasures.COUNT_ORDINAL])) {
                // No need to test whether subjectMeasure is NaN or not. If it's NaN, computedValues[COUNT_ORDINAL] will remain NaN. As in the case for SUM.
                // Such test would be necessary, if we were to assign computedValues[COUNT_ORDINAL] = 1.
                //if (!Double.isNaN(subjectMeasure)) {
                computedValues[MOLAPMeasures.COUNT_ORDINAL] = subjectMeasure;
                //}
            }
            else if (!Double.isNaN(subjectMeasure)) {
                computedValues[MOLAPMeasures.COUNT_ORDINAL] += subjectMeasure;
            }
        });
    }

    protected void storeDecodedDataToAggregateResultManager(int chunkId,
                                                            MOLAPMeasures preaggregatedMeasures) {
        int globalChunkId = howManyTimesHaveFlushed * numberOfChunks + chunkId;
        long offset;
        RoaringBitmap currentBitmap;
        int countstar;
        StringBuilder dimensionValuesStringBuilder = new StringBuilder();
        AnalyzedAttribute measure;
        LinkedList<AggregationFunction> aggregationFunctions;
        boolean isAtMostOne;
        HashMap<AggregationFunction, String> aggregateResultsFilenameByMeasure;
        String SHA1OfFilename;
        double[] computedValues;
        boolean hasAverage;
        boolean sumIsNaN;
        boolean countIsNaN;
        boolean computeCountWithSum;

        BufferedWriter bwBitmapSize = null;
        if (GlobalSettings.PROFILE_BITMAP_SIZE) {
            try {
                bwBitmapSize = new BufferedWriter(new FileWriter(GlobalSettings.PROFILE_BITMAP_SIZE_FILE_NAME, true));
            }
            catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        for (Map.Entry<Long, RoaringBitmap> offsetBitmap: bitmaps.get(chunkId).entrySet()) {
            offset = offsetBitmap.getKey();
            currentBitmap = offsetBitmap.getValue();

            if (serializeDecodedValue(globalChunkId, offset) && dimensionsDifferFromNull()) {
                if (GlobalSettings.PROFILE_BITMAP_SIZE) {
                    try {
                        bwBitmapSize.write(currentBitmap.getCardinality() + ", " + currentBitmap.getSizeInBytes() + "\n");
                    }
                    catch (IOException e) {
                        throw new IllegalStateException(e);
                    }
                }

                countstar = currentBitmap.getCardinality();
                if (countstar > 0) { // if there are subjects in the bitmap, then use them to compute the measures
                    dimensionValuesStringBuilder.setLength(0);
                    for (int i = 0; i < idxForDecoding.length; i++) {
                        dimensionValuesStringBuilder.append(getCleanValue(distinctValuesPerDimension.get(dimensions.get(i)).get(idxForDecoding[i])));
                    }

                    if (aggregateResultsFilenames.containsKey(null)) { // if contains no-measure; store data only if they need to be computed
                        SHA1OfFilename = dictionaryFromFilenameToSHA1.get(aggregateResultsFilenames.get(null).get(AggregationFunction.COUNT_STAR));
                        aggregateResultManager.addResultToAggregateResults(SHA1OfFilename, dimensionValuesStringBuilder, countstar);
                    }

                    if (aggregationFunctionsForEachMeasure != null) {
                        for (Map.Entry<AnalyzedAttribute, double[]> entry: computedValuesForEachMeasure.entrySet()) {
                            Arrays.fill(entry.getValue(), Double.NaN);
                        }
                        computeAggregatedValuesForEachMeasure(currentBitmap, preaggregatedMeasures);

                        for (Map.Entry<AnalyzedAttribute, LinkedList<AggregationFunction>> entry: aggregationFunctionsForEachMeasure.entrySet()) {
                            measure = entry.getKey();
                            aggregationFunctions = entry.getValue();

                            isAtMostOne = measure.isAtMostOne();

                            aggregateResultsFilenameByMeasure = aggregateResultsFilenames.get(measure);
                            computedValues = computedValuesForEachMeasure.get(measure);

                            hasAverage = false;
                            for (AggregationFunction aggregationFunction: aggregationFunctions) {
                                switch (aggregationFunction) {
                                    case COUNT:
                                        if (!Double.isNaN(computedValues[MOLAPMeasures.COUNT_ORDINAL])) {
                                            SHA1OfFilename = dictionaryFromFilenameToSHA1.get(aggregateResultsFilenameByMeasure.get(AggregationFunction.COUNT));
                                            aggregateResultManager.addResultToAggregateResults(SHA1OfFilename, dimensionValuesStringBuilder, computedValues[MOLAPMeasures.COUNT_ORDINAL]);
                                        }
                                        break;
                                    case SUM:
                                        if (!Double.isNaN(computedValues[MOLAPMeasures.SUM_ORDINAL])) {
                                            SHA1OfFilename = dictionaryFromFilenameToSHA1.get(aggregateResultsFilenameByMeasure.get(AggregationFunction.SUM));
                                            aggregateResultManager.addResultToAggregateResults(SHA1OfFilename, dimensionValuesStringBuilder, computedValues[MOLAPMeasures.SUM_ORDINAL]);
                                        }
                                        break;
                                    case AVERAGE:
                                        hasAverage = true;
                                        break;
                                    case MIN:
                                        if (!Double.isNaN(computedValues[MOLAPMeasures.MIN_ORDINAL])) {
                                            SHA1OfFilename = dictionaryFromFilenameToSHA1.get(aggregateResultsFilenameByMeasure.get(AggregationFunction.MIN));
                                            aggregateResultManager.addResultToAggregateResults(SHA1OfFilename, dimensionValuesStringBuilder, computedValues[MOLAPMeasures.MIN_ORDINAL]);
                                        }
                                        break;
                                    case MAX:
                                        if (!Double.isNaN(computedValues[MOLAPMeasures.MAX_ORDINAL])) {
                                            SHA1OfFilename = dictionaryFromFilenameToSHA1.get(aggregateResultsFilenameByMeasure.get(AggregationFunction.MAX));
                                            aggregateResultManager.addResultToAggregateResults(SHA1OfFilename, dimensionValuesStringBuilder, computedValues[MOLAPMeasures.MAX_ORDINAL]);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (hasAverage) {
                                sumIsNaN = Double.isNaN(computedValues[MOLAPMeasures.SUM_ORDINAL]);
                                countIsNaN = Double.isNaN(computedValues[MOLAPMeasures.COUNT_ORDINAL]);
                                computeCountWithSum = false;
                                if (sumIsNaN) {
                                    if (countIsNaN && isAtMostOne) {
                                        computeCountWithSum = true;
                                    }
                                    computeSumAggregatedValuesForThisMeasure(computedValues, preaggregatedMeasures, measure, currentBitmap, computeCountWithSum);
                                }
                                if (countIsNaN && !computeCountWithSum) {
                                    computeCountAggregatedValuesForThisMeasure(computedValues, preaggregatedMeasures, measure, currentBitmap);
                                }
                                computedValues[MOLAPMeasures.AVERAGE_ORDINAL] = computedValues[MOLAPMeasures.SUM_ORDINAL] / computedValues[MOLAPMeasures.COUNT_ORDINAL];

                                if (!Double.isNaN(computedValues[MOLAPMeasures.AVERAGE_ORDINAL])) {
                                    SHA1OfFilename = dictionaryFromFilenameToSHA1.get(aggregateResultsFilenameByMeasure.get(AggregationFunction.AVERAGE));
                                    aggregateResultManager.addResultToAggregateResults(SHA1OfFilename, dimensionValuesStringBuilder, computedValues[MOLAPMeasures.AVERAGE_ORDINAL]);
                                }
                            }
                        }
                    }
                }
                idOfNextTupleToFlush++;
            }
        }

        if (GlobalSettings.PROFILE_BITMAP_SIZE) {
            try {
                bwBitmapSize.close();
            }
            catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    protected void computeAndStoreAggregateResults(
            MOLAPMeasures preaggregatedMeasures) {
        long time = System.currentTimeMillis();
        if (printHeader == true) { // if it is the first time flushing, first store the header of the csv file
            storeDecodedHeaderToAggregateResultManager();
            printHeader = false;
        }
        for (int i = 0; i < numberOfChunks; i++) {
            storeDecodedDataToAggregateResultManager(i, preaggregatedMeasures);
        }
        aggregateResultManager.commit(); // all the results have been passed to the manager
        PerformanceMeasurements.timeToComputeAndStoreAggregateResults[0] += Utils.calculateRunTimeInMilliseconds(time);
    }

    protected void updateAggregate(int parentChunkId, long parentOffset,
                                   boolean isLastParentOffset,
                                   MOLAPMeasures preaggregatedMeasures) {
        updateChildBitmapUsingParentBitmap(parentChunkId, parentOffset);

        if (timeToFlush(parentChunkId, isLastParentOffset)) {
            triggerPropagationInTreeLevelBelow(preaggregatedMeasures);
            computeAndStoreAggregateResults(preaggregatedMeasures);
            emptyBitmaps();
            howManyTimesHaveFlushed++;
        }
    }

    /*
        This function updates aggregates in each child of this aggregate (the
        parent): children receive facts from the parent. If it's time to flush
        the result a child will propagate the facts to its children, then flush
        the results, and empty bitmaps.

        This function is therefore recursive, but the recusion is pushing the
        facts down the tree in a lazy manner. Only once a child aggregate saw
        all the necessary facts to complete the chunk, that it triggers the
        next-level propagation and flushes the results.
    */
    protected void updateSubtree(int currentChunkID,
                                 MOLAPMeasures preaggregatedMeasures) {
        Iterator<Long> iterator;
        int chunkID = isRoot ? 0 : currentChunkID;
        long offset;
        boolean hasNext;
        for (AggregateInLattice child: childrenAggregates) {
            iterator = bitmaps.get(chunkID).keySet().iterator();
            hasNext = iterator.hasNext();
            while (hasNext) {
                offset = iterator.next();
                hasNext = iterator.hasNext();
                child.updateAggregate(currentChunkID, offset, !hasNext, preaggregatedMeasures);
            }
        }
    }

    protected void computeAndStoreResultsOfRemainingChunksInMemory(
            MOLAPMeasures preaggregatedMeasures) {
        if (isRoot == false && howManyTimesHaveFlushed == 0) {
            triggerPropagationInTreeLevelBelow(preaggregatedMeasures);
            computeAndStoreAggregateResults(preaggregatedMeasures);
            //emptyBitmaps();
            howManyTimesHaveFlushed++;
        }

        for (AggregateInLattice child: childrenAggregates) {
            child.computeAndStoreResultsOfRemainingChunksInMemory(preaggregatedMeasures);
        }
    }

    public String serializeAggregateInLattice() {
        String prefix = "";
        StringBuilder agg = new StringBuilder("Dimensions: ");
        for (AnalyzedAttribute dim: dimensions) {
            agg.append(prefix).append(dim.getEncoding());
            prefix = ", ";
        }
        if (minimumMemoryCostPerDimension != null) {
            prefix = "";
            agg.append(" MinimumMemoryCostPerDimension: ");
            for (AnalyzedAttribute dim: dimensions) {
                agg.append(prefix).append(minimumMemoryCostPerDimension.get(dim));
                prefix = "x";
            }
        }
        agg.append(" MinimumMemoryCost: ").append(minimumMemoryCost);
        agg.append(" NumberOfChunks: ").append(numberOfChunks);
        agg.append(" ChunkSize: ").append(chunkSize);
        agg.append(" FlushToDiskRate: ").append(flushToDiskRate);
        agg.append(" AggregationFunctionsForEachMeasure: ");
        for (Map.Entry<AnalyzedAttribute, LinkedList<AggregationFunction>> entry: aggregationFunctionsForEachMeasure.entrySet()) {
            agg.append(entry.getKey().getEncoding()).append(": [");
            prefix = "";
            for (AggregationFunction af: entry.getValue()) {
                agg.append(prefix).append(af);
                prefix = ", ";
            }
            agg.append("]");
        }
        return agg.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AggregateInLattice newAgg = (AggregateInLattice) o;

        return dimensions.equals(newAgg.dimensions);
    }

    @Override
    public int hashCode() {
        return dimensions.hashCode();
    }
}
