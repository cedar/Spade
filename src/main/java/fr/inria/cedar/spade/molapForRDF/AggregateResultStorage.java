//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import fr.inria.cedar.spade.datastructures.Pair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class AggregateResultStorage {
    private static final Logger LOGGER = Logger.getLogger(AggregateResultStorage.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public abstract boolean supportsInterestingnessMeasureComputation();

    public abstract void addHeaderToAggregateResults(String aggregateFilename, String aggregateSHA1, StringBuilder dimensionsHeader, String aggregatedMeasureHeader);

    public abstract void addResultToAggregateResults(String aggregateSHA1, StringBuilder dimensions, double aggregatedMeasure);

    public abstract List<String> retrieveAggregateResults(String SHA1OfFilename);

    public abstract void storeAggregateResultToCSV(String SHA1OfFilename, String filename);

    public abstract HashMap<String, ArrayList<String>> retrieveAggregatesResults(ArrayList<String> SHA1OfFilenames);

    public abstract void storeAggregatesResultsToCSV(ArrayList<String> SHA1OfFilenames, HashMap<String, String> dictionaryFromSHA1ToFilename);

    public abstract void commit();

    public abstract void commit(HashMap<String, double[]> aggregatesProfiles);

    public abstract ArrayList<Pair<String, Double>> storeTopKToCSV (int k, HashMap<String, String> dictionaryFromSHA1ToFilename);

    public abstract void removeData();

    public abstract void closeConnection();
}
