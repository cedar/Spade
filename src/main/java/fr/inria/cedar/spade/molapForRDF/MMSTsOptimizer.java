//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import java.util.*;
import org.apache.log4j.Level;

public class MMSTsOptimizer {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(MMSTsOptimizer.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected HashMap<Integer, ArrayList<AggregateInLattice>> globalStructure;
    protected ArrayList<AggregateInLattice> mmsts;
    protected int numberOfLevels;

    public MMSTsOptimizer() {
        globalStructure = new HashMap<>();
        mmsts = new ArrayList<>();
    }

    protected static class AggregateInLatticeComparatorByCost implements
            Comparator<AggregateInLattice> {
        @Override
        public int compare(AggregateInLattice aggregate1,
                           AggregateInLattice aggregate2) {
            int costObject1 = aggregate1.minimumMemoryCost;
            int costObject2 = aggregate2.minimumMemoryCost;

            if (costObject1 == costObject2) {
                return 0;
            }
            if (costObject1 < costObject2) {
                return -1;
            }
            return 1;
        }
    }

    public void addMMST(AggregateInLattice node) {
        mmsts.add(node);
    }

    public void addMMST(MinimumMemorySpanningTree mmst) {
        addMMST(mmst.getRoot());
    }

    public void computeGlobalSolution() {
        constructGlobalDataStructure();

        AggregateInLattice firstAggregate, secondAggregate;
        int firstAggregateCost, secondAggregateCost;

        for (int level = 0; level < numberOfLevels; level++) {
            ArrayList<AggregateInLattice> currentLevel = globalStructure.get(level);

            while (!currentLevel.isEmpty()) {
                firstAggregate = currentLevel.remove(0);

                Iterator<AggregateInLattice> itrSecondAggregate = currentLevel.iterator();
                while (itrSecondAggregate.hasNext()) {
                    secondAggregate = itrSecondAggregate.next();

                    if (secondAggregate.equals(firstAggregate)) {
                        firstAggregateCost = computeCostOfNode(firstAggregate);
                        secondAggregateCost = computeCostOfNode(secondAggregate);

                        if (firstAggregateCost <= secondAggregateCost) {
                            secondAggregate.parentAggregate.childrenAggregates.remove(secondAggregate);
                            secondAggregate.parentAggregate = null;
                        }
                        else {
                            firstAggregate.parentAggregate.childrenAggregates.remove(firstAggregate);
                            firstAggregate.parentAggregate = null;
                        }
                        itrSecondAggregate.remove();
                    }
                }
            }

            // this code works
            /*for(int firstAggregatePosition = 0; firstAggregatePosition < currentLevelSize; firstAggregatePosition++){
                firstAggregate = currentLevel.get(firstAggregatePosition);

                for(int secondAggregatePosition = firstAggregatePosition + 1; secondAggregatePosition < currentLevelSize; secondAggregatePosition++){
                    secondAggregate = currentLevel.get(secondAggregatePosition);
                    if(secondAggregate.parentAggregate!=null && secondAggregate.equals(firstAggregate)){
                        firstAggregateCost = computeCostOfNode(firstAggregate);
                        secondAggregateCost = computeCostOfNode(secondAggregate);

                        if(firstAggregateCost != -1 && secondAggregateCost != -1 && firstAggregateCost <= secondAggregateCost){
                            secondAggregate.parentAggregate.childrenAggregates.remove(secondAggregate);
                            secondAggregate.parentAggregate=null;
                        }
                    }
                }
            }*/
        }
    }

    private void constructGlobalDataStructure() {
        globalStructure.put(0, new ArrayList<>());
        for (AggregateInLattice agg: mmsts) {
            globalStructure.get(0).add(agg);
        }
        numberOfLevels = 1;
        while (constructGivenLevelOfGlobalDataStructure(numberOfLevels) == true) {
            numberOfLevels++;
        }
        //LOGGER.info("Number of levels: " + numberOfLevels);
    }

    private boolean constructGivenLevelOfGlobalDataStructure(int level) {
        ArrayList<AggregateInLattice> pastLevel = globalStructure.get(level - 1);
        for (AggregateInLattice currentAggregate: pastLevel) {
            for (AggregateInLattice child: currentAggregate.childrenAggregates) {
                if (!globalStructure.containsKey(level)) {
                    globalStructure.put(level, new ArrayList<>());
                }
                globalStructure.get(level).add(child);
            }
        }
        return globalStructure.containsKey(level);
    }

    private int computeCostOfNode(AggregateInLattice node) {
        if (node == null) {
            return -1;
        }
        if (node.isRoot) {
            return node.minimumMemoryCost;
        }
        return node.minimumMemoryCost + computeCostOfNode(node.parentAggregate);
    }
}
