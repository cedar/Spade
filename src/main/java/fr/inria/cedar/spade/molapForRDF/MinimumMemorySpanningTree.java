//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MinimumMemorySpanningTree {
    private static final Logger LOGGER = Logger.getLogger(MinimumMemorySpanningTree.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected DatabaseHandler handler;

    protected HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesPerDimension;
    protected HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesInChunkPerDimension;

    protected ArrayList<AnalyzedAttribute> dimensions;
    protected ArrayList<AnalyzedAttribute> measures;
    protected HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsForEachMeasure;

    protected AggregateInLattice root; // root is considered at level zero
    protected LinkedHashMap<Integer, ArrayList<AggregateInLattice>> allAggregatesInLattice;
    protected int numberOfLevels;

    protected String decodedCF;

    public MinimumMemorySpanningTree(DatabaseHandler handler,
                                     ArrayList<AnalyzedAttribute> dimensions,
                                     HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesPerDimension,
                                     HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesInChunkPerDimension,
                                     ArrayList<AnalyzedAttribute> measures,
                                     HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsForEachAttribute,
                                     String decodedCF) {
        this.handler = handler;
        this.decodedCF = decodedCF;
        this.nrOfDistinctValuesPerDimension = new HashMap<>();
        for (Map.Entry<AnalyzedAttribute, Integer> entry: nrOfDistinctValuesPerDimension.entrySet()) {
            this.nrOfDistinctValuesPerDimension.put(entry.getKey(), entry.getValue() + 1); // plus 1 because molap inserts the null value in the domain in order to model missing dimensions
        }
        this.nrOfDistinctValuesInChunkPerDimension = nrOfDistinctValuesInChunkPerDimension;
        this.dimensions = dimensions;
        this.measures = measures;

        aggregationFunctionsForEachMeasure = new HashMap<>();

        if (measures != null) {
            for (AnalyzedAttribute measure: measures) {
                aggregationFunctionsForEachMeasure.put(measure, aggregationFunctionsForEachAttribute.get(measure));
            }
        }

        Collections.sort(dimensions, new AttributeComparatorByNumberOfDistinctValues());

        this.root = new AggregateInLattice(dimensions, nrOfDistinctValuesInChunkPerDimension, aggregationFunctionsForEachMeasure, decodedCF);

        int cost = 1;
        for (AnalyzedAttribute dim: dimensions) {
            cost = cost * nrOfDistinctValuesInChunkPerDimension.get(dim);
        }
        this.root.setRoot();
        this.root.setMinimumMemoryCost(cost);
        allAggregatesInLattice = new LinkedHashMap<>();
        allAggregatesInLattice.put(0, new ArrayList<>(Arrays.asList(this.root)));
        numberOfLevels = 1;
    }

    protected static class AttributeComparatorByNumberOfDistinctValues implements
            Comparator<AnalyzedAttribute> {
        @Override
        public int compare(AnalyzedAttribute attribute1,
                           AnalyzedAttribute attribute2) {
            Integer numberOfDistinctObjects1 = attribute1.getNumberOfDistinctObjects();
            Integer numberOfDistinctObjects2 = attribute2.getNumberOfDistinctObjects();

            if (numberOfDistinctObjects1 == null && numberOfDistinctObjects2 == null) {
                return 0;
            }
            if (numberOfDistinctObjects1 == null) {
                return 1;
            }
            if (numberOfDistinctObjects2 == null) {
                return -1;
            }

            int res = numberOfDistinctObjects1.compareTo(numberOfDistinctObjects2);

            if(res == 0){
                long encoding1 = attribute1.getEncoding();
                long encoding2 = attribute2.getEncoding();
                res = Long.compare(encoding1, encoding2);
            }

            return res; // the lower the number of distinct values the better
        }
    }

    public ArrayList<AnalyzedAttribute> getDimensions() {
        return dimensions;
    }

    public ArrayList<AnalyzedAttribute> getMeasures() {
        return measures;
    }

    public AggregateInLattice getRoot() {
        return root;
    }

    // returns the root of a logical MMST, that is, a tree in which the memory of the nodes has not been allocated
    public AggregateInLattice computeMMST() {
        enumerateAllLattice();
        findMinimumMemorySpanningTree();
        return root;
    }

    private void enumerateAllLattice() {
        enumerateLevelBelowTheGivenOne(0);
    }

    // generates the lattice recursively, one level at the time
    private void enumerateLevelBelowTheGivenOne(int level) {
        ArrayList<AggregateInLattice> aggsInGivenLevel = allAggregatesInLattice.get(level);

        // stop when the level containing candidates of length 1 is reached
        if (aggsInGivenLevel.get(0).dimensions.size() == 1) {
            numberOfLevels = level + 1;
            return;
        }

        // find the aggregates to be added to the next level in the lattice
        ArrayList<AggregateInLattice> aggsInNewLevel = new ArrayList<>();
        // for each aggregate in the given level, generate all combinations having one less dimension
        // and insert into the next level only those combinations that are not already in it
        for (AggregateInLattice ail: aggsInGivenLevel) {
            Iterator<int[]> iterator = CombinatoricsUtils.combinationsIterator(ail.dimensions.size(), ail.dimensions.size() - 1);
            while (iterator.hasNext()) {
                final int[] combination = iterator.next();
                ArrayList<AnalyzedAttribute> dims = new ArrayList<>();
                for (int i = 0; i < combination.length; i++) {
                    dims.add(ail.dimensions.get(combination[i]));
                }
                // construct an aggregate based on the current combination of dimensions
                //Collections.sort(dimensions, new AttributeComparatorByNumberOfDistinctValues());
                AggregateInLattice currAgg = new AggregateInLattice(dims, nrOfDistinctValuesInChunkPerDimension, aggregationFunctionsForEachMeasure, decodedCF);
                if (!aggsInNewLevel.contains(currAgg)) {
                    aggsInNewLevel.add(currAgg);
                }
            }
            allAggregatesInLattice.put(level + 1, aggsInNewLevel);
        }
        enumerateLevelBelowTheGivenOne(level + 1);
    }

    /*
     * based on the enumerated lattice, finds the minimum memory spanning tree
     * */
    private void findMinimumMemorySpanningTree() {
        // start from the level below the root and consider all nodes in each level one at the time.
        // Compute the cost of finding such node from each node in the above level and choose the node
        // that minimizes such cost
        for (int currentLevel = 1; currentLevel < numberOfLevels; currentLevel++) {
            ArrayList<AggregateInLattice> aggregatesInCurrentLevel = allAggregatesInLattice.get(currentLevel);
            ArrayList<AggregateInLattice> aggregatesInAboveLevel = allAggregatesInLattice.get(currentLevel - 1);
            boolean assigned;

            for (int i = 0; i < aggregatesInCurrentLevel.size(); i++) {
                AggregateInLattice agg1 = aggregatesInCurrentLevel.get(i);
                assigned = false;
                for (int j = 0; j < aggregatesInAboveLevel.size(); j++) {
                    AggregateInLattice agg2 = aggregatesInAboveLevel.get(j);

                    // compute the cost of finding an aggregate from the current level using an aggregate from the above level
                    HashMap<AnalyzedAttribute, Integer> costPerDimension = costToComputeAgg1FromAgg2(agg1, agg2);
                    if (costPerDimension != null) {
                        int cost = 1;
                        for (Map.Entry<AnalyzedAttribute, Integer> entry: costPerDimension.entrySet()) {
                            cost = cost * entry.getValue();
                        }

                        if (assigned == false) {
                            agg1.setMinimumMemoryCost(cost);
                            agg1.setMinimumMemoryCostPerDimension(costPerDimension);
                            agg1.setParentAggregate(agg2);
                            agg2.addChildAggregate(agg1);
                            agg1.setFlushToDiskRate(computeFlushingToDiskRate(agg2, agg1));
                            assigned = true;
                        }
                        else {
                            if (cost < agg1.getMinimumMemoryCost()) {
                                agg1.setMinimumMemoryCost(cost);
                                agg1.setMinimumMemoryCostPerDimension(costPerDimension);
                                agg1.parentAggregate.childrenAggregates.remove(agg1);
                                agg1.setParentAggregate(agg2);
                                agg2.addChildAggregate(agg1);
                                agg1.setFlushToDiskRate(computeFlushingToDiskRate(agg2, agg1));
                            }
                        }
                    }
                }
            }
        }
    }

    private HashMap<AnalyzedAttribute, Integer> costToComputeAgg1FromAgg2(
            AggregateInLattice agg1, AggregateInLattice agg2) {
        int i = 0, j = 0;
        HashMap<AnalyzedAttribute, Integer> cost = null;
        boolean allDimensionsAreThere = true;
        // check that the dimensions of Agg1 are all in Agg2
        while (i < agg1.dimensions.size() && allDimensionsAreThere == true) {
            allDimensionsAreThere = false;
            while (j < agg2.dimensions.size() && allDimensionsAreThere == false) {
                if (agg1.dimensions.get(i).getEncoding() == agg2.dimensions.get(j).getEncoding()) {
                    allDimensionsAreThere = true;
                }
                j++;
            }
            i++;
        }

        if (allDimensionsAreThere) {
            cost = new HashMap<>();
            int lengthOfPrefix;
            for (i = 0; i < agg1.dimensions.size() && agg1.dimensions.get(i).getEncoding() == agg2.dimensions.get(i).getEncoding(); i++);
            lengthOfPrefix = i;

            for (i = 0; i < lengthOfPrefix; i++) {
                cost.put(agg1.dimensions.get(i), nrOfDistinctValuesPerDimension.get(agg1.dimensions.get(i)));
            }
            for (i = lengthOfPrefix; i < agg1.dimensions.size(); i++) {
                cost.put(agg1.dimensions.get(i), nrOfDistinctValuesInChunkPerDimension.get(agg1.dimensions.get(i)));
            }
        }
        return cost;
    }

    private int computeFlushingToDiskRate(AggregateInLattice parentAgg,
                                          AggregateInLattice childAgg) {
        int rate = 1;
        for (AnalyzedAttribute parentDim: parentAgg.dimensions) {
            int nrOfGroups = nrOfDistinctValuesPerDimension.get(parentDim) / nrOfDistinctValuesInChunkPerDimension.get(parentDim);
            if (nrOfDistinctValuesPerDimension.get(parentDim) % nrOfDistinctValuesInChunkPerDimension.get(parentDim) != 0) {
                nrOfGroups = nrOfGroups + 1;
            }
            rate = rate * nrOfGroups;
            if (!childAgg.dimensions.contains(parentDim)) {
                break;
            }
        }
        return rate;
    }

    public String serializeMMST() {
        if (root != null) {
            return serializeSubtree(root, 0);
        }
        return null;
    }

    private String serializeSubtree(AggregateInLattice ail, int indentation) {
        StringBuilder agg = new StringBuilder("\n");
        for (int i = 0; i < indentation; i++) {
            agg.append("\t");
        }
        agg.append(ail.serializeAggregateInLattice());
        for (AggregateInLattice child: ail.childrenAggregates) {
            agg.append(serializeSubtree(child, indentation + 1));
        }

        return agg.toString();
    }

    public static void setFieldsInAggregates(AggregateInLattice root,
                                             DatabaseHandler handler,
                                             HashMap<AnalyzedAttribute, ArrayList<String>> distinctValuesPerDimension,
                                             HashMap<AnalyzedAttribute, Integer> nrOfGroupsPerDimension,
                                             HashMap<String, String> dictionaryFromFilenameToSHA1,
                                             //HashMap<String, String> dictionaryFromSHA1ToFilename,
                                             AggregateResultManager aggregateResultManager) {
        root.setFields(handler, distinctValuesPerDimension, nrOfGroupsPerDimension, dictionaryFromFilenameToSHA1, /* dictionaryFromSHA1ToFilename, */aggregateResultManager);
        for (AggregateInLattice child: root.childrenAggregates) {
            setFieldsInAggregates(child, handler, distinctValuesPerDimension, nrOfGroupsPerDimension, dictionaryFromFilenameToSHA1, /* dictionaryFromSHA1ToFilename, */aggregateResultManager);
        }
    }

    public void instantiateMMST() {
        root.setUpSubtree();
    }

    public void removeNonAverageAggregationFunctions() {
        root.removeNonAverageAggregationFunctions();
    }

    public void evaluate(int numberOfChunks, String tableName, MOLAPMeasures preaggregatedMeasures, boolean inMemory) {
        if (inMemory) {
            // if we run in memory, the translator has already filled the root with all the data
            root.updateSubtree(0, preaggregatedMeasures);
            root.computeAndStoreAggregateResults(preaggregatedMeasures);
        }
        else {
            try {
                BufferedReader chunk;
                for (int i = 0; i < numberOfChunks; i++) {
                    chunk = getChunk(tableName, i);
                    root.setGlobalChunkID(i);
                    root.loadChunkToRoot(chunk);
                    chunk.close();
                    root.updateSubtree(i, preaggregatedMeasures);
                    root.computeAndStoreAggregateResults(preaggregatedMeasures);
                }
                root.computeAndStoreResultsOfRemainingChunksInMemory(preaggregatedMeasures);
            }
            catch (IOException ex) {
                throw new IllegalStateException(ex);
            }
        }
    }

    protected BufferedReader getChunk(String tableName, int chunkId) {
        String sqlQuery = "select * from " + tableName + " where chunkid=" + chunkId;
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            if (rs.next() == false) {
                return null;
            }
            else {
                InputStream stream = rs.getBinaryStream(2);
                return new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            }
        }
        catch (SQLException | IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public void clean(String tableName, String directoryWithData) {
        /*ArrayList<String> molap;
        String sqlQuery = "select c.relname as key FROM pg_catalog.pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace where c.relname ='" + tableName + "' and c.relkind='r'";*/
        String sqlQuery = "drop table " + tableName + ";";
        try {
            /*molap = handler.getResultSelectFromKeyColumn(sqlQuery);
            for (String d: molap) {
                sqlQuery = "DROP TABLE " + d + ";";
                handler.executeUpdate(sqlQuery);
            }*/
            handler.executeUpdate(sqlQuery);
            Path pathToBeDeleted = Paths.get(directoryWithData);
            deleteDirectory(pathToBeDeleted.toFile());
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Cleaning with the query: " + sqlQuery + "; " + ex);
        }
    }

    private boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file: allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }
}
