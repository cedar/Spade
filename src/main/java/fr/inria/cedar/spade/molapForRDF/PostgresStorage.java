//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.datastructures.Pair;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.jdbc.PgConnection;

// TODO: add timers to measure timeToExportAggregateResultsToCSVFiles
// long time = System.currentTimeMillis();
// PerformanceMeasurements.timeToExportAggregateResultsToCSVFiles[0] += Utils.calculateRunTimeInMilliseconds(time);

public class PostgresStorage extends AggregateResultStorage {
    private static final Logger LOGGER = Logger.getLogger(PostgresStorage.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    private DatabaseHandler handler;
    private LinkedList<String> storedTableNames;
    protected HashMap<String,LinkedList<Pair<String,Double>>> cacheMappingTableNamesToListOfTuplesToStore;

    private HashMap<String, double[]> aggregatesProfiles;
    protected InterestingnessFunction interestingnessFunction;

    private StringBuilder sqlQuery;

    public PostgresStorage(DatabaseHandler handler, InterestingnessFunction interestingnessFunction){
        this.handler = handler;
        this.interestingnessFunction = interestingnessFunction;
        storedTableNames = new LinkedList<>();
        sqlQuery = new StringBuilder();
        cacheMappingTableNamesToListOfTuplesToStore = new HashMap<>();
        aggregatesProfiles = new HashMap<>();
    }

    @Override
    public boolean supportsInterestingnessMeasureComputation(){
        if(interestingnessFunction == InterestingnessFunction.VAR_MEAN || interestingnessFunction == InterestingnessFunction.VAR_SUM || interestingnessFunction == InterestingnessFunction.VAR_FEATURE_SCALING){
            return true;
        }
        return false;
    }

    @Override
    public void addHeaderToAggregateResults(String aggregateFilename, String aggregateSHA1, StringBuilder dimensionsHeader, String aggregatedMeasureHeader){
        String tableName = "aggregateResults_" + aggregateSHA1;

        sqlQuery.setLength(0);
        sqlQuery.append("CREATE TABLE IF NOT EXISTS ").append(tableName).append(" ( ")
                .append("dimnsionsvalues text, ")
                .append("aggregatedmeasure double precision );");

        try {
            handler.executeUpdate(sqlQuery.toString());
            storedTableNames.add(tableName);
            cacheMappingTableNamesToListOfTuplesToStore.put(tableName, new LinkedList());
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void addResultToAggregateResults(String aggregateSHA1, StringBuilder dimensions, double aggregatedMeasure){
        String tableName = "aggregateResults_" + aggregateSHA1;
        LinkedList<Pair<String,Double>> tuples = cacheMappingTableNamesToListOfTuplesToStore.get(tableName);

        if(tuples.size() >= GlobalSettings.REDIS_MAX_PIPELINE_SIZE){
            batchInsertIntoTable(tableName, tuples);
            tuples.clear();
        }
        tuples.add(new Pair(dimensions.toString(), aggregatedMeasure));
    }

    @Override
    public List<String> retrieveAggregateResults(String SHA1OfFilename){

        return null;
    }


    @Override
    public void storeAggregateResultToCSV(String SHA1OfFilename, String filename){
        try (FileOutputStream fileOutputStream = new FileOutputStream(filename)) {
            sqlQuery.setLength(0);
            sqlQuery.append("select * from aggregateResults_").append(SHA1OfFilename);

            new CopyManager((PgConnection) handler.getConnection())
                    .copyOut("COPY (" + sqlQuery.toString() + ") TO STDOUT WITH (FORMAT CSV, HEADER)", fileOutputStream);
        } catch (SQLException | IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public HashMap<String, ArrayList<String>> retrieveAggregatesResults(ArrayList<String> SHA1OfFilenames){
        return null;

    }

    @Override
    public void storeAggregatesResultsToCSV(ArrayList<String> SHA1OfFilenames, HashMap<String, String> dictionaryFromSHA1ToFilename){
        for(String SHA1OfFilename: SHA1OfFilenames){
            storeAggregateResultToCSV(SHA1OfFilename, dictionaryFromSHA1ToFilename.get(SHA1OfFilename));
        }
    }

    @Override
    public ArrayList<Pair<String, Double>> storeTopKToCSV (int k, HashMap<String, String> dictionaryFromSHA1ToFilename){
        String filename, filenameSHA1;
        double score;
        ArrayList<Pair<String,Double>> allResults = new ArrayList<>();
        ArrayList<Pair<String,Double>> topKFilenames = new ArrayList<>();
        ArrayList<String> topKFilenamesSHA1 = new ArrayList<>();
        Pair<String,Double> currentPair;

        for(Map.Entry<String,String> entry: dictionaryFromSHA1ToFilename.entrySet()){
            filenameSHA1 = entry.getKey();
            filename = entry.getValue();
            double[] fileProfile = aggregatesProfiles.get(filenameSHA1);

            score = computeInterestingnessMeasureOfAggregate(filenameSHA1, fileProfile);

            allResults.add(new Pair<>(filenameSHA1,score));
        }

        sortResults(allResults);

        int maxNrOfResults = allResults.size();
        for (int i = 0; i < k && i < maxNrOfResults; i++){
            currentPair = allResults.get(i);
            topKFilenames.add(new Pair<>(dictionaryFromSHA1ToFilename.get(currentPair.getKey()),currentPair.getValue()));
            topKFilenamesSHA1.add(currentPair.getKey());
        }

        storeAggregatesResultsToCSV(topKFilenamesSHA1, dictionaryFromSHA1ToFilename);

        return topKFilenames;
    }

    private double computeInterestingnessMeasureOfAggregate(String filenameSHA1, double[] fileProfile){
        StringBuilder numerator = new StringBuilder();
        StringBuilder denominator = new StringBuilder();
        double score = Double.NaN;
        sqlQuery.setLength(0);
        sqlQuery.append("select ");
        switch (interestingnessFunction){
            case VAR_MEAN:
                numerator.append("sum((aggregatedmeasure-").append(fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]).append(")*(aggregatedmeasure-").append(fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]).append("))");
                if(fileProfile[MOLAPMeasures.COUNT_ORDINAL] > 1){
                    denominator.append((fileProfile[MOLAPMeasures.COUNT_ORDINAL]-1)*fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]*fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]);
                }
                else{
                    denominator.append((fileProfile[MOLAPMeasures.COUNT_ORDINAL])*fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]*fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]);
                }
                break;
            case VAR_SUM:
                numerator.append("sum((aggregatedmeasure-").append(fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]).append(")*(aggregatedmeasure-").append(fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]).append("))");
                if(fileProfile[MOLAPMeasures.COUNT_ORDINAL] > 1){
                    denominator.append((fileProfile[MOLAPMeasures.COUNT_ORDINAL]-1)*fileProfile[MOLAPMeasures.SUM_ORDINAL]*fileProfile[MOLAPMeasures.SUM_ORDINAL]);
                }
                else{
                    denominator.append((fileProfile[MOLAPMeasures.COUNT_ORDINAL])*fileProfile[MOLAPMeasures.SUM_ORDINAL]*fileProfile[MOLAPMeasures.SUM_ORDINAL]);
                }
                break;
            case VAR_FEATURE_SCALING:
                if(fileProfile[MOLAPMeasures.MIN_ORDINAL] == fileProfile[MOLAPMeasures.MAX_ORDINAL]) {
                    numerator.append("sum((aggregatedmeasure-").append(fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]).append(")*(aggregatedmeasure-").append(fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]).append("))");
                }
                else{
                    numerator.append("sum(")
                            .append("(")
                            .append("((aggregatedmeasure-").append(fileProfile[MOLAPMeasures.MIN_ORDINAL]).append(")/").append(fileProfile[MOLAPMeasures.MAX_ORDINAL]-fileProfile[MOLAPMeasures.MIN_ORDINAL])
                            .append(")-").append(fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]).append(")")
                            .append("*")
                            .append("(((aggregatedmeasure-").append(fileProfile[MOLAPMeasures.MIN_ORDINAL]).append(")/").append(fileProfile[MOLAPMeasures.MAX_ORDINAL]-fileProfile[MOLAPMeasures.MIN_ORDINAL])
                            .append(")-").append(fileProfile[MOLAPMeasures.AVERAGE_ORDINAL]).append(")")
                            .append(")");
                }
                if(fileProfile[MOLAPMeasures.COUNT_ORDINAL] > 1){
                    denominator.append((fileProfile[MOLAPMeasures.COUNT_ORDINAL]-1));
                }
                else{
                    denominator.append(1);
                }
                break;
            default:
                throw new IllegalStateException("Interestingness function not supported");
        }
        sqlQuery.append(numerator.toString()).append("/").append(denominator.toString());
        sqlQuery.append(" from aggregateResults_").append(filenameSHA1);

        try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
            while (rs.next()) {
                score = rs.getDouble(1);
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }
        return score;
    }

    @Override
    public void commit(){
        String tableName;
        LinkedList<Pair<String,Double>> tuples;

        for(Map.Entry<String,LinkedList<Pair<String,Double>>> entry: cacheMappingTableNamesToListOfTuplesToStore.entrySet()){
            tableName = entry.getKey();
            tuples = entry.getValue();

            if(!tuples.isEmpty()) {
                batchInsertIntoTable(tableName, tuples);
                tuples.clear();
            }
        }
    }

    @Override
    public void commit(HashMap<String, double[]> aggregatesProfiles) {
        commit();

        for(Map.Entry<String,double[]> agg: aggregatesProfiles.entrySet()){
            this.aggregatesProfiles.put(agg.getKey(), agg.getValue());
        }
    }

    private void batchInsertIntoTable(String tableName, LinkedList<Pair<String,Double>> tuples){
        String prefixForValues = "";

        sqlQuery.setLength(0);
        sqlQuery.append("INSERT INTO ").append(tableName).append(" VALUES ");
        for(Pair<String,Double> tuple: tuples){
            sqlQuery.append(prefixForValues).append("(");
            sqlQuery.append("'").append(tuple.getKey().replace("'","\'")).append("',").append(tuple.getValue());
            sqlQuery.append(")");
            prefixForValues = ",";
        }
        try {
            handler.executeUpdate(sqlQuery.toString());
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void removeData(){
        for(String tableName: storedTableNames){
            sqlQuery.setLength(0);
            sqlQuery.append("DROP TABLE ").append(tableName);
            try {
                handler.executeUpdate(sqlQuery.toString());
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    @Override
    public void closeConnection(){}

    private void sortResults(ArrayList<Pair<String,Double>> results){
        Collections.sort(results, new ResultsComparator());
    }

    private static class ResultsComparator implements
            Comparator<Pair<String, Double>> {
        @Override
        public int compare(Pair<String, Double> agg1, Pair<String, Double> agg2) {
            Double score1 = agg1.getValue();
            Double score2 = agg2.getValue();

            if (score1 == null) {
                return -1;
            }
            if (score2 == null) {
                return -1;
            }

            return -score1.compareTo(score2);
        }
    }
}
