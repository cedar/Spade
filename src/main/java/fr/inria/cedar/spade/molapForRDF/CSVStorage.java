//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import fr.inria.cedar.spade.configuration.PerformanceMeasurements;
import fr.inria.cedar.spade.datastructures.Pair;
import fr.inria.cedar.spade.datastructures.Utils;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class CSVStorage extends AggregateResultStorage {
    private static final Logger LOGGER = Logger.getLogger(CSVStorage.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected HashMap<String, BufferedWriter> dictionaryFromAggregateSHA1ToBufferedWriter;

    public CSVStorage() {
        dictionaryFromAggregateSHA1ToBufferedWriter = new HashMap<>();
    }

    @Override
    public boolean supportsInterestingnessMeasureComputation() {
        return false;
    }

    @Override
    public void addHeaderToAggregateResults(String aggregateFilename,
                                            String aggregateSHA1,
                                            StringBuilder dimensionsHeader,
                                            String aggregatedMeasureHeader) {
        long time = System.currentTimeMillis();
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(aggregateFilename));
            bw.write(dimensionsHeader.toString() + aggregatedMeasureHeader + "\n");
            dictionaryFromAggregateSHA1ToBufferedWriter.put(aggregateSHA1, bw);
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
        PerformanceMeasurements.totalTimeToExportAggregateResultsToCSVFiles[0] += Utils.calculateRunTimeInMilliseconds(time);
    }

    @Override
    public void addResultToAggregateResults(String aggregateSHA1,
                                            StringBuilder dimensions,
                                            double aggregatedMeasure) {
        long time = System.currentTimeMillis();
        try {
            BufferedWriter bw = dictionaryFromAggregateSHA1ToBufferedWriter.get(aggregateSHA1);
            bw.write(dimensions + String.valueOf(aggregatedMeasure) + "\n");
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
        PerformanceMeasurements.totalTimeToExportAggregateResultsToCSVFiles[0] += Utils.calculateRunTimeInMilliseconds(time);
    }

    @Override
    public List<String> retrieveAggregateResults(String SHA1OfFilename) {
        return null;
    }

    @Override
    public void storeAggregateResultToCSV(String SHA1OfFilename, String filename) {
    }

    @Override
    public HashMap<String, ArrayList<String>> retrieveAggregatesResults(
            ArrayList<String> SHA1OfFilenames) {
        return null;
    }

    @Override
    public void storeAggregatesResultsToCSV(ArrayList<String> SHA1OfFilenames,
                                            HashMap<String, String> dictionaryFromSHA1ToFilename) {
    }

    @Override
    public ArrayList<Pair<String, Double>> storeTopKToCSV(int k,
                                                          HashMap<String, String> dictionaryFromSHA1ToFilename) {
        return null;
    }

    @Override
    public void commit() {
        for (Map.Entry<String, BufferedWriter> entry: dictionaryFromAggregateSHA1ToBufferedWriter.entrySet()) {
            BufferedWriter bw = entry.getValue();
            if (bw != null) {
                try {
                    bw.close();
                }
                catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }

    @Override
    public void commit(HashMap<String, double[]> aggregatesProfiles) {
    }

    @Override
    public void removeData() {
        /*try (Stream<Path> walk = Files.walk(Paths.get(GlobalSettings.FLASK_FOLDER_PATH))) {
            // all csv file in the sourceFolder (recursive)
            List<String> result = walk.map(x -> x.toString()).filter(f -> f.endsWith(".csv")).collect(Collectors.toList());

            // build the profile for each file
            for (String filename: result) {
                LOGGER.info(filename);
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }*/
    }

    @Override
    public void closeConnection() {
    }
}
