//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.molapForRDF;

import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MOLAPMeasures {
    private static final Logger LOGGER = Logger.getLogger(MOLAPMeasures.class.getName());

    // integer constants more efficient than enums: will hopefully be inlined during compilation
    public static final int COUNT_ORDINAL;
    public static final int SUM_ORDINAL;
    public static final int AVERAGE_ORDINAL;
    public static final int MIN_ORDINAL;
    public static final int MAX_ORDINAL;
    public static final int VARIANCE_ORDINAL;
    public static final int ESTIMATED_TOTAL_GROUP_SIZE;
    public static final int EXTRAPOLATED_SUM_ORDINAL;
    public static final int NUMBER_OF_PREAGGREGATED_VALUE_TYPES;

    static {
        LOGGER.setLevel(Level.INFO);

        COUNT_ORDINAL = 0;
        SUM_ORDINAL = 1;
        AVERAGE_ORDINAL = 2;
        MIN_ORDINAL = 3;
        MAX_ORDINAL = 4;
        VARIANCE_ORDINAL = 5;
        ESTIMATED_TOTAL_GROUP_SIZE = 6;
        EXTRAPOLATED_SUM_ORDINAL = 7;

        NUMBER_OF_PREAGGREGATED_VALUE_TYPES = 8;
    }

    public static int getOrdinal(AggregationFunction aggregationFunction) {
        switch (aggregationFunction) {
            case COUNT:
                return COUNT_ORDINAL;
            case SUM:
                return SUM_ORDINAL;
            case AVERAGE:
                return AVERAGE_ORDINAL;
            case MIN:
                return MIN_ORDINAL;
            case MAX:
                return MAX_ORDINAL;
            default:
                throw new IllegalStateException("No ordinal for " + aggregationFunction + " aggregation function");
        }
    }

    protected final DatabaseHandler handler;
    protected final CandidateFactSet candidateFactSet;
    protected final boolean useEarlyStop;

    // AnalyzedAttribute refers to a measure
    // double[][] contains the values of the pre-aggreagted measures for each subject
    // first index goes over aggregation functions, and the second on subject ids
    protected final HashMap<AnalyzedAttribute, double[][]> memoryAllocationForMeasures;
    protected final HashMap<AnalyzedAttribute, HashMap<Integer, ArrayList<Double>>> memoryAllocationOfRawValuesForMeasures;

    public MOLAPMeasures(DatabaseHandler handler,
                         CandidateFactSet candidateFactSet,
                         boolean useEarlyStop) {
        this.handler = handler;
        this.candidateFactSet = candidateFactSet;
        this.useEarlyStop = useEarlyStop;
        memoryAllocationForMeasures = new HashMap<>();
        memoryAllocationOfRawValuesForMeasures = new HashMap<>();
    }

    public void updateMeasures(ArrayList<AnalyzedAttribute> measures,
                               HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsPerEachAttribute) {
        for (AnalyzedAttribute measure: measures) {
            addMeasure(measure, aggregationFunctionsPerEachAttribute.get(measure));
        }
    }

    public double[][] getPreaggregatedValuesArrayPerMeasure(
            AnalyzedAttribute measure) {
        return memoryAllocationForMeasures.get(measure);
    }

    public HashMap<Integer, ArrayList<Double>> getRawValuesOfMeasure(
            AnalyzedAttribute measure) {
        return memoryAllocationOfRawValuesForMeasures.get(measure);
    }

    /*
     * adds one single measure with all of the aggregation functions assigned to it
     */
    protected void addMeasure(AnalyzedAttribute measure,
                              LinkedList<AggregationFunction> aggregationFunctions) {
        if (!(memoryAllocationForMeasures.containsKey(measure))) { // || memoryAllocationOfRawValuesForMeasures.containsKey(measure))) {
            boolean isAtMostOne = measure.isAtMostOne();

            LinkedList<AggregationFunction> aggregationFunctionsList = new LinkedList<>();

            boolean hasCount = false;
            boolean hasSum = false;
            boolean hasSomeAggregationFunction = false;
            boolean hasRawValuesList = false;
            for (AggregationFunction aggregationFunction: aggregationFunctions) {
                switch (aggregationFunction) {
                    case COUNT:
                        hasCount = true;
                        hasSomeAggregationFunction = true;
                        break;
                    case SUM:
                        hasSum = true;
                        hasSomeAggregationFunction = true;
                        aggregationFunctionsList.add(aggregationFunction);
                        break;
                    case MIN:
                    case MAX:
                        if (!isAtMostOne) {
                            hasSomeAggregationFunction = true;
                            aggregationFunctionsList.add(aggregationFunction);
                        }
                        break;
                    case RAW_VALUES_LIST:
                        if (useEarlyStop && !isAtMostOne) {
                            hasRawValuesList = true;
                        }
                        break;
                    default: // skip AVERAGE
                        break;
                }
            }

            // if hasCount is true, then we need to compute COUNT
            // however, if we have SUM and the measure is at-most-one, we can reconstruct COUNT from SUM
            // therefore, we only need to load COUNT if we can't reconstruct it from SUM
            if (hasCount || !(isAtMostOne && hasSum)) {
                aggregationFunctionsList.add(AggregationFunction.COUNT);
                hasSomeAggregationFunction = true;
            }

            if (hasSomeAggregationFunction) {
                double[][] preaggregatedValuesByAggregationFunction = new double[5][];
                int candidateFactSetSize = candidateFactSet.getSize();

                for (AggregationFunction aggregationFunction: aggregationFunctionsList) {
                    switch (aggregationFunction) {
                        case COUNT:
                            preaggregatedValuesByAggregationFunction[COUNT_ORDINAL] = new double[candidateFactSetSize];
                            break;
                        case SUM:
                            preaggregatedValuesByAggregationFunction[SUM_ORDINAL] = new double[candidateFactSetSize];
                            break;
                        case MIN:
                            preaggregatedValuesByAggregationFunction[MIN_ORDINAL] = new double[candidateFactSetSize];
                            break;
                        case MAX:
                            preaggregatedValuesByAggregationFunction[MAX_ORDINAL] = new double[candidateFactSetSize];
                            break;
                        default: // no memory allocation for AVERAGE; this branch is never reached
                            break;
                    }
                    memoryAllocationForMeasures.put(measure, preaggregatedValuesByAggregationFunction);
                }
            }
            else {
                // it's important to add the measure to the list of loaded measures to indicate for later
                memoryAllocationForMeasures.put(measure, null);
            }

            if (hasRawValuesList) {
                memoryAllocationOfRawValuesForMeasures.put(measure, new HashMap<>());

                // adding RAW_VALUES_LIST here to ensure it is the last in the list
                aggregationFunctionsList.add(AggregationFunction.RAW_VALUES_LIST);
            }
            // commented since no longer in use in the decision process whether to load the measure or not
            /*else {
                memoryAllocationOfRawValuesForMeasures.put(measure, null);
            }*/

            if (hasSomeAggregationFunction || hasRawValuesList) {
                fillMemoryWithMeasureValues(measure, aggregationFunctionsList, hasRawValuesList);
            }
        }
    }

    protected void fillMemoryWithMeasureValues(AnalyzedAttribute measure,
                                               LinkedList<AggregationFunction> aggregationFunctions,
                                               boolean hasRawValuesList) {
        double[][] memoryAllocationForMeasure = memoryAllocationForMeasures.get(measure);
        HashMap<Integer, ArrayList<Double>> memoryAllocationOfRawValues = memoryAllocationOfRawValuesForMeasures.get(measure);

        String sqlQuery = constructQueryToGetData(measure, aggregationFunctions);
        if (hasRawValuesList) {
            // removing RAW_VALUES_LIST from the list, it is the last one
            aggregationFunctions.removeLast();
        }

        try {
            handler.executeUpdate("set enable_hashjoin=off;");
        } catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }

        int subject_id;
        int j;
        double value;
        String rawValues;
        ArrayList<Double> rawValuesListParsed = new ArrayList<>(3); // hint for the initial capacity of the array: we hope to not have more than 3 occurrences of a measure per subject
        try (PreparedStatement stmt = handler.getPreparedStatementWithoutClosingLastOne(sqlQuery)) {
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    subject_id = rs.getInt(1) - 1;
                    j = 2;
                    for (AggregationFunction aggregationFunction: aggregationFunctions) {
                        value = rs.getDouble(j);
                        switch (aggregationFunction) {
                            case COUNT:
                                if (rs.wasNull() || value == 0) {
                                    memoryAllocationForMeasure[COUNT_ORDINAL][subject_id] = Double.NaN;
                                }
                                else {
                                    memoryAllocationForMeasure[COUNT_ORDINAL][subject_id] = value;
                                }
                                break;
                            case SUM:
                                if (rs.wasNull()) {
                                    memoryAllocationForMeasure[SUM_ORDINAL][subject_id] = Double.NaN;
                                }
                                else {
                                    memoryAllocationForMeasure[SUM_ORDINAL][subject_id] = value;
                                }
                                break;
                            case MIN:
                                if (rs.wasNull()) {
                                    memoryAllocationForMeasure[MIN_ORDINAL][subject_id] = Double.NaN;
                                }
                                else {
                                    memoryAllocationForMeasure[MIN_ORDINAL][subject_id] = value;
                                }
                                break;
                            case MAX:
                                if (rs.wasNull()) {
                                    memoryAllocationForMeasure[MAX_ORDINAL][subject_id] = Double.NaN;
                                }
                                else {
                                    memoryAllocationForMeasure[MAX_ORDINAL][subject_id] = value;
                                }
                                break;
                            default: // no AVERAGE; this branch is never reached
                                break;
                        }
                        j++;
                    }
                    if (hasRawValuesList) {
                        // RAW_VALUES_LIST is the last in the list of aggregation functions
                        // j was incremented in the end of the for loop or equals 2 if the loop was empty
                        rawValues = rs.getString(j);
                        if (!rs.wasNull()) {
                            rawValuesListParsed.clear(); // avoids object reallocation, preserves the initial capacity of the ArrayList
                            for (String element: rawValues.split(",")) {
                                rawValuesListParsed.add(Double.parseDouble(element));
                            }
                            memoryAllocationOfRawValues.put(subject_id, rawValuesListParsed);
                        }
                    }
                }
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }

        try {
            handler.executeUpdate("set enable_hashjoin=on;");
        } catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /*
     * Constructs one single query to get all the aggregation functions of a given measure.
     * Note that AVERAGE is not retrieved from the database as it is computed as SUM/COUNT.
     */
    protected String constructQueryToGetData(AnalyzedAttribute measure,
                                             LinkedList<AggregationFunction> aggregationFunctions) {
        StringBuilder sqlQuery = new StringBuilder("select cfs.subject_id");

        for (AggregationFunction aggregationFunction: aggregationFunctions) {
            switch (aggregationFunction) {
                case COUNT:
                    sqlQuery.append(", m.COUNT_o");
                    break;
                case SUM:
                    sqlQuery.append(", m.SUM_o");
                    break;
                case MIN:
                    sqlQuery.append(", m.MIN_o");
                    break;
                case MAX:
                    sqlQuery.append(", m.MAX_o");
                    break;
                case RAW_VALUES_LIST:
                    sqlQuery.append(", m.raw_values_list");
                    break;
                default: // no AVERAGE; this branch is never reached
                    break;
            }
        }
        sqlQuery.append(" from ").append(candidateFactSet.getTableName()).append(" as cfs left join t_").append(measure.getEncoding()).append("_preaggregated as m on cfs.s=m.s"); // we don't have to order by cfs.subject_id asc
        return sqlQuery.toString();
    }
}
