//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import java.util.ArrayList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AttributeBasedCandidateFactSetSelector extends CandidateFactSetSelector {
    private static final Logger LOGGER = Logger.getLogger(AttributeBasedCandidateFactSetSelector.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }
    protected ArrayList<Long> properties;

    public AttributeBasedCandidateFactSetSelector(ArrayList<Long> newProperties,
                                                  DatabaseHandler handler) {
        this.properties = newProperties;
        this.handler = handler;
    }

    @Override
    public CandidateFactSet getFactsFromCandidateFactSet(
            String candidateFactSetEncoding) {
        /**
         * The returned CFS is composed of subjects such that each subject has all the specified properties.
         * Each property is outgoing.
         */
        StringBuilder query = new StringBuilder("select s from encoded_triples where ");
        for (int i = 0; i < properties.size(); i++) {
            query.append("s in (select distinct s from encoded_triples where p=").append(properties.get(i)).append(" ) ");
            if (i + 1 < properties.size()) {
                query.append(" and ");
            }
        }

        return new CandidateFactSet(handler, query.toString(), null);
    }

    @Override
    public ArrayList<String> getAllCandidateFactSets() {
        // TODO: getAllCandidateFactSets
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
