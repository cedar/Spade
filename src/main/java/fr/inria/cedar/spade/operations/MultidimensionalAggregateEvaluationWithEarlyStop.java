//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.configuration.PerformanceMeasurements;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.datastructures.estimation.EarlyStopSample;
import fr.inria.cedar.spade.datastructures.estimation.Estimator;
import fr.inria.cedar.spade.datastructures.estimation.MultidimensionalAggregateEstimate;
import fr.inria.cedar.spade.molapForRDF.AggregateResultManager;
import fr.inria.cedar.spade.molapForRDF.MOLAPMeasures;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MultidimensionalAggregateEvaluationWithEarlyStop extends Operation {
    private static final Logger LOGGER = Logger.getLogger(MultidimensionalAggregateEvaluationWithEarlyStop.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected LinkedList<EarlyStopSample> aggregatesWithSamples;
    protected HashMap<AnalyzedAttribute, ArrayList<String>> distinctValuesPerDimension;
    protected InterestingnessFunction interestingnessFunction;
    protected HashMap<String, String> dictionaryFromFilenameToSHA1;
    protected AggregateResultManager aggregateResultManager;
    protected TopKSelectorForMultidimensionalSingleAggregateEstimates earlyStopTopK;

    public MultidimensionalAggregateEvaluationWithEarlyStop(
            DatabaseHandler handler,
            LinkedList<EarlyStopSample> aggregatesWithSamples,
            HashMap<AnalyzedAttribute, Integer> chunkSizePerAttribute,
            HashMap<AnalyzedAttribute, ArrayList<String>> distinctValuesPerDimension,
            InterestingnessFunction interestingnessFunction,
            HashMap<String, String> dictionaryFromFilenameToSHA1,
            AggregateResultManager aggregateResultManager) {
        this.handler = handler;
        this.aggregatesWithSamples = aggregatesWithSamples;
        this.distinctValuesPerDimension = distinctValuesPerDimension;
        this.interestingnessFunction = interestingnessFunction;
        this.dictionaryFromFilenameToSHA1 = dictionaryFromFilenameToSHA1;
        this.aggregateResultManager = aggregateResultManager;
        earlyStopTopK = new TopKSelectorForMultidimensionalSingleAggregateEstimates(handler, distinctValuesPerDimension, aggregatesWithSamples, interestingnessFunction, dictionaryFromFilenameToSHA1, aggregateResultManager);
    }

    protected void stopSingleAggregate(
            MultidimensionalAggregateEstimate aggregateEstimate,
            Estimator singleAggregateEstimator,
            StringBuilder reason,
            long startTime,
            long endTime,
            int iteration,
            int iterationsWithoutStoppage) {
        AnalyzedAttribute measure = singleAggregateEstimator.getMeasure();
        AggregationFunction aggregationFunction = singleAggregateEstimator.getAggregationFunction();
        aggregateEstimate.removeEstimator(measure, aggregationFunction);
        aggregateEstimate.getAggregate().removeMeasureAggregationFunctionCombination(measure, aggregationFunction);

        PerformanceMeasurements.numberOfStoppedAggregates[0]++;
        PerformanceMeasurements.totalNumberOfStoppedAggregates[0]++;
        //LOGGER.info("Aggregate\n" + reason + singleAggregateEstimate.toString(time) + "\n");
        StringBuilder message = new StringBuilder();
        message.append("Aggregate: ").append(singleAggregateEstimator.toShortString(startTime, endTime, iteration, iterationsWithoutStoppage, false)).append("\n\tstopped ").append(reason);
        LOGGER.info(message.toString());
    }

    public double runEarlyStop(MOLAPMeasures preaggregatedMeasures,
                               double globalKthBestScore) {
        long time = System.currentTimeMillis();

        if (earlyStopTopK.size() <= GlobalSettings.K_FOR_TOP_K) {
            LOGGER.info("At most k = " + GlobalSettings.K_FOR_TOP_K + " aggregates present in the cadidate fact set: no interest in early-stop, terminating");
            return globalKthBestScore;
        }

        earlyStopTopK.setMeasures(preaggregatedMeasures);

        //LOGGER.info("Starting early-stop evaluation with aggregates");//:\n" + earlyStopTopK.toString());
        boolean allEarlyStopEligibleAggregatesEvaluated = false;
        Estimator singleAggregateEstimator;
        MultidimensionalAggregateEstimate aggregateEstimate;
        Estimator kthSingleAggregateEstimator;
        boolean anAggregateWasStopped;
        int iteration = 0;
        int iterationsWithoutStoppage = 0;
        StringBuilder reason = new StringBuilder();
        double aggregateUpperBound;
        double kthLocalTopKAggregateLowerBound;
        double newGlobalKthBestScore = globalKthBestScore;

        do { // do-while to save one condition evaluation in the beginning (always true)
            //LOGGER.info("Iteration: " + iteration);

            // in the first iteration of the do-while loop the local top-k doesn't need to be sorted
            kthSingleAggregateEstimator = earlyStopTopK.getKthSingleAggregateEstimator();

            if (kthSingleAggregateEstimator == null) {
                return newGlobalKthBestScore;
            }

            // there are at most k-1 aggregates left or we reached maximum number of iterations without stoppage, and not all aggregates have been evaluated
            if (iterationsWithoutStoppage <= GlobalSettings.EARLY_STOP_MAX_NUMBER_OF_ITERATIONS_WITHOUT_STOPPAGE && !allEarlyStopEligibleAggregatesEvaluated) {
                anAggregateWasStopped = false;
                allEarlyStopEligibleAggregatesEvaluated = true;

                for (Iterator<Estimator> iter = earlyStopTopK.getAllSingleAggregateEstimators().iterator(); iter.hasNext();) {
                    singleAggregateEstimator = iter.next();
                    aggregateEstimate = singleAggregateEstimator.getAggregateEstimate();

                    // retrieve only once per multidimensional aggregate (and not for each single aggregate)
                    aggregateEstimate.retrieveBatchFromEachGroup(iteration);
                    singleAggregateEstimator.updateEstimatesAndGetStage(iteration);

                    if (iteration > 0) {
                        if (singleAggregateEstimator.isComparable()) { // comparable estimate
                            // compare with the global kth best score
                            aggregateUpperBound = singleAggregateEstimator.getUpperBoundEstimate();
                            if (aggregateUpperBound < globalKthBestScore) {
                                reason.setLength(0);
                                reason.append("due to earlyStopAggregateUpperBound: ").append(aggregateUpperBound).append(", globalKthBestScore: ").append(globalKthBestScore).append("\n");
                                stopSingleAggregate(aggregateEstimate, singleAggregateEstimator, reason, time, System.currentTimeMillis(), iteration, iterationsWithoutStoppage);
                                iter.remove(); // remove from the local top-k
                                anAggregateWasStopped = true;
                            }
                            // compare with kth aggregate in local top-k
                            else if (kthSingleAggregateEstimator.isComparable()) { // both comparable
                                kthLocalTopKAggregateLowerBound = kthSingleAggregateEstimator.getLowerBoundEstimate();
                                if (aggregateUpperBound < kthLocalTopKAggregateLowerBound) {
                                    reason.setLength(0);
                                    reason.append("due to earlyStopAggregateUpperBound: ").append(aggregateUpperBound).append(", kthLocalTopKAggregateLowerBound: ").append(kthLocalTopKAggregateLowerBound).append("\n");
                                    stopSingleAggregate(aggregateEstimate, singleAggregateEstimator, reason, time, System.currentTimeMillis(), iteration, iterationsWithoutStoppage);
                                    iter.remove(); // remove from the local top-k
                                    anAggregateWasStopped = true;
                                }
                            }
                        }

                        if (!singleAggregateEstimator.sampleExhausted()) {
                            allEarlyStopEligibleAggregatesEvaluated = false;
                        }
                    }
                    else {
                        allEarlyStopEligibleAggregatesEvaluated = false;
                    }
                }

                if (anAggregateWasStopped) {
                    iterationsWithoutStoppage = 0;
                }
                else {
                    iterationsWithoutStoppage++;
                }
                iteration++;

                earlyStopTopK.sortAggregates(); // we must sort the local top-k
                //LOGGER.info("Local top-k:\n" + earlyStopTopK.toString());
                //LOGGER.info("Local top-k:\n" + earlyStopTopK.toShortString(time, System.currentTimeMillis(), iteration, iterationsWithoutStoppage, true));
            }
            else { // last iteration of the do-while loop
                if (kthSingleAggregateEstimator.sampleExhausted()) {
                    double pointEstimate = kthSingleAggregateEstimator.getPointEstimate();
                    if (globalKthBestScore < pointEstimate) {
                        newGlobalKthBestScore = pointEstimate;
                    }
                }
                else {
                    kthLocalTopKAggregateLowerBound = kthSingleAggregateEstimator.getLowerBoundEstimate();
                    if (globalKthBestScore < kthLocalTopKAggregateLowerBound) {
                        newGlobalKthBestScore = kthLocalTopKAggregateLowerBound;
                    }
                }

                //LOGGER.info("Terminating early-stop evaluation. Local top-k:\n" + earlyStopTopK.toString());
                //LOGGER.info("Terminating early-stop evaluation. Local top-k:\n" + earlyStopTopK.toShortString(time, System.currentTimeMillis(), iteration, iterationsWithoutStoppage, false));
                break;
            }
        }
        while (true);

        return newGlobalKthBestScore;
    }
}
