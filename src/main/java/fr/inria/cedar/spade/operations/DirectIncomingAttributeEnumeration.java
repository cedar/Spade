//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class DirectIncomingAttributeEnumeration extends AttributeEnumeration {
    private static final Logger LOGGER = Logger.getLogger(DirectIncomingAttributeEnumeration.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public DirectIncomingAttributeEnumeration(DatabaseHandler handler) {
        this.handler = handler;
    }

    @Override
    public LinkedList<Long> getAllAttributes(CandidateFactSet candidateFactSet) {
        LinkedList<Long> attributes = new LinkedList<>();
        StringBuilder sqlQuery = new StringBuilder("select distinct t.p from encoded_triples t where t.o in (select s from ");
        sqlQuery.append(candidateFactSet.getTableName())
                .append(") and p <> ")
                .append(GlobalSettings.TYPE_PROPERTY_ENCODING);
        try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
            while (rs.next()) {
                attributes.add(rs.getLong(1));
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Unable to compute distinct incoming properties based on the triple table; " + sqlQuery + "; " + ex);
        }

        // if there are some derivations, then add derived attributes
        if (GlobalSettings.ENABLE_COUNT_DERIVATION
            || GlobalSettings.ENABLE_KWD_EXTRACTION
            || GlobalSettings.ENABLE_LANGUAGE_DETECTION
            || GlobalSettings.ENABLE_PATH_DERIVATION) {
            sqlQuery.setLength(0);
            sqlQuery.append("select distinct a.attributeencoding from encoded_triples t join allattributes a on t.p = a.derivedfromrootencoding where t.o in (select s from")
                    .append(candidateFactSet.getTableName())
                    .append(") and t.p <> ")
                    .append(GlobalSettings.TYPE_PROPERTY_ENCODING)
                    .append(" and a.derivationlevel <= ")
                    .append(GlobalSettings.MAX_DERIVATION_DEPTH)
                    .append(" and (");
            addDerivationConditions(sqlQuery);
            sqlQuery.append(")");

            try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
                while (rs.next()) {
                    attributes.add(rs.getLong(1));
                }
            }
            catch (SQLException ex) {
                throw new IllegalStateException("Unable to compute distinct incoming derived properties based on the triple table; " + sqlQuery + "; " + ex);
            }
        }

        return attributes;
    }

    @Override
    public LinkedList<Long> getAllAttributesUsingSummary(
            CandidateFactSet candidateFactSet) {
        // TODO: getAllAttributesUsingSummary
        throw new UnsupportedOperationException("Not supported yet");
    }
}
