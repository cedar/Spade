//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class SummaryBasedCandidateFactSetSelector extends CandidateFactSetSelector {
    private static final Logger LOGGER = Logger.getLogger(SummaryBasedCandidateFactSetSelector.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public SummaryBasedCandidateFactSetSelector(DatabaseHandler handler) {
        this.handler = handler;
    }

    @Override
    public CandidateFactSet getFactsFromCandidateFactSet(
            String candidateFactSetEncoding) {
        StringBuilder sqlQuery = new StringBuilder("select graphnode as s from ");
        sqlQuery.append(GlobalSettings.SUMMARY_REP_TABLE_NAME).append(" where summarynode = ").append(candidateFactSetEncoding);

        return new CandidateFactSet(handler, sqlQuery.toString(), null);
    }

    // accesses the database in order to find and return the encodings of all distinct summary nodes
    @Override
    public ArrayList<String> getAllCandidateFactSets() {
        LOGGER.info("Searching summary nodes");
        ArrayList<String> distinctSummaryNodes = new ArrayList<>();
        StringBuilder sqlQuery = new StringBuilder("select distinct summarynode from ");
        sqlQuery.append(GlobalSettings.SUMMARY_REP_TABLE_NAME);
        try {
            distinctSummaryNodes = handler.getResultSetSelectFromSummaryNodesColumn(sqlQuery.toString());
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Unable to find all the summary nodes " + sqlQuery + "\n" + ex);
        }
        if (distinctSummaryNodes.isEmpty()) {
            throw new IllegalStateException("The summary has no nodes");
        }
        LOGGER.info("\tfound: " + distinctSummaryNodes);

        return distinctSummaryNodes;
    }
}
