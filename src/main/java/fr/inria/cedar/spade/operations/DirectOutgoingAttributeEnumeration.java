//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import fr.inria.cedar.spade.datastructures.CandidateFactSetSelectionMethod;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class DirectOutgoingAttributeEnumeration extends AttributeEnumeration {
    private static final Logger LOGGER = Logger.getLogger(DirectOutgoingAttributeEnumeration.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public DirectOutgoingAttributeEnumeration(DatabaseHandler handler) {
        this.handler = handler;
    }

    @Override
    public LinkedList<Long> getAllAttributes(CandidateFactSet candidateFactSet) {
        LinkedList<Long> attributes = new LinkedList<>();
        StringBuilder sqlQuery = new StringBuilder("select distinct t.p from encoded_triples t where t.s in (select s from ");
        sqlQuery.append(candidateFactSet.getTableName())
                .append(") and t.p <> ")
                .append(GlobalSettings.TYPE_PROPERTY_ENCODING);
        try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
            while (rs.next()) {
                attributes.add(rs.getLong(1));
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Unable to compute distinct outgoing properties based on the triple table; " + sqlQuery + "; " + ex);
        }

        // if there are some derivations, then add derived attributes
        if (GlobalSettings.ENABLE_COUNT_DERIVATION
            || GlobalSettings.ENABLE_PATH_DERIVATION
            || GlobalSettings.ENABLE_KWD_EXTRACTION
            || GlobalSettings.ENABLE_LANGUAGE_DETECTION) {
            sqlQuery.setLength(0);
            sqlQuery.append("select distinct a.attributeencoding from encoded_triples t join allattributes a on t.p = a.derivedfromrootencoding where t.s in (select s from")
                    .append(candidateFactSet.getTableName())
                    .append(") and t.p <> ")
                    .append(GlobalSettings.TYPE_PROPERTY_ENCODING)
                    .append(" and a.derivationlevel <= ")
                    .append(GlobalSettings.MAX_DERIVATION_DEPTH);
            try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
                while (rs.next()) {
                    attributes.add(rs.getLong(1));
                }
            }
            catch (SQLException ex) {
                throw new IllegalStateException("Unable to compute distinct outgoing derived properties based on the triple table; " + sqlQuery + "; " + ex);
            }
        }

        return attributes;
    }

    @Override
    public LinkedList<Long> getAllAttributesUsingSummary(
            CandidateFactSet candidateFactSet) {
        CandidateFactSetSelectionMethod candidateFactSetSelectionMethod = GlobalSettings.CANDIDATE_FACT_SET_SELECTION;
        StringBuilder sqlQuery = new StringBuilder();
        LinkedList<Long> attributes = new LinkedList<>();
        switch (candidateFactSetSelectionMethod) {
            case TYPED_BASED_CANDIDATE_FACT_SET_SELECTION:
                sqlQuery.append("select distinct t.p from ")
                        .append(GlobalSettings.SUMMARY_TABLE_NAME)
                        .append(" t where t.s in (select s from ")
                        .append(GlobalSettings.SUMMARY_TABLE_NAME)
                        .append(" where p = ")
                        .append(GlobalSettings.TYPE_PROPERTY_ENCODING)
                        .append(" and o = ")
                        .append(candidateFactSet.getEncoding())
                        .append(") and t.p <> ")
                        .append(GlobalSettings.TYPE_PROPERTY_ENCODING);

                try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
                    while (rs.next()) {
                        attributes.add(rs.getLong(1));
                    }
                }
                catch (SQLException ex) {
                    throw new IllegalStateException("Unable to compute distinct outgoing properties based on the summary table; " + sqlQuery + "; " + ex);
                }

                // if there are some derivations, then add derived attributes
                if (GlobalSettings.ENABLE_COUNT_DERIVATION
                    || GlobalSettings.ENABLE_PATH_DERIVATION
                    || GlobalSettings.ENABLE_KWD_EXTRACTION
                    || GlobalSettings.ENABLE_LANGUAGE_DETECTION) {
                    sqlQuery.setLength(0);
                    sqlQuery.append("select distinct a.attributeencoding from ")
                            .append(GlobalSettings.SUMMARY_TABLE_NAME)
                            .append(" t join allattributes a on t.p = a.derivedfromrootencoding where t.s in (select s from ")
                            .append(GlobalSettings.SUMMARY_TABLE_NAME)
                            .append(" where p = ")
                            .append(GlobalSettings.TYPE_PROPERTY_ENCODING)
                            .append(" and o = ")
                            .append(candidateFactSet.getEncoding())
                            .append(") and a.derivationlevel <= ")
                            .append(GlobalSettings.MAX_DERIVATION_DEPTH)
                            .append(" and (");
                    addDerivationConditions(sqlQuery);
                    sqlQuery.append(")");

                    try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
                        while (rs.next()) {
                            attributes.add(rs.getLong(1));
                        }
                    }
                    catch (SQLException ex) {
                        throw new IllegalStateException("Unable to compute distinct outgoing derived properties based on the triple table; " + sqlQuery + "; " + ex);
                    }
                }
                break;
            case SUMMARY_BASED_CANDIDATE_FACT_SET_SELECTION:
                sqlQuery.append("select distinct p from ")
                        .append(GlobalSettings.SUMMARY_TABLE_NAME)
                        .append(" where s = ")
                        .append(candidateFactSet.getEncoding())
                        .append(" and p <> ")
                        .append(GlobalSettings.TYPE_PROPERTY_ENCODING);

                try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
                    while (rs.next()) {
                        attributes.add(rs.getLong(1));
                    }
                }
                catch (SQLException ex) {
                    throw new IllegalStateException("Unable to compute distinct outgoing properties based on the summary table; " + sqlQuery + "; " + ex);
                }

                // if there are some derivations, then add derived attributes
                if (GlobalSettings.ENABLE_COUNT_DERIVATION
                    || GlobalSettings.ENABLE_PATH_DERIVATION
                    || GlobalSettings.ENABLE_KWD_EXTRACTION
                    || GlobalSettings.ENABLE_LANGUAGE_DETECTION) {
                    sqlQuery.setLength(0);
                    sqlQuery.append("select distinct a.attributeencoding from ")
                            .append(GlobalSettings.SUMMARY_TABLE_NAME)
                            .append(" t join allattributes a on t.p = a.derivedfromrootencoding where t.s = ")
                            .append(candidateFactSet.getEncoding())
                            .append(" and t.p <> ")
                            .append(GlobalSettings.TYPE_PROPERTY_ENCODING)
                            .append(" and a.derivationlevel <= ")
                            .append(GlobalSettings.MAX_DERIVATION_DEPTH)
                            .append(" and (");
                    addDerivationConditions(sqlQuery);
                    sqlQuery.append(")");

                    try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
                        while (rs.next()) {
                            attributes.add(rs.getLong(1));
                        }
                    }
                    catch (SQLException ex) {
                        throw new IllegalStateException("Unable to compute distinct outgoing derived properties based on the summary table; " + sqlQuery + "; " + ex);
                    }
                }
                break;
            case ATTRIBUTE_BASED_CANDIDATE_FACT_SET_SELECTION:
            case SAMPLE_BASED_CANDIDATE_FACT_SET_SELECTION:
                throw new UnsupportedOperationException("getAllAttributesUsingSummary cannot be used for finding the attributes with "
                                                        + candidateFactSetSelectionMethod + ". Use getAllAttributes method instead.");
            default:
                throw new IllegalStateException("No value corresponding to " + candidateFactSetSelectionMethod);
        }

        return attributes;
    }
}
