//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import com.google.common.base.Optional;
import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfile;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObjectFactory;
import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class LanguageAttributeDerivation extends AttributeDerivation {
    private static final Logger LOGGER = Logger.getLogger(LanguageAttributeDerivation.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    private List<LanguageProfile> languageProfiles;
    private TextObjectFactory textObjectFactory;
    private LanguageDetector languageDetector;

    public LanguageAttributeDerivation(long originalAttributeEncoding,
                                       DatabaseHandler handler) {
        this.originalAttributeEncoding = originalAttributeEncoding;
        this.handler = handler;
    }

    @Override
    public long derive() {
        Logger.getRootLogger().setLevel(Level.OFF);
        String sqlQuery = null, derivedAttributeEncoding;
        String derived_name = GlobalSettings.DERIVED_ATTRIBUTE_PREFIX + "LanguageOf" + originalAttributeEncoding + ">";
        try {
            sqlQuery = "SELECT key from dictionary where value = '" + derived_name + "'";
            derivedAttributeEncoding = handler.getSQLSelectSingleResult(sqlQuery);
            if (!derivedAttributeEncoding.equals("-1")) {
                return Long.parseLong(derivedAttributeEncoding);
            }

            languageProfiles = new LanguageProfileReader().readAllBuiltIn();
            languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
                    .withProfiles(languageProfiles).minimalConfidence(0.8)
                    .build();
            //create a text object factory
            textObjectFactory = CommonTextObjectFactories.forDetectingShortCleanText();

            String object_value;
            HashMap<String, String> subjectsObjects = getSubjectObject();
            HashMap<String, Long> subjectsWithLanguageEncoding = new HashMap<>();
            HashMap<String, Long> cache = new HashMap<>();

            for (Map.Entry<String, String> entry: subjectsObjects.entrySet()) {
                String subj = entry.getKey();
                object_value = entry.getValue();

                if (object_value.length() > GlobalSettings.LENGTH_FOR_LANGUAGE_DETECTION) {
                    //object_value = object_value.replaceAll("`-=\\[];',\\./~!@#\\$%\\^&\\*\\(\\)_\\+\\{}\\|:<>\\?", " ");
                    //object_value = object_value.replaceAll("[^a-zA-Z0-9]", " ");
                    //object_value = object_value.trim().replaceAll(" +", " ");
                    object_value = object_value.replaceAll("[^\\d\\p{L}\\s]", " ");

                    Optional<LdLocale> lang;
                    lang = languageDetector.detect(textObjectFactory.forText(object_value));
                    long enc;

                    if (lang.isPresent()) {
                        String language = getLanguageFullName(lang.get().getLanguage());

                        if (language != null) {
                            if (cache.containsKey(language)) {
                                enc = cache.get(language);
                            }
                            else {
                                enc = getDictionaryValue(language);
                                cache.put(language, enc);
                            }
                            subjectsWithLanguageEncoding.put(subj, enc);
                        }
                    }
                }
            }

            if (!subjectsWithLanguageEncoding.isEmpty()) {
                long encoding = makeDerivedAttribute(subjectsWithLanguageEncoding, derived_name);
                LOGGER.info("Derived attribute " + derived_name + " with encoding " + encoding);
                return encoding;
            }

        }
        catch (SQLException ex) {
            throw new IllegalStateException("Unable to create the derived attribute: " + sqlQuery + "; " + ex);
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }

        return -1;
    }

    private HashMap<String, String> getSubjectObject() {
        String sqlQuery = "select s, cleanvalue as o from t_" + originalAttributeEncoding + " join dictionary on o=key";
        HashMap<String, String> res = new HashMap<>();
        try {
            res = handler.getResultSelectFromSubjectAndObject(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }
        return res;
    }

    /**
     * @param str String that I want to put into dictionary
     *
     * @return encoding of string in dictionary
     */
    private long getDictionaryValue(String str) {
        String sqlQuery = null;
        String encoding = null;

        try {
            sqlQuery = "SELECT key from dictionary where value = '\"" + str + "\"'";
            encoding = handler.getSQLSelectSingleResult(sqlQuery);

            if (encoding.equals("-1")) { // if the attribute is not in the dictionary add it
                sqlQuery = "INSERT INTO dictionary(value,cleanvalue) VALUES ('\"" + str + "\"','" + str + "')";
                handler.executeUpdate(sqlQuery);
            }
            // get the encoding of the derived attribute
            sqlQuery = "SELECT key from dictionary where value = '\"" + str + "\"'";
            encoding = handler.getSQLSelectSingleResult(sqlQuery);

        }
        catch (SQLException ex) {
            throw new IllegalStateException("couldn't insert object into dictionary " + sqlQuery + "; " + ex);
        }
        return Long.parseLong(encoding);
    }

    /**
     * @param map                  a map of String->integer containing subjects with the encoding of the objects
     * @param derivedAttributeName name of the attribute we want to create, starts with prefix
     *
     * @return encoding of derived attribute
     */
    private long makeDerivedAttribute(HashMap<String, Long> map,
                                      String derivedAttributeName) {
        String sqlQuery = null, derivedAttributeEncoding = null;
        try {
            sqlQuery = "INSERT INTO dictionary(value,cleanvalue) VALUES ('" + derivedAttributeName + "','" + derivedAttributeName + "')";
            handler.executeUpdate(sqlQuery);

            // get the encoding of the derived attribute
            sqlQuery = "SELECT key from dictionary where value = '" + derivedAttributeName + "'";
            derivedAttributeEncoding = handler.getSQLSelectSingleResult(sqlQuery);

            // create a new table in the database to contain the pairs subject, object of the newly derived attribute
            sqlQuery = "CREATE TABLE IF NOT EXISTS t_" + derivedAttributeEncoding + "(s INTEGER NOT NULL,"
                       + " o INTEGER NOT NULL) WITH (OIDS=FALSE);"
                       + " CREATE UNIQUE INDEX t_" + derivedAttributeEncoding + "_i_so"
                       + " ON t_" + derivedAttributeEncoding + " USING btree(s, o)"
                       + " TABLESPACE pg_default;"
                       + " CREATE UNIQUE INDEX t_" + derivedAttributeEncoding + "_i_os"
                       + " ON t_" + derivedAttributeEncoding + " USING btree(o, s)"
                       + " TABLESPACE pg_default;";

            handler.executeUpdate(sqlQuery);
            sqlQuery = getSQLBatchInsertIntoDerivedAttributeTable(derivedAttributeEncoding, map);
            handler.executeUpdate(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Couldn't create table for derived attribute "
                                            + derivedAttributeName + " (enc: " + derivedAttributeEncoding + " ), sqlQuery: " + sqlQuery + "; " + ex);
        }
        return Long.parseLong(derivedAttributeEncoding);
    }

    private String getSQLBatchInsertIntoDerivedAttributeTable(
            String derivedAttribute, HashMap<String, Long> results) {
        StringBuilder stringBuilder = new StringBuilder();
        String prefix = "";
        stringBuilder.append("INSERT INTO t_").append(derivedAttribute).append("(s, o) VALUES ");
        for (Map.Entry<String, Long> entry: results.entrySet()) {
            stringBuilder.append(prefix);
            prefix = ", ";
            stringBuilder.append("(").append(entry.getKey()).append(", ").append(entry.getValue()).append(")");
        }
        return stringBuilder.toString();
    }

    private String getLanguageFullName(String shortName) {
        switch (shortName) {
            case "af":
                return "Afrikaans";
            case "an":
                return "Aragonese";
            case "ar":
                return "Arabic";
            case "ast":
                return "Asturian";
            case "be":
                return "Belarusian";
            case "br":
                return "Breton";
            case "ca":
                return "Catalan";
            case "bg":
                return "Bulgarian";
            case "bn":
                return "Bengali";
            case "cs":
                return "Czech";
            case "cy":
                return "Welsh";
            case "da":
                return "Danish";
            case "de":
                return "German";
            case "el":
                return "Greek";
            case "en":
                return "English";
            case "es":
                return "Spanish";
            case "et":
                return "Estonian";
            case "eu":
                return "Basque";
            case "fa":
                return "Persian";
            case "fi":
                return "Finnish";
            case "fr":
                return "French";
            case "ga":
                return "Irish";
            case "gl":
                return "Galician";
            case "gu":
                return "Gujarati";
            case "he":
                return "Hebrew";
            case "hi":
                return "Hindi";
            case "hr":
                return "Croatian";
            case "ht":
                return "Haitian";
            case "hu":
                return "Hungarian";
            case "id":
                return "Indonesian";
            case "is":
                return "Icelandic";
            case "it":
                return "Italian";
            case "ja":
                return "Japanese";
            case "km":
                return "Khmer";
            case "kn":
                return "Kannada";
            case "ko":
                return "Korean";
            case "lt":
                return "Lithuanian";
            case "lv":
                return "Latvian";
            case "mk":
                return "Macedonian";
            case "ml":
                return "Malayalam";
            case "mr":
                return "Marathi";
            case "ms":
                return "Malay";
            case "mt":
                return "Maltese";
            case "ne":
                return "Nepali";
            case "nl":
                return "Dutch";
            case "no":
                return "Norwegian";
            case "oc":
                return "Occitan";
            case "pa":
                return "Punjabi";
            case "pl":
                return "Polish";
            case "pt":
                return "Portuguese";
            case "ro":
                return "Romanian";
            case "ru":
                return "Russian";
            case "sk":
                return "Slovak";
            case "sl":
                return "Slovene";
            case "so":
                return "Somali";
            case "sq":
                return "Albanian";
            case "sr":
                return "Serbian";
            case "sv":
                return "Swedish";
            case "sw":
                return "Swahili";
            case "ta":
                return "Tamil";
            case "te":
                return "Telugu";
            case "th":
                return "Thai";
            case "tl":
                return "Tagalog";
            case "tr":
                return "Turkish";
            case "uk":
                return "Ukrainian";
            case "ur":
                return "Urdu";
            case "vi":
                return "Vietnamese";
            case "wa":
                return "Walloon";
            case "yi":
                return "Yiddish";
            case "zh-cn":
                return "Simplified Chinese";
            case "zh-tw":
                return "Traditional Chinese";
            default:
                return null;
        }
    }
}
