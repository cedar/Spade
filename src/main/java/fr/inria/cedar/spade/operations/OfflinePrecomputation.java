//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.DataProfiler.items.Item;
import fr.inria.cedar.DataProfiler.profiles.AnalysisResult;
import fr.inria.cedar.DataProfiler.profiles.Histogram;
import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.SuggestedDerivation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Properties;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class OfflinePrecomputation {
    private static final Logger LOGGER = Logger.getLogger(OfflinePrecomputation.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected DatabaseHandler handler;
    protected LinkedList<AnalyzedAttribute> allAttributes;
    protected int numberOfDerivedAttributes;

    public OfflinePrecomputation(DatabaseHandler handler) {
        this.handler = handler;
        this.allAttributes = new LinkedList<>();
        this.numberOfDerivedAttributes = 0;
    }

    protected void createAllAttributesMetaDataTable() {
        String sqlQuery = "create table if not exists allattributes(attributeencoding integer not null, derivationlevel integer not null, derivedfromencoding integer, derivedfromrootencoding integer, derivationtype text, pathlength integer, isnumerical boolean not null, hasonlyliteralstrvalues boolean not null, minvalue double precision not null, maxvalue double precision not null, meanvallength real not null, primary key (attributeencoding))";

        try {
            handler.executeUpdate(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }
    }

    protected Properties prepareProperties() {
        Properties properties = new Properties();
        properties.put("statistics.median", "false");
        properties.put("statistics.variance", "false");
        properties.put("statistics.variance_on_values_normalized_by_feature_scaling", "false");
        properties.put("statistics.variance_on_values_normalized_by_sum", "false");
        properties.put("statistics.variance_on_values_normalized_by_mean", "false");
        properties.put("statistics.skewness", "false");
        properties.put("statistics.kurtosis", "false");
        properties.put("statistics.entropy", "false");
        properties.put("statistics.normalized_entropy", "false");
        properties.put("statistics.number_of_distinct_types", "true");
        properties.put("statistics.distinct_types", "true");
        properties.put("string.long_string_threshold", "50");
        properties.put("statistics.number_of_items_of_type_among_all_values_by_type_sets", "true");
        properties.put("statistics.number_of_items_of_type_among_all_values_by_most_specific_type", "true");
        properties.put("statistics.percentage_of_types_among_all_values_by_type_sets", "true");
        properties.put("statistics.percentage_of_types_among_all_values_by_most_specific_type", "true");
        properties.put("statistics.type_by_majority_voting_by_type_set", "true");
        properties.put("statistics.type_by_majority_voting_by_most_specific_type", "true");
        properties.put("statistics.equi_width_histogram_result", "false");
        properties.put("statistics.equi_depth_histogram_result", "false");

        return properties;
    }

    protected void addObjectValuesToHistogram(Histogram h,
                                              AnalyzedAttribute attribute) {
        String sqlQuery;

        if (attribute.getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
            sqlQuery = "select o as value from t_" + attribute.getEncoding();
        }
        else {
            sqlQuery = "select value from t_" + attribute.getEncoding() + " join dictionary on o = key";
        }

        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                h.addValue(rs.getString(1));
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }
    }

    protected void computeNumberOfSubjects(AnalyzedAttribute attribute) {
        long attributeEncoding = attribute.getEncoding();

        // find the number of subjects that have a value of the attribute
        String sqlQuery = "select count(*) from t_" + attributeEncoding;
        Integer numberOfSubjects = null;
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                numberOfSubjects = rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL Query: " + sqlQuery + "; " + ex);
        }

        attribute.setNumberOfSubjects(numberOfSubjects);
    }

    protected void computeNumberOfDistinctSubjects(AnalyzedAttribute attribute) {
        long attributeEncoding = attribute.getEncoding();

        // find the number of distinct subjects that have a value of the attribute
        String sqlQuery = "select count(1) from (select distinct s from t_" + attributeEncoding + ") as q";
        Integer numberOfDistinctSubjects = null;
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                numberOfDistinctSubjects = rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL Query: " + sqlQuery + "; " + ex);
        }

        attribute.setNumberOfDistinctSubjects(numberOfDistinctSubjects);
    }

    protected void computeNumberOfDistinctObjects(AnalyzedAttribute attribute) {
        long attributeEncoding = attribute.getEncoding();

        // find the number of distinct objects that have a value of the attribute
        String sqlQuery = "select count(1) from (select distinct o from t_" + attributeEncoding + ") as q";
        Integer numberOfDistinctObjects = null;
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                numberOfDistinctObjects = rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL Query: " + sqlQuery + "; " + ex);
        }

        attribute.setNumberOfDistinctObjects(numberOfDistinctObjects);
    }

    protected double computeAlmostOneRatio(AnalyzedAttribute attribute,
                                           int numberOfDistinctSubjects) {
        long attributeEncoding = attribute.getEncoding();

        // find the number of distinct subjects where the attribute has more than one value
        String sqlQuery = "select count(*) from (select s from t_" + attributeEncoding + " group by s having count(*) > 1) as q";
        int numberOfResourcesHavingMoreThanOneValueOfAttribute = 0;
        Double ratio = null;
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                numberOfResourcesHavingMoreThanOneValueOfAttribute = rs.getInt(1);
            }
            ratio = (double) numberOfResourcesHavingMoreThanOneValueOfAttribute / numberOfDistinctSubjects;
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }

        return ratio;
    }

    // computes statistics for an attribute that were not computed by the profiler
    protected void computeRemainingStatistics(AnalyzedAttribute attribute) {
        if (attribute.getNumberOfSubjects() == null) {
            computeNumberOfSubjects(attribute);
        }

        if (attribute.getNumberOfDistinctSubjects() == null) {
            computeNumberOfDistinctSubjects(attribute);
        }

        if (attribute.getNumberOfDistinctObjects() == null) {
            computeNumberOfDistinctObjects(attribute);
        }

        if (attribute.getAlmostOneRatio() == null) {
            attribute.setAlmostOneRatio(computeAlmostOneRatio(attribute, attribute.getNumberOfDistinctSubjects()));
        }
    }

    // the aggregation functions set here correspond to all the values that
    // must be pre-aggregated for any possible online scenario
    protected void setAggregationFunctions(AnalyzedAttribute attribute) {
        boolean isAtMostOne = attribute.isAtMostOne();
        boolean isNumerical = attribute.isNumerical();

        if (!isAtMostOne) {
            attribute.addAggregationFunction(AggregationFunction.COUNT);
        }

        if (isNumerical) {
            attribute.addAggregationFunction(AggregationFunction.SUM);
            attribute.addAggregationFunction(AggregationFunction.MIN);
            attribute.addAggregationFunction(AggregationFunction.MAX);
        }

        // if the attribute has multi-valued subjects, and is numerical
        if (!isAtMostOne && isNumerical) {
            attribute.addAggregationFunction(AggregationFunction.RAW_VALUES_LIST);
        }
    }

    protected void analyzeAttribute(AnalyzedAttribute attribute) {
        Histogram histogram = new Histogram();
        Properties properties = prepareProperties();

        histogram.init(properties);
        addObjectValuesToHistogram(histogram, attribute);
        AnalysisResult histogramResult = histogram.close();

        attribute.setMinValue(histogramResult.getMin());
        attribute.setMaxValue(histogramResult.getMax());

        boolean hasOnlyNumericalTypes = true;
        boolean hasIRIORBlankNodeType = false;
        for (Item.Type type: histogramResult.getPercentageOfTypeAmongAllValuesByMostSpecificType().keySet()) {
            switch (type) {
                case IRI:
                case BLANK_NODE:
                    hasIRIORBlankNodeType = true;
                    // fall-thru
                case DATE:
                case TIME:
                case DATE_TIME:
                case NULL:
                case LONG_STRING:
                case STRING:
                    hasOnlyNumericalTypes = false;
                    break;
            }
        }
        attribute.setIsNumerical(hasOnlyNumericalTypes);
        attribute.setHasOnlyLiteralStringValues(!hasOnlyNumericalTypes && !hasIRIORBlankNodeType);

        if (histogramResult.getAverageLengthOfAnyString() != null) {
            attribute.setMeanValueLength(histogramResult.getAverageLengthOfAnyString());
        }

        computeRemainingStatistics(attribute);
    }

    protected void addAttributeToAllAttributesTable(AnalyzedAttribute attribute) {
        StringBuilder sqlQuery = null;
        try {
            sqlQuery = new StringBuilder("insert into allattributes values (");

            sqlQuery.append(attribute.getEncoding()).append(",");
            int derivationLevel = attribute.getDerivationLevel();
            sqlQuery.append(derivationLevel).append(",");
            if (derivationLevel > 0) {
                sqlQuery.append(attribute.getDerivedFrom().getEncoding()).append(",");
                sqlQuery.append(attribute.getDerivedFromRoot().getEncoding()).append(",'");
                sqlQuery.append(attribute.getDerivationType().toString()).append("',");
                numberOfDerivedAttributes++;
            }
            else {
                sqlQuery.append("null").append(",");
                sqlQuery.append("null").append(",");
                sqlQuery.append("null").append(",");
            }
            sqlQuery.append(attribute.getPathLength()).append(",");

            sqlQuery.append(attribute.isNumerical()).append(",");
            sqlQuery.append(attribute.hasOnlyLiteralStringValues()).append(",");

            Double minValue = attribute.getMinValue();
            if (minValue.equals(Double.POSITIVE_INFINITY)) {
                sqlQuery.append("'infinity',");
            }
            else if (minValue.equals(Double.NEGATIVE_INFINITY)) {
                sqlQuery.append("'-infinity',");
            }
            else {
                sqlQuery.append(minValue).append(",");
            }
            Double maxValue = attribute.getMaxValue();
            if (maxValue.equals(Double.NEGATIVE_INFINITY)) {
                sqlQuery.append("'-infinity',");
            }
            else if (maxValue.equals(Double.POSITIVE_INFINITY)) {
                sqlQuery.append("'infinity',");
            }
            else {
                sqlQuery.append(maxValue).append(",");
            }

            sqlQuery.append(attribute.getMeanValueLength()).append(")");

            handler.executeUpdate(sqlQuery.toString());
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }
    }

    protected static void setSuggestedDerivations(AnalyzedAttribute attribute) {
        if (GlobalSettings.ENABLE_COUNT_DERIVATION
            && !attribute.isAtMostOne()) {
            attribute.addSuggestedDerivation(SuggestedDerivation.COUNT_ATTR);
        }

        if (GlobalSettings.ENABLE_PATH_DERIVATION) {
            attribute.addSuggestedDerivation(SuggestedDerivation.PATH);
        }

        if (GlobalSettings.ENABLE_KWD_EXTRACTION
            && attribute.getMeanValueLength() > GlobalSettings.LENGTH_FOR_KWD_DETECTION
            && attribute.hasOnlyLiteralStringValues()) {
            attribute.addSuggestedDerivation(SuggestedDerivation.KWD_IN_TEXT);
        }

        if (GlobalSettings.ENABLE_LANGUAGE_DETECTION
            && attribute.getMeanValueLength() > GlobalSettings.LENGTH_FOR_LANGUAGE_DETECTION
            && attribute.hasOnlyLiteralStringValues()) {
            attribute.addSuggestedDerivation(SuggestedDerivation.LANG_DET);
        }
    }

    public LinkedList<Long> createDerivedAttributes(
            long originalAttributeEncoding,
            SuggestedDerivation derivation) {
        long derived;
        LinkedList<Long> derivedAttributesEncodings = new LinkedList<>();
        switch (derivation) {
            case COUNT_ATTR:
                CountAttributeDerivation countAttr = new CountAttributeDerivation(originalAttributeEncoding, handler);
                derived = countAttr.derive();
                if (derived != -1) {
                    derivedAttributesEncodings.add(derived);
                }
                break;
            case PATH:
                PathAttributeDerivation pathThroughAttr = new PathAttributeDerivation(originalAttributeEncoding, handler);
                derivedAttributesEncodings.addAll(pathThroughAttr.derivationWithSummary(GlobalSettings.PATH_DERIVATION_LENGTH));
                break;
            case LANG_DET:
                LanguageAttributeDerivation langOfAttr = new LanguageAttributeDerivation(originalAttributeEncoding, handler);
                derived = langOfAttr.derive();
                if (derived != -1) {
                    derivedAttributesEncodings.add(derived);
                }
                break;
            case KWD_IN_TEXT:
                KeywordAttributeDerivation kwsInAttr = new KeywordAttributeDerivation(originalAttributeEncoding, handler);
                derived = kwsInAttr.derive();
                if (derived != -1) {
                    derivedAttributesEncodings.add(derived);
                }
                break;
        }

        return derivedAttributesEncodings;
    }

    protected void setKnownStatistics(AnalyzedAttribute attribute,
                                      SuggestedDerivation suggestedDerivation,
                                      AnalyzedAttribute derivedAttribute) {
        derivedAttribute.setDerivationLevel(attribute.getDerivationLevel() + 1);
        derivedAttribute.setDerivedFrom(attribute);
        AnalyzedAttribute root = attribute.getDerivedFromRoot();
        derivedAttribute.setDerivedFromRoot(root == null ? attribute : root);
        derivedAttribute.setDerivationType(suggestedDerivation);

        switch (suggestedDerivation) {
            case COUNT_ATTR:
                derivedAttribute.setIsNumerical(true);
                derivedAttribute.setAlmostOneRatio(0D);
                derivedAttribute.setNumberOfDistinctSubjects(attribute.getNumberOfDistinctSubjects());
                derivedAttribute.setNumberOfSubjects(attribute.getNumberOfDistinctSubjects());
                break;
            case PATH:
                derivedAttribute.setPathLength(GlobalSettings.PATH_DERIVATION_LENGTH);
                break;
            case LANG_DET:
            case KWD_IN_TEXT:
                derivedAttribute.setIsNumerical(false);
                derivedAttribute.setHasOnlyLiteralStringValues(true);
                break;
        }
    }

    protected LinkedList<AnalyzedAttribute> deriveNewAttributes(
            AnalyzedAttribute attribute) {
        setSuggestedDerivations(attribute);

        LinkedList<Long> newDerivedAttributes;
        AnalyzedAttribute newDerivedAnalyzedAttribute;
        LinkedList<AnalyzedAttribute> newDerivedAnalyzedAttributes = new LinkedList<>();
        for (SuggestedDerivation suggestedDerivation: attribute.getSuggestedDerivations()) {
            newDerivedAttributes = createDerivedAttributes(attribute.getEncoding(), suggestedDerivation);

            for (Long newDerivedAttributeEncoding: newDerivedAttributes) {
                newDerivedAnalyzedAttribute = new AnalyzedAttribute(newDerivedAttributeEncoding);

                setKnownStatistics(attribute, suggestedDerivation, newDerivedAnalyzedAttribute);

                newDerivedAnalyzedAttributes.add(newDerivedAnalyzedAttribute);
            }
        }

        return newDerivedAnalyzedAttributes;
    }

    public void deriveNewAttributes(
            LinkedList<AnalyzedAttribute> attributesToBeAnalyzedAndDerivedFrom) {
        AnalyzedAttribute attribute;
        while ((attribute = attributesToBeAnalyzedAndDerivedFrom.pollFirst()) != null) { // while there are attributes to be analyzed, get the first one and remove it from the queue
            analyzeAttribute(attribute);

            allAttributes.add(attribute);
            addAttributeToAllAttributesTable(attribute);

            if (attribute.getDerivationLevel() < GlobalSettings.MAX_DERIVATION_DEPTH) {
                attributesToBeAnalyzedAndDerivedFrom.addAll(deriveNewAttributes(attribute));
            }
        }
    }

    public int deriveNewAttributesFromInputGraph() {
        DirectOutgoingAttributeEnumeration directOutgoingAttributeEnumeration = new DirectOutgoingAttributeEnumeration(handler);
        LinkedList<Long> originalProperties = directOutgoingAttributeEnumeration.getOriginalPropertiesUsingSummary();
        AnalyzedAttribute attribute;
        LinkedList<AnalyzedAttribute> attributesToBeAnalyzedAndDerivedFrom = new LinkedList<>();

        createAllAttributesMetaDataTable();

        // initialize the queue of attributes to be analyzed with original properties at level 0 (similar to Breadth First Search)
        for (Long originalPropertyEncoding: originalProperties) {
            attribute = new AnalyzedAttribute(originalPropertyEncoding);
            attribute.setDerivationLevel(0);
            attributesToBeAnalyzedAndDerivedFrom.add(attribute);
        }

        deriveNewAttributes(attributesToBeAnalyzedAndDerivedFrom);

        for (AnalyzedAttribute analyzedAttribute: allAttributes) {
            setAggregationFunctions(analyzedAttribute);
        }

        return numberOfDerivedAttributes;
    }

    public LinkedList<AnalyzedAttribute> getAllAttributes() {
        return allAttributes;
    }

    /*
	 * Constructs one single query to get all the aggregation functions of a given attribute.
     * Note that AVERAGE is not retrieved from the database as it is computed as SUM/COUNT.
     */
    protected String constructQueryToMaterializePreaggregatedTable(
            AnalyzedAttribute attribute) {
        StringBuilder sqlQuery = new StringBuilder("create table if not exists ");
        sqlQuery.append("t_").append(attribute.getEncoding()).append("_preaggregated as select m.s as s");

        boolean isCountDerivedAttribute = false;
        if (attribute.getDerivedFrom() != null && attribute.getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
            isCountDerivedAttribute = true;
        }

        if (isCountDerivedAttribute) {
            for (AggregationFunction af: attribute.getAggregationFunctions()) {
                switch (af) {
                    case COUNT:
                        sqlQuery.append(", COUNT(m.o) as COUNT_o");
                        break;
                    case SUM:
                        sqlQuery.append(", SUM(cast (m.o as double precision)) as SUM_o");
                        break;
                    case MIN:
                        sqlQuery.append(", MIN(cast (m.o as double precision)) as MIN_o");
                        break;
                    case MAX:
                        sqlQuery.append(", MAX(cast (m.o as double precision)) as MAX_o");
                        break;
                    case RAW_VALUES_LIST:
                        sqlQuery.append(", string_agg(cast (m.o as text), ',') as raw_values_list");
                        break;
                    default:
                        break;
                }
            }
        }
        else {
            for (AggregationFunction af: attribute.getAggregationFunctions()) {
                switch (af) {
                    case COUNT:
                        sqlQuery.append(", COUNT(d.cleanvalue) as COUNT_o");
                        break;
                    case SUM:
                        sqlQuery.append(", SUM(cast (d.cleanvalue as double precision)) as SUM_o");
                        break;
                    case MIN:
                        sqlQuery.append(", MIN(cast (d.cleanvalue as double precision)) as MIN_o");
                        break;
                    case MAX:
                        sqlQuery.append(", MAX(cast (d.cleanvalue as double precision)) as MAX_o");
                        break;
                    case RAW_VALUES_LIST:
                        sqlQuery.append(", string_agg(d.cleanvalue, ',') as raw_values_list");
                        break;
                    default:
                        break;
                }
            }
        }

        if (isCountDerivedAttribute) {
            sqlQuery.append(" from t_").append(attribute.getEncoding()).append(" as m group by m.s; ");
        }
        else {
            sqlQuery.append(" from (t_").append(attribute.getEncoding()).append(" as m join dictionary d on m.o = d.key) group by m.s; ");
        }
        sqlQuery.append("create unique index if not exists ").append("t_").append(attribute.getEncoding()).append("_preaggregated_i_s on t_").append(attribute.getEncoding()).append("_preaggregated using btree(s) tablespace pg_default; ");
        sqlQuery.append("cluster t_").append(attribute.getEncoding()).append("_preaggregated using ").append("t_").append(attribute.getEncoding()).append("_preaggregated_i_s; ");

        return sqlQuery.toString();
    }

    public void preaggregateAttributes(LinkedList<AnalyzedAttribute> attributes) {
        for (AnalyzedAttribute attribute: attributes) {
            if (!attribute.getAggregationFunctions().isEmpty()) {
                // create pre-aggregated table if it does not already exist
                String sqlQuery = constructQueryToMaterializePreaggregatedTable(attribute);
                try (PreparedStatement stmt = handler.getPreparedStatementWithoutClosingLastOne(sqlQuery)) {
                    stmt.executeUpdate();
                    if (!GlobalSettings.DATABASE_AUTOCOMMIT) {
                        handler.commit();
                    }
                }
                catch (SQLException ex) {
                    throw new IllegalStateException(ex + " " + sqlQuery);
                }
            }
        }
    }

    public void runAnalyze() {
        String sqlQuery = "ANALYZE VERBOSE";
        try {
            handler.executeUpdate(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex + " " + sqlQuery);
        }
    }
}
