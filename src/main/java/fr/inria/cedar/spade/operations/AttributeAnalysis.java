//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.DataProfiler.items.Item;
import fr.inria.cedar.DataProfiler.profiles.AnalysisResult;
import fr.inria.cedar.DataProfiler.profiles.Histogram;
import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AttributeAnalysis extends Operation {
    protected static final Logger LOGGER = Logger.getLogger(AttributeAnalysis.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public AttributeAnalysis(DatabaseHandler handler) {
        this.handler = handler;

        String sqlQuery = "create table if not exists attributeprofiles(cfsencoding integer not null, attributeencoding integer not null, isnumerical boolean, hasonlyliteralstrvalues boolean, minvalue double precision, maxvalue double precision, meanvallength real, nrdistsub integer, nrdistobj integer, nrsub integer, nrdistobjtosubratio real, support real, almostoneratio real, primary key (cfsencoding, attributeencoding))";

        try {
            handler.executeUpdate(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }
    }

    protected AnalyzedAttribute loadAttributeFromAllAttributesTable(
            long attributeEncoding,
            boolean recomputeStatisticsWithinCandidateFactSet) {
        StringBuilder sqlQuery = new StringBuilder("select ");
        if (!recomputeStatisticsWithinCandidateFactSet) {
            sqlQuery.append("*");
        }
        else {
            sqlQuery.append("attributeencoding, derivationlevel, derivedfromencoding, derivedfromrootencoding, derivationtype, pathlength");
        }
        sqlQuery.append(" from allattributes where attributeencoding = ")
                .append(attributeEncoding);
        AnalyzedAttribute attribute = new AnalyzedAttribute(attributeEncoding);
        int derivationLevel;
        try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
            while (rs.next()) {
                derivationLevel = rs.getInt(2);
                attribute.setDerivationLevel(derivationLevel);
                if (derivationLevel > 0) {
                    attribute.setDerivedFrom(new AnalyzedAttribute(rs.getInt(3)));
                    attribute.setDerivedFromRoot(new AnalyzedAttribute(rs.getInt(4)));
                    attribute.setDerivationType(SuggestedDerivation.convert(rs.getString(5)));
                    attribute.setPathLength(rs.getInt(6));
                }

                if (!recomputeStatisticsWithinCandidateFactSet) {
                    attribute.setIsNumerical(rs.getBoolean(7));
                    attribute.setHasOnlyLiteralStringValues(rs.getBoolean(8));
                    attribute.setMinValue(rs.getDouble(9));
                    attribute.setMaxValue(rs.getDouble(10));
                    attribute.setMeanValueLength(rs.getDouble(11));
                }
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }

        return attribute;
    }

    protected boolean loadAttributeProfile(CandidateFactSet candidateFactSet,
                                           AnalyzedAttribute attribute,
                                           boolean recomputeStatisticsWithinCandidateFactSet) {
        // if the profile of the attribute is already in the database load it
        boolean found = false;

        StringBuilder sqlQuery = new StringBuilder("select ");
        if (recomputeStatisticsWithinCandidateFactSet) {
            sqlQuery.append("*");
        }
        else {
            sqlQuery.append("nrdistsub, nrdistobj, nrsub, nrdistobjtosubratio, support, almostoneratio");
        }
        sqlQuery.append(" from attributeprofiles where cfsencoding = ")
                .append(candidateFactSet.getEncoding()).append(" and attributeencoding =").append(attribute.getEncoding());

        try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
            while (rs.next()) {
                if (recomputeStatisticsWithinCandidateFactSet) {
                    attribute.setIsNumerical(rs.getBoolean(3));
                    attribute.setHasOnlyLiteralStringValues(rs.getBoolean(4));
                    attribute.setMinValue(rs.getDouble(5));
                    attribute.setMaxValue(rs.getDouble(6));
                    attribute.setMeanValueLength(rs.getDouble(7));

                    attribute.setNumberOfDistinctSubjects(rs.getInt(8));
                    attribute.setNumberOfDistinctObjects(rs.getInt(9));
                    attribute.setNumberOfSubjects(rs.getInt(10));
                    attribute.setDistinctObjectsToSubjectsRatio(rs.getDouble(11));
                    attribute.setSupport(rs.getDouble(12));
                    attribute.setAlmostOneRatio(rs.getDouble(13));
                }
                else {
                    attribute.setNumberOfDistinctSubjects(rs.getInt(1));
                    attribute.setNumberOfDistinctObjects(rs.getInt(2));
                    attribute.setNumberOfSubjects(rs.getInt(3));
                    attribute.setDistinctObjectsToSubjectsRatio(rs.getDouble(4));
                    attribute.setSupport(rs.getDouble(5));
                    attribute.setAlmostOneRatio(rs.getDouble(6));
                }

                found = true;
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }

        return found;
    }

    protected Properties prepareProperties() {
        Properties properties = new Properties();
        properties.put("statistics.median", "false");
        properties.put("statistics.variance", "false");
        properties.put("statistics.variance_on_values_normalized_by_feature_scaling", "false");
        properties.put("statistics.variance_on_values_normalized_by_sum", "false");
        properties.put("statistics.variance_on_values_normalized_by_mean", "false");
        properties.put("statistics.skewness", "false");
        properties.put("statistics.kurtosis", "false");
        properties.put("statistics.entropy", "false");
        properties.put("statistics.normalized_entropy", "false");
        properties.put("statistics.number_of_distinct_types", "true");
        properties.put("statistics.distinct_types", "true");
        properties.put("string.long_string_threshold", "50");
        properties.put("statistics.number_of_items_of_type_among_all_values_by_type_sets", "true");
        properties.put("statistics.number_of_items_of_type_among_all_values_by_most_specific_type", "true");
        properties.put("statistics.percentage_of_types_among_all_values_by_type_sets", "true");
        properties.put("statistics.percentage_of_types_among_all_values_by_most_specific_type", "true");
        properties.put("statistics.type_by_majority_voting_by_type_set", "true");
        properties.put("statistics.type_by_majority_voting_by_most_specific_type", "true");
        properties.put("statistics.equi_width_histogram_result", "false");
        properties.put("statistics.equi_depth_histogram_result", "false");

        return properties;
    }

    protected void addObjectValuesToHistogram(CandidateFactSet candidateFactSet,
                                              Histogram h,
                                              AnalyzedAttribute attribute) {
        StringBuilder sqlQuery = new StringBuilder();

        if (attribute.getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
            sqlQuery.append("select o as value from t_").append(attribute.getEncoding()).append(" where s in ( select s from ").append(candidateFactSet.getTableName()).append(" )");
        }
        else {
            sqlQuery.append("select value from t_").append(attribute.getEncoding()).append(" join dictionary on o = key where s in ( select s from ").append(candidateFactSet.getTableName()).append(" )");
        }

        try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
            while (rs.next()) {
                h.addValue(rs.getString(1));
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }
    }

    /*protected void computeNumberOfSubjects(CandidateFactSet candidateFactSet,
                                           AnalyzedAttribute attribute) {
        long attributeEncoding = attribute.getEncoding();

        // find the number of subjects that have a value of the attribute
        String sqlQuery = "select count(*) from t_" + attributeEncoding + " where s in (select s from " + candidateFactSet.getTableName() + " )";
        Integer numberOfSubjects = null;
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                numberOfSubjects = rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL Query: " + sqlQuery + "; " + ex);
        }

        attribute.setNumberOfSubjects(numberOfSubjects);
    }

    protected void computeNumberOfDistinctSubjects(
            CandidateFactSet candidateFactSet, AnalyzedAttribute attribute) {
        long attributeEncoding = attribute.getEncoding();

        // find the number of distinct subjects that have a value of the attribute
        String sqlQuery = "select count(1) from (select distinct s from t_" + attributeEncoding + " where s in (select s from " + candidateFactSet.getTableName() + ")" + ") as q";
        Integer numberOfDistinctSubjects = null;
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                numberOfDistinctSubjects = rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL Query: " + sqlQuery + "; " + ex);
        }

        attribute.setNumberOfDistinctSubjects(numberOfDistinctSubjects);
    }

    protected void computeNumberOfDistinctObjects(
            CandidateFactSet candidateFactSet, AnalyzedAttribute attribute) {
        long attributeEncoding = attribute.getEncoding();

        // find the number of distinct objects that have a value of the attribute
        String sqlQuery = "select count(1) from (select distinct o from t_" + attributeEncoding + " where s in (select s from " + candidateFactSet.getTableName() + ")" + ") as q";
        Integer numberOfDistinctObjects = null;
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                numberOfDistinctObjects = rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL Query: " + sqlQuery + "; " + ex);
        }

        attribute.setNumberOfDistinctObjects(numberOfDistinctObjects);
    }*/

    protected void computeAllStatisticsInOneQuery(
            CandidateFactSet candidateFactSet, AnalyzedAttribute attribute) {
        long attributeEncoding = attribute.getEncoding();

        // find the number of distinct subjects, distinct objects, and the total number of subjects that have a value of the attribute within the candidate fact set
        StringBuilder sqlQuery = new StringBuilder("with attr_in_cfs as (select s, o from t_");
        sqlQuery.append(attributeEncoding)
                .append(" where s in (select s from ")
                .append(candidateFactSet.getTableName())
                .append(")) select count(distinct s) as cds, count(distinct o) as cdo, count(s) as cs from attr_in_cfs union all select count(*) as cds, -1 as cdo, -1 as cs from attr_in_cfs group by s having count(*) > 1");

        Integer numberOfDistinctSubjects = null;
        Integer numberOfDistinctObjects = null;
        Integer numberOfSubjects = null;
        double almostOneRatio = 0;
        try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
            if (rs.next()) {
                numberOfDistinctSubjects = rs.getInt(1);
                numberOfDistinctObjects = rs.getInt(2);
                numberOfSubjects = rs.getInt(3);
            }
            if (rs.next()) {
                int numberOfResourcesHavingMoreThanOneValueOfAttribute = rs.getInt(1);
                almostOneRatio = (double) numberOfResourcesHavingMoreThanOneValueOfAttribute / numberOfDistinctSubjects;
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL Query: " + sqlQuery + "; " + ex);
        }

        attribute.setNumberOfDistinctSubjects(numberOfDistinctSubjects);
        attribute.setNumberOfDistinctObjects(numberOfDistinctObjects);
        attribute.setNumberOfSubjects(numberOfSubjects);
        attribute.setAlmostOneRatio(almostOneRatio);
    }

    protected boolean computeAndCheckSupport(CandidateFactSet candidateFactSet,
                                             AnalyzedAttribute attribute) {
        double supportLevel = (double) attribute.getNumberOfDistinctSubjects() / candidateFactSet.getSize();
        attribute.setSupport(supportLevel);

        return supportLevel >= GlobalSettings.SUPPORT_THRESHOLD;
    }

    /*protected double computeAlmostOneRatio(CandidateFactSet candidateFactSet,
                                           AnalyzedAttribute attribute,
                                           int numberOfDistinctSubjects) {
        long attributeEncoding = attribute.getEncoding();

        // find the number of distinct subjects where the attribute has more than one value
        StringBuilder sqlQuery = new StringBuilder("select count(*) from ");
        sqlQuery.append(candidateFactSet.getTableName()).append(" where s in (select s from t_").append(attributeEncoding).append(" group by s having count(*) > 1)");
        int numberOfResourcesHavingMoreThanOneValueOfAttribute = 0;
        Double ratio = null;
        try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
            while (rs.next()) {
                numberOfResourcesHavingMoreThanOneValueOfAttribute = rs.getInt(1);
            }
            ratio = (double) numberOfResourcesHavingMoreThanOneValueOfAttribute / numberOfDistinctSubjects;
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }

        return ratio;
    }

    // computes statistics for an attribute that were not computed by the profiler
    protected void computeRemainingStatistics(CandidateFactSet candidateFactSet,
                                              AnalyzedAttribute attribute) {
        attribute.setDistinctObjectsToSubjectsRatio((double) attribute.getNumberOfDistinctObjects() / attribute.getNumberOfSubjects());

        attribute.setAlmostOneRatio(computeAlmostOneRatio(candidateFactSet, attribute, attribute.getNumberOfDistinctSubjects()));
    }*/

    protected void computeProfile(CandidateFactSet candidateFactSet,
                                  AnalyzedAttribute attribute) {
        Histogram histogram = new Histogram();
        Properties properties = prepareProperties();
        histogram.init(properties);

        // need to add values to histogram
        addObjectValuesToHistogram(candidateFactSet, histogram, attribute);
        AnalysisResult histogramResult = histogram.close();

        attribute.setMinValue(histogramResult.getMin());
        attribute.setMaxValue(histogramResult.getMax());

        // no need any more; computed before
        //attribute.setNumberOfDistinctObjects(Math.toIntExact(histogramResult.getNumberOfDistinctValues()));

        boolean hasOnlyNumericalTypes = true;
        boolean hasIRIORBlankNodeType = false;
        for (Item.Type type: histogramResult.getPercentageOfTypeAmongAllValuesByMostSpecificType().keySet()) {
            switch (type) {
                case IRI:
                case BLANK_NODE:
                    hasIRIORBlankNodeType = true;
                    // fall-thru
                case DATE:
                case TIME:
                case DATE_TIME:
                case NULL:
                case LONG_STRING:
                case STRING:
                    hasOnlyNumericalTypes = false;
                    break;
            }
        }
        attribute.setIsNumerical(hasOnlyNumericalTypes);
        attribute.setHasOnlyLiteralStringValues(!hasOnlyNumericalTypes && !hasIRIORBlankNodeType);

        if (histogramResult.getAverageLengthOfAnyString() != null) {
            attribute.setMeanValueLength(histogramResult.getAverageLengthOfAnyString());
        }
    }

    protected void storeAttributeProfile(CandidateFactSet candidateFactSet,
                                         AnalyzedAttribute attribute) {
        StringBuilder sqlQuery = null;
        try {
            sqlQuery = new StringBuilder("insert into attributeprofiles values (");
            sqlQuery.append(candidateFactSet.getEncoding()).append(",")
                    .append(attribute.getEncoding()).append(",")
                    .append(attribute.isNumerical()).append(",")
                    .append(attribute.hasOnlyLiteralStringValues()).append(",");

            Double minValue = attribute.getMinValue();
            if (minValue.equals(Double.POSITIVE_INFINITY)) {
                sqlQuery.append("'infinity',");
            }
            else if (minValue.equals(Double.NEGATIVE_INFINITY)) {
                sqlQuery.append("'-infinity',");
            }
            else {
                sqlQuery.append(minValue).append(",");
            }
            Double maxValue = attribute.getMaxValue();
            if (maxValue.equals(Double.NEGATIVE_INFINITY)) {
                sqlQuery.append("'-infinity',");
            }
            else if (maxValue.equals(Double.POSITIVE_INFINITY)) {
                sqlQuery.append("'infinity',");
            }
            else {
                sqlQuery.append(maxValue).append(",");
            }

            sqlQuery.append(attribute.getMeanValueLength()).append(",")
                    .append(attribute.getNumberOfDistinctSubjects()).append(",")
                    .append(attribute.getNumberOfDistinctObjects()).append(",")
                    .append(attribute.getNumberOfSubjects()).append(",")
                    .append(attribute.getDistinctObjectsToSubjectsRatio()).append(",")
                    .append(attribute.getSupport()).append(",")
                    .append(attribute.getAlmostOneRatio()).append(")");
            handler.executeUpdate(sqlQuery.toString());
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }
    }

    protected void setAggregationFunctions(
            AnalyzedAttribute attribute) {
        boolean isAtMostOne = attribute.isAtMostOne();
        boolean isAlmostOne = attribute.isAlmostOne();
        boolean isNumerical = attribute.isNumerical();

        // if the majority of subjects in the CFS have more than one value of the attribute, then we assign COUNT
        if (!isAlmostOne) {
            attribute.addAggregationFunction(AggregationFunction.COUNT);
        }

        if (isNumerical) {
            attribute.addAggregationFunction(AggregationFunction.SUM);
            attribute.addAggregationFunction(AggregationFunction.AVERAGE);
            attribute.addAggregationFunction(AggregationFunction.MIN);
            attribute.addAggregationFunction(AggregationFunction.MAX);
        }

        // if the attribute has multi-valued subjects, and is numerical
        if (!isAtMostOne && isNumerical) {
            attribute.addAggregationFunction(AggregationFunction.RAW_VALUES_LIST);
        }
    }

    protected boolean retrieveOrComputeMissingStatisticsAndStoreThemInAttributeProfilesTable(
            CandidateFactSet candidateFactSet,
            AnalyzedAttribute attribute,
            HashMap<Long, AnalyzedAttribute> attributesByEncoding,
            boolean recomputeStatisticsWithinCandidateFactSet) {
        boolean frequent;

        if (attribute.getDerivedFrom() != null) {
            attribute.setDerivedFrom(attributesByEncoding.get(attribute.getDerivedFrom().getEncoding()));
            attribute.setDerivedFromRoot(attributesByEncoding.get(attribute.getDerivedFromRoot().getEncoding()));
        }

        if (!loadAttributeProfile(candidateFactSet, attribute, recomputeStatisticsWithinCandidateFactSet)) {
            computeAllStatisticsInOneQuery(candidateFactSet, attribute);
            attribute.setDistinctObjectsToSubjectsRatio((double) attribute.getNumberOfDistinctObjects() / attribute.getNumberOfSubjects());
            frequent = computeAndCheckSupport(candidateFactSet, attribute);
            if (frequent) { // compute and store the profile of an attribute only if it's frequent
                if (recomputeStatisticsWithinCandidateFactSet) {
                    computeProfile(candidateFactSet, attribute);
                }
                //computeRemainingStatistics(candidateFactSet, attribute);
                storeAttributeProfile(candidateFactSet, attribute);
                setAggregationFunctions(attribute);
                OfflinePrecomputation.setSuggestedDerivations(attribute);
            }
        }
        else {
            setAggregationFunctions(attribute);
            OfflinePrecomputation.setSuggestedDerivations(attribute);
            frequent = true;
        }

        return frequent;
    }

    protected void removeComputedAttributes(
            LinkedList<AnalyzedAttribute> analyzedAttributes,
            HashMap<Long, LinkedList<SuggestedDerivation>> derivedAttributesToBeComputed) {
        Long encodingOfParent;
        LinkedList<SuggestedDerivation> suggestedDerivations;
        for (AnalyzedAttribute analyzedAttribute: analyzedAttributes) {
            if (analyzedAttribute.getDerivationLevel() > 0) {
                encodingOfParent = analyzedAttribute.getDerivedFrom().getEncoding();
                suggestedDerivations = derivedAttributesToBeComputed.get(encodingOfParent);
                if (suggestedDerivations != null) { // this can happen because of multiple paths derived from the same parent
                    suggestedDerivations.remove(analyzedAttribute.getDerivationType());
                    if (suggestedDerivations.isEmpty()) {
                        derivedAttributesToBeComputed.remove(encodingOfParent);
                    }
                }
            }
        }
    }

    protected LinkedList<AnalyzedAttribute> computeAndPreaggregateMissingDerivedAttributes(
            CandidateFactSet candidateFactSet,
            LinkedList<AnalyzedAttribute> analyzedAttributes,
            HashMap<Long, AnalyzedAttribute> attributesByEncoding,
            HashMap<Long, LinkedList<SuggestedDerivation>> derivedAttributesToBeComputed) {
        removeComputedAttributes(analyzedAttributes, derivedAttributesToBeComputed);
        LinkedList<AnalyzedAttribute> newAttributes = new LinkedList<>();

        if (!derivedAttributesToBeComputed.isEmpty()) {
            LinkedList<SuggestedDerivation> suggestedDerivations;
            OfflinePrecomputation offlinePrecomputation = new OfflinePrecomputation(handler);
            LinkedList<Long> newAttributeEncodings;
            AnalyzedAttribute newAttribute;
            AnalyzedAttribute parent;

            // intialize the queue
            Long attributeEncoding;
            for (Map.Entry<Long, LinkedList<SuggestedDerivation>> entry: derivedAttributesToBeComputed.entrySet()) {
                attributeEncoding = entry.getKey();
                parent = attributesByEncoding.get(attributeEncoding);
                suggestedDerivations = entry.getValue();
                for (SuggestedDerivation suggestedDerivation: suggestedDerivations) {
                    newAttributeEncodings = offlinePrecomputation.createDerivedAttributes(attributeEncoding, suggestedDerivation);
                    for (Long newAttributeEncoding: newAttributeEncodings) {
                        newAttribute = new AnalyzedAttribute(newAttributeEncoding);
                        offlinePrecomputation.setKnownStatistics(parent, suggestedDerivation, newAttribute);
                        newAttributes.add(newAttribute);
                    }
                }
            }

            offlinePrecomputation.deriveNewAttributes(newAttributes);
            newAttributes = offlinePrecomputation.getAllAttributes();

            for (AnalyzedAttribute attribute: newAttributes) {
                attributesByEncoding.put(attribute.getEncoding(), attribute);
            }

            AnalyzedAttribute attribute;
            for (Iterator<AnalyzedAttribute> iter = newAttributes.iterator(); iter.hasNext();) {
                attribute = iter.next();

                if (!retrieveOrComputeMissingStatisticsAndStoreThemInAttributeProfilesTable(candidateFactSet, attribute, attributesByEncoding, GlobalSettings.RECOMPUTE_STATISTICS_WITHIN_CANDIDATE_FACT_SET)) {
                    iter.remove();
                }
            }

            offlinePrecomputation.preaggregateAttributes(newAttributes);
        }

        return newAttributes;
    }

    public LinkedList<AnalyzedAttribute> getAnalysisResult(
            CandidateFactSet candidateFactSet,
            LinkedList<Long> attributeEncodings) {
        AnalyzedAttribute attribute;
        LinkedList<AnalyzedAttribute> analyzedAttributes = new LinkedList<>();
        HashMap<Long, AnalyzedAttribute> attributesByEncoding = new HashMap<>();
        HashMap<Long, LinkedList<SuggestedDerivation>> derivedAttributesToBeComputed = new HashMap<>();

        for (Long attributeEncoding: attributeEncodings) {
            attribute = loadAttributeFromAllAttributesTable(attributeEncoding, GlobalSettings.RECOMPUTE_STATISTICS_WITHIN_CANDIDATE_FACT_SET);
            analyzedAttributes.add(attribute);

            attributesByEncoding.put(attributeEncoding, attribute);
            if (attribute.getDerivationLevel() < GlobalSettings.MAX_DERIVATION_DEPTH) {
                derivedAttributesToBeComputed.put(attribute.getEncoding(), attribute.getSuggestedDerivations());
            }
        }

        for (Iterator<AnalyzedAttribute> iter = analyzedAttributes.iterator(); iter.hasNext();) {
            attribute = iter.next();

            if (!retrieveOrComputeMissingStatisticsAndStoreThemInAttributeProfilesTable(candidateFactSet, attribute, attributesByEncoding, GlobalSettings.RECOMPUTE_STATISTICS_WITHIN_CANDIDATE_FACT_SET)) {
                derivedAttributesToBeComputed.remove(attribute.getEncoding());
            }
        }

        analyzedAttributes.addAll(computeAndPreaggregateMissingDerivedAttributes(candidateFactSet, analyzedAttributes, attributesByEncoding, derivedAttributesToBeComputed));

        return analyzedAttributes;
    }
}
