//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import java.util.ArrayList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class CandidateFactSetSelector extends Operation {
    private static final Logger LOGGER = Logger.getLogger(CandidateFactSetSelector.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public abstract CandidateFactSet getFactsFromCandidateFactSet(
            String candidateFactSetEncoding);

    public abstract ArrayList<String> getAllCandidateFactSets();
}
