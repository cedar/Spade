//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class AttributeDerivation extends Operation {
    private static final Logger LOGGER = Logger.getLogger(AttributeDerivation.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected long originalAttributeEncoding;

    public abstract long derive();
}
