//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.Pair;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class PathAttributeDerivation extends AttributeDerivation {
    private static final Logger LOGGER = Logger.getLogger(PathAttributeDerivation.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public PathAttributeDerivation(long originalAttributeEncoding,
                                   DatabaseHandler handler) {
        this.originalAttributeEncoding = originalAttributeEncoding;
        this.handler = handler;
    }

    @Override
    public long derive() {
        return 0;
    }

    public LinkedList<Long> derivationWithSummary(int length) {
        LinkedList<Long> res = new LinkedList<>();
        ArrayList<ArrayList<Long>> allPaths = new ArrayList<>();
        ArrayList<Pair<String, String>> subj_obj = new ArrayList<>();
        ResultSet rs;
        String sqlQuery = null;

        try {
            sqlQuery = buildQueryToRetrievePathsWithSummary(length);
            rs = handler.getResultSet(sqlQuery);

            while (rs.next()) { // retrieve all possible paths of specified length
                ArrayList<Long> properties = new ArrayList<>();
                for (int i = 0; i < length; i++) {
                    properties.add(rs.getLong(i + 1));
                }

                allPaths.add(properties);
            }

            if (!allPaths.isEmpty()) {
                for (ArrayList<Long> path: allPaths) { // for each path create a derived attribute
                    String propName = GlobalSettings.DERIVED_ATTRIBUTE_PREFIX + "SummaryPathOf" + originalAttributeEncoding;
                    for (Long p: path) {
                        propName = propName + "/" + p;
                    }
                    propName = propName + ">";

                    // check if the attribute is already in the dictionary
                    sqlQuery = "SELECT key from dictionary where value = '" + propName + "'";
                    String propEnc = handler.getSQLSelectSingleResult(sqlQuery);
                    if (!propEnc.equals("-1")) // if it is in the dictionary add its encoding to the list of results
                    {
                        res.add(Long.parseLong(propEnc));
                    }
                    else { // if it not in the dictionary create the derived attribute
                        subj_obj.clear();
                        // retrieve the data
                        sqlQuery = buildQueryToRetrieveDataWithSummary(path);
                        rs = handler.getResultSet(sqlQuery);

                        while (rs.next()) {
                            String subj = rs.getString(1);
                            String obj = rs.getString(2);

                            Pair pp = new Pair(subj, obj);
                            if (!subj_obj.contains(pp)) {
                                subj_obj.add(pp);
                            }
                        }

                        if (!subj_obj.isEmpty()) { // if some data has been found create the table with the derived attribute
                            propEnc = makeDerivedAttribute(propName, subj_obj);
                            res.add(Long.parseLong(propEnc));
                            LOGGER.info("Derived attribute " + propName + " with encoding " + propEnc);
                        }
                    }
                }
            }

        }
        catch (NumberFormatException | SQLException ex) {
            throw new IllegalStateException("sqlQuery: " + sqlQuery + "; " + ex);
        }

        return res;
    }

    private String buildQueryToRetrievePathsWithSummary(int length) {
        int i;
        StringBuilder sqlQuery = new StringBuilder("select distinct E1.p");
        for (i = 1; i < length; i++) {
            sqlQuery.append(", E").append(i + 1).append(".p ");
        }
        sqlQuery.append(" from ").append(GlobalSettings.SUMMARY_TABLE_NAME).append(" E0 ");
        for (i = 0; i < length; i++) {
            sqlQuery.append(", ").append(GlobalSettings.SUMMARY_TABLE_NAME).append(" E").append(i + 1);
        }
        sqlQuery.append(" where E0.p=").append(originalAttributeEncoding);
        for (i = 0; i < length; i++) {
            sqlQuery.append(" and E").append(i).append(".o = E").append(i + 1).append(".s ");
        }
        for (i = 0; i < length; i++) {
            sqlQuery.append(" and E").append(i + 1).append(".p <>").append(GlobalSettings.TYPE_PROPERTY_ENCODING);
        }
        return sqlQuery.toString();
    }

    private String buildQueryToRetrieveDataWithSummary(
            ArrayList<Long> properties) {
        int i;
        StringBuilder sqlQuery = new StringBuilder("select t_");
        sqlQuery.append(originalAttributeEncoding)
                .append(".s, P").append(properties.size()).append(".o ")
                .append("from t_").append(originalAttributeEncoding);
        for (i = 0; i < properties.size(); i++) {
            sqlQuery.append(", t_").append(properties.get(i)).append(" P").append(i + 1);
        }
        sqlQuery.append(" where t_").append(originalAttributeEncoding).append(".o = P1.s ");
        for (i = 1; i < properties.size(); i++) {
            sqlQuery.append(" and P").append(i).append(".o = P").append(i + 1).append(".s");
        }
        return sqlQuery.toString();
    }

    private String makeDerivedAttribute(String attName,
                                        ArrayList<Pair<String, String>> subj_obj) {
        String sqlQuery = null, derivedAttributeEncoding = null;
        try {
            sqlQuery = "INSERT INTO dictionary(value,cleanvalue) VALUES ($dagger$" + attName + "$dagger$,'" + attName + "')";
            handler.executeUpdate(sqlQuery);
            // get the encoding of the derived attribute
            sqlQuery = "SELECT key from dictionary where value = $dagger$" + attName + "$dagger$";
            derivedAttributeEncoding = handler.getSQLSelectSingleResult(sqlQuery);

            sqlQuery = "CREATE TABLE IF NOT EXISTS t_" + derivedAttributeEncoding + "(s INTEGER NOT NULL,"
                       + " o INTEGER NOT NULL) WITH (OIDS=FALSE);"
                       + " CREATE UNIQUE INDEX t_" + derivedAttributeEncoding + "_i_so"
                       + " ON t_" + derivedAttributeEncoding + " USING btree(s, o)"
                       + " TABLESPACE pg_default;"
                       + " CREATE UNIQUE INDEX t_" + derivedAttributeEncoding + "_i_os"
                       + " ON t_" + derivedAttributeEncoding + " USING btree(o, s)"
                       + " TABLESPACE pg_default;";

            handler.executeUpdate(sqlQuery);

            sqlQuery = getSQLBatchInsertIntoDerivedAttributeTable(derivedAttributeEncoding, subj_obj);
            handler.executeUpdate(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Couldn't create table for derived attribute "
                                            + attName + " (enc: " + derivedAttributeEncoding + " ), sqlQuery: " + sqlQuery + "; " + ex);
        }
        return derivedAttributeEncoding;
    }

    private String getSQLBatchInsertIntoDerivedAttributeTable(
            String derivedAttribute, ArrayList<Pair<String, String>> results) {
        StringBuilder stringBuilder = new StringBuilder();
        String prefix = "";
        stringBuilder.append("INSERT INTO t_").append(derivedAttribute).append("(s, o) VALUES ");
        for (Pair<String, String> entry: results) {
            stringBuilder.append(prefix);
            prefix = ", ";
            stringBuilder.append("(").append(entry.getKey()).append(", ").append(entry.getValue()).append(")");
        }
        return stringBuilder.toString();
    }
}
