//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class SampleBasedCandidateFactSetSelector extends CandidateFactSetSelector {
    private static final Logger LOGGER = Logger.getLogger(SampleBasedCandidateFactSetSelector.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }
    protected CandidateFactSet cfs;
    protected String dimensionOfSample;

    public SampleBasedCandidateFactSetSelector(CandidateFactSet cfs,
                                               String dimension,
                                               DatabaseHandler handler) {
        this.cfs = cfs;
        this.dimensionOfSample = dimension;
        this.handler = handler;
    }

    @Override
    public CandidateFactSet getFactsFromCandidateFactSet(
            String candidateFactSetEncoding) {
        String tableName = cfs.getTableName() + "_sample";
        String query = "CREATE TABLE IF NOT EXISTS " + tableName + "(s) AS "
                       + " SELECT s FROM " + cfs.getTableName()
                       + " ORDER BY random() LIMIT " + dimensionOfSample + ";"
                       + " CREATE UNIQUE INDEX IF NOT EXISTS " + tableName + "_i_s"
                       + " ON " + tableName + " USING btree(s)"
                       + " TABLESPACE pg_default;";
        try {
            handler.executeUpdate(query);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Error occurs when reading/writing to files. " + query + "\n" + ex);
        }

        return new CandidateFactSet(handler, query, tableName);
    }

    @Override
    public ArrayList<String> getAllCandidateFactSets() {
        // TODO: getAllCandidateFactSets
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
