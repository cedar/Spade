//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import ca.pfv.spmf.algorithms.frequentpatterns.dFIN.AlgoDFIN;
import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.*;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AggregateDimensionsAndMeasuresSelector {
    private static final Logger LOGGER = Logger.getLogger(AggregateDimensionsAndMeasuresSelector.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected CandidateFactSet cfs;
    protected LinkedList<AnalyzedAttribute> allAttributes;
    protected HashMap<Integer, ArrayList<AnalyzedAttribute>> setsOfDimensions;
    protected HashMap<Integer, ArrayList<AnalyzedAttribute>> setsOfMeasures;
    protected HashMap<AnalyzedAttribute, Integer> suggestedChunkSizePerAttribute;
    protected HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> suggestedAggFunctionsPerAttribute;
    protected HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesPerAttribute;
    protected HashMap<Long, ArrayList<Long>> sampleForFrequentItemsetMining;

    protected DatabaseHandler handler;

    public AggregateDimensionsAndMeasuresSelector(CandidateFactSet cfs,
                                                      LinkedList<AnalyzedAttribute> allAttributes,
                                                      DatabaseHandler handler) {
        this.cfs = cfs;
        this.handler = handler;
        this.allAttributes = allAttributes;
        setsOfDimensions = new HashMap<>();
        setsOfMeasures = new HashMap<>();
        suggestedChunkSizePerAttribute = new HashMap<>();
        suggestedAggFunctionsPerAttribute = new HashMap<>();
        nrOfDistinctValuesPerAttribute = new HashMap<>();
    }

    public HashMap<Integer, ArrayList<AnalyzedAttribute>> getSetsOfDimensions() {
        return setsOfDimensions;
    }

    public HashMap<Integer, ArrayList<AnalyzedAttribute>> getSetsOfMeasures() {
        return setsOfMeasures;
    }

    public HashMap<AnalyzedAttribute, Integer> getNumberOfDistinctValuesPerAttribute() {
        return nrOfDistinctValuesPerAttribute;
    }

    public HashMap<AnalyzedAttribute, Integer> getSuggestedChunkSizePerAttribute() {
        return suggestedChunkSizePerAttribute;
    }

    public HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> getSuggestedAggFunctionsPerAttribute() {
        return suggestedAggFunctionsPerAttribute;
    }

    protected static class AttributeComparator implements
            Comparator<AnalyzedAttribute> {
        @Override
        public int compare(AnalyzedAttribute attribute1,
                           AnalyzedAttribute attribute2) {
            Double support1 = attribute1.getSupport();
            Double support2 = attribute2.getSupport();

            if (support1 == null && support2 == null) {
                return 0;
            }
            if (support1 == null) {
                return 1;
            }
            if (support2 == null) {
                return -1;
            }

            int supportComparison = -support1.compareTo(support2); // the higher the support the better

            if (supportComparison == 0) { // tie
                Integer numberOfDistinctObjects1 = attribute1.getNumberOfDistinctObjects();
                Integer numberOfDistinctObjects2 = attribute2.getNumberOfDistinctObjects();

                if (numberOfDistinctObjects1 == null && numberOfDistinctObjects2 == null) {
                    return 0;
                }
                if (numberOfDistinctObjects1 == null) {
                    return 1;
                }
                if (numberOfDistinctObjects2 == null) {
                    return -1;
                }

                return numberOfDistinctObjects1.compareTo(numberOfDistinctObjects2); // the lower the number of distinct values the better
            }

            return supportComparison;
        }
    }

    protected static class AttributeComparatorByNrOfDistinctValues implements
            Comparator<AnalyzedAttribute> {
        @Override
        public int compare(AnalyzedAttribute attribute1,
                           AnalyzedAttribute attribute2) {
            Integer numberOfDistinctObjects1 = attribute1.getNumberOfDistinctObjects();
            Integer numberOfDistinctObjects2 = attribute2.getNumberOfDistinctObjects();

            if (numberOfDistinctObjects1 == null && numberOfDistinctObjects2 == null) {
                return 0;
            }
            if (numberOfDistinctObjects1 == null) {
                return 1;
            }
            if (numberOfDistinctObjects2 == null) {
                return -1;
            }

            return numberOfDistinctObjects1.compareTo(numberOfDistinctObjects2); // the lower the number of distinct values the better
        }
    }

    protected boolean goodCandidateDimension(AnalyzedAttribute dim) {
        Double distinctObjectsToSubjectsRatio = dim.getDistinctObjectsToSubjectsRatio();
        Integer numberOfDistinctValues = dim.getNumberOfDistinctObjects();
        Double support = dim.getSupport();

        return support >= GlobalSettings.SUPPORT_THRESHOLD
               && numberOfDistinctValues > 1 && numberOfDistinctValues <= GlobalSettings.MAX_NUMBER_OF_DISTINCT_VALUES_PER_DIMENSION
               && distinctObjectsToSubjectsRatio <= GlobalSettings.DISTINCT_OBJECTS_TO_SUBJECTS_RATIO_THRESHOLD;
    }

    protected boolean goodCandidateMeasure(AnalyzedAttribute meas) {
        AnalyzedAttribute measD = meas.getDerivedFrom();
        SuggestedDerivation sgMeas = meas.getDerivationType();
        // don't use language and kwd derived attributes as measures
        if (measD != null && (sgMeas == SuggestedDerivation.KWD_IN_TEXT || sgMeas == SuggestedDerivation.LANG_DET)) {
            return false;
        }

        boolean measureIsAlmostOne = meas.isAlmostOne();
        boolean measureType = meas.isNumerical();

        return !measureIsAlmostOne || measureType;
    }


    /**
     * if the set of dimensions already contains one that is related through derivation to the dimension that we are trying to insert
     * do not insert
     *
     * @param attributes set of dimensions
     * @param attribute candidate attribute to be added
     * @return true if there is lineage between the attribute and the set of dimensions (thus, the attribute should not be added to it)
     */
    protected boolean thereIsLineage(ArrayList<AnalyzedAttribute> attributes,
                                     AnalyzedAttribute attribute) {
        AnalyzedAttribute AttributeDerived = attribute.getDerivedFrom();

        for (AnalyzedAttribute ap: attributes) {
            AnalyzedAttribute apD = ap.getDerivedFrom();
            if ((attribute.getEncoding() == ap.getEncoding()) || (apD != null && AttributeDerived != null && apD.getEncoding() == AttributeDerived.getEncoding()) || (apD != null && apD.equals(attribute)) || (AttributeDerived != null && AttributeDerived.equals(ap))) {
                return true;
            }
        }

        return false;
    }


    public void findFrequentSetsOfDimensions(int sizeOfSample) {
        try {
            String input = generateSample(sizeOfSample);
            String output = "output.txt"; // the path for saving the found frequent itemsets
            double minsup = GlobalSettings.SUPPORT_THRESHOLD;

            AlgoDFIN algorithm = new AlgoDFIN();
            algorithm.runAlgorithm(input, minsup, output);
            //algorithm.printStats();

            HashMap<Integer, ArrayList<AnalyzedAttribute>> allGoodSetsOfDimensions = findGoodSetsOfDimensionsFromFrequentItemsets(output);
            setsOfDimensions = findMaximalGoodSetsOfDimensions(allGoodSetsOfDimensions);

            int suggestedChDim = GlobalSettings.DIMENSION_OF_CHUNK;
            for (Map.Entry<Integer, ArrayList<AnalyzedAttribute>> entry: setsOfDimensions.entrySet()) {
                for (AnalyzedAttribute ap: entry.getValue()) {
                    if (suggestedChDim == 0) {
                        suggestedChunkSizePerAttribute.put(ap, ap.getNumberOfDistinctObjects() + 1);
                    }
                    else {
                        suggestedChunkSizePerAttribute.put(ap, suggestedChDim);
                    }
                    nrOfDistinctValuesPerAttribute.put(ap, ap.getNumberOfDistinctObjects());
                }
            }

            File in = new File(input);
            File out = new File(output);
            in.delete();
            out.delete();

            associateMeasuresToSetsOfDimensions();
        }
        catch (IOException ex) {
            throw new IllegalStateException("problems with frequent mining" + ex);
        }
    }

    private HashMap<Integer, ArrayList<AnalyzedAttribute>> findMaximalGoodSetsOfDimensions(
            HashMap<Integer, ArrayList<AnalyzedAttribute>> allSets) {
        HashMap<Integer, ArrayList<AnalyzedAttribute>> result = new HashMap<>();
        int currentSet = 0;
        boolean found;

        for (Map.Entry<Integer, ArrayList<AnalyzedAttribute>> entry1: allSets.entrySet()) {
            found = false;
            for (Map.Entry<Integer, ArrayList<AnalyzedAttribute>> entry2: allSets.entrySet()) {
                if (!entry2.getKey().equals(entry1.getKey()) && entry2.getValue().containsAll(entry1.getValue())) {
                    found = true;
                    break;
                }
            }
            if (found == false) {
                currentSet++;
                result.put(currentSet, entry1.getValue());
            }
        }
        return result;
    }

    private String generateSample(int sizeOfSample) {
        sampleForFrequentItemsetMining = new HashMap<>();
        String sqlquery, filename;
        long s, p;

        String cfsSampleTablename = getRandomCandidateFactSet(sizeOfSample);
        try {
            sqlquery = getQueryToObtainAllRDFPropertiesOfCFS(cfsSampleTablename);
            ResultSet rs = handler.getResultSet(sqlquery);
            while (rs.next()) {
                s = rs.getLong(1);
                p = rs.getLong(2);
                AnalyzedAttribute a = getAnalyzedAttributeGivenEncoding(p);
                if (a != null) {
                    if (goodCandidateDimension(a) == true) {
                        addItemToSample(s, p);
                    }
                }
            }
            rs.close();

            // get the derived attributes as well
            for (AnalyzedAttribute aa: allAttributes) {
                AnalyzedAttribute derived = aa.getDerivedFrom();
                if (derived != null && goodCandidateDimension(aa)) {
                    rs = handler.getResultSet("select distinct cfs.s from " + cfsSampleTablename + " as cfs join t_" + aa.getEncoding() + " as dim on cfs.s=dim.s");
                    while (rs.next()) {
                        s = rs.getLong(1);
                        addItemToSample(s, aa.getEncoding());
                    }
                    rs.close();
                }
            }

            filename = cfs.getEncoding() + "_sampleForFIM.txt";
            flushSampleToFile(filename);

        }
        catch (SQLException e1) {
            throw new IllegalStateException(e1);
        }
        removeTableFromDatabase(cfsSampleTablename);
        return filename;
    }

    private void addItemToSample(long s, long p) {
        if (sampleForFrequentItemsetMining.containsKey(s)) {
            if (!sampleForFrequentItemsetMining.get(s).contains(p)) {
                sampleForFrequentItemsetMining.get(s).add(p);
            }
        }
        else {
            ArrayList<Long> itemset = new ArrayList<>();
            itemset.add(p);
            sampleForFrequentItemsetMining.put(s, itemset);
        }
    }

    private void flushSampleToFile(String filename) {
        try {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
                for (Map.Entry<Long, ArrayList<Long>> entry: sampleForFrequentItemsetMining.entrySet()) {
                    if (!entry.getValue().isEmpty()) {
                        for (Long item: entry.getValue()) {
                            writer.write(item + " ");
                        }
                        writer.write("\n");
                    }
                }
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private String getRandomCandidateFactSet(int sizeOfSample) {
        String tableName = cfs.getTableName() + "_sample";
        StringBuilder sqlQuery = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
        if(sizeOfSample == 0){
            sqlQuery.append(tableName).append("(s) AS ")
                    .append(" SELECT s")
                    .append(" FROM ").append(cfs.getTableName()).append(";")
                    .append(" CREATE UNIQUE INDEX IF NOT EXISTS ").append(tableName).append("_i_s")
                    .append(" ON ").append(tableName).append(" USING btree(s)")
                    .append(" TABLESPACE pg_default;");
        }
        else {
            sqlQuery.append(tableName).append("(s) AS ")
                    .append(" SELECT s")
                    .append(" FROM ").append(cfs.getTableName())
                    .append(" ORDER BY random() LIMIT ").append(sizeOfSample).append(";")
                    .append(" CREATE UNIQUE INDEX IF NOT EXISTS ").append(tableName).append("_i_s")
                    .append(" ON ").append(tableName).append(" USING btree(s)")
                    .append(" TABLESPACE pg_default;");
        }
        try {
            handler.executeUpdate(sqlQuery.toString());
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }
        return tableName;
    }

    private String getQueryToObtainAllRDFPropertiesOfCFS(String CFSTablename) {
        // old query:
        return "select s, p from encoded_triples where p <> " + GlobalSettings.TYPE_PROPERTY_ENCODING + " and s in ( select s from " + CFSTablename + " )";

        // query updated
        // return "select distinct s, p from encoded_triples where p <> " + GlobalSettings.TYPE_PROPERTY_ENCODING + " and s in ( select s from " + CFSTablename + " ) order by s";
    }

    private void removeTableFromDatabase(String tablename) {
        String sqlquery = "drop table " + tablename;
        try {
            handler.executeUpdate(sqlquery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private AnalyzedAttribute getAnalyzedAttributeGivenEncoding(long encoding) {
        for (AnalyzedAttribute aa: allAttributes) {
            if (aa.getEncoding() == encoding) {
                return aa;
            }
        }
        return null;
    }

    private HashMap<Integer, ArrayList<AnalyzedAttribute>> findGoodSetsOfDimensionsFromFrequentItemsets(
            String filename) {
            HashMap<Integer, ArrayList<AnalyzedAttribute>> result = new HashMap<>();
        int currentSetOfDimensions = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split(" ");
                if (checkIfItemsetIsGoodCandidateSetOfDimensions(tokens) == true) {
                    ArrayList<AnalyzedAttribute> itemset = new ArrayList<>();
                    for (int i = 0; i < tokens.length - 2; i++) {
                        itemset.add(getAnalyzedAttributeGivenEncoding(Long.parseLong(tokens[i])));
                    }
                    Collections.sort(itemset, new AttributeComparatorByNrOfDistinctValues());
                    currentSetOfDimensions++;
                    result.put(currentSetOfDimensions, itemset);
                }
            }
        }
        catch (FileNotFoundException ex) {
            throw new IllegalStateException(ex);
        }
        catch (IOException ex2) {
            throw new IllegalStateException(ex2);
        }

        return result;
    }

    private boolean checkIfItemsetIsGoodCandidateSetOfDimensions(
            String[] itemset) {
        if (itemset.length - 2 > GlobalSettings.MAX_NUMBER_OF_DIMENSIONS) {
            return false;
        }

        for (int i = 0; i < itemset.length - 2; i++) {
            AnalyzedAttribute a1 = getAnalyzedAttributeGivenEncoding(Long.parseLong(itemset[i]));
            for (int j = i + 1; j < itemset.length - 2; j++) {
                AnalyzedAttribute a2 = getAnalyzedAttributeGivenEncoding(Long.parseLong(itemset[j]));
                if (checkIfThereIsLineageBetweenTwoAttributes(a1, a2) == true) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkIfThereIsLineageBetweenTwoAttributes(
            AnalyzedAttribute a1, AnalyzedAttribute a2) {
        AnalyzedAttribute a1Derived = a1.getDerivedFrom();
        AnalyzedAttribute a2Derived = a2.getDerivedFrom();

        return (a1.getEncoding() == a2.getEncoding())
               || (a2Derived != null && a1Derived != null && a2Derived.getEncoding() == a1Derived.getEncoding())
               || (a2Derived != null && a2Derived.equals(a1))
               || (a1Derived != null && a1Derived.equals(a2));
    }

    private void associateMeasuresToSetsOfDimensions() {
        ArrayList<AnalyzedAttribute> allFrequentAttributes = new ArrayList<>();

        Double supportDim;
        for (AnalyzedAttribute attribute: allAttributes) {
            supportDim = attribute.getSupport();
            if (supportDim >= GlobalSettings.SUPPORT_THRESHOLD) {
                allFrequentAttributes.add(attribute);
            }
        }

        // associate a set of measures to each found set of dimensions
        for (Map.Entry<Integer, ArrayList<AnalyzedAttribute>> entry: setsOfDimensions.entrySet()) {
            ArrayList<AnalyzedAttribute> measures = new ArrayList<>();
            for (AnalyzedAttribute ap: allFrequentAttributes) {
                if (goodCandidateMeasure(ap) == true && thereIsLineage(entry.getValue(), ap) == false) {
                    measures.add(ap);
                }
            }
            setsOfMeasures.put(entry.getKey(), measures);
        }

        for (AnalyzedAttribute ap: allFrequentAttributes) {
            suggestedAggFunctionsPerAttribute.put(ap, ap.getAggregationFunctions());
        }
    }
}
