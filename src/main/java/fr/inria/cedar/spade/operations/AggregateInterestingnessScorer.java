//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.datastructures.Pair;
import fr.inria.cedar.spade.molapForRDF.MOLAPMeasures;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AggregateInterestingnessScorer extends Operation {
    private static final Logger LOGGER = Logger.getLogger(AggregateInterestingnessScorer.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    private final InterestingnessFunction interestingnessFunction;
    private final String COMMA_DELIMITER = ",";
    private final ArrayList<Pair<String, Double>> results;

    public AggregateInterestingnessScorer(InterestingnessFunction interestingnessFunction) {
        this.interestingnessFunction = interestingnessFunction;
        results = new ArrayList<>();
    }

    protected String renameFile(String oldFilename, int ordinalNumber,
                                int paddingSize) throws IOException {
        File oldFile = new File(oldFilename);

        int position = oldFilename.lastIndexOf("/");
        String prefix = oldFilename.substring(0, position + 1);
        String suffix = (position != -1) ? oldFilename.substring(position + 1) : "";
        String newFilename = String.format(prefix + "%0" + paddingSize + "d", ordinalNumber) + "_" + suffix;
        File newFile = new File(newFilename);

        if (newFile.exists()) {
            throw new IOException("File " + newFile + " exists");
        }

        boolean success = oldFile.renameTo(newFile);

        if (!success) {
            throw new IllegalStateException("File " + oldFilename + " was not successfully renamed");
        }

        return newFilename;
    }

    public ArrayList<Pair<String, Double>> score(HashMap<String, double[]> aggregateProfiles, HashMap<String,String> dictionaryFromFilenameToSHA1) {
        String filename, filenameSHA1;
        for(Map.Entry<String,String> entry: dictionaryFromFilenameToSHA1.entrySet()){
            filename = entry.getKey();
            filenameSHA1 = entry.getValue();
            computeProfileWithKnownStatistics(filename, aggregateProfiles.get(filenameSHA1));
        }

        sortResults();
        renameResults(results);

        return results;
    }

    public void renameResults(ArrayList<Pair<String,Double>> pairsFilenameIM){
        int i = 1;
        int paddingSize = (int) Math.log10(pairsFilenameIM.size()) + 1;
        String newFilename;
        try{
            for (Pair<String, Double> filenameValue: pairsFilenameIM) {
                newFilename = renameFile(filenameValue.getKey(), i, paddingSize);
                filenameValue.setKey(newFilename);
                i++;
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private void computeProfileWithKnownStatistics(String csvFilename, double[] filenameStatistics){
        ArrayList<Double> values = readValuesFromCSV(csvFilename);

        if (values.size() > 0) {
            Double[] dblArray = new Double[values.size()];
            dblArray = values.toArray(dblArray);
            Double score = null;

            switch (interestingnessFunction) {
                case VAR_FEATURE_SCALING:
                    score = EfficientComputations.varianceWithKnownMean(EfficientComputations.normalizeByFeatureScaling(dblArray, filenameStatistics[MOLAPMeasures.MIN_ORDINAL], filenameStatistics[MOLAPMeasures.MAX_ORDINAL]), filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL]);
                    break;
                case VAR_SUM:
                    if (filenameStatistics[MOLAPMeasures.SUM_ORDINAL] == 0D) {
                        score = EfficientComputations.varianceWithKnownMean(dblArray, filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL]);
                    }
                    else {
                        score = EfficientComputations.varianceOnRawValuesNormalizedByDivisionBySumWithKnownMean(dblArray, filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL], filenameStatistics[MOLAPMeasures.SUM_ORDINAL]);
                    }
                    break;
                case VAR_MEAN:
                    if (filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL] == 0D) {
                        score = EfficientComputations.varianceWithKnownMean(dblArray, filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL]);
                    }
                    else {
                        score = EfficientComputations.varianceOnRawValuesNormalizedByDivisionByMeanWithKnownMean(dblArray, filenameStatistics[MOLAPMeasures.AVERAGE_ORDINAL]);
                    }
                    break;
                case SKEWNESS:
                    score = EfficientComputations.skewness(dblArray);
                    break;
                case KURTOSIS:
                    score = EfficientComputations.kurtosis(dblArray);
                    break;
                default:
                    throw new IllegalStateException(interestingnessFunction + " not supported");
            }
            results.add(new Pair<>(csvFilename, score));
        }
    }

    /*private Properties prepareProperties() {
        Properties properties = new Properties();
        properties.put("statistics.median", "false");
        properties.put("statistics.variance_on_values_normalized_by_feature_scaling", "false");
        properties.put("statistics.variance_on_values_normalized_by_sum", "false");
        properties.put("statistics.variance_on_values_normalized_by_mean", "false");
        properties.put("statistics.skewness", "false");
        properties.put("statistics.kurtosis", "false");
        switch (interestingnessFunction) {
            case VAR_FEATURE_SCALING:
                properties.put("statistics.variance", "false");
                properties.put("statistics.variance_on_values_normalized_by_feature_scaling", "true");
                break;
            case VAR_SUM:
                properties.put("statistics.variance", "true");
                properties.put("statistics.variance_on_values_normalized_by_sum", "true");
                break;
            case VAR_MEAN:
                properties.put("statistics.variance", "true");
                properties.put("statistics.variance_on_values_normalized_by_mean", "true");
                break;
            case SKEWNESS:
                properties.put("statistics.variance", "false");
                properties.put("statistics.skewness", "true");
                break;
            case KURTOSIS:
                properties.put("statistics.variance", "false");
                properties.put("statistics.kurtosis", "true");
                break;
        }
        properties.put("statistics.entropy", "false");
        properties.put("statistics.normalized_entropy", "false");
        properties.put("statistics.number_of_distinct_types", "false");
        properties.put("statistics.distinct_types", "false");
        properties.put("string.long_string_threshold", "50");
        properties.put("statistics.number_of_items_of_type_among_all_values_by_type_sets", "false");
        properties.put("statistics.number_of_items_of_type_among_all_values_by_most_specific_type", "false");
        properties.put("statistics.percentage_of_types_among_all_values_by_type_sets", "false");
        properties.put("statistics.percentage_of_types_among_all_values_by_most_specific_type", "false");
        properties.put("statistics.type_by_majority_voting_by_type_set", "false");
        properties.put("statistics.type_by_majority_voting_by_most_specific_type", "false");
        properties.put("statistics.equi_width_histogram_result", "false");
        properties.put("equi_width_histogram.number_of_bins", "10");
        properties.put("equi_width_histogram.normalize_bins_to_PMF", "false");
        properties.put("statistics.equi_depth_histogram_result", "false");
        properties.put("equi_depth_histogram.number_of_bins", "10");

        return properties;
    }*/

    private ArrayList<Double> readValuesFromCSV(String csvFilename){
        String[] values;
        String valueString;
        ArrayList<Double> result = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFilename))) {
            String line;
            if (br.readLine() != null) { // skip the header
                while ((line = br.readLine()) != null) {
                    values = line.split(COMMA_DELIMITER);
                    valueString = values[values.length - 1];
                    result.add(Double.parseDouble(valueString));

                }
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
        return result;
    }

    /*private void addCsvValuesToHistogram(Histogram h, String csvFilename) {
        String[] values;
        String valueString;
        double value;
        HashSet<Type> types = new HashSet<>();
        types.add(Type.REAL);
        try (BufferedReader br = new BufferedReader(new FileReader(csvFilename))) {
            String line;
            if (br.readLine() != null) { // skip the header
                while ((line = br.readLine()) != null) {
                    values = line.split(COMMA_DELIMITER);
                    valueString = values[values.length - 1];
                    value = Double.parseDouble(valueString);
                    h.addValue(valueString, types, value);
                }
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }*/

    private static class ResultsComparator implements
            Comparator<Pair<String, Double>> {
        @Override
        public int compare(Pair<String, Double> agg1, Pair<String, Double> agg2) {
            Double score1 = agg1.getValue();
            Double score2 = agg2.getValue();

            if (score1 == null) {
                return -1;
            }
            if (score2 == null) {
                return -1;
            }

            return -score1.compareTo(score2);
        }
    }

    private void sortResults() {
        // sort results by given interestingness function
        Collections.sort(results, new ResultsComparator());
    }

    public void sortResults(ArrayList<Pair<String,Double>> results){
        Collections.sort(results, new ResultsComparator());
    }
}
