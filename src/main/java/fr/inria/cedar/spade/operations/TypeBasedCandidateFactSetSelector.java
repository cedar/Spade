//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * This is a simple implementation of a candidate fact selection which will only return
 * sets of resources sharing a certain type.
 * For now, we need to tell it which type it is. Other extensions and refactoring are possible.
 *
 */
public class TypeBasedCandidateFactSetSelector extends CandidateFactSetSelector {
    private static final Logger LOGGER = Logger.getLogger(TypeBasedCandidateFactSetSelector.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public TypeBasedCandidateFactSetSelector(DatabaseHandler handler) {
        this.handler = handler;
    }

    @Override
    public CandidateFactSet getFactsFromCandidateFactSet(
            String candidateFactSetEncoding) {
        // the encoding of the type of the resources which will make up the candidate fact set
        StringBuilder tableName = new StringBuilder("t_");
        tableName.append(candidateFactSetEncoding);
        StringBuilder sqlQuery = new StringBuilder("select * from ");
        sqlQuery.append(tableName);

        return new CandidateFactSet(handler, sqlQuery.toString(), tableName.toString());
    }

    // accesses the database in order to find and return the encodings of all distinct types of resources
    @Override
    public ArrayList<String> getAllCandidateFactSets() {
        LOGGER.info("Searching types");
        ArrayList<String> distinctTypes = new ArrayList<>();
        // access the summary to select all the objects of the TYPE property, given its encoding
        StringBuilder sqlQuery = new StringBuilder("select distinct o from ");
        sqlQuery.append(GlobalSettings.SUMMARY_TABLE_NAME)
                .append(" where p = ")
                .append(GlobalSettings.TYPE_PROPERTY_ENCODING);
        try {
            distinctTypes = handler.getResultSelectFromObjectColumn(sqlQuery.toString());
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Unable to find all the types " + sqlQuery + "\n" + ex);
        }
        if (distinctTypes.isEmpty()) {
            throw new IllegalStateException("Resources having no type property");
        }
        LOGGER.info("\tfound: " + distinctTypes);

        return distinctTypes;
    }
}
