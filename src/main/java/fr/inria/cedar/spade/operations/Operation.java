//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.database.DatabaseHandler;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Top-level class for various steps carried over during Dagger exploration
 *
 */
public abstract class Operation {
    private static final Logger LOGGER = Logger.getLogger(Operation.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected DatabaseHandler handler; // all operations need this so we put it at the root.
}
