//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import fr.inria.cedar.ontosql.db.*;
import fr.inria.cedar.ontosql.db.Dictionary;
import fr.inria.cedar.ontosql.reasoning.OntologyUtils;
import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import org.apache.commons.math3.util.Pair;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class KeywordAttributeDerivation extends AttributeDerivation {
    private static final Logger LOGGER = Logger.getLogger(KeywordAttributeDerivation.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    private static String[] englishStopwords = {"[", "]", "``", "''", "make", "(", ")", "a", "as", "able", "about", "above", "according", "accordingly", "across", "actually", "after", "afterwards", "again", "against", "aint", "all", "allow", "allows", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "an", "and", "another", "any", "anybody", "anyhow", "anyone", "anything", "anyway", "anyways", "anywhere", "apart", "appear", "appreciate", "appropriate", "are", "arent", "around", "as", "aside", "ask", "asking", "associated", "at", "available", "away", "awfully", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "believe", "below", "beside", "besides", "best", "better", "between", "beyond", "both", "brief", "but", "by", "cmon", "cs", "came", "can", "cant", "cannot", "cant", "cause", "causes", "certain", "certainly", "changes", "clearly", "co", "com", "come", "comes", "concerning", "consequently", "consider", "considering", "contain", "containing", "contains", "corresponding", "could", "couldnt", "course", "currently", "definitely", "described", "despite", "did", "didnt", "different", "do", "does", "doesnt", "doing", "dont", "done", "down", "downwards", "during", "each", "edu", "eg", "eight", "either", "else", "elsewhere", "enough", "entirely", "especially", "et", "etc", "even", "ever", "every", "everybody", "everyone", "everything", "everywhere", "ex", "exactly", "example", "except", "far", "few", "ff", "fifth", "first", "five", "followed", "following", "follows", "for", "former", "formerly", "forth", "four", "from", "further", "furthermore", "get", "gets", "getting", "given", "gives", "go", "goes", "going", "gone", "got", "gotten", "greetings", "had", "hadnt", "happens", "hardly", "has", "hasnt", "have", "havent", "having", "he", "hes", "hello", "help", "hence", "her", "here", "heres", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "hi", "him", "himself", "his", "hither", "hopefully", "how", "howbeit", "however", "i", "id", "ill", "im", "ive", "ie", "if", "ignored", "immediate", "in", "inasmuch", "inc", "indeed", "indicate", "indicated", "indicates", "inner", "insofar", "instead", "into", "inward", "is", "isnt", "it", "itd", "itll", "its", "its", "itself", "just", "keep", "keeps", "kept", "know", "knows", "known", "last", "lately", "later", "latter", "latterly", "least", "less", "lest", "let", "lets", "like", "liked", "likely", "little", "look", "looking", "looks", "ltd", "mainly", "many", "may", "maybe", "me", "mean", "meanwhile", "merely", "might", "more", "moreover", "most", "mostly", "much", "must", "my", "myself", "name", "namely", "nd", "near", "nearly", "necessary", "need", "needs", "neither", "never", "nevertheless", "new", "next", "nine", "no", "nobody", "non", "none", "noone", "nor", "normally", "not", "nothing", "novel", "now", "nowhere", "obviously", "of", "off", "often", "oh", "ok", "okay", "old", "on", "once", "one", "ones", "only", "onto", "or", "other", "others", "otherwise", "ought", "our", "ours", "ourselves", "out", "outside", "over", "overall", "own", "particular", "particularly", "per", "perhaps", "placed", "please", "plus", "possible", "presumably", "probably", "provides", "que", "quite", "qv", "rather", "rd", "re", "really", "reasonably", "regarding", "regardless", "regards", "relatively", "respectively", "right", "said", "same", "saw", "say", "saying", "says", "second", "secondly", "see", "seeing", "seem", "seemed", "seeming", "seems", "seen", "self", "selves", "sensible", "sent", "serious", "seriously", "seven", "several", "shall", "she", "should", "shouldnt", "since", "six", "so", "some", "somebody", "somehow", "someone", "something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specified", "specify", "specifying", "still", "sub", "such", "sup", "sure", "ts", "take", "taken", "tell", "tends", "th", "than", "thank", "thanks", "thanx", "that", "thats", "thats", "the", "their", "theirs", "them", "themselves", "then", "thence", "there", "theres", "thereafter", "thereby", "therefore", "therein", "theres", "thereupon", "these", "they", "theyd", "theyll", "theyre", "theyve", "think", "third", "this", "thorough", "thoroughly", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "took", "toward", "towards", "tried", "tries", "truly", "try", "trying", "twice", "two", "un", "under", "unfortunately", "unless", "unlikely", "until", "unto", "up", "upon", "us", "use", "used", "useful", "uses", "using", "usually", "value", "various", "very", "via", "viz", "vs", "want", "wants", "was", "wasnt", "way", "we", "wed", "well", "were", "weve", "welcome", "well", "went", "were", "werent", "what", "whats", "whatever", "when", "whence", "whenever", "where", "wheres", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whos", "whoever", "whole", "whom", "whose", "why", "will", "willing", "wish", "with", "within", "without", "wont", "wonder", "would", "would", "wouldnt", "yes", "yet", "you", "youd", "youll", "youre", "youve", "your", "yours", "yourself", "yourselves", "zero"};
    private static HashSet<String> englishStopWordSet = new HashSet<>(Arrays.asList(englishStopwords));

    private static String[] swedishStopwords = {"aderton", "adertonde", "aldrig", "alla", "allas", "allt", "alltid", "andra", "andras", "annan", "annat", "artonde", "artonn", "att", "av", "bakom", "bara", "beslut", "beslutat", "beslutit", "bland", "blev", "bli", "blir", "blivit", "bort", "borta", "bra", "dag", "dagar", "dagarna", "dagen", "de", "del", "delen", "dem", "den", "denna", "deras", "dess", "dessa", "det", "detta", "dig", "din", "dina", "dit", "ditt", "dock", "dom", "du", "e", "efter", "eftersom", "ej", "elfte", "eller", "elva", "emot", "en", "enkel", "enkelt", "enkla", "enligt", "ens", "er", "era", "ers", "ert", "ett", "ettusen", "fanns", "fem", "femte", "femtio", "femtionde", "femton", "femtonde", "fick", "fin", "finnas", "finns", "fjorton", "fjortonde", "fler", "flera", "flesta", "fram", "fyra", "fyrtio", "fyrtionde", "genast", "genom", "gick", "gjorde", "gjort", "god", "goda", "godare", "godast", "gott", "ha", "hade", "haft", "han", "hans", "har", "heller", "hellre", "helst", "helt", "henne", "hennes", "hit", "hon", "honom", "hundra", "hundraen", "hundraett", "hur", "i", "ibland", "icke", "idag", "igen", "imorgon", "in", "inga", "ingen", "ingenting", "inget", "innan", "inne", "inom", "inte", "inuti", "ja", "jag", "jo", "ju", "just", "kan", "kanske", "knappast", "kom", "komma", "kommer", "kommit", "kr", "kunde", "kunna", "kunnat", "kvar", "legat", "ligga", "ligger", "lika", "lilla", "lite", "liten", "litet", "man", "med", "mej", "mellan", "men", "mer", "mera", "mest", "mig", "min", "mina", "mindre", "minst", "mitt", "mittemot", "mot", "mycket", "ned", "nederst", "nedersta", "nedre", "nej", "ner", "ni", "nio", "nionde", "nittio", "nittionde", "nitton", "nittonde", "nog", "noll", "nr", "nu", "nummer", "och", "ofta", "oftast", "olika", "olikt", "om", "oss", "rakt", "redan", "sa", "sade", "sagt", "samma", "sedan", "senare", "senast", "sent", "sextio", "sextionde", "sexton", "sextonde", "sig", "sin", "sina", "sist", "sista", "siste", "sitt", "sitta", "sju", "sjunde", "sjuttio", "sjuttionde", "sjutton", "sjuttonde", "ska", "skall", "skulle", "slutligen", "snart", "som", "stor", "stora", "stort", "ta", "tack", "tar", "tidig", "tidigare", "tidigast", "tidigt", "till", "tills", "tillsammans", "tio", "tionde", "tjugo", "tjugoen", "tjugoett", "tjugonde", "tjugotre", "tjungo", "tolfte", "tolv", "tre", "tredje", "trettio", "trettionde", "tretton", "trettonde", "under", "upp", "ur", "ut", "utan", "ute", "va", "vad", "var", "vara", "varit", "varje", "varken", "vars", "vart", "vem", "vems", "verkligen", "vi", "vid", "vidare", "viktig", "viktigare", "viktigast", "viktigt", "vilka", "vilkas", "vilken", "vilket", "vill"};
    private static HashSet<String> swedishStopwordsSet = new HashSet<>(Arrays.asList(swedishStopwords));

    public KeywordAttributeDerivation(long originalAttributeEncoding,
                                      DatabaseHandler handler) {
        this.originalAttributeEncoding = originalAttributeEncoding;
        this.handler = handler;
    }

    @Override
    public long derive() {
        String derivedAttributeName, sqlQuery = null, derivedAttributeEncoding;
        derivedAttributeName = GlobalSettings.DERIVED_ATTRIBUTE_PREFIX + "KeywordsOf" + originalAttributeEncoding + ">";

        try {
            sqlQuery = "SELECT key from dictionary where value = '" + derivedAttributeName + "'";
            derivedAttributeEncoding = handler.getSQLSelectSingleResult(sqlQuery);
            if (!derivedAttributeEncoding.equals("-1")) {
                return Long.parseLong(derivedAttributeEncoding);
            }

            int fetchSize = 10000; //represent the number of data fetched (you can add more if your dictionary is big).

            HashMap<String, Long> cache = new HashMap<>();
            HashMap<String, Integer> freqMapGlobal = new HashMap<>();
            HashMap<String, ArrayList<String>> dataStructure = new HashMap<>();
            ArrayList<Pair<String, String>> subjects_with_object = new ArrayList<>();
            ArrayList<Pair<String, Long>> subjects_with_object_encoding = new ArrayList<>();
            Properties props = new Properties();
            props.put("annotators", "tokenize, ssplit, pos, lemma");
            StanfordCoreNLP pipeline = new StanfordCoreNLP(props, false);

            HashMap<String, String> subjectsObjects = getSubjectObject();
            for (Map.Entry<String, String> entry: subjectsObjects.entrySet()) {
                String subj = entry.getKey();
                String object_value = entry.getValue();
                ArrayList<String> currentLemmas = new ArrayList<>();

                object_value = object_value.replaceAll("[^\\d\\p{L}\\s]", " ");
                object_value = object_value.toLowerCase();
                Annotation document = pipeline.process(object_value);

                //generate frequency map
                for (CoreMap sentence: document.get(CoreAnnotations.SentencesAnnotation.class)) {
                    for (CoreLabel token: sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                        String word = token.get(CoreAnnotations.TextAnnotation.class);
                        if (!(isStopword(word))) {
                            String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
                            if (lemma.length() > 2 && !(lemma.startsWith("-"))) {
                                int freq = freqMapGlobal.getOrDefault(lemma, 0);
                                freqMapGlobal.put(lemma, ++freq);
                                if (!currentLemmas.contains(lemma)) {
                                    currentLemmas.add(lemma);
                                }
                            }
                        }
                    }
                }
                dataStructure.put(subj, currentLemmas);
            }

            if (!dataStructure.isEmpty()) { // if lemmas were found go on creating the derived attribute
                // Generate vocabulary
                Map<String, Integer> vocabulary = freqMapGlobal.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .limit(GlobalSettings.MAX_NUMBER_OF_DISTINCT_VALUES_PER_DIMENSION)
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

                for (Map.Entry<String, ArrayList<String>> entry: dataStructure.entrySet()) {
                    String subj = entry.getKey();
                    ArrayList<String> currentLemmas = entry.getValue();

                    for (String lemma: currentLemmas) {
                        if (vocabulary.containsKey(lemma)) {
                            subjects_with_object.add(new Pair(subj, lemma));
                        }
                    }
                }

                DataSource ds = createDataSource(); // see below how to create this object.
                Dictionary dictionary = OntologyUtils.createDictionary(ds, "dictionary", fetchSize);
                long enc = -1;
                for (Pair<String, String> entry: subjects_with_object) {
                    if (cache.containsKey(entry.getSecond())) {
                        enc = cache.get(entry.getSecond());
                    }
                    else {
                        if (dictionary.containsValue(entry.getValue())) {
                            enc = dictionary.getKey(entry.getValue());
                            cache.put(entry.getValue(), enc);
                        }
                        else {
                            enc = getDictionaryValue(entry.getValue());
                            cache.put(entry.getValue(), enc);
                        }
                    }
                    subjects_with_object_encoding.add(new Pair(entry.getKey(), enc));
                }

                if (!subjects_with_object_encoding.isEmpty()) {
                    long encoding = makeDerivedAttribute(subjects_with_object_encoding, derivedAttributeName, handler);
                    LOGGER.info("Derived attribute " + derivedAttributeName + " with encoding " + encoding);
                    return encoding;
                }
            }
        }
        catch (SQLException | DictionaryException | InexistentValueException | UnsupportedDatabaseEngineException | NumberFormatException ex) {
            throw new IllegalStateException("Unable to create the derived attribute: " + sqlQuery + "; " + ex);
        }
        return -1;
    }

    private DataSource createDataSource() {
        try {
            return DataSourceFactory.createDataSource(DatabaseEngine.POSTGRESQL,
                                                      GlobalSettings.DATABASE_URL,
                                                      Integer.parseInt(GlobalSettings.DATABASE_PORT), GlobalSettings.DATABASE_NAME,
                                                      GlobalSettings.DATABASE_USER, GlobalSettings.DATABASE_PASSWORD);
        }
        catch (UnsupportedDatabaseEngineException | NumberFormatException ex) {
            throw new IllegalStateException(ex);
        }
    }

    //contains a list of common english stopwords
    //returns if a word is a stopword or not.
    private static boolean isStopword(String word) {
        if (word.length() <= 2) {
            return true;
        }
        if (word.charAt(0) >= '0' && word.charAt(0) <= '9') {
            return true; //remove numbers, "25th", etc
        }

        return (englishStopWordSet.contains(word) || swedishStopwordsSet.contains(word));
    }

    private static boolean isSwedish(String word) {
        /*String[] encodings = {"u00e4", "u00e5", "u00f6", "u201d", "u2019"};
        for (String encoding: encodings) {
            if (word.contains(encoding)) {
                return true;
            }
        }*/
        return swedishStopwordsSet.contains(word);
    }

    /**
     * @param map                  a map of String->integer containing subjects with the encoding of the objects
     * @param derivedAttributeName name of the attribute we want to create, starts with prefix
     * @param handler              database handler, required to to execute queries
     *
     * @return encoding of derived attribute
     */
    private long makeDerivedAttribute(ArrayList<Pair<String, Long>> map,
                                      String derivedAttributeName,
                                      DatabaseHandler handler) {
        String sqlQuery = null, derivedAttributeEncoding = null;
        try {
            sqlQuery = "INSERT INTO dictionary(value,cleanvalue) VALUES ($dagger$" + derivedAttributeName + "$dagger$,'" + derivedAttributeName + "')";
            handler.executeUpdate(sqlQuery);

            // get the encoding of the derived attribute
            sqlQuery = "SELECT key from dictionary where value = $dagger$" + derivedAttributeName + "$dagger$";
            derivedAttributeEncoding = handler.getSQLSelectSingleResult(sqlQuery);

            // create a new table in the database to contain the pairs subject, object of the newly derived attribute
            sqlQuery = "CREATE TABLE IF NOT EXISTS t_" + derivedAttributeEncoding + "(s INTEGER NOT NULL,"
                       + " o INTEGER NOT NULL) WITH (OIDS=FALSE);"
                       + " CREATE UNIQUE INDEX t_" + derivedAttributeEncoding + "_i_so"
                       + " ON t_" + derivedAttributeEncoding + " USING btree(s, o)"
                       + " TABLESPACE pg_default;"
                       + " CREATE UNIQUE INDEX t_" + derivedAttributeEncoding + "_i_os"
                       + " ON t_" + derivedAttributeEncoding + " USING btree(o, s)"
                       + " TABLESPACE pg_default;";

            handler.executeUpdate(sqlQuery);
            sqlQuery = getSQLBatchInsertIntoDerivedAttributeTable(derivedAttributeEncoding, map);
            handler.executeUpdate(sqlQuery);

        }
        catch (SQLException ex) {
            throw new IllegalStateException("Couldn't create table for derived attribute "
                                            + derivedAttributeName + " (enc: " + derivedAttributeEncoding + " )"
                                            + ", sqlQuery: " + sqlQuery + "; " + ex);
        }
        return Long.parseLong(derivedAttributeEncoding);
    }

    /**
     * @param str String that I want to put into dictionary
     *
     * @return encoding of string in dictionary
     */
    protected long getDictionaryValue(String str) {
        String sqlQuery = null;
        String encoding = null;

        try {
            sqlQuery = "SELECT key from dictionary where value = '\"" + str + "\"'";
            encoding = handler.getSQLSelectSingleResult(sqlQuery);

            if (encoding.equals("-1")) { // if the attribute is not in the dictionary add it
                sqlQuery = "INSERT INTO dictionary(value,cleanvalue) VALUES ('\"" + str + "\"','" + str + "')";
                handler.executeUpdate(sqlQuery);
            }
            // get the encoding of the derived attribute
            sqlQuery = "SELECT key from dictionary where value = '\"" + str + "\"'";
            encoding = handler.getSQLSelectSingleResult(sqlQuery);

        }
        catch (SQLException ex) {
            throw new IllegalStateException("Couldn't insert object into dictionary: " + sqlQuery + "; " + ex);
        }
        return Long.parseLong(encoding);
    }

    private String getSQLBatchInsertIntoDerivedAttributeTable(
            String derivedAttribute, ArrayList<Pair<String, Long>> results) {
        StringBuilder stringBuilder = new StringBuilder();
        String prefix = "";
        stringBuilder.append("INSERT INTO t_").append(derivedAttribute).append("(s, o) VALUES ");
        for (Pair<String, Long> entry: results) {
            stringBuilder.append(prefix);
            prefix = ", ";
            stringBuilder.append("(").append(entry.getFirst()).append(", ").append(entry.getSecond()).append(")");
        }
        return stringBuilder.toString();
    }

    private HashMap<String, String> getSubjectObject() {
        String sqlQuery = "select s, cleanvalue as o from t_" + originalAttributeEncoding + " join dictionary on o=key";
        HashMap<String, String> res;
        try {
            res = handler.getResultSelectFromSubjectAndObject(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }
        return res;
    }
}
