//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.datastructures.estimation.EarlyStopSample;
import fr.inria.cedar.spade.datastructures.estimation.Estimator;
import fr.inria.cedar.spade.datastructures.estimation.MultidimensionalAggregateEstimate;
import fr.inria.cedar.spade.molapForRDF.AggregateInLattice;
import fr.inria.cedar.spade.molapForRDF.AggregateResultManager;
import fr.inria.cedar.spade.molapForRDF.MOLAPMeasures;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class TopKSelectorForMultidimensionalSingleAggregateEstimates {
    private static final Logger LOGGER = Logger.getLogger(TopKSelectorForMultidimensionalSingleAggregateEstimates.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected InterestingnessFunction interestingnessFunction;
    protected HashSet<MultidimensionalAggregateEstimate> aggregateEstimates;
    protected ArrayList<Estimator> rankedSingleAggregateEstimators;

    public TopKSelectorForMultidimensionalSingleAggregateEstimates(
            DatabaseHandler handler,
            HashMap<AnalyzedAttribute, ArrayList<String>> distinctValuesPerDimension,
            LinkedList<EarlyStopSample> aggregatesWithSamples,
            InterestingnessFunction interestingnessFunction,
            HashMap<String, String> dictionaryFromFilenameToSHA1,
            AggregateResultManager aggregateResultManager) {
        this.interestingnessFunction = interestingnessFunction;
        this.aggregateEstimates = new HashSet<>();
        this.rankedSingleAggregateEstimators = new ArrayList<>();

        AggregateInLattice aggregate;
        HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionForEachMeasure;
        AnalyzedAttribute measure;
        LinkedList<AggregationFunction> aggregationFunctions;
        StringBuilder stringBuilder;
        String filename;
        MultidimensionalAggregateEstimate aggregateEstimate;
        HashMap<AggregateInLattice, MultidimensionalAggregateEstimate> aggregateEstimatesByAggregate = new HashMap<>();
        boolean aggregationFunctionSupportedByEarlyStop;
        Estimator singleAggregateEstimator;

        for (EarlyStopSample sample: aggregatesWithSamples) {
            aggregate = sample.getAggregateInLattice();
            aggregationFunctionForEachMeasure = aggregate.getAggregationFunctionsForEachMeasure();
            for (Map.Entry<AnalyzedAttribute, LinkedList<AggregationFunction>> entry: aggregationFunctionForEachMeasure.entrySet()) {
                measure = entry.getKey();
                if (measure == null) { // skip no-measure single aggregates
                    continue;
                }
                aggregationFunctions = entry.getValue();
                for (AggregationFunction aggregationFunction: aggregationFunctions) {
                    switch (interestingnessFunction) {
                        case VAR_FEATURE_SCALING:
                        case VAR_SUM:
                        case VAR_MEAN:
                        case SKEWNESS:
                        case KURTOSIS:
                            switch (aggregationFunction) {
                                case MIN:
                                case MAX:
                                case SUM:
                                case AVERAGE:
                                    aggregationFunctionSupportedByEarlyStop = true;
                                    break;
                                default:
                                    aggregationFunctionSupportedByEarlyStop = false;
                            }
                            break;
                        default:
                            aggregationFunctionSupportedByEarlyStop = false;
                    }
                    if (!aggregationFunctionSupportedByEarlyStop) {
                        //LOGGER.info("Skipping the early-stop evaluation of " + aggregate + ": early-stop cannot handle its aggregation function or the interesingness function");
                        continue;
                    }
                    stringBuilder = aggregate.getFilenameByAggregationFunctionAndMeasurePrefix();
                    filename = aggregate.getFilenameByAggregationFunctionAndMeasure(stringBuilder, aggregationFunction, measure);
                    if (dictionaryFromFilenameToSHA1.containsKey(filename)) { // already exists in the dictionary => already computed in another lattice
                        continue;
                    }

                    if (aggregateEstimatesByAggregate.containsKey(aggregate)) {
                        aggregateEstimate = aggregateEstimatesByAggregate.get(aggregate);
                    }
                    else {
                        aggregateEstimate = new MultidimensionalAggregateEstimate(handler, aggregate, sample);
                        aggregateEstimatesByAggregate.put(aggregate, aggregateEstimate);
                    }
                    aggregateEstimates.add(aggregateEstimate);

                    singleAggregateEstimator = aggregateEstimate.addEstimator(aggregateEstimate, measure, aggregationFunction, interestingnessFunction, 0.95);
                    rankedSingleAggregateEstimators.add(singleAggregateEstimator);
                }
            }
        }
    }

    public void setMeasures(MOLAPMeasures measures) {
        for (MultidimensionalAggregateEstimate aggregateEstimate: aggregateEstimates) {
            aggregateEstimate.setMeasures(measures);
        }
    }

    public int size() {
        return rankedSingleAggregateEstimators.size();
    }

    protected static class NewEstimatorComparator
            implements
            Comparator<Estimator> {
        @Override
        public int compare(Estimator estimator1,
                           Estimator estimator2) {
            boolean agg1IsComparable = estimator1.isComparable();
            boolean agg2IsComparable = estimator2.isComparable();

            Double score1 = null;
            Double score2 = null;
            if (agg1IsComparable) {
                score1 = estimator1.getPointEstimate();
            }
            if (agg2IsComparable) {
                score2 = estimator2.getPointEstimate();
            }

            if (score1 == null && score2 == null) {
                return 0;
            }
            if (score1 == null) {
                return 1;
            }
            if (score2 == null) {
                return -1;
            }

            return -score1.compareTo(score2);
        }
    }

    protected void sortAggregates() {
        Collections.sort(rankedSingleAggregateEstimators, new NewEstimatorComparator());
    }

    /*
        Returns kth aggregate. Returns kth highest score aggregate if
        sortAggregate method was called beforehand.
     */
    public Estimator getKthSingleAggregateEstimator() {
        return rankedSingleAggregateEstimators.size() < GlobalSettings.K_FOR_TOP_K ? null : rankedSingleAggregateEstimators.get(GlobalSettings.K_FOR_TOP_K - 1);
    }

    /*
        Returns last aggregate. Returns last scored non-null aggregate if
        sortAggregate method was called beforehand.
     */
    public Estimator getLastSingleAggregateEstimator() {
        return rankedSingleAggregateEstimators.isEmpty() ? null : rankedSingleAggregateEstimators.get(rankedSingleAggregateEstimators.size() - 1);
    }

    /*
        Returns all aggregates in arbitrary order. Returns sorted aggregates if
        sortAggregate method was called beforehand.
     */
    public ArrayList<Estimator> getAllSingleAggregateEstimators() {
        return rankedSingleAggregateEstimators;
    }

    @Override
    public String toString() {
        sortAggregates();

        StringBuilder result = new StringBuilder();
        int i = 1;
        for (Estimator aggregate: rankedSingleAggregateEstimators) {
            result.append(i).append(". ").append(aggregate.toString()).append("\n");
            i++;
        }
        if (rankedSingleAggregateEstimators.isEmpty()) {
            result.append("empty");
        }

        return result.toString();
    }

    public String toShortString(long startTime, long endTime, int iteration,
                                int iterationsWithoutStoppage,
                                boolean useFilename) {
        sortAggregates();

        StringBuilder result = new StringBuilder();
        int i = 1;
        for (Estimator aggregate: rankedSingleAggregateEstimators) {
            result.append(i).append(". ").append(aggregate.toShortString(startTime, endTime, iteration, iterationsWithoutStoppage, useFilename)).append("\n");
            i++;
        }
        if (rankedSingleAggregateEstimators.isEmpty()) {
            result.append("empty");
        }

        return result.toString();
    }
}
