//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class CountAttributeDerivation extends AttributeDerivation {
    private static final Logger LOGGER = Logger.getLogger(CountAttributeDerivation.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public CountAttributeDerivation(long originalAttributeEncoding,
                                    DatabaseHandler handler) {
        this.originalAttributeEncoding = originalAttributeEncoding;
        this.handler = handler;
    }

    // compute the counts for derived attribute; give a name to the attribute and insert it into the dictionary;
    // create a table for the derived attribute and insert in it the found counts
    @Override
    public long derive() {
        String sqlQuery = null;
        String derivedAttributeName;
        String derivedAttributeEncoding;
        try {
            derivedAttributeName = GlobalSettings.DERIVED_ATTRIBUTE_PREFIX + "CountOf" + originalAttributeEncoding + ">";

            // check if the attribute is already in the dictionary
            sqlQuery = "SELECT key from dictionary where value = $dagger$" + derivedAttributeName + "$dagger$";
            derivedAttributeEncoding = handler.getSQLSelectSingleResult(sqlQuery);
            if (!derivedAttributeEncoding.equals("-1")) {
                return Long.parseLong(derivedAttributeEncoding);
            }

            // if the attribute is not already in the dictionary get its results and, if there are some results, create the derived attribute
            // find, for each subject in the CFS having the given attribute, the number of associated objects
            sqlQuery = "SELECT s, COUNT(*) FROM t_" + originalAttributeEncoding + " group by s";
            HashMap<String, Integer> result = handler.getResultSelectFromSubjectAndCount(sqlQuery);

            if (!result.isEmpty()) {
                sqlQuery = "INSERT INTO dictionary(value,cleanvalue) VALUES ($dagger$" + derivedAttributeName + "$dagger$, '" + derivedAttributeName + "')";
                handler.executeUpdate(sqlQuery);

                // get the encoding of the derived attribute
                sqlQuery = "SELECT key from dictionary where value = $dagger$" + derivedAttributeName + "$dagger$";
                derivedAttributeEncoding = handler.getSQLSelectSingleResult(sqlQuery);

                // create a new table in the database to contain the pairs subject, object of the newly derived attribute
                sqlQuery = "CREATE TABLE IF NOT EXISTS t_" + derivedAttributeEncoding + "(s INTEGER NOT NULL, "
                           + " o INTEGER NOT NULL) WITH (OIDS=FALSE);"
                           + " CREATE UNIQUE INDEX t_" + derivedAttributeEncoding + "_i_so"
                           + " ON t_" + derivedAttributeEncoding + " USING btree(s, o)"
                           + " TABLESPACE pg_default;"
                           + " CREATE UNIQUE INDEX t_" + derivedAttributeEncoding + "_i_os"
                           + " ON t_" + derivedAttributeEncoding + " USING btree(o, s)"
                           + " TABLESPACE pg_default;";
                handler.executeUpdate(sqlQuery);
                sqlQuery = getSQLBatchInsertIntoDerivedAttributeTable(derivedAttributeEncoding, result);
                handler.executeUpdate(sqlQuery);

                LOGGER.info("Derived attribute " + derivedAttributeName + " with encoding " + derivedAttributeEncoding);
                return Long.parseLong(derivedAttributeEncoding);
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Unable to create the derived attribute: " + sqlQuery + "; " + ex);
        }
        return -1;
    }

    protected String getSQLBatchInsertIntoDerivedAttributeTable(
            String derivedAttribute, HashMap<String, Integer> results) {
        StringBuilder stringBuilder = new StringBuilder();
        String prefix = "";
        stringBuilder.append("INSERT INTO t_").append(derivedAttribute).append("(s, o) VALUES ");
        for (Map.Entry<String, Integer> entry: results.entrySet()) {
            stringBuilder.append(prefix);
            prefix = ", ";
            stringBuilder.append("(").append(entry.getKey()).append(", ").append(entry.getValue()).append(")");
        }
        return stringBuilder.toString();
    }
}
