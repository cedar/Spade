//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.operations;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import fr.inria.cedar.spade.datastructures.SuggestedDerivation;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class AttributeEnumeration extends Operation {
    private static final Logger LOGGER = Logger.getLogger(AttributeEnumeration.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public LinkedList<Long> getOriginalProperties() {
        LinkedList<Long> originalProperties = new LinkedList<>();
        String sqlQuery = "select distinct p from encoded_triples where p <> "
                          + GlobalSettings.TYPE_PROPERTY_ENCODING;
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                originalProperties.add(rs.getLong(1));
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Unable to compute distinct outgoing properties based on the triple table; " + sqlQuery + "; " + ex);
        }

        return originalProperties;
    }

    public LinkedList<Long> getOriginalPropertiesUsingSummary() {
        LinkedList<Long> originalProperties = new LinkedList<>();
        String sqlQuery = "select distinct p from "
                          + GlobalSettings.SUMMARY_TABLE_NAME
                          + " where p <> " + GlobalSettings.TYPE_PROPERTY_ENCODING;
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                originalProperties.add(rs.getLong(1));
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Unable to compute distinct outgoing properties based on the summary table; " + sqlQuery + "; " + ex);
        }

        return originalProperties;
    }

    protected void addDerivationConditions(StringBuilder sqlQuery) {
        boolean addOr = false;

        if (GlobalSettings.ENABLE_COUNT_DERIVATION) {
            addOr = true;
            sqlQuery.append("a.derivationtype = '")
                    .append(SuggestedDerivation.COUNT_ATTR)
                    .append("'");
        }

        if (GlobalSettings.ENABLE_PATH_DERIVATION) {
            if (addOr) {
                sqlQuery.append(" or ");
            }
            addOr = true;
            sqlQuery.append("(a.derivationtype = '")
                    .append(SuggestedDerivation.PATH).append("'")
                    .append(" and a.pathlength = ")
                    .append(GlobalSettings.PATH_DERIVATION_LENGTH)
                    .append(")");
        }

        if (GlobalSettings.ENABLE_LANGUAGE_DETECTION) {
            if (addOr) {
                sqlQuery.append(" or ");
            }
            addOr = true;
            sqlQuery.append("a.derivationtype = '")
                    .append(SuggestedDerivation.LANG_DET)
                    .append("'");
        }

        if (GlobalSettings.ENABLE_KWD_EXTRACTION) {
            if (addOr) {
                sqlQuery.append(" or ");
            }
            sqlQuery.append("a.derivationtype = '")
                    .append(SuggestedDerivation.KWD_IN_TEXT)
                    .append("'");
        }
    }

    public abstract LinkedList<Long> getAllAttributes(
            CandidateFactSet candidateFactSet);

    public abstract LinkedList<Long> getAllAttributesUsingSummary(
            CandidateFactSet candidateFactSet);
}
