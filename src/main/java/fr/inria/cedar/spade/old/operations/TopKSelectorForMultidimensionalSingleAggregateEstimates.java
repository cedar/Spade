//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.operations;

import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import fr.inria.cedar.spade.old.datastructures.estimation.MultidimensionalAggregateEstimate;
import fr.inria.cedar.spade.old.datastructures.estimation.MultidimensionalSingleAggregateEstimate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class TopKSelectorForMultidimensionalSingleAggregateEstimates {
    private static final Logger LOGGER = Logger.getLogger(TopKSelectorForMultidimensionalSingleAggregateEstimates.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    /*protected class SetUpRunnable implements Runnable {
		protected ArrayList<MultidimensionalSingleAggregateEstimate> aggs;
		protected DatabaseHandler hand;
		protected SingleAggregate ag;
		protected MultidimensionalAggregateEstimate aggE;
		protected InterestingnessFunction i;
		protected double cl;

		public SetUpRunnable(ArrayList<MultidimensionalSingleAggregateEstimate> aggs, DatabaseHandler hand, SingleAggregate ag, MultidimensionalAggregateEstimate aggE, InterestingnessFunction i, double cl) {
			this.aggs = aggs;
			this.hand = hand;
			this.ag = ag;
			this.aggE = aggE;
			this.i = i;
			this.cl = cl;
		}

		@Override
		public void run() {
			aggs.add(new MultidimensionalSingleAggregateEstimate(hand, ag, aggE, i, cl));
		}
	};*/
    protected int k;
    protected InterestingnessFunction interestingnessFunction;
    protected ArrayList<MultidimensionalSingleAggregateEstimate> rankedAggregates;

    public TopKSelectorForMultidimensionalSingleAggregateEstimates(
            DatabaseHandler handler,
            HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregatesToSingleAggregates,
            int k,
            InterestingnessFunction interestingnessFunction,
            ArrayList<MultidimensionalSingleAggregate> aggregates) {
        this.k = k;
        this.interestingnessFunction = interestingnessFunction;
        rankedAggregates = new ArrayList<>();
        HashMap<MultidimensionalAggregate, MultidimensionalAggregateEstimate> aggregateEstimatesByAggregate = new HashMap<>();
        MultidimensionalAggregateEstimate aggEst;
        /*Thread thread;
		//ArrayList<Thread> threads = new ArrayList<>();
		Future submittedThread;
		ArrayList<Future> submittedThreads = new ArrayList<>();
		ExecutorService pool = Executors.newFixedThreadPool(4);*/
        long time = System.currentTimeMillis();
        AnalyzedAttribute measure;
        //AggregationFunction aggregationFunction;
        //boolean aggregationFunctionSupportedByEarlyStop;
        for (MultidimensionalSingleAggregate agg: aggregates) {
            measure = agg.getMeasure();
            if (measure == null) { // skip no-measure single aggregates
                continue;
            }
            /*aggregationFunction = agg.getAggregationFunction();
			switch (interestingnessFunction) {
				case VAR_FEATURE_SCALING:
				case VAR_SUM:
				case VAR_MEAN:
				case SKEWNESS:
				case KURTOSIS:
					switch (aggregationFunction) {
						case MIN:
						case MAX:
						case SUM:
						case AVERAGE:
							aggregationFunctionSupportedByEarlyStop = true;
							break;
						default:
							aggregationFunctionSupportedByEarlyStop = false;
					}
					break;
				default:
					aggregationFunctionSupportedByEarlyStop = false;
			}
			if (!aggregationFunctionSupportedByEarlyStop) {
				LOGGER.info("Skipping early-stop evaluation of " + agg + "\nEarly-stop cannot handle its aggregation function or the interesingness function.");
				continue;
			}*/

            if (aggregateEstimatesByAggregate.containsKey(agg.getAggregate())) {
                aggEst = aggregateEstimatesByAggregate.get(agg.getAggregate());
            }
            else {
                aggEst = new MultidimensionalAggregateEstimate(handler, agg.getAggregate());
                aggregateEstimatesByAggregate.put(agg.getAggregate(), aggEst);
            }

            MultidimensionalSingleAggregateEstimate msae = new MultidimensionalSingleAggregateEstimate(handler, aggregatesToSingleAggregates, agg, aggEst, interestingnessFunction, 0.95);
            if (msae.hasDataToAggregateOn()) {
                rankedAggregates.add(msae);
            }
            else {
                MultidimensionalAggregate key = agg.getAggregate();
                HashSet<MultidimensionalSingleAggregate> singleAggregates = aggregatesToSingleAggregates.get(key);
                singleAggregates.remove(agg);
                if (singleAggregates.isEmpty()) {
                    aggregatesToSingleAggregates.get(key).remove(agg);
                    if (aggregatesToSingleAggregates.get(key).isEmpty()) {
                        aggregatesToSingleAggregates.remove(key);
                        key.dropAuxiliaryTable();
                    }
                    aggregateEstimatesByAggregate.remove(key);
                }
            }
        }
        /*thread = new Thread(new SetUpRunnable(rankedAggregates, handler, agg, aggEst, interestingnessFunction, 0.95));
				//threads.add(thread);
				//pool.execute(thread);
				submittedThread = pool.submit(thread);
				submittedThreads.add(submittedThread);
			}
		}
		//for (Thread th: threads) {
		for (Future th: submittedThreads) {
			try {
				//th.join();
				th.get();
			}
			catch (InterruptedException | ExecutionException ex) {
				throw new IllegalStateException(ex);
			}
		}*/
        LOGGER.info("Early-stop setup time: " + (System.currentTimeMillis() - time) + " ms");
    }

    protected class MultidimensionalSingleAggregateEstimateComparator implements
            Comparator<MultidimensionalSingleAggregateEstimate> {
        @Override
        public int compare(MultidimensionalSingleAggregateEstimate agg1,
                           MultidimensionalSingleAggregateEstimate agg2) {
            MultidimensionalSingleAggregateEstimate.EstimationStage agg1EstimationStage = agg1.getStage();
            MultidimensionalSingleAggregateEstimate.EstimationStage agg2EstimationStage = agg2.getStage();

            Double score1 = null;
            Double score2 = null;
            if (!agg1EstimationStage.equals(MultidimensionalSingleAggregateEstimate.EstimationStage.INCOMPARABLE_ESTIMATE)) {
                score1 = agg1.getPointEstimate();
            }
            if (!agg2EstimationStage.equals(MultidimensionalSingleAggregateEstimate.EstimationStage.INCOMPARABLE_ESTIMATE)) {
                score2 = agg2.getPointEstimate();
            }

            if (score1 == null && score2 == null) {
                return 0;
            }
            if (score1 == null) {
                return 1;
            }
            if (score2 == null) {
                return -1;
            }

            return -score1.compareTo(score2);
        }
    }

    protected void sortAggregates() {
        Collections.sort(rankedAggregates, new MultidimensionalSingleAggregateEstimateComparator());
    }

    /*
		Returns kth aggregate. Returns kth highest score aggregate if
		sortAggregate method was called beforehand.
     */
    public MultidimensionalSingleAggregateEstimate getKthAggregate() {
        if (rankedAggregates.size() >= k) {
            return rankedAggregates.get(k - 1);
        }

        return null;
    }

    /*
		Returns all aggregates in arbitrary order. Returns sorted aggregates if
		sortAggregate method was called beforehand.
     */
    public ArrayList<MultidimensionalSingleAggregateEstimate> getAggregates() {
        return rankedAggregates;
    }

    /*
		Returns top-k aggregates. Aggregates must be sorted beforehand by a call
		to sortAggregate method.
     */
    public ArrayList<MultidimensionalSingleAggregate> getTopKFullyEvaluatedAggregates() {
        ArrayList<MultidimensionalSingleAggregate> topK = new ArrayList<>();

        int i = 0;
        Double lastScore = null;
        Double currentScore;
        MultidimensionalSingleAggregateEstimate aggEst;
        MultidimensionalSingleAggregate agg;
        for (Iterator<MultidimensionalSingleAggregateEstimate> iter = rankedAggregates.iterator(); iter.hasNext();) {
            aggEst = iter.next();
            if (aggEst.getStage() != MultidimensionalSingleAggregateEstimate.EstimationStage.FULLY_EVALUATED) {
                continue;
            }

            agg = aggEst.getMultidimensionalSingleAggregate();
            currentScore = agg.getInterestingnessFunctionScore(interestingnessFunction);

            // cut after k but don't break ties: keep k-th ex aequo
            if (currentScore.equals(lastScore) || i < k) {
                topK.add(agg);
                lastScore = currentScore;
                i++;
            }
            else {
                break;
            }
        }

        return topK;
    }

    @Override
    public String toString() {
        sortAggregates();

        StringBuilder result = new StringBuilder();
        int i = 1;
        for (MultidimensionalSingleAggregateEstimate aggregate: rankedAggregates) {
            result.append(i).append(". ").append(aggregate.toString()).append("\n");
            i++;
        }
        if (rankedAggregates.isEmpty()) {
            result.append("empty");
        }

        return result.toString();
    }

    public String toShortString(long startTime, long endTime) {
        sortAggregates();

        StringBuilder result = new StringBuilder();
        int i = 1;
        for (MultidimensionalSingleAggregateEstimate aggregate: rankedAggregates) {
            result.append(i).append(". ").append(aggregate.getMultidimensionalSingleAggregate().toShortString(startTime, endTime)).append("\n");
            i++;
        }
        if (rankedAggregates.isEmpty()) {
            result.append("empty");
        }

        return result.toString();
    }
}
