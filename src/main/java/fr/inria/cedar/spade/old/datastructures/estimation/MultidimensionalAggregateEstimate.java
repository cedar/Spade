//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures.estimation;

import fr.inria.cedar.DataProfiler.items.Item;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import fr.inria.cedar.spade.datastructures.SuggestedDerivation;
import fr.inria.cedar.spade.old.operations.MultidimensionalAggregateEvaluationWithEarlyStop;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MultidimensionalAggregateEstimate {
    private static final Logger LOGGER = Logger.getLogger(MultidimensionalAggregateEstimate.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected final DatabaseHandler handler;
    protected final MultidimensionalAggregate aggregate;
    protected String tableName;
    protected HashMap<ArrayList<String>, ResultSet> cursorsByGroup;
    protected HashMap<ArrayList<String>, ArrayList<Double>> retrievedTuplesByGroup;
    protected ArrayList<ArrayList<String>> lastUpdatedGroups;
    protected Integer numberOfTuplesInLastSampling;
    protected Integer m;
    protected HashMap<AggregationFunction, HashMap<InterestingnessFunction, Estimator>> estimators;

    public MultidimensionalAggregateEstimate(DatabaseHandler handler,
                                             MultidimensionalAggregate aggregate) {
        this.handler = handler;
        this.aggregate = aggregate;
        cursorsByGroup = new HashMap<>();
        retrievedTuplesByGroup = new HashMap<>();
        estimators = new HashMap<>();
        numberOfTuplesInLastSampling = null;
    }

    // reference: https://www.baeldung.com/sha-256-hashing-java
    protected static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        String hex;

        for (int i = 0; i < hash.length / 2; i++) {
            hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) { // only one digit
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }

    protected static String generateTableNameAsHash(String tableName) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(tableName.getBytes());
        byte[] bytes = messageDigest.digest();

        return bytesToHex(bytes);
    }

    // create a table (d1, d2, ..., dN, m) corresponding to the aggregate and index it on dimensions
    protected final void createAuxiliaryTable() {
        if (aggregate.getAuxiliaryTableName() != null) {
            return;
        }

        ArrayList<AnalyzedAttribute> dimensions = aggregate.getDimensions();
        StringBuilder dimensionsStringBuilder = new StringBuilder();
        String prefix = "";
        long measureEncoding = aggregate.getMeasure().getEncoding();
        for (AnalyzedAttribute dim: dimensions) {
            long dimensionEncoding = dim.getEncoding();
            dimensionsStringBuilder.append(prefix).append(dimensionEncoding);
            prefix = ", ";
        }
        String dimensionsString = dimensionsStringBuilder.toString();

        try {
            // candidate fact set encoding + "_" + MD5 hash of dimensions and measure encodings concatenated
            // shortened for the purpose of avoiding length constraints on the table name
            tableName = aggregate.getCandidateFactSet().getTableName() + "_" + generateTableNameAsHash(dimensionsString + measureEncoding);

            String sqlQuery = "create table if not exists " + tableName + " as";
            // At the moment no primary key is set, it may be done, but it seems like there's no need.
            // A reference in case: https://stackoverflow.com/questions/11176383/create-table-as-with-primary-key-in-one-statement-postgresql

            String selectPart = " select ";
            if (aggregate.getMeasure().getDerivedFrom() == null
                || aggregate.getMeasure().getDerivationType() != SuggestedDerivation.COUNT_ATTR) { // not derived or not count
                selectPart += "dict.value as m";
            }
            else {
                selectPart += "t_" + measureEncoding + ".o as m";
            }
            String fromPart = "";
            sqlQuery += MultidimensionalAggregateEvaluationWithEarlyStop.constructBodyOfAggregateQuery(aggregate, selectPart, fromPart);

            try {
                handler.executeUpdate(sqlQuery);
            }
            catch (SQLException ex) {
                throw new IllegalStateException(ex + "; query: " + sqlQuery);
            }
            aggregate.setAuxiliaryTableName(tableName);
        }
        catch (NoSuchAlgorithmException ex) {
            throw new IllegalStateException(ex);
        }
    }

    protected static String groupListToString(ArrayList<String> groupList) {
        StringBuilder groupListStringBuilder = new StringBuilder();
        String prefix = "";
        for (String columnValue: groupList) {
            groupListStringBuilder.append(prefix);
            groupListStringBuilder.append(columnValue);
            prefix = ", ";
        }

        return groupListStringBuilder.toString();
    }

    protected final void indexAuxiliaryTableAndOpenCursorsByGroup() {
        String sqlQuery = null;
        try {
            int numberOfDimensions = aggregate.getDimensions().size();
            String dimensionList = aggregate.getDimensionList();

            // index d column
            sqlQuery = "create index idx_" + tableName + " on " + tableName + "(" + dimensionList + ")";
            handler.executeUpdate(sqlQuery);

            // find min value
            sqlQuery = "select " + dimensionList + " from " + tableName + " order by " + dimensionList + " limit 1";
            ResultSet nextGroupResultSet = handler.getResultSet(sqlQuery);
            ArrayList<String> groupList = new ArrayList<>();
            nextGroupResultSet.next();
            for (int i = 1; i <= numberOfDimensions; i++) {
                groupList.add(Long.toString(nextGroupResultSet.getLong("d" + i)));
            }
            boolean nextGroup = true;
            String groupListString;
            do {
                // open cursors corresponding to the key
                groupListString = groupListToString(groupList);
                sqlQuery = "select m from " + tableName + " where (" + dimensionList + ") = (" + groupListString + ")";
                cursorsByGroup.put(groupList, handler.getResultSet(sqlQuery));
                retrievedTuplesByGroup.put(groupList, new ArrayList<>());

                // find the next group, reference: https://stackoverflow.com/questions/52514329/sql-how-to-disable-result-of-aggregate-on-empty-table
                sqlQuery = "select " + dimensionList + " from " + tableName
                           + " where (" + dimensionList + ") > (" + groupListString + ")"
                           + " order by " + dimensionList + " limit 1";
                nextGroupResultSet = handler.getResultSet(sqlQuery);
                groupList = new ArrayList<>();
                if (nextGroupResultSet.next()) {
                    for (int i = 1; i <= numberOfDimensions; i++) {
                        groupList.add(Long.toString(nextGroupResultSet.getLong("d" + i)));
                    }
                }
                else {
                    nextGroup = false;
                }
            }
            while (nextGroup);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Early-stop indexing failed with the query: " + sqlQuery + "; " + ex);
        }
    }

    protected final boolean tableHasData() {
        String sqlQuery = "select 1 from " + tableName + " limit 1";
        try {
            ResultSet firstTuple = handler.getResultSet(sqlQuery);
            if (firstTuple.next()) {
                return true;
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }

        return false;
    }

    protected final boolean setUpIndexStriding() {
        createAuxiliaryTable();

        boolean dataToAggregateOnIsPresent = tableHasData();

        if (!dataToAggregateOnIsPresent) {
            return false;
        }

        indexAuxiliaryTableAndOpenCursorsByGroup();

        return true;
    }

    protected void addEstimator(AggregationFunction af,
                                InterestingnessFunction interestingnessFunction,
                                Estimator estimator) {
        if (!estimators.containsKey(af)) {
            estimators.put(af, new HashMap<>());
        }
        estimators.get(af).put(interestingnessFunction, estimator);
    }

    public boolean addEstimator(
            HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates,
            MultidimensionalSingleAggregate agg,
            InterestingnessFunction interestingnessFunction,
            double confidenceLevel) {
        //synchronized(this) {
        if (estimators.isEmpty()) {
            if (!setUpIndexStriding()) {
                return false;
            }
        }
        //}

        Estimator estimator = null;
        AggregationFunction aggregationFunction = agg.getAggregationFunction();
        switch (interestingnessFunction) {
            case VAR_FEATURE_SCALING:
            case VAR_SUM:
            case VAR_MEAN:
            case SKEWNESS:
            case KURTOSIS:
                switch (aggregationFunction) {
                    case MIN:
                        estimator = new InterestingnessFunctionOverMinEstimator(handler, aggregates, agg, interestingnessFunction, confidenceLevel, cursorsByGroup, retrievedTuplesByGroup, tableName);
                        break;
                    case MAX:
                        estimator = new InterestingnessFunctionOverMaxEstimator(handler, aggregates, agg, interestingnessFunction, confidenceLevel, cursorsByGroup, retrievedTuplesByGroup, tableName);
                        break;
                    case SUM:
                        estimator = new InterestingnessFunctionOverSumEstimator(handler, aggregates, agg, interestingnessFunction, confidenceLevel, cursorsByGroup, retrievedTuplesByGroup, tableName);
                        break;
                    case AVERAGE:
                        estimator = new InterestingnessFunctionOverAverageEstimator(handler, aggregates, agg, interestingnessFunction, confidenceLevel, cursorsByGroup, retrievedTuplesByGroup, tableName);
                        break;
                }
                break;
            default:
                return false;
        }

        addEstimator(aggregationFunction, interestingnessFunction, estimator);

        return true;
    }

    protected LinkedList<AggregationFunction> getAggregateFunctions(
            HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates,
            MultidimensionalAggregate aggregate) {
        HashSet<MultidimensionalSingleAggregate> singleAggregates = aggregates.get(aggregate);
        LinkedList<AggregationFunction> aggregationFunctions = new LinkedList<>();

        for (MultidimensionalSingleAggregate singleAggregate: singleAggregates) {
            aggregationFunctions.add(singleAggregate.getAggregationFunction());
        }

        return aggregationFunctions;
    }

    public void removeEstimator(
            HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates,
            AggregationFunction af,
            InterestingnessFunction interestingnessFunction) {
        estimators.get(af).remove(interestingnessFunction);
        if (estimators.get(af).isEmpty()) {
            estimators.remove(af);
        }

        // drop auxilary table if it's not going to be used anymore
        if (estimators.isEmpty()) {
            LinkedList<AggregationFunction> aggregationFunctions = getAggregateFunctions(aggregates, aggregate);
            // table can't be reused in regular evaluation
            if (!aggregationFunctions.contains(AggregationFunction.COUNT_STAR)
                && !aggregationFunctions.contains(AggregationFunction.COUNT)
                && !aggregationFunctions.contains(AggregationFunction.COUNT_DISTINCT)) {
                aggregate.dropAuxiliaryTable(); // drops materialized table used for purpose of Index Striding
            }
        }
    }

    public Estimator getEstimator(AggregationFunction af,
                                  InterestingnessFunction interestingnessFunction) {
        return estimators.get(af).get(interestingnessFunction);
    }

    /*
		Returns number of tuples retrieved in this sampling.
     */
    public void sampleFromEachGroup() {
        lastUpdatedGroups = new ArrayList<>();

        ResultSet rs;
        String value;
        int numberOfRetrievalsPerGroup = 10;
        int i;
        for (ArrayList<String> group: cursorsByGroup.keySet()) {
            rs = cursorsByGroup.get(group);
            i = 0;
            try {
                while (rs.next() && i < numberOfRetrievalsPerGroup) {
                    value = rs.getString("m");
                    if (value.startsWith("\"")) {
                        value = Item.contentInLiteral(value);
                    }
                    try {
                        double numericalValue = Double.parseDouble(value);
                        retrievedTuplesByGroup.get(group).add(numericalValue);
                        lastUpdatedGroups.add(group);
                        i++;
                    }
                    catch (NumberFormatException ex) {
                        // skip this value, it's not numerical
                    }
                }
            }
            catch (SQLException ex) {
                throw new IllegalStateException(ex);
            }
        }

        numberOfTuplesInLastSampling = lastUpdatedGroups.size(); // the count of retrieved tuples
    }

    public Integer getNumberOfTuplesInLastSampling() {
        return numberOfTuplesInLastSampling;
    }
}
