//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures;

import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class PairAnalyzedAttributeLevel {
    private static final Logger LOGGER = Logger.getLogger(PairAnalyzedAttributeLevel.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected AnalyzedAttribute attribute;
    protected int level;

    public PairAnalyzedAttributeLevel(AnalyzedAttribute attribute, int level) {
        this.attribute = attribute;
        this.level = level;
    }

    public AnalyzedAttribute getAnalyzedAttribute() {
        return attribute;
    }

    public int getLevel() {
        return level;
    }
}
