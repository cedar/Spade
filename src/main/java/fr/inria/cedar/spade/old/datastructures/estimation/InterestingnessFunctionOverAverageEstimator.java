//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures.estimation;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.database.ResultRow;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class InterestingnessFunctionOverAverageEstimator extends DeltaMethodEstimator {
    private static final Logger LOGGER = Logger.getLogger(InterestingnessFunctionOverAverageEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public InterestingnessFunctionOverAverageEstimator(
            DatabaseHandler handler,
            HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates,
            MultidimensionalSingleAggregate aggregate,
            InterestingnessFunction interestingnessFunction,
            Double confidenceLevel,
            HashMap<ArrayList<String>, ResultSet> cursorsByGroup,
            HashMap<ArrayList<String>, ArrayList<Double>> retrievedTuplesByGroup,
            String tableName) {
        super(handler, aggregates, aggregate, interestingnessFunction, confidenceLevel, cursorsByGroup, retrievedTuplesByGroup, tableName);
    }

    @Override
    protected void precomputeStatisticsOnGroups() {
        D = retrievedTuplesByGroup.size(); // number of groups
        averages = new Double[D]; // average of retrieved elements of each group
        variances = new Double[D]; // variance of retrieved elements of each group
        ArrayList<Double> valuesInGroup; // elements in current group
        int numberOfElementsInGroup; // number of elements in current group
        Double[] values; // all the retrieved values in current group (always of size numberOfElementsInGroup)
        m = null; // the highest number of retrieved elements among all the groups
        int i = 0;
        for (ArrayList<String> group: retrievedTuplesByGroup.keySet()) {
            valuesInGroup = retrievedTuplesByGroup.get(group);
            numberOfElementsInGroup = valuesInGroup.size();
            values = new Double[numberOfElementsInGroup];
            valuesInGroup.toArray(values);

            averages[i] = EfficientComputations.mean(values);
            variances[i] = EfficientComputations.varianceWithKnownMean(values, averages[i]);

            if (m == null || m < valuesInGroup.size()) {
                m = valuesInGroup.size();
            }

            i++;
        }
    }

    @Override
    protected void updateEstimatesForFeatureScalingNormalizedVariance() {
        precomputeStatisticsOnGroups();

        Double[] normalizedValues = EfficientComputations.normalizeByFeatureScaling(averages);
        pointEstimate = EfficientComputations.variance(normalizedValues);

        computeDeltaMethodConfidenceIntervalForVariance();
    }

    @Override
    protected void updateEstimatesForSumNormalizedVariance() {
        precomputeStatisticsOnGroups();

        pointEstimate = EfficientComputations.varianceOnRawValuesNormalizedByDivisionBySum(averages);

        computeDeltaMethodConfidenceIntervalForVariance();
    }

    @Override
    protected void updateEstimatesForMeanNormalizedVariance() {
        precomputeStatisticsOnGroups();

        pointEstimate = EfficientComputations.varianceOnRawValuesNormalizedByDivisionByMean(averages);

        computeDeltaMethodConfidenceIntervalForVariance();
    }

    @Override
    protected void updateEstimatesForSkewness() {
        precomputeStatisticsOnGroups();

        pointEstimate = EfficientComputations.skewness(averages);

        computeDeltaMethodConfidenceIntervalForSkewness();
    }

    @Override
    protected void updateEstimatesForKurtosis() {
        precomputeStatisticsOnGroups();

        pointEstimate = EfficientComputations.kurtosis(averages);

        computeDeltaMethodConfidenceIntervalForKurtosis();
    }

    @Override
    public ArrayList<ResultRow> getResult() {
        ArrayList<ResultRow> result = new ArrayList<>();
        for (ArrayList<String> group: retrievedTuplesByGroup.keySet()) {
            ArrayList<Double> valuesInGroup = retrievedTuplesByGroup.get(group);
            Double[] values = new Double[valuesInGroup.size()];
            valuesInGroup.toArray(values);

            result.add(new ResultRow(group, EfficientComputations.mean(values)));
        }

        return result;
    }
}
