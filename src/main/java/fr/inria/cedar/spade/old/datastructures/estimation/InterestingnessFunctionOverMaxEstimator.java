//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures.estimation;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.database.ResultRow;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class InterestingnessFunctionOverMaxEstimator extends Estimator {
    private static final Logger LOGGER = Logger.getLogger(InterestingnessFunctionOverMaxEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected double globalMax;

    public InterestingnessFunctionOverMaxEstimator(
            DatabaseHandler handler,
            HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates,
            MultidimensionalSingleAggregate aggregate,
            InterestingnessFunction interestingnessFunction,
            Double confidenceLevel,
            HashMap<ArrayList<String>, ResultSet> cursorsByGroup,
            HashMap<ArrayList<String>, ArrayList<Double>> retrievedTuplesByGroup,
            String tableName) {
        super(handler, aggregates, aggregate, interestingnessFunction, confidenceLevel, cursorsByGroup, retrievedTuplesByGroup, tableName);
        globalMax = aggregate.getAggregate().getMeasure().getMaxValue();
    }

    protected Double[] computeMaxInGroups() {
        ArrayList<Double> valuesInGroup;
        int numberOfElementsInGroup;
        Double[] values;
        D = retrievedTuplesByGroup.size(); // number of groups
        Double[] maxs = new Double[D];
        int i = 0;
        for (ArrayList<String> group: retrievedTuplesByGroup.keySet()) {
            valuesInGroup = retrievedTuplesByGroup.get(group);

            numberOfElementsInGroup = valuesInGroup.size();
            values = new Double[numberOfElementsInGroup];
            valuesInGroup.toArray(values);

            maxs[i] = EfficientComputations.max(values);
            i++;
        }

        return maxs;
    }

    protected void computeIntervalForVariance(Double[] values,
                                              double globalMaxNormalized) {
        lowerBoundEstimate = 0.0;

        double min = EfficientComputations.min(values);

        // Popoviciu's inequality on variance
        double diff = (globalMaxNormalized - min);
        upperBoundEstimate = (diff * diff) / 4;
    }

    @Override
    protected void updateEstimatesForFeatureScalingNormalizedVariance() {
        Double[] maxs = computeMaxInGroups();

        Double[] normalizedValues = EfficientComputations.normalizeByFeatureScaling(maxs);
        pointEstimate = EfficientComputations.variance(normalizedValues);

        double globalMaxNormalized = 1;
        computeIntervalForVariance(normalizedValues, globalMaxNormalized);
    }

    @Override
    protected void updateEstimatesForSumNormalizedVariance() {
        Double[] maxs = computeMaxInGroups();

        double sum = EfficientComputations.sum(maxs);
        Double[] normalizedValues = EfficientComputations.normalizeByDivisionBySumWithKnownSum(maxs, sum);
        pointEstimate = EfficientComputations.variance(normalizedValues);

        double globalMaxNormalized = globalMax / sum;
        computeIntervalForVariance(normalizedValues, globalMaxNormalized);
    }

    @Override
    protected void updateEstimatesForMeanNormalizedVariance() {
        Double[] maxs = computeMaxInGroups();

        double mean = EfficientComputations.mean(maxs);
        Double[] normalizedValues = EfficientComputations.normalizeByDivisionByMeanWithKnownMean(maxs, mean);
        pointEstimate = EfficientComputations.variance(normalizedValues);

        double globalMaxNormalized = globalMax / mean;
        computeIntervalForVariance(normalizedValues, globalMaxNormalized);
    }

    @Override
    protected void updateEstimatesForSkewness() {
        Double[] maxs = computeMaxInGroups();

        pointEstimate = EfficientComputations.skewness(maxs);

        // TODO: compute interval
    }

    @Override
    protected void updateEstimatesForKurtosis() {
        Double[] maxs = computeMaxInGroups();

        pointEstimate = EfficientComputations.kurtosis(maxs);

        // TODO: compute interval
    }

    @Override
    public ArrayList<ResultRow> getResult() {
        ArrayList<ResultRow> result = new ArrayList<>();
        for (ArrayList<String> group: retrievedTuplesByGroup.keySet()) {
            ArrayList<Double> valuesInGroup = retrievedTuplesByGroup.get(group);
            Double[] values = new Double[valuesInGroup.size()];
            valuesInGroup.toArray(values);

            result.add(new ResultRow(group, EfficientComputations.max(values)));
        }

        return result;
    }
}
