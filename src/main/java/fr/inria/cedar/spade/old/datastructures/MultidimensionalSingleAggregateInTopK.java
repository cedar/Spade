//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MultidimensionalSingleAggregateInTopK {
    private static final Logger LOGGER = Logger.getLogger(MultidimensionalSingleAggregateInTopK.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    final protected MultidimensionalSingleAggregate multidimensionalSingleAggregate;
    final protected String fileName;
    final protected Double interestingnessFunctionScore;

    public MultidimensionalSingleAggregateInTopK(
            MultidimensionalSingleAggregate multidimensionalSingleAggregate,
            Double interestingnessFunctionScore) {
        this.multidimensionalSingleAggregate = multidimensionalSingleAggregate;
        fileName = null;
        this.interestingnessFunctionScore = interestingnessFunctionScore;
    }

    public MultidimensionalSingleAggregateInTopK(String fileName,
                                                 Double interestingnessFunctionScore) {
        this.multidimensionalSingleAggregate = null;
        this.fileName = fileName;
        this.interestingnessFunctionScore = interestingnessFunctionScore;
    }

    public MultidimensionalSingleAggregate getMultidimensionalSingleAggregate() {
        return multidimensionalSingleAggregate;
    }

    public String getFileName() {
        return fileName;
    }

    public Double getInterestingnessFunctionScore() {
        return interestingnessFunctionScore;
    }
}
