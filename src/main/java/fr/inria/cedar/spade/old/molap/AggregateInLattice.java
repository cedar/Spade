//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.molap;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.SuggestedDerivation;
import fr.inria.cedar.spade.datastructures.Utils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AggregateInLattice {
    private static final Logger LOGGER = Logger.getLogger(AggregateInLattice.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected Catalog catalog;
    protected ArrayList<AnalyzedAttribute> dimensions;
    protected AnalyzedAttribute dimensionMissingFromFather;
    protected boolean isRoot;
    protected boolean rootIsLoaded;
    protected int flushToDiskRate;
    protected int globalChunkID;
    protected int minimumMemoryCost;
    protected HashMap<AnalyzedAttribute, Integer> minimumMemoryCostPerDimension;
    protected ArrayList<BufferedWriter> aggregateResults;
    protected boolean printHeader;
    protected AggregateInLattice fatherAggregate;
    protected LinkedList<AggregateInLattice> childrenAggregates;
    protected String directoryName;
    protected String filename;
    // Integer refers to a chunk
    // AnalyzedAttribute refers to a measure
    // Aggregation function refers to the afs applied to a given measure
    // double[] contains the data
    protected HashMap<Integer, HashMap<AnalyzedAttribute, HashMap<AggregationFunction, double[]>>> memoryAllocation;
    protected HashMap<Integer, double[]> memoryAllocationForCountStar;
    private int nrOfChunks;
    private int chunkSize;
    // used to avoid allocating memory for ArrayLists
    private int[] fatherMultiIdx;
    private int howManyTimesHaveFlushed;
    private int[] multiIdxChunk;
    private int[] multiIdxOffset;
    private int[] idxForDecoding;

    public AggregateInLattice(ArrayList<AnalyzedAttribute> dimensions,
                              Catalog catalog) {
        this.dimensions = dimensions;
        this.catalog = catalog;
        childrenAggregates = new LinkedList<>();
        aggregateResults = new ArrayList<>();
        howManyTimesHaveFlushed = 0;

        globalChunkID = -1;
        printHeader = true;
        isRoot = false;
        flushToDiskRate = -1;
        nrOfChunks = -1;
        minimumMemoryCost = -1;
        chunkSize = 1;
        for (AnalyzedAttribute d: this.dimensions) {
            chunkSize = chunkSize * catalog.nrOfDistinctValuesInChunkPerDimension.get(d);
        }

        multiIdxChunk = new int[dimensions.size()];
        multiIdxOffset = new int[dimensions.size()];
        idxForDecoding = new int[dimensions.size()];
        String decodedCF;

        try {
            decodedCF = catalog.handler.getResultSelectFromValueColumn("select value from dictionary where key=" + catalog.candidateFactSet.getEncoding()).get(0);
            decodedCF = Utils.getShorterAttributeName(decodedCF);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Decoding CFS failed. " + ex);
        }

        directoryName = GlobalSettings.FLASK_FOLDER_PATH + GlobalSettings.DATABASE_NAME + "/" + decodedCF + "/" + GlobalSettings.INTERESTINGNESS_FUNCTION + "/";
        File directory = new File(directoryName);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        filename = "CFS_" + decodedCF + "_Dimensions";
        for (AnalyzedAttribute ap: dimensions) {
            filename += "_" + ap.getEncoding();
        }
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public ArrayList<AnalyzedAttribute> getDimensions() {
        return dimensions;
    }

    public void setDimensions(ArrayList<AnalyzedAttribute> dimensions) {
        this.dimensions = dimensions;
    }

    public AnalyzedAttribute getDimensionMissingFromFather() {
        return dimensionMissingFromFather;
    }

    public void setDimensionMissingFromFather(AnalyzedAttribute dimension) {
        this.dimensionMissingFromFather = dimension;
    }

    public boolean getIsRoot() {
        return isRoot;
    }

    public void setIsRoot(boolean isRoot) {
        this.isRoot = isRoot;
    }

    public AggregateInLattice getFatherAggregate() {
        return fatherAggregate;
    }

    /*
    * once the aggregate knows who his father is, it can determine the dimension that it is missing from the father.
    * Such dimension is used as an additional measure by the aggregate
    * */
    public void setFatherAggregate(AggregateInLattice father) {
        this.fatherAggregate = father;
        if (fatherAggregate != null) {
            for (AnalyzedAttribute a: father.dimensions) {
                if (!dimensions.contains(a)) {
                    dimensionMissingFromFather = a;
                }
            }
            fatherMultiIdx = new int[father.dimensions.size()];
        }
    }

    public LinkedList<AggregateInLattice> getChildrenAggregates() {
        return childrenAggregates;
    }

    public void setChildrenAggregates(
            LinkedList<AggregateInLattice> childrenAggregates) {
        this.childrenAggregates = childrenAggregates;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public String getFileName() {
        return filename;
    }

    public void setFileName(String fileName) {
        this.filename = fileName;
    }

    protected void setFlushToDiskRate(int flushRate) {
        this.flushToDiskRate = flushRate;
    }

    protected void addChildAggregate(AggregateInLattice child) {
        childrenAggregates.add(child);
    }

    protected void setRoot() {
        isRoot = true;
    }

    protected void setMinimumMemoryCostPerDimension(
            HashMap<AnalyzedAttribute, Integer> cost) {
        this.minimumMemoryCostPerDimension = cost;
        computeNrOfChunks();
    }

    /*
     * compute how many chunks the aggregate needs to have
     * */
    private void computeNrOfChunks() {
        nrOfChunks = 1;
        if (!isRoot) {
            for (Map.Entry<AnalyzedAttribute, Integer> entry: minimumMemoryCostPerDimension.entrySet()) {
                int nrOfChunksPerDimension = entry.getValue() / catalog.nrOfDistinctValuesInChunkPerDimension.get(entry.getKey());
                if (entry.getValue() % catalog.nrOfDistinctValuesInChunkPerDimension.get(entry.getKey()) != 0) {
                    nrOfChunksPerDimension = nrOfChunksPerDimension + 1;
                }
                nrOfChunks = nrOfChunks * nrOfChunksPerDimension;
            }
        }
    }

    /*
    * allocate memory to keep track of:
    *  - results of count star
    *  - for each measure, results of each aggregation function that can be applied to it
    *  - if the aggregate is not the root, use, as measure, the dimension it is missing from the parent
    * */
    protected void allocateMemory() {
        if (isRoot) {
            nrOfChunks = 1;
        }

        memoryAllocation = new HashMap<>();
        memoryAllocationForCountStar = new HashMap<>();

        for (int i = 0; i < nrOfChunks; i++) {
            // memory for count star is always allocated
            double[] v = new double[chunkSize];
            Arrays.fill(v, Double.NaN);
            memoryAllocationForCountStar.put(i, v);

            HashMap<AnalyzedAttribute, HashMap<AggregationFunction, double[]>> valuesByMeasure = new HashMap<>();
            if (catalog.measures != null) {
                for (AnalyzedAttribute measure: catalog.measures) {
                    LinkedList<AggregationFunction> afs = catalog.aggregationFunctionsPerEachAttribute.get(measure);
                    HashMap<AggregationFunction, double[]> valuesByAggFunc = new HashMap<>();
                    for (AggregationFunction af: afs) {
                        v = new double[chunkSize];
                        Arrays.fill(v, Double.NaN);
                        valuesByAggFunc.put(af, v);
                    }
                    valuesByMeasure.put(measure, valuesByAggFunc);
                }
            }

            // set up memory to consider, as measure, the dimension missing from the father
            if (dimensionMissingFromFather != null) {
                LinkedList<AggregationFunction> afs = catalog.aggregationFunctionsPerEachAttribute.get(dimensionMissingFromFather);
                HashMap<AggregationFunction, double[]> valuesByAggFunc = new HashMap<>();
                for (AggregationFunction af: afs) {
                    v = new double[chunkSize];
                    Arrays.fill(v, Double.NaN);
                    valuesByAggFunc.put(af, v);
                }
                valuesByMeasure.put(dimensionMissingFromFather, valuesByAggFunc);
            }
            memoryAllocation.put(i, valuesByMeasure);
        }
    }

    // given an array of doubles it assigns them as measure. Each value in the array is assigned as follows:
    // * the first value to the count star
    // * a value for each measure, for each aggregation function (in the order defined by the variables in the catalog)
    // * a value for each aggregation function of the dimension missing from the father (if these is such a dimension)
    protected void setMeasure(int chunkId, int offset, double[] values) {
        int i = 0;
        double[] vals = memoryAllocationForCountStar.get(chunkId);
        vals[offset] = values[i];
        i++;
        // if there are any measures set them
        if (catalog.measures != null) {
            for (AnalyzedAttribute measure: catalog.measures) {
                for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(measure)) {
                    double[] data = memoryAllocation.get(chunkId).get(measure).get(af);
                    data[offset] = values[i];
                    i++;
                }
            }
        }
        if (dimensionMissingFromFather != null) {
            for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(dimensionMissingFromFather)) {
                double[] data = memoryAllocation.get(chunkId).get(dimensionMissingFromFather).get(af);
                data[offset] = values[i];
                i++;
            }
        }
    }

    public void loadRootWithNaNs() {
        if (isRoot) {
            this.cleanChunk(0);
        }
    }

    /*
     * fill with NaNs all the memory allocated to the aggregate
     * */
    private void cleanMyMemory() {
        for (int i = 0; i < nrOfChunks; i++) {
            cleanChunk(i);
        }
    }

    /*
    * fill memory allocated to a given chunk with NaNs
    * */
    protected void cleanChunk(int chunkID) {
        Arrays.fill(memoryAllocationForCountStar.get(chunkID), Double.NaN);

        if (catalog.measures != null) {
            for (AnalyzedAttribute measure: catalog.measures) {
                for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(measure)) {
                    Arrays.fill(memoryAllocation.get(chunkID).get(measure).get(af), Double.NaN);
                }
            }
        }

        if (dimensionMissingFromFather != null) {
            for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(dimensionMissingFromFather)) {
                Arrays.fill(memoryAllocation.get(chunkID).get(dimensionMissingFromFather).get(af), Double.NaN);
            }
        }
    }

    private void propagateMyMemory() {
        for (int i = 0; i < nrOfChunks; i++) {
            updateSubtree(i);
        }
    }

    public void updateSubtree(int currentChunkID) {
        if (this.isRoot) {
            globalChunkID = currentChunkID;
        }

        for (AggregateInLattice child: childrenAggregates) {
            for (int offset = 0; offset < chunkSize; offset++) {
                if (!Double.isNaN(getCountStarAtOffset(currentChunkID, offset)) || offset == chunkSize - 1) {
                    child.updateMeasure(currentChunkID, offset);
                }
            }
        }
        if (isRoot && currentChunkID == catalog.nrOfChunks - 1) {
            closeAllWriters();
        }
    }

    private void closeAllWriters() {
        if (aggregateResults != null) {
            closeWriter();
        }
        for (AggregateInLattice child: childrenAggregates) {
            child.closeAllWriters();
        }
    }

    private void closeWriter() {
        try {
            for (BufferedWriter bw: aggregateResults) {
                bw.close();
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private double getCountStarAtOffset(int chunk, int offset) {
        double[] countStarValues;
        if (this.isRoot) {
            countStarValues = memoryAllocationForCountStar.get(0);
        }
        else {
            countStarValues = memoryAllocationForCountStar.get(chunk);
        }
        return countStarValues[offset];
    }

    private void updateMeasure(int fatherChunkId, int fatherOffset) {
        int mychunkId = computeMyChunkId(fatherChunkId);
        int myOffset = computeMyOffset(fatherOffset);
        updateMyValues(fatherChunkId, fatherOffset, mychunkId, myOffset, findValueOfMissingDimension(fatherChunkId, fatherOffset));
        if (timeToFlush(fatherChunkId, fatherOffset)) {
            propagateMyMemory();
            flushMyMemoryToDisk();
            cleanMyMemory();
            howManyTimesHaveFlushed++;
        }
    }

    private boolean timeToFlush(int fatherChunkId, int fatherOffset) {
        return (fatherChunkId + 1) % flushToDiskRate == 0 && fatherOffset == fatherAggregate.chunkSize - 1;
    }

    private int computeMyChunkId(int fatherChunkId) {
        return (fatherChunkId) % nrOfChunks;
    }

    /*
    * given the monodimensional index of the father, first find the indices related to every dimension fo the father
    * then find myoffset by only considering the dimensions the current aggregate has
    * */
    private int computeMyOffset(int fatherOffset) {
        computeFatherMultiIdx(fatherOffset);
        int currentDenom = 1, myOffset = 0;
        for (int i = 0; i < dimensions.size() - 1; i++) {
            currentDenom = currentDenom * catalog.nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));
        }

        for (int i = dimensions.size() - 1; i >= 0; i--) {
            int value;
            value = fatherMultiIdx[fatherAggregate.dimensions.indexOf(dimensions.get(i))];
            myOffset = myOffset + value * currentDenom;
            if (i > 0) {
                currentDenom = currentDenom / catalog.nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i - 1));
            }
        }
        return myOffset;
    }

    /*
    * given the monodimensional index of the father, the function computes, for each dimension of the father, the index
    * that identifies the distinct value of such dimension
    * */
    private void computeFatherMultiIdx(int fatherUniIdx) {
        int currentDenom = 1;

        for (int i = 0; i < fatherAggregate.dimensions.size() - 1; i++) {
            currentDenom = currentDenom * catalog.nrOfDistinctValuesInChunkPerDimension.get(fatherAggregate.dimensions.get(i));
        }

        for (int i = fatherAggregate.dimensions.size() - 1; i >= 0; i--) {
            fatherMultiIdx[i] = fatherUniIdx / currentDenom;
            fatherUniIdx = fatherUniIdx % currentDenom;
            if (i > 0) {
                currentDenom = currentDenom / catalog.nrOfDistinctValuesInChunkPerDimension.get(fatherAggregate.dimensions.get((i - 1)));
            }
        }
    }

    private String findValueOfMissingDimension(int fatherChunkId,
                                               int fatherOffset) {
        int idxOfValue = fatherAggregate.dimensions.indexOf(dimensionMissingFromFather);
        if (fatherAggregate.serializeDecodedValue(fatherChunkId, fatherOffset)) {
            String val = catalog.distinctValuesPerDimension.get(dimensionMissingFromFather).get(fatherAggregate.idxForDecoding[idxOfValue]);
            if (val != null && !val.equals("null")) {
                return val;
            }
        }
        return null;
    }

    private void updateMyValues(int fatherChunkId, int fatherOffset,
                                int myChunkId, int myOffset,
                                String valueOfMissingDimension) {
        // update count star
        double[] fatherCountStar;
        if (fatherAggregate.isRoot) {
            fatherCountStar = fatherAggregate.memoryAllocationForCountStar.get(0);
        }
        else {
            fatherCountStar = fatherAggregate.memoryAllocationForCountStar.get(fatherChunkId);
        }
        double[] myCountStar = memoryAllocationForCountStar.get(myChunkId);

        if (Double.isNaN(myCountStar[myOffset])) {
            myCountStar[myOffset] = fatherCountStar[fatherOffset];
        }
        else {
            if (!Double.isNaN(fatherCountStar[fatherOffset])) {
                myCountStar[myOffset] = myCountStar[myOffset] + fatherCountStar[fatherOffset];
            }
        }

        HashMap<AnalyzedAttribute, HashMap<AggregationFunction, double[]>> fatherChunk;
        if (fatherAggregate.isRoot) {
            fatherChunk = fatherAggregate.memoryAllocation.get(0);
        }
        else {
            fatherChunk = fatherAggregate.memoryAllocation.get(fatherChunkId);
        }
        HashMap<AnalyzedAttribute, HashMap<AggregationFunction, double[]>> myChunk = memoryAllocation.get(myChunkId);

        // if there are any measures update every one
        if (catalog.measures != null) {
            for (AnalyzedAttribute measure: catalog.measures) {
                for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(measure)) {
                    double[] fatherValues = fatherChunk.get(measure).get(af);
                    double[] myValues = myChunk.get(measure).get(af);

                    if (Double.isNaN(myValues[myOffset])) {
                        switch (af) {
                            case SUM:
                                myValues[myOffset] = fatherValues[fatherOffset] * fatherCountStar[fatherOffset];
                                break;
                            case MIN:
                            case MAX:
                            case COUNT:
                                myValues[myOffset] = fatherValues[fatherOffset];
                                break;
                        }
                    }
                    else {
                        switch (af) {
                            case SUM:
                                if (!Double.isNaN(fatherValues[fatherOffset])) {
                                    myValues[myOffset] = myValues[myOffset] + (fatherValues[fatherOffset] * fatherCountStar[fatherOffset]);
                                }
                                break;
                            case COUNT:
                                if (!Double.isNaN(fatherValues[fatherOffset])) {
                                    myValues[myOffset] = myValues[myOffset] + fatherValues[fatherOffset];
                                }
                                break;
                            case MIN:
                                if ((!Double.isNaN(fatherValues[fatherOffset])) && fatherValues[fatherOffset] < myValues[myOffset]) {
                                    myValues[myOffset] = fatherValues[fatherOffset];
                                }
                                break;
                            case MAX:
                                if ((!Double.isNaN(fatherValues[fatherOffset])) && fatherValues[fatherOffset] > myValues[myOffset]) {
                                    myValues[myOffset] = fatherValues[fatherOffset];
                                }
                                break;
                        }
                    }
                }
            }
        }
        // update dimension missing from parent
        if (dimensionMissingFromFather != null && valueOfMissingDimension != null && !valueOfMissingDimension.equals("null")) {
            for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(dimensionMissingFromFather)) {
                double[] myValues = myChunk.get(dimensionMissingFromFather).get(af);

                if (!Double.isNaN(fatherCountStar[fatherOffset])) {
                    if (Double.isNaN(myValues[myOffset])) {
                        switch (af) {
                            case SUM:
                                myValues[myOffset] = Double.parseDouble(valueOfMissingDimension) * fatherCountStar[fatherOffset];
                                break;
                            case MIN:
                            case MAX:
                                myValues[myOffset] = Double.parseDouble(valueOfMissingDimension);
                                break;
                            case COUNT:
                                myValues[myOffset] = 1;
                                break;
                        }
                    }
                    else {
                        switch (af) {
                            case SUM:
                                myValues[myOffset] = myValues[myOffset] + (Double.parseDouble(valueOfMissingDimension) * fatherCountStar[fatherOffset]);
                                break;
                            case COUNT:
                                myValues[myOffset] = myValues[myOffset] + 1;
                                break;
                            case MIN:
                                if (Double.parseDouble(valueOfMissingDimension) < myValues[myOffset]) {
                                    myValues[myOffset] = Double.parseDouble(valueOfMissingDimension);
                                }
                                break;
                            case MAX:
                                if (Double.parseDouble(valueOfMissingDimension) > myValues[myOffset]) {
                                    myValues[myOffset] = Double.parseDouble(valueOfMissingDimension);
                                }
                                break;
                        }
                    }
                }
            }
        }

    }

    protected void flushMyMemoryToDisk() {
        for (int i = 0; i < nrOfChunks; i++) {
            flushChunkToDisk(i);
        }
    }

    private void flushChunkToDisk(int chunkID) {
        try {
            if (printHeader) {
                // bufferedWriter for count(*)
                aggregateResults.add(new BufferedWriter(new FileWriter(directoryName + filename + "__COUNT_STAR.csv")));
                // bufferedWriter for each measure
                if (catalog.measures != null) {
                    for (AnalyzedAttribute measure: catalog.measures) {
                        for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(measure)) {
                            aggregateResults.add(new BufferedWriter(new FileWriter(directoryName + filename + "__" + af + "_" + measure.getEncoding() + ".csv")));
                        }
                        if (catalog.avgForEachAttribute != null && catalog.avgForEachAttribute.get(measure) != null) {
                            aggregateResults.add((new BufferedWriter(new FileWriter(directoryName + filename + "__" + AggregationFunction.AVERAGE + "_" + measure.getEncoding() + ".csv"))));
                        }
                    }
                }
                // bufferedWriter for dimension missing from father
                if (dimensionMissingFromFather != null) {
                    for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(dimensionMissingFromFather)) {
                        aggregateResults.add(new BufferedWriter(new FileWriter(directoryName + filename + "__" + af + "_" + dimensionMissingFromFather.getEncoding() + ".csv")));
                    }
                    if (catalog.avgForEachAttribute != null && catalog.avgForEachAttribute.get(dimensionMissingFromFather) != null) {
                        aggregateResults.add((new BufferedWriter(new FileWriter(directoryName + filename + "__" + AggregationFunction.AVERAGE + "_" + dimensionMissingFromFather.getEncoding() + ".csv"))));
                    }
                }

                serializeDecodedHeaderToWriters();
                printHeader = false;
            }
            serializeDecodedDataToWriters(chunkID);
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private void serializeDecodedHeaderToWriters() {
        String decoded;
        int nrBW = 0;
        try {
            // write dimensions to all writers
            for (AnalyzedAttribute dim: dimensions) {
                for (BufferedWriter bw: aggregateResults) {
                    bw.write(decodeAttributeName(dim));
                    bw.write(", ");
                }
            }
            aggregateResults.get(nrBW).write("count(*)");
            nrBW++;

            if (catalog.measures != null) {
                for (AnalyzedAttribute measure: catalog.measures) {
                    decoded = decodeAttributeName(measure);
                    for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(measure)) {
                        switch (af) {
                            case COUNT:
                                aggregateResults.get(nrBW).write("count(" + decoded + ")");
                                break;
                            case MAX:
                                aggregateResults.get(nrBW).write("max(" + decoded + ")");
                                break;
                            case MIN:
                                aggregateResults.get(nrBW).write("min(" + decoded + ")");
                                break;
                            case SUM:
                                aggregateResults.get(nrBW).write("sum(" + decoded + ")");
                                break;
                        }
                        nrBW++;
                    }
                    if (catalog.avgForEachAttribute != null && catalog.avgForEachAttribute.get(measure) != null) {
                        aggregateResults.get(nrBW).write("avg(" + decoded + ")");
                        nrBW++;
                    }
                }
            }

            if (dimensionMissingFromFather != null) {
                decoded = decodeAttributeName(dimensionMissingFromFather);
                for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(dimensionMissingFromFather)) {
                    switch (af) {
                        case COUNT:
                            aggregateResults.get(nrBW).write("count(" + decoded + ")");
                            break;
                        case MAX:
                            aggregateResults.get(nrBW).write("max(" + decoded + ")");
                            break;
                        case MIN:
                            aggregateResults.get(nrBW).write("min(" + decoded + ")");
                            break;
                        case SUM:
                            aggregateResults.get(nrBW).write("sum(" + decoded + ")");
                            break;
                    }
                    nrBW++;
                }
                if (catalog.avgForEachAttribute != null && catalog.avgForEachAttribute.get(dimensionMissingFromFather) != null) {
                    aggregateResults.get(nrBW).write("avg(" + decoded + ")");
                }
            }

            for (BufferedWriter bw: aggregateResults) {
                bw.write("\n");
            }

        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private String decodeAttributeName(AnalyzedAttribute attribute) {
        Pattern p = Pattern.compile("\\d+");
        Matcher m;

        String decoded = getValueFromDictionary(attribute.getEncoding());

        AnalyzedAttribute derivedFrom = attribute.getDerivedFrom();
        if (derivedFrom != null) {
            SuggestedDerivation derivationType = attribute.getDerivationType();
            if (derivationType != SuggestedDerivation.PATH) {
                //decoded = Utils.getShorterAttributeName(decoded);
                decoded = Utils.getShorterAttributeName(getValueFromDictionary(derivedFrom.getEncoding()));
                switch (derivationType) {
                    case COUNT_ATTR:
                        decoded = "count(" + decoded + ")";
                        break;
                    case KWD_IN_TEXT:
                        decoded = "keywords(" + decoded + ")";
                        break;
                    case LANG_DET:
                        decoded = "language(" + decoded + ")";
                        break;
                }

                return decoded;
            }
            else { // for paths need to decode all the attributes in the path
                boolean analyze = false;
                StringTokenizer multiTokenizer = new StringTokenizer(decoded, "/>");
                StringBuilder decodedPath = new StringBuilder();

                while (multiTokenizer.hasMoreTokens()) {
                    String elem = multiTokenizer.nextToken();
                    if (analyze) {
                        decodedPath.append("/").append(Utils.getShorterAttributeName(getValueFromDictionary(Long.parseLong(elem))));
                    }

                    if (elem.startsWith("Summary")) {
                        m = p.matcher(elem);
                        while (m.find()) {
                            decodedPath.append(Utils.getShorterAttributeName(getValueFromDictionary(Long.parseLong(m.group()))));
                        }
                        analyze = true;
                    }
                }
                return decodedPath.toString();
            }
        }
        return Utils.getShorterAttributeName(decoded);
    }

    String getValueFromDictionary(long key) {
        String value = null;
        try {
            value = catalog.handler.getResultSelectFromValueColumn("select value from dictionary where key=" + key).get(0);
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }
        return value;
    }

    private void serializeDecodedDataToWriters(int chunkId) {
        try {
            int nrBW, globalChunkId = howManyTimesHaveFlushed * nrOfChunks + chunkId;
            for (int offset = 0; offset < chunkSize; offset++) { // write dimension values to all buffered writers whose af(measure) is not NaN
                nrBW = 0;
                if (serializeDecodedValue(globalChunkId, offset) && dimensionsDifferFromNull() == true && atLeastOneMeasureIsNotNull(chunkId, offset)) {
                    // check if count(*) is not NaN and write it to file
                    if (!Double.isNaN(memoryAllocationForCountStar.get(chunkId)[offset])) {
                        for (int i = 0; i < idxForDecoding.length; i++) {
                            aggregateResults.get(nrBW).write(getCleanValue(catalog.distinctValuesPerDimension.get(dimensions.get(i)).get(idxForDecoding[i])));
                        }

                        aggregateResults.get(nrBW).write(memoryAllocationForCountStar.get(chunkId)[offset] + "\n");
                    }
                    nrBW++;

                    if (catalog.measures != null) {
                        for (AnalyzedAttribute measure: catalog.measures) {
                            for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(measure)) {
                                if (!Double.isNaN(memoryAllocation.get(chunkId).get(measure).get(af)[offset])) {
                                    for (int i = 0; i < idxForDecoding.length; i++) {
                                        aggregateResults.get(nrBW).write(getCleanValue(catalog.distinctValuesPerDimension.get(dimensions.get(i)).get(idxForDecoding[i])));
                                    }
                                    aggregateResults.get(nrBW).write(memoryAllocation.get(chunkId).get(measure).get(af)[offset] + "\n");
                                }
                                nrBW++;
                            }

                            if (catalog.avgForEachAttribute != null && catalog.avgForEachAttribute.get(measure) != null) {
                                Double sum = memoryAllocation.get(chunkId).get(measure).get(AggregationFunction.SUM)[offset];
                                Double countstar = memoryAllocationForCountStar.get(chunkId)[offset];
                                if (!Double.isNaN(sum) && !Double.isNaN(countstar)) {
                                    double avg = sum / countstar;
                                    for (int i = 0; i < idxForDecoding.length; i++) {
                                        aggregateResults.get(nrBW).write(getCleanValue(catalog.distinctValuesPerDimension.get(dimensions.get(i)).get(idxForDecoding[i])));
                                    }
                                    aggregateResults.get(nrBW).write(avg + "\n");
                                }
                                nrBW++;
                            }
                        }
                    }

                    // if there is a dimension missing from the father
                    if (dimensionMissingFromFather != null) {
                        for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(dimensionMissingFromFather)) {
                            if (!Double.isNaN(memoryAllocation.get(chunkId).get(dimensionMissingFromFather).get(af)[offset])) {
                                for (int i = 0; i < idxForDecoding.length; i++) {
                                    aggregateResults.get(nrBW).write(getCleanValue(catalog.distinctValuesPerDimension.get(dimensions.get(i)).get(idxForDecoding[i])));
                                }
                                aggregateResults.get(nrBW).write(memoryAllocation.get(chunkId).get(dimensionMissingFromFather).get(af)[offset] + "\n");
                            }
                            nrBW++;
                        }
                        if (catalog.avgForEachAttribute != null && catalog.avgForEachAttribute.get(dimensionMissingFromFather) != null) {
                            Double sum = memoryAllocation.get(chunkId).get(dimensionMissingFromFather).get(AggregationFunction.SUM)[offset];
                            Double countstar = memoryAllocationForCountStar.get(chunkId)[offset];
                            if (!Double.isNaN(sum) && !Double.isNaN(countstar)) {
                                double avg = sum / countstar;
                                for (int i = 0; i < idxForDecoding.length; i++) {
                                    aggregateResults.get(nrBW).write(getCleanValue(catalog.distinctValuesPerDimension.get(dimensions.get(i)).get(idxForDecoding[i])));
                                }
                                aggregateResults.get(nrBW).write(avg + "\n");
                            }
                        }
                    }
                }
            }
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    String getCleanValue(String val) {
        if (val.equals("null")) {
            return "empty value, ";
        }
        else {
            return val.replaceAll(",", " ") + ", ";
        }
    }

    private boolean atLeastOneMeasureIsNotNull(int chunkId, int offset) {
        if (!Double.isNaN(memoryAllocationForCountStar.get(chunkId)[offset])) {
            return true;
        }

        if (catalog.measures != null) {
            for (AnalyzedAttribute measure: catalog.measures) {
                for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(measure)) {
                    if (!Double.isNaN(memoryAllocation.get(chunkId).get(measure).get(af)[offset])) {
                        return true;
                    }
                }
            }
        }

        if (dimensionMissingFromFather != null) {
            for (AggregationFunction af: catalog.aggregationFunctionsPerEachAttribute.get(dimensionMissingFromFather)) {
                if (!Double.isNaN(memoryAllocation.get(chunkId).get(dimensionMissingFromFather).get(af)[offset])) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean serializeDecodedValue(int chunkID, int offset) {
        int currentDenom = 1;
        if (this.isRoot) {
            chunkID = globalChunkID;
        }

        for (int i = 0; i < dimensions.size() - 1; i++) {
            currentDenom = currentDenom * catalog.nrOfGroupsPerDimension.get(dimensions.get(i));
        }

        for (int i = multiIdxChunk.length - 1; i >= 0; i--) {
            multiIdxChunk[i] = chunkID / currentDenom;
            chunkID = chunkID % currentDenom;
            if (i > 0) {
                currentDenom = currentDenom / catalog.nrOfGroupsPerDimension.get(dimensions.get(i - 1));
            }
        }

        currentDenom = 1;
        for (int i = 0; i < dimensions.size() - 1; i++) {
            currentDenom = currentDenom * catalog.nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));
        }

        for (int i = multiIdxOffset.length - 1; i >= 0; i--) {
            multiIdxOffset[i] = offset / currentDenom;
            offset = offset % currentDenom;
            if (i > 0) {
                currentDenom = currentDenom / catalog.nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i - 1));
            }
        }

        for (int i = 0; i < dimensions.size(); i++) {
            ArrayList<String> distValues = catalog.distinctValuesPerDimension.get(dimensions.get(i));
            int idx = multiIdxChunk[i] * catalog.nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i)) + multiIdxOffset[i];
            if (idx >= distValues.size()) {
                return false;
            }
            idxForDecoding[i] = idx;
        }

        return true;
    }

    private boolean dimensionsDifferFromNull() {
        for (int i = 0; i < dimensions.size(); i++) {
            if (catalog.distinctValuesPerDimension.get(dimensions.get(i)).get(idxForDecoding[i]).equals("null")) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AggregateInLattice newAgg = (AggregateInLattice) o;

        return dimensions.equals(newAgg.dimensions);
    }

    @Override
    public int hashCode() {
        return dimensions.hashCode();
    }

    @Override
    public String toString() {
        int i;
        StringBuilder res = new StringBuilder("Aggregate has dimensions: [");
        for (i = 0; i < dimensions.size() - 1; i++) {
            res.append(dimensions.get(i).getEncoding()).append(", ");
        }
        res.append(dimensions.get(i).getEncoding()).append("]");
        if (minimumMemoryCostPerDimension != null) {
            res.append(" ( ");
            for (i = 0; i < dimensions.size() - 1; i++) {
                res.append(minimumMemoryCostPerDimension.get(dimensions.get(i))).append("x");
            }
            res.append(minimumMemoryCostPerDimension.get(dimensions.get(i))).append(" ) ");
        }
        res.append(" stored in ").append(nrOfChunks).append(" chunks of dimension ( ");
        for (i = 0; i < dimensions.size() - 1; i++) {
            res.append(catalog.nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i))).append("x");
        }
        res.append(catalog.nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i))).append(" ) ");
        res.append("flushes every ").append(flushToDiskRate).append(" chunks. ");
        return res.toString();
    }
}
