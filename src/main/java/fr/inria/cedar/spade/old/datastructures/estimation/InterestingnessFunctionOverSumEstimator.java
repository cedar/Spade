//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures.estimation;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.database.ResultRow;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class InterestingnessFunctionOverSumEstimator extends DeltaMethodEstimator {
    private static final Logger LOGGER = Logger.getLogger(InterestingnessFunctionOverSumEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected HashMap<ArrayList<String>, Integer> groupSizesMap;
    protected Integer[] groupSizes;

    public InterestingnessFunctionOverSumEstimator(
            DatabaseHandler handler,
            HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates,
            MultidimensionalSingleAggregate aggregate,
            InterestingnessFunction interestingnessFunction,
            Double confidenceLevel,
            HashMap<ArrayList<String>, ResultSet> cursorsByGroup,
            HashMap<ArrayList<String>, ArrayList<Double>> retrievedTuplesByGroup,
            String tableName) {
        super(handler, aggregates, aggregate, interestingnessFunction, confidenceLevel, cursorsByGroup, retrievedTuplesByGroup, tableName);
        groupSizesMap = new HashMap<>();
        computeGroupCounts();
    }

    protected final void computeGroupCounts() {
        try {
            int numberOfDimensions = aggregate.getDimensions().size();

            String dimensionList = aggregate.getAggregate().getDimensionList();
            String sqlQuery = "select " + dimensionList + ", count(m) as countm from " + tableName + " group by (" + dimensionList + ")";
            ResultSet groupCounts = handler.getResultSet(sqlQuery);
            ArrayList<String> group;
            Integer count;
            ArrayList<ResultRow> countResults = new ArrayList<>();
            while (groupCounts.next()) {
                group = new ArrayList<>();
                for (int i = 1; i <= numberOfDimensions; i++) {
                    group.add(Long.toString(groupCounts.getLong("d" + i)));
                }
                count = groupCounts.getInt("countm");
                groupSizesMap.put(group, count);
                countResults.add(new ResultRow(group, Double.valueOf(count)));
            }
            Double[] values = new Double[countResults.size()];
            int i = 0;
            for (ResultRow value: countResults) {
                values[i] = value.getAggregatedValue();
                i++;
            }

            Double interestingnessScore;
            Double[] normalizedValues;
            HashSet<MultidimensionalSingleAggregate> singleAggregates = aggregates.get(aggregate.getAggregate());
            for (MultidimensionalSingleAggregate singleAggregate: singleAggregates) {
                if (singleAggregate.getAggregationFunction() == AggregationFunction.COUNT) {
                    singleAggregate.setAggregationResults(countResults);

                    switch (interestingnessFunction) {
                        case VAR_FEATURE_SCALING:
                            normalizedValues = EfficientComputations.normalizeByFeatureScaling(values);
                            interestingnessScore = EfficientComputations.variance(normalizedValues);
                            break;
                        case VAR_SUM:
                            interestingnessScore = EfficientComputations.varianceOnRawValuesNormalizedByDivisionBySum(values);
                            break;
                        case VAR_MEAN:
                            interestingnessScore = EfficientComputations.varianceOnRawValuesNormalizedByDivisionByMean(values);
                            break;
                        case SKEWNESS:
                            interestingnessScore = EfficientComputations.skewness(values);
                            break;
                        case KURTOSIS:
                            interestingnessScore = EfficientComputations.kurtosis(values);
                            break;
                        default:
                            throw new IllegalStateException("Unsupported interestingness function");
                    }
                    singleAggregate.setInterestingnessFunctionScore(interestingnessFunction, interestingnessScore);
                }
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    protected void precomputeStatisticsOnGroups() {
        D = retrievedTuplesByGroup.size(); // number of groups
        groupSizes = new Integer[D]; // true size of each group
        averages = new Double[D]; // average of retrieved elements of each group
        variances = new Double[D]; // variance of retrieved elements of each group
        ArrayList<Double> valuesInGroup; // elements in current group
        int numberOfElementsInGroup; // number of elements in current group
        Double[] values; // all the retrieved values in current group (always of size numberOfElementsInGroup)
        m = null; // the highest number of retrieved elements among all the groups
        int i = 0;
        for (ArrayList<String> group: retrievedTuplesByGroup.keySet()) {
            groupSizes[i] = groupSizesMap.get(group);

            valuesInGroup = retrievedTuplesByGroup.get(group);
            numberOfElementsInGroup = valuesInGroup.size();
            values = new Double[numberOfElementsInGroup];
            valuesInGroup.toArray(values);

            averages[i] = EfficientComputations.mean(values);
            variances[i] = EfficientComputations.varianceWithKnownMean(values, averages[i]);

            if (m == null || m < valuesInGroup.size()) {
                m = valuesInGroup.size();
            }

            i++;
        }
    }

    protected Double[] computeSumsInGroups() {
        Double[] sums = new Double[D];
        for (int j = 0; j < D; j++) {
            // obtain the sum in the group by extrapolation: multiply observed average by the real size of the group
            sums[j] = averages[j] * groupSizes[j];
        }

        return sums;
    }

    @Override
    protected void updateEstimatesForFeatureScalingNormalizedVariance() {
        precomputeStatisticsOnGroups();

        Double[] sums = computeSumsInGroups();
        Double[] normalizedValues = EfficientComputations.normalizeByFeatureScaling(sums);
        pointEstimate = EfficientComputations.variance(normalizedValues);

        computeDeltaMethodConfidenceIntervalForVariance();
    }

    @Override
    protected void updateEstimatesForSumNormalizedVariance() {
        precomputeStatisticsOnGroups();

        Double[] sums = computeSumsInGroups();
        pointEstimate = EfficientComputations.varianceOnRawValuesNormalizedByDivisionBySum(sums);

        computeDeltaMethodConfidenceIntervalForVariance();
    }

    @Override
    protected void updateEstimatesForMeanNormalizedVariance() {
        precomputeStatisticsOnGroups();

        Double[] sums = computeSumsInGroups();
        pointEstimate = EfficientComputations.varianceOnRawValuesNormalizedByDivisionByMean(sums);

        computeDeltaMethodConfidenceIntervalForVariance();
    }

    @Override
    protected void updateEstimatesForSkewness() {
        precomputeStatisticsOnGroups();

        Double[] sums = computeSumsInGroups();
        pointEstimate = EfficientComputations.skewness(sums);

        computeDeltaMethodConfidenceIntervalForSkewness();
    }

    @Override
    protected void updateEstimatesForKurtosis() {
        precomputeStatisticsOnGroups();

        Double[] sums = computeSumsInGroups();
        pointEstimate = EfficientComputations.kurtosis(sums);

        computeDeltaMethodConfidenceIntervalForKurtosis();
    }

    @Override
    public ArrayList<ResultRow> getResult() {
        ArrayList<ResultRow> result = new ArrayList<>();
        for (ArrayList<String> group: retrievedTuplesByGroup.keySet()) {
            ArrayList<Double> valuesInGroup = retrievedTuplesByGroup.get(group);
            Double[] values = new Double[valuesInGroup.size()];
            valuesInGroup.toArray(values);

            // obtain the sum in the group by extrapolation: multiply observed average by the real size of the group
            result.add(new ResultRow(group, EfficientComputations.mean(values) * groupSizesMap.get(group)));
        }

        return result;
    }
}
