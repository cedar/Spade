//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.molap;

import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import java.util.*;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Lattice {
    private static final Logger LOGGER = Logger.getLogger(Lattice.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected Catalog catalog;
    protected AggregateInLattice root; // root is considered at level zero
    // associates, to each level in the lattice, the set of aggregates in it
    protected LinkedHashMap<Integer, ArrayList<AggregateInLattice>> allAggregatesInLattice;
    protected int nrOfLevels;

    public Lattice(Catalog catalog) {
        this.catalog = catalog;
        this.root = new AggregateInLattice(catalog.dimensions, catalog);
        this.root.setRoot();
        allAggregatesInLattice = new LinkedHashMap<>();
        allAggregatesInLattice.put(0, new ArrayList<>(Arrays.asList(this.root)));
        nrOfLevels = 1;
    }

    /*
     * returns the root of the Minimum Memory Spanning Tree derived from the lattice
     * */
    public AggregateInLattice getRunningMMST() {
        enumerateAllLattice();
        findMinimumMemorySpanningTree();

        for (int i = 0; i < nrOfLevels; i++) {
            for (AggregateInLattice agg: allAggregatesInLattice.get(i)) {
                agg.allocateMemory();
            }
        }
        return root;
    }

    private void enumerateAllLattice() {
        enumerateLevelBelowTheGivenOne(0);
    }

    // generates the lattice recursively, one level at the time
    private void enumerateLevelBelowTheGivenOne(int level) {
        ArrayList<AggregateInLattice> aggsInGivenLevel = allAggregatesInLattice.get(level);

        // stop when the level containing candidates of length 1 is reached
        if (aggsInGivenLevel.get(0).dimensions.size() == 1) {
            nrOfLevels = level + 1;
            return;
        }

        // find the aggregates to be added to the next level in the lattice
        ArrayList<AggregateInLattice> aggsInNewLevel = new ArrayList<>();
        // for each aggregate in the given level, generate all combinations having one less dimension
        // and insert into the next level only those combinations that are not already in it
        for (AggregateInLattice ail: aggsInGivenLevel) {
            Iterator<int[]> iterator = CombinatoricsUtils.combinationsIterator(ail.dimensions.size(), ail.dimensions.size() - 1);
            while (iterator.hasNext()) {
                final int[] combination = iterator.next();
                ArrayList<AnalyzedAttribute> dims = new ArrayList<>();
                for (int i = 0; i < combination.length; i++) {
                    dims.add(ail.dimensions.get(combination[i]));
                }
                // construct an aggregate based on the current combination
                AggregateInLattice currAgg = new AggregateInLattice(dims, catalog);
                if (!aggsInNewLevel.contains(currAgg)) {
                    aggsInNewLevel.add(currAgg);
                }
            }
            allAggregatesInLattice.put(level + 1, aggsInNewLevel);
        }
        enumerateLevelBelowTheGivenOne(level + 1);
    }


    /*
     * based on the enumerated lattice, finds the minimum memory spanning tree
     * */
    private void findMinimumMemorySpanningTree() {
        // start from the level below the root and consider all nodes in each level one at the time.
        // Compute the cost of finding such node from each node in the above level and choose the node
        // that minimizes such cost
        for (int currentLevel = 1; currentLevel < nrOfLevels; currentLevel++) {
            ArrayList<AggregateInLattice> aggregatesInCurrentLevel = allAggregatesInLattice.get(currentLevel);
            ArrayList<AggregateInLattice> aggregatesInAboveLevel = allAggregatesInLattice.get(currentLevel - 1);
            boolean assigned;

            for (int i = 0; i < aggregatesInCurrentLevel.size(); i++) {
                AggregateInLattice agg1 = aggregatesInCurrentLevel.get(i);
                assigned = false;
                for (int j = 0; j < aggregatesInAboveLevel.size(); j++) {
                    AggregateInLattice agg2 = aggregatesInAboveLevel.get(j);

                    // compute the cost of finding an aggregate from the current level using an aggregate from the above level
                    HashMap<AnalyzedAttribute, Integer> costPerDimension = costToComputeAgg1FromAgg2(agg1, agg2);
                    if (costPerDimension != null) {
                        int cost = 1;
                        for (Map.Entry<AnalyzedAttribute, Integer> entry: costPerDimension.entrySet()) {
                            cost = cost * entry.getValue();
                        }

                        if (assigned == false) {
                            agg1.minimumMemoryCost = cost;
                            agg1.setMinimumMemoryCostPerDimension(costPerDimension);
                            agg1.setFatherAggregate(agg2);
                            agg2.addChildAggregate(agg1);
                            agg1.setFlushToDiskRate(computeFlushingToDiskRate(agg2, agg1));
                            assigned = true;
                        }
                        else {
                            if (cost < agg1.minimumMemoryCost) {
                                agg1.minimumMemoryCost = cost;
                                agg1.setMinimumMemoryCostPerDimension(costPerDimension);
                                agg1.fatherAggregate.childrenAggregates.remove(agg1);
                                agg1.setFatherAggregate(agg2);
                                agg2.addChildAggregate(agg1);
                                agg1.setFlushToDiskRate(computeFlushingToDiskRate(agg2, agg1));
                            }
                        }
                    }
                }
            }
        }
    }

    private int computeFlushingToDiskRate(AggregateInLattice fatherAgg,
                                          AggregateInLattice childAgg) {
        int rate = 1;
        for (AnalyzedAttribute fatherDim: fatherAgg.dimensions) {
            int nrOfGroups = catalog.nrOfDistinctValuesPerDimension.get(fatherDim) / catalog.nrOfDistinctValuesInChunkPerDimension.get(fatherDim);
            if (catalog.nrOfDistinctValuesPerDimension.get(fatherDim) % catalog.nrOfDistinctValuesInChunkPerDimension.get(fatherDim) != 0) {
                nrOfGroups = nrOfGroups + 1;
            }

            rate = rate * nrOfGroups;

            if (!childAgg.dimensions.contains(fatherDim)) {
                break;
            }
        }
        return rate;
    }

    private HashMap<AnalyzedAttribute, Integer> costToComputeAgg1FromAgg2(
            AggregateInLattice agg1, AggregateInLattice agg2) {
        int i = 0, j = 0;
        HashMap<AnalyzedAttribute, Integer> cost = null;
        boolean allDimensionsAreThere = true;
        // check that the dimensions of Agg1 are all in Agg2
        while (i < agg1.dimensions.size() && allDimensionsAreThere == true) {
            allDimensionsAreThere = false;
            while (j < agg2.dimensions.size() && allDimensionsAreThere == false) {
                if (agg1.dimensions.get(i).getEncoding() == agg2.dimensions.get(j).getEncoding()) {
                    allDimensionsAreThere = true;
                }
                j++;
            }
            i++;
        }

        if (allDimensionsAreThere) {
            cost = new HashMap<>();
            int lengthOfPrefix;
            for (i = 0; i < agg1.dimensions.size() && agg1.dimensions.get(i).getEncoding() == agg2.dimensions.get(i).getEncoding(); i++);
            lengthOfPrefix = i;

            for (i = 0; i < lengthOfPrefix; i++) {
                cost.put(agg1.dimensions.get(i), catalog.nrOfDistinctValuesPerDimension.get(agg1.dimensions.get(i)));
            }
            for (i = lengthOfPrefix; i < agg1.dimensions.size(); i++) {
                cost.put(agg1.dimensions.get(i), catalog.nrOfDistinctValuesInChunkPerDimension.get(agg1.dimensions.get(i)));
            }
        }

        return cost;
    }

    public void printMMST() {
        printTree(root, 0);
    }

    private void printTree(AggregateInLattice agg, int level) {
        for (int i = 0; i < level; i++) {
            System.err.print("\t");
        }
        for (AnalyzedAttribute d: agg.dimensions) {
            System.err.print(d.getEncoding() + ", ");
        }
        System.err.println();
        for (AggregateInLattice child: agg.childrenAggregates) {
            printTree(child, level + 1);
        }
    }

    @Override
    public String toString() {
        String result = new String();
        result = result + "\nThe lattice has a total of " + nrOfLevels + " levels: ";
        for (Map.Entry<Integer, ArrayList<AggregateInLattice>> entry: allAggregatesInLattice.entrySet()) {
            result = result + "\n\nLevel " + entry.getKey() + " :";
            ArrayList<AggregateInLattice> aggs = entry.getValue();
            for (AggregateInLattice a: aggs) {
                result = result + "\t" + a;
            }
        }
        return result;
    }
}
