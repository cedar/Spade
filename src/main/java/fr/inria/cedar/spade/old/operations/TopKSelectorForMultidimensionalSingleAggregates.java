//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.operations;

import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregateInTopK;
import fr.inria.cedar.spade.datastructures.Pair;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class TopKSelectorForMultidimensionalSingleAggregates {
    private static final Logger LOGGER = Logger.getLogger(TopKSelectorForMultidimensionalSingleAggregates.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected ArrayList<MultidimensionalSingleAggregateInTopK> rankedAggregates;
    protected int k;
    protected InterestingnessFunction interestingnessFunction;

    public TopKSelectorForMultidimensionalSingleAggregates(int k,
                                                           InterestingnessFunction interestingnessFunction) {
        rankedAggregates = new ArrayList<>();
        this.k = k;
        this.interestingnessFunction = interestingnessFunction;
    }

    protected class MultidimensionalSingleAggregateInTopKComparator implements
            Comparator<MultidimensionalSingleAggregateInTopK> {
        @Override
        public int compare(MultidimensionalSingleAggregateInTopK agg1,
                           MultidimensionalSingleAggregateInTopK agg2) {
            Double score1 = agg1.getInterestingnessFunctionScore();
            Double score2 = agg2.getInterestingnessFunctionScore();

            return -score1.compareTo(score2);
        }
    }

    protected void sortAggregates(
            ArrayList<MultidimensionalSingleAggregateInTopK> aggregates) {
        Collections.sort(aggregates, new MultidimensionalSingleAggregateInTopKComparator());
    }

    protected ArrayList<MultidimensionalSingleAggregateInTopK> trimTopK(
            ArrayList<MultidimensionalSingleAggregateInTopK> currentTopK, int k) {
        // trim results to the new top-k
        ArrayList<MultidimensionalSingleAggregateInTopK> result = new ArrayList<>();
        if (!currentTopK.isEmpty()) {
            result.add(currentTopK.get(0));
            Double previous;
            Double current;
            for (int i = 1; i < currentTopK.size(); i++) {
                previous = currentTopK.get(i - 1).getInterestingnessFunctionScore();
                current = currentTopK.get(i).getInterestingnessFunctionScore();

                // cut after k but don't break ties: keep k-th ex aequo
                if (current.equals(previous) || i < k) {
                    result.add(currentTopK.get(i));
                }
                else {
                    break;
                }
            }
        }

        return result;
    }

    // updates the top-k without breaking ties
    public void addEarlyStopAggregatesAndUpdateTopK(
            ArrayList<MultidimensionalSingleAggregate> earlyStopAggregatesToBeAdded) {
        // add current top-k
        Double interestingnessFunctionScore;
        for (MultidimensionalSingleAggregate aggregate: earlyStopAggregatesToBeAdded) {
            interestingnessFunctionScore = aggregate.getInterestingnessFunctionScore(interestingnessFunction);
            rankedAggregates.add(new MultidimensionalSingleAggregateInTopK(aggregate, interestingnessFunctionScore));
        }

        // sort newly added aggregates together with current top-k
        sortAggregates(rankedAggregates);

        // update top-k
        rankedAggregates = trimTopK(rankedAggregates, k);
    }

    // updates the top-k without breaking ties
    public void addMOLAPAggregatesAndUpdateTopK(
            ArrayList<Pair<String, Double>> MOLAPAggregatesToBeAdded) {
        // add current top-k
        for (Pair<String, Double> aggregate: MOLAPAggregatesToBeAdded) {
            rankedAggregates.add(new MultidimensionalSingleAggregateInTopK(aggregate.getKey(), aggregate.getValue()));
        }

        // sort newly added aggregates together with current top-k
        sortAggregates(rankedAggregates);

        // update top-k
        rankedAggregates = trimTopK(rankedAggregates, k);
    }

    public int getK() {
        return k;
    }

    public ArrayList<MultidimensionalSingleAggregateInTopK> getTopKAggregates() {
        return rankedAggregates;
    }

    public MultidimensionalSingleAggregateInTopK getKthAggregate() {
        int size = rankedAggregates.size();

        return (size != 0) ? rankedAggregates.get(size - 1) : null;
    }
}
