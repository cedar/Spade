//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.molap;

import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Catalog {
    private static final Logger LOGGER = Logger.getLogger(Catalog.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected CandidateFactSet candidateFactSet;
    protected ArrayList<AnalyzedAttribute> dimensions;
    protected ArrayList<AnalyzedAttribute> measures;
    protected HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsPerEachAttribute;
    protected HashMap<AnalyzedAttribute, Boolean> avgForEachAttribute;
    protected HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesPerDimension;
    protected HashMap<AnalyzedAttribute, ArrayList<String>> distinctValuesPerDimension;
    protected HashMap<AnalyzedAttribute, Integer> nrOfGroupsPerDimension;
    protected HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesInChunkPerDimension;
    protected int nrOfChunks;
    protected int chunkSize;
    protected String tableName;
    protected String directoryWithData;
    protected DatabaseHandler handler;

    public Catalog(CandidateFactSet candidateFactSet,
                   ArrayList<AnalyzedAttribute> dimensions,
                   ArrayList<AnalyzedAttribute> measures, int nrOfChunks,
                   String tableName, String directoryWithData,
                   DatabaseHandler handler,
                   HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesInChunkPerDimension,
                   HashMap<AnalyzedAttribute, ArrayList<String>> distinctValuesPerDimension,
                   HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsPerEachAttribute,
                   HashMap<AnalyzedAttribute, Boolean> avgForEachAttribute,
                   HashMap<AnalyzedAttribute, Integer> nrOfGroupsPerDimension) {
        this.candidateFactSet = candidateFactSet;
        this.dimensions = dimensions;
        this.measures = measures;
        this.nrOfChunks = nrOfChunks;
        this.tableName = tableName;
        this.directoryWithData = directoryWithData;
        this.handler = handler;
        this.nrOfDistinctValuesInChunkPerDimension = nrOfDistinctValuesInChunkPerDimension;
        this.distinctValuesPerDimension = distinctValuesPerDimension;
        this.aggregationFunctionsPerEachAttribute = aggregationFunctionsPerEachAttribute;
        this.avgForEachAttribute = avgForEachAttribute;
        this.nrOfGroupsPerDimension = nrOfGroupsPerDimension;

        nrOfDistinctValuesPerDimension = new HashMap<>();
        for (Map.Entry<AnalyzedAttribute, ArrayList<String>> entry: distinctValuesPerDimension.entrySet()) {
            nrOfDistinctValuesPerDimension.put(entry.getKey(), entry.getValue().size());
        }
        chunkSize = 1;
        for (Map.Entry<AnalyzedAttribute, Integer> entry: nrOfDistinctValuesInChunkPerDimension.entrySet()) {
            chunkSize = nrOfChunks * entry.getValue();
        }
    }

    public int getNumberOfChunks() {
        return nrOfChunks;
    }

    public String getTableName() {
        return tableName;
    }

    public boolean loadChunk(int chunkId, AggregateInLattice root) {
        int nrOfColumns = 1;
        if (measures != null) {
            for (AnalyzedAttribute ap: measures) {
                nrOfColumns = nrOfColumns + aggregationFunctionsPerEachAttribute.get(ap).size();
            }
        }

        root.cleanChunk(0);

        String sqlQuery = "select * from " + tableName + " where chunkid=" + chunkId;
        ResultSet rs = handler.getResultSet(sqlQuery);
        try {
            if (rs.next() == false) {
                root.globalChunkID = chunkId;
                root.rootIsLoaded = false;
                return false;
            }
            else {
                InputStream stream = rs.getBinaryStream(2);
                String line;
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
                double[] values = new double[nrOfColumns];
                while ((line = bufferedReader.readLine()) != null) {
                    String[] tokens = line.split(" ");
                    int offset = Integer.parseInt(tokens[0]);

                    for (int i = 1; i < tokens.length; i++) {
                        if (tokens[i].equals("null")) {
                            values[i - 1] = Double.NaN;
                        }
                        else {
                            values[i - 1] = Double.parseDouble(tokens[i]);
                        }
                    }
                    root.setMeasure(0, offset, values);
                }
                root.rootIsLoaded = false;
                root.globalChunkID = chunkId;
                root.flushMyMemoryToDisk();
                //return true;
            }
        }
        catch (SQLException | IOException ex) {
            throw new IllegalStateException(ex);
        }
        return true;
    }

    public void clean() {
        ArrayList<String> molap;
        String sqlQuery = "select c.relname as key FROM pg_catalog.pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace where c.relname ='" + tableName + "' and c.relkind='r'";
        try {
            molap = handler.getResultSelectFromKeyColumn(sqlQuery);
            for (String d: molap) {
                sqlQuery = "DROP TABLE " + d + ";";
                handler.executeUpdate(sqlQuery);
            }
            Path pathToBeDeleted = Paths.get(directoryWithData);
            deleteDirectory(pathToBeDeleted.toFile());
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Cleaning with the query: " + sqlQuery + "; " + ex);
        }
    }

    public ArrayList<AnalyzedAttribute> getMeasures() {
        return measures;
    }

    private boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file: allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    @Override
    public String toString() {
        String result = "\nCATALOG: ";
        for (AnalyzedAttribute ap: dimensions) {
            result = result + "\n" + ap.getEncoding() + ": " + distinctValuesPerDimension.get(ap);
        }
        /*for (int i=0; i<dimensions.size(); i++){
            result = result + "\n- Dimension: " + dimensions.get(i)
                    + " with " + nrOfDistinctValuesPerDimension.get(dimensions.get(i)) + " distinct values "
                    + "(" + nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i)) + " per chunk): [";
            ArrayList<String> currentDistinctValues = distinctValuesPerDimension.get(dimensions.get(i));
            for(int j=0; j<currentDistinctValues.size() - 1; j++)
                result = result + currentDistinctValues.get(j) + ", ";
            result = result + currentDistinctValues.get(currentDistinctValues.size()-1) + ']';
        }
        result = result + "\nThe data is divided in " + nrOfChunks + " chunks.";*/
        return result;
    }
}
