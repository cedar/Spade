//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures.estimation;

import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MultidimensionalSingleAggregateEstimate {
    private static final Logger LOGGER = Logger.getLogger(MultidimensionalSingleAggregateEstimate.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public static boolean isEligibleForEarlyStop(
            MultidimensionalSingleAggregate aggregate,
            InterestingnessFunction interestingnessFunction) {
        ArrayList<AnalyzedAttribute> dimensions = aggregate.getDimensions();

        double upperBound = 1.0;
        for (AnalyzedAttribute dimension: dimensions) {
            upperBound *= dimension.getNumberOfDistinctObjects();
        }

        return upperBound <= 100; // too many distinct values, thus too many groups
    }

    public enum EstimationStage {
        INCOMPARABLE_ESTIMATE,
        COMPARABLE_ESTIMATE,
        FULLY_EVALUATED;
    };
    protected MultidimensionalSingleAggregate singleAggregate;
    protected InterestingnessFunction interestingnessFunction;
    protected MultidimensionalAggregateEstimate aggregateEstimate;
    protected boolean dataToAggregateOnIsPresent;
    protected Estimator estimator;
    protected EstimationStage estimationStage;

    /*
		An instance of SingleMultidimensionalAggregateEstimate class can only be created if its
		eligibilty of creation was verified by calling isEligibleForEarlyStop
		method prior to the constructor invokation.
     */
    public MultidimensionalSingleAggregateEstimate(DatabaseHandler handler,
                                                   HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggs,
                                                   MultidimensionalSingleAggregate singleAggregate,
                                                   MultidimensionalAggregateEstimate aggregateEstimate,
                                                   InterestingnessFunction interestingnessFunction,
                                                   double confidenceLevel) {
        this.singleAggregate = singleAggregate;
        this.interestingnessFunction = interestingnessFunction;
        this.aggregateEstimate = aggregateEstimate;
        dataToAggregateOnIsPresent = aggregateEstimate.addEstimator(aggs, singleAggregate, interestingnessFunction, confidenceLevel);
        if (dataToAggregateOnIsPresent) {
            estimator = aggregateEstimate.getEstimator(singleAggregate.getAggregationFunction(), interestingnessFunction);
        }
        else {
            estimator = null;
        }
        estimationStage = EstimationStage.INCOMPARABLE_ESTIMATE;
    }

    public boolean hasDataToAggregateOn() {
        return dataToAggregateOnIsPresent;
    }

    public void sampleFromEachGroup() {
        aggregateEstimate.sampleFromEachGroup();
    }

    public EstimationStage updateEstimates() {
        if (estimator == null) {
            return EstimationStage.INCOMPARABLE_ESTIMATE;
        }

        if (estimationStage.equals(EstimationStage.FULLY_EVALUATED)) {
            return EstimationStage.FULLY_EVALUATED;
        }

        if (aggregateEstimate.getNumberOfTuplesInLastSampling().equals(0)) { // sample from groups, if there are 0 new tuples retrieved, then aggregate is fully evaluated
            estimationStage = EstimationStage.FULLY_EVALUATED;
        }
        else if (estimator.isComparable()) { // estimates are statistically meaningful; sufficiently many tuples have been retrieved
            estimationStage = EstimationStage.COMPARABLE_ESTIMATE;
            // experimental
            /*if (estimator.isGoodApproximation()) {
				estimationStage = EstimationStage.FULLY_EVALUATED;
			}*/
        }

        if (!estimationStage.equals(EstimationStage.INCOMPARABLE_ESTIMATE)) { // update estimates if sufficiently many or all the tuples have been retrieved
            estimator.updateEstimates();
        }

        if (estimationStage.equals(EstimationStage.FULLY_EVALUATED)) {
            singleAggregate.setAggregationResults(estimator.getResult());
            singleAggregate.setInterestingnessFunctionScore(interestingnessFunction, estimator.getPointEstimate());
        }

        return estimationStage;
    }

    public EstimationStage getStage() {
        return estimationStage;
    }

    public Double getPointEstimate() {
        return estimator.getPointEstimate();
    }

    public Double getLowerBoundEstimate() {
        return estimator.getLowerBoundEstimate();
    }

    public Double getUpperBoundEstimate() {
        return estimator.getUpperBoundEstimate();
    }

    public Double getConfidenceIntervalLength() {
        return getUpperBoundEstimate() - getLowerBoundEstimate();
    }

    public MultidimensionalSingleAggregate getMultidimensionalSingleAggregate() {
        return singleAggregate;
    }

    public MultidimensionalAggregateEstimate getMultidimensionalAggregateEstimate() {
        return aggregateEstimate;
    }

    public Double getFinalScore() {
        if (!estimationStage.equals(EstimationStage.FULLY_EVALUATED)) {
            throw new IllegalStateException("This aggregate's score is still being estimated.");
        }

        return getPointEstimate();
    }

    public Double getFinalScoreNoStageCheck() {
        return getPointEstimate();
    }

    @Override
    public String toString() {
        Double pointEstimate = getPointEstimate();
        Double lowerBound = getLowerBoundEstimate();
        Double upperBound = getUpperBoundEstimate();
        Double confidenceLevel = (estimator != null) ? estimator.getConfidenceLevel() * 100 : null;
        Double intervalLength = null;
        if (lowerBound != null && upperBound != null) {
            intervalLength = getConfidenceIntervalLength();
        }

        String pointEstimateString = (pointEstimate == null) ? "null" : pointEstimate.toString();
        String lowerBoundString = (lowerBound == null) ? "null" : lowerBound.toString();
        String upperBoundString = (upperBound == null) ? "null" : upperBound.toString();
        String confidenceLevelString = confidenceLevel.toString();
        String intervalLengthString = (intervalLength == null) ? "null" : intervalLength.toString();
        String stageString = getStage().toString();

        return "Aggregate: " + singleAggregate.toString() + "\nPoint estimate: " + pointEstimateString
               + "\n" + confidenceLevelString + "% confidence interval: ["
               + lowerBoundString + ", " + upperBoundString + "], length: "
               + intervalLengthString + "\nStage: " + stageString;
    }
}
