//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures.estimation;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.NormalDistributionImpl;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class DeltaMethodEstimator extends Estimator {
    private static final Logger LOGGER = Logger.getLogger(DeltaMethodEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected Double[] averages;
    protected Double[] variances;

    public DeltaMethodEstimator(
            DatabaseHandler handler,
            HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates,
            MultidimensionalSingleAggregate aggregate,
            InterestingnessFunction interestingnessFunction,
            Double confidenceLevel,
            HashMap<ArrayList<String>, ResultSet> cursorsByGroup,
            HashMap<ArrayList<String>, ArrayList<Double>> retrievedTuplesByGroup,
            String tableName) {
        super(handler, aggregates, aggregate, interestingnessFunction, confidenceLevel, cursorsByGroup, retrievedTuplesByGroup, tableName);
    }

    protected abstract void precomputeStatisticsOnGroups();

    protected void computeDeltaMethodConfidenceIntervalForVariance() {
        double averagesSum = EfficientComputations.sum(averages);
        double t;
        double tau = 0;
        for (int s = 0; s < D; s++) {
            t = (2.0 / (D - 1.0)) * (averages[s] - (1.0 / D) * averagesSum);
            tau += variances[s] * t * t;
        }
        double quantile = 0;
        try {
            quantile = (new NormalDistributionImpl(0, 1)).inverseCumulativeProbability((confidenceLevel + 1.0) / 2.0);
        }
        catch (MathException ex) {
            throw new IllegalStateException(ex);
        }
        double error = Math.sqrt((quantile * quantile * tau) / m);
        lowerBoundEstimate = pointEstimate - error;
        upperBoundEstimate = pointEstimate + error;
    }

    protected void computeDeltaMethodConfidenceIntervalForSkewness() {
        double averagesSum = EfficientComputations.sum(averages);
        double x, y, t;

        double fx = EfficientComputations.variance(averages);
        double a = Math.pow(fx, (2.0 / 3));

        double c = 0;
        for (int i = 0; i < D; i++) {
            t = averages[i] - (1.0 / D) * averagesSum;
            c += t * t * t;
        }
        c *= 2 / (3 * D);
        double b = c * Math.pow(fx, (-1.0 / 3));

        double d = 0;
        for (int i = 0; i < D; i++) {
            d += averages[i] * averages[i] - (2.0 * averages[i] / D) * averagesSum;
        }
        d /= D;

        double tau = 0;
        for (int s = 0; s < averages.length; s++) {
            x = (3.0 / D) * (averages[s] * averages[s] - d + 2.0 * averages[s] * averagesSum);
            y = (2.0 / (D - 1.0)) * (averages[s] - (1.0 / D) * averagesSum);
            t = a * x + b * y;
            tau += variances[s] * t * t;
        }
        double quantile = 0;
        try {
            quantile = (new NormalDistributionImpl(0, 1)).inverseCumulativeProbability((confidenceLevel + 1.0) / 2.0);
        }
        catch (MathException ex) {
            throw new IllegalStateException(ex);
        }
        double error = Math.sqrt((quantile * quantile * tau) / m);
        lowerBoundEstimate = pointEstimate - error;
        upperBoundEstimate = pointEstimate + error;
    }

    protected void computeDeltaMethodConfidenceIntervalForKurtosis() {
        double averagesSum = EfficientComputations.sum(averages);
        double x, y, t;
        double fx = EfficientComputations.variance(averages);
        double a = Math.pow(((D - 1) / D) * fx, -2.0);

        double c = 0;
        for (int i = 0; i < D; i++) {
            t = averages[i] - (1.0 / D) * averagesSum;
            c += t * t * t * t;
        }
        c *= (2.0 * (D - 1.0) * (D - 1.0)) / (D * D * D * fx * fx * fx);
        double b = c * Math.pow(fx, (-1.0 / 3.0));

        double d = 0;
        for (int i = 0; i < D; i++) {
            d += averages[i] * averages[i] * averages[i] - ((3.0 * averages[i] * averages[i]) / D) + (3.0 * averages[i] / (D * D)) * averagesSum * averagesSum;
        }
        d /= D;

        double tau = 0;
        for (int s = 0; s < averages.length; s++) {
            x = (4.0 / D) * (averages[s] * averages[s] * averages[s] - d + 3.0 * averages[s] * averages[s] * averagesSum + ((3.0 * averages[s]) / D) * averagesSum * averagesSum);
            y = (2.0 / (D - 1.0)) * (averages[s] - (1.0 / D) * averagesSum);
            t = a * x + b * y;
            tau += variances[s] * t * t;
        }
        double quantile = 0;
        try {
            quantile = (new NormalDistributionImpl(0, 1)).inverseCumulativeProbability((confidenceLevel + 1.0) / 2.0);
        }
        catch (MathException ex) {
            throw new IllegalStateException(ex);
        }
        double error = Math.sqrt((quantile * quantile * tau) / m);
        lowerBoundEstimate = pointEstimate - error;
        upperBoundEstimate = pointEstimate + error;
    }
}
