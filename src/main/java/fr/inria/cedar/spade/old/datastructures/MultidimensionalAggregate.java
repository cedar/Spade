//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures;

import fr.inria.cedar.spade.database.DatabaseHandler;
import java.sql.SQLException;
import java.util.ArrayList;

import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MultidimensionalAggregate {
    private static final Logger LOGGER = Logger.getLogger(MultidimensionalAggregate.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected final DatabaseHandler handler;
    protected CandidateFactSet candidateFactSet;
    protected ArrayList<AnalyzedAttribute> dimensions;
    protected AnalyzedAttribute measure;
    protected String auxiliaryTableName;

    public MultidimensionalAggregate(DatabaseHandler handler,
                                     MultidimensionalSingleAggregate multidimensionalSingleAggregate) {
        this.handler = handler;
        candidateFactSet = multidimensionalSingleAggregate.getCandidateFactSet();
        dimensions = multidimensionalSingleAggregate.dimensions;
        measure = multidimensionalSingleAggregate.measure;
    }

    public CandidateFactSet getCandidateFactSet() {
        return candidateFactSet;
    }

    public ArrayList<AnalyzedAttribute> getDimensions() {
        return dimensions;
    }

    public String getDimensionList() {
        StringBuilder dimensionList = new StringBuilder();
        String prefix = "";
        int numberOfDimensions = dimensions.size();
        for (int i = 1; i <= numberOfDimensions; i++) {
            dimensionList.append(prefix).append("d").append(i);
            prefix = ", ";
        }

        return dimensionList.toString();
    }

    public AnalyzedAttribute getMeasure() {
        return measure;
    }

    public void setAuxiliaryTableName(String tableName) {
        auxiliaryTableName = tableName;
    }

    public String getAuxiliaryTableName() {
        return auxiliaryTableName;
    }

    public void dropAuxiliaryTable() {
        if (auxiliaryTableName == null) {
            return;
        }

        String sqlQuery = "drop table " + auxiliaryTableName;
        try {
            handler.executeUpdate(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }

        auxiliaryTableName = null; // table was dropped
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof MultidimensionalAggregate)) {
            return false;
        }

        MultidimensionalAggregate ma = (MultidimensionalAggregate) o;

        return ma.dimensions.equals(((MultidimensionalAggregate) o).dimensions) && ma.measure.equals(((MultidimensionalAggregate) o).measure);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + dimensions.hashCode();
        if (measure != null) {
            result = 31 * result + measure.hashCode();
        }
        return result;
    }
}
