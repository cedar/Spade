//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures.estimation;

import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.database.ResultRow;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class Estimator {
    private static final Logger LOGGER = Logger.getLogger(Estimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected DatabaseHandler handler;
    protected HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates;
    protected MultidimensionalSingleAggregate aggregate;
    protected InterestingnessFunction interestingnessFunction;
    protected Double confidenceLevel;
    protected String tableName;
    protected HashMap<ArrayList<String>, ResultSet> cursorsByGroup;
    protected HashMap<ArrayList<String>, ArrayList<Double>> retrievedTuplesByGroup;
    protected Integer D;
    protected Integer m;
    protected Long numberOfRowsInAuxiliaryTable;
    protected Double pointEstimate;
    protected Double upperBoundEstimate;
    protected Double lowerBoundEstimate;

    public Estimator(
            DatabaseHandler handler,
            HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates,
            MultidimensionalSingleAggregate aggregate,
            InterestingnessFunction interestingnessFunction,
            Double confidenceLevel,
            HashMap<ArrayList<String>, ResultSet> cursorsByGroup,
            HashMap<ArrayList<String>, ArrayList<Double>> retrievedTuplesByGroup,
            String tableName) {
        this.handler = handler;
        this.aggregates = aggregates;
        this.aggregate = aggregate;
        this.interestingnessFunction = interestingnessFunction;
        this.confidenceLevel = confidenceLevel;
        this.tableName = tableName;
        this.cursorsByGroup = cursorsByGroup;
        this.retrievedTuplesByGroup = retrievedTuplesByGroup;
    }

    /*
		Returns true if estimation of interestingness score is statistically
		meaningful.
     */
    public boolean isComparable() {
        m = null;
        int numberOfValuesInGroup;
        for (ArrayList<String> group: retrievedTuplesByGroup.keySet()) {
            numberOfValuesInGroup = retrievedTuplesByGroup.get(group).size();
            if (m == null || m < numberOfValuesInGroup) {
                m = numberOfValuesInGroup;
            }
        }

        return m != null && m >= 30;
    }

    // experimental, use with care!
    /*
		Returns true if estimation of interestingness score was computed from
		at least 1% data points.
     */
    public boolean isGoodApproximation() {
        m = null;
        for (ArrayList<String> group: retrievedTuplesByGroup.keySet()) {
            int numberOfValuesInGroup = retrievedTuplesByGroup.get(group).size();
            if (m == null || m < numberOfValuesInGroup) {
                m = numberOfValuesInGroup;
            }
        }

        if (m == null) {
            return false;
        }

        return m >= 50;
        /*double length = (upperBoundEstimate == null || lowerBoundEstimate == null) ? Double.MAX_VALUE : upperBoundEstimate - lowerBoundEstimate; // the length of the confidence interval
		double lengthThreshold = 0.1;
		boolean shortInterval = length < lengthThreshold;

		return m >= 30 && shortInterval;*/
    }

    protected abstract void updateEstimatesForFeatureScalingNormalizedVariance();

    protected abstract void updateEstimatesForSumNormalizedVariance();

    protected abstract void updateEstimatesForMeanNormalizedVariance();

    protected abstract void updateEstimatesForSkewness();

    protected abstract void updateEstimatesForKurtosis();

    public void updateEstimates() {
        switch (interestingnessFunction) {
            case VAR_FEATURE_SCALING:
                updateEstimatesForFeatureScalingNormalizedVariance();
                break;
            case VAR_SUM:
                updateEstimatesForSumNormalizedVariance();
                break;
            case VAR_MEAN:
                updateEstimatesForMeanNormalizedVariance();
                break;
            case SKEWNESS:
                updateEstimatesForSkewness();
                break;
            case KURTOSIS:
                updateEstimatesForKurtosis();
                break;
            default:
                throw new IllegalStateException("Unsupported interestingness function");
        }
    }

    /*
		Returns a list containing pairs of form (group ID, group's aggregated
		value), using aggregation function specific for the estimator.
     */
    public abstract ArrayList<ResultRow> getResult();

    public Double getPointEstimate() {
        return pointEstimate;
    }

    public Double getLowerBoundEstimate() {
        return lowerBoundEstimate;
    }

    public Double getUpperBoundEstimate() {
        return upperBoundEstimate;
    }

    public Double getConfidenceLevel() {
        return confidenceLevel;
    }
}
