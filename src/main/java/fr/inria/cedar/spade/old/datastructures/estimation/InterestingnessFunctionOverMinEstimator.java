//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures.estimation;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.database.ResultRow;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class InterestingnessFunctionOverMinEstimator extends Estimator {
    private static final Logger LOGGER = Logger.getLogger(InterestingnessFunctionOverMinEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected double globalMin;

    public InterestingnessFunctionOverMinEstimator(
            DatabaseHandler handler,
            HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates,
            MultidimensionalSingleAggregate aggregate,
            InterestingnessFunction interestingnessFunction,
            Double confidenceLevel,
            HashMap<ArrayList<String>, ResultSet> cursorsByGroup,
            HashMap<ArrayList<String>, ArrayList<Double>> retrievedTuplesByGroup,
            String tableName) {
        super(handler, aggregates, aggregate, interestingnessFunction, confidenceLevel, cursorsByGroup, retrievedTuplesByGroup, tableName);
        globalMin = aggregate.getAggregate().getMeasure().getMinValue();
    }

    protected Double[] computeMinInGroups() {
        ArrayList<Double> valuesInGroup;
        int numberOfElementsInGroup;
        Double[] values;
        D = retrievedTuplesByGroup.size(); // number of groups
        Double[] mins = new Double[D];
        int i = 0;
        for (ArrayList<String> group: retrievedTuplesByGroup.keySet()) {
            valuesInGroup = retrievedTuplesByGroup.get(group);

            numberOfElementsInGroup = valuesInGroup.size();
            values = new Double[numberOfElementsInGroup];
            valuesInGroup.toArray(values);

            mins[i] = EfficientComputations.min(values);
            i++;
        }

        return mins;
    }

    protected void computeIntervalForVariance(Double[] values,
                                              double globalMinNormalized) {
        lowerBoundEstimate = 0.0;

        double max = EfficientComputations.max(values);

        // Popoviciu's inequality on variance
        double diff = (max - globalMinNormalized);
        upperBoundEstimate = (diff * diff) / 4;
    }

    @Override
    protected void updateEstimatesForFeatureScalingNormalizedVariance() {
        Double[] mins = computeMinInGroups();

        Double[] normalizedValues = EfficientComputations.normalizeByFeatureScaling(mins);
        pointEstimate = EfficientComputations.variance(normalizedValues);

        double globalMinNormalized = 0;
        computeIntervalForVariance(normalizedValues, globalMinNormalized);
    }

    @Override
    protected void updateEstimatesForSumNormalizedVariance() {
        Double[] mins = computeMinInGroups();

        double sum = EfficientComputations.sum(mins);
        Double[] normalizedValues = EfficientComputations.normalizeByDivisionBySumWithKnownSum(mins, sum);
        pointEstimate = EfficientComputations.variance(normalizedValues);

        double globalMinNormalized = globalMin / sum;
        computeIntervalForVariance(normalizedValues, globalMinNormalized);
    }

    @Override
    protected void updateEstimatesForMeanNormalizedVariance() {
        Double[] mins = computeMinInGroups();

        double mean = EfficientComputations.mean(mins);
        Double[] normalizedValues = EfficientComputations.normalizeByDivisionByMeanWithKnownMean(mins, mean);
        pointEstimate = EfficientComputations.variance(normalizedValues);

        double globalMinNormalized = globalMin / mean;
        computeIntervalForVariance(normalizedValues, globalMinNormalized);
    }

    @Override
    protected void updateEstimatesForSkewness() {
        Double[] mins = computeMinInGroups();

        pointEstimate = EfficientComputations.skewness(mins);

        // TODO: compute interval
    }

    @Override
    protected void updateEstimatesForKurtosis() {
        Double[] mins = computeMinInGroups();

        pointEstimate = EfficientComputations.kurtosis(mins);

        // TODO: compute interval
    }

    @Override
    public ArrayList<ResultRow> getResult() {
        ArrayList<ResultRow> result = new ArrayList<>();
        for (ArrayList<String> group: retrievedTuplesByGroup.keySet()) {
            ArrayList<Double> valuesInGroup = retrievedTuplesByGroup.get(group);
            Double[] values = new Double[valuesInGroup.size()];
            valuesInGroup.toArray(values);

            result.add(new ResultRow(group, EfficientComputations.min(values)));
        }

        return result;
    }
}
