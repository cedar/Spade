//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.operations;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.database.ResultRow;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregate;
import fr.inria.cedar.spade.old.datastructures.MultidimensionalSingleAggregateInTopK;
import fr.inria.cedar.spade.datastructures.SuggestedDerivation;
import fr.inria.cedar.spade.old.datastructures.estimation.MultidimensionalAggregateEstimate;
import fr.inria.cedar.spade.old.datastructures.estimation.MultidimensionalSingleAggregateEstimate;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import fr.inria.cedar.spade.operations.Operation;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MultidimensionalAggregateEvaluationWithEarlyStop extends Operation {
    private static final Logger LOGGER = Logger.getLogger(MultidimensionalAggregateEvaluationWithEarlyStop.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected CandidateFactSet candidateFactSet;
    protected ArrayList<MultidimensionalSingleAggregate> singleAggregates;
    protected HashMap<MultidimensionalAggregate, HashSet<MultidimensionalSingleAggregate>> aggregates;
    protected InterestingnessFunction interestingnessFunction;
    protected TopKSelectorForMultidimensionalSingleAggregates globalTopK;

    protected final void groupSingleAggregatesByDimensionsAndMeasure() {
        aggregates = new HashMap<>();
        HashMap<MultidimensionalAggregate, MultidimensionalAggregate> allKeys = new HashMap<>();

        for (MultidimensionalSingleAggregate singleAggregate: singleAggregates) {
            MultidimensionalAggregate key = singleAggregate.getAggregate();
            if (aggregates.containsKey(key)) {
                allKeys.get(key);
                singleAggregate.setAggregate(key);
                aggregates.get(key).add(singleAggregate);
            }
            else {
                allKeys.put(key, key);
                HashSet<MultidimensionalSingleAggregate> singleAggregatesByDimensionsAndMeasure = new HashSet<>();
                singleAggregatesByDimensionsAndMeasure.add(singleAggregate);
                aggregates.put(key, singleAggregatesByDimensionsAndMeasure);
            }
        }
    }

    public MultidimensionalAggregateEvaluationWithEarlyStop(
            DatabaseHandler handler,
            ArrayList<MultidimensionalSingleAggregate> singleAggregates,
            InterestingnessFunction interestingnessFunction,
            TopKSelectorForMultidimensionalSingleAggregates globalTopK) {
        this.handler = handler;
        this.singleAggregates = singleAggregates;
        groupSingleAggregatesByDimensionsAndMeasure();
        this.interestingnessFunction = interestingnessFunction;
        this.globalTopK = globalTopK;
    }

    protected void removeSingleAggregateFromLocalTopK(
            MultidimensionalAggregateEstimate aggregateEstimate,
            MultidimensionalSingleAggregateEstimate singleAggregateEstimate) {
        aggregateEstimate.removeEstimator(aggregates, singleAggregateEstimate.getMultidimensionalSingleAggregate().getAggregationFunction(), interestingnessFunction);
        MultidimensionalSingleAggregate singleAggregate = singleAggregateEstimate.getMultidimensionalSingleAggregate();
        MultidimensionalAggregate aggregate = singleAggregate.getAggregate();
        aggregates.get(aggregate).remove(singleAggregate);
        if (aggregates.get(aggregate).isEmpty()) {
            aggregates.remove(aggregate);
            aggregate.dropAuxiliaryTable();
        }
    }

    protected void stopSingleAggregateFromLocalTopK(
            MultidimensionalAggregateEstimate aggregateEstimate,
            MultidimensionalSingleAggregateEstimate singleAggregateEstimate,
            String reason,
            long startTime,
            long endTime) {
        removeSingleAggregateFromLocalTopK(aggregateEstimate, singleAggregateEstimate);

        //LOGGER.info("Stopped " + reason + singleAggregateEstimate.toString(time) + "\n");
        LOGGER.info("Stopped " + reason + singleAggregateEstimate.getMultidimensionalSingleAggregate().toShortString(startTime, endTime) + "\n");
    }

    protected void updateLocalTopKAndAggregatesToBeFullyEvaluated(
            TopKSelectorForMultidimensionalSingleAggregateEstimates earlyStopTopK) {
        MultidimensionalSingleAggregateEstimate singleAggregateEstimate;
        MultidimensionalAggregateEstimate aggregateEstimate;
        MultidimensionalSingleAggregate singleAggregate;
        for (Iterator<MultidimensionalSingleAggregateEstimate> it = earlyStopTopK.getAggregates().iterator(); it.hasNext();) {
            singleAggregateEstimate = it.next();
            aggregateEstimate = singleAggregateEstimate.getMultidimensionalAggregateEstimate();
            singleAggregate = singleAggregateEstimate.getMultidimensionalSingleAggregate();
            if (singleAggregateEstimate.getStage() == MultidimensionalSingleAggregateEstimate.EstimationStage.FULLY_EVALUATED
                || singleAggregate.getInterestingnessFunctionScore(interestingnessFunction) != null) { // it was already computed in InterestingnessFunctionOverSumEstimator
                removeSingleAggregateFromLocalTopK(aggregateEstimate, singleAggregateEstimate);
            }
            else {
                // remove the single aggregate estimate from local top-k, it will be evaluated in Postgres (taken from the updated hashmap)
                it.remove();
            }
        }
    }

    // find top-k among early-stop eligible aggregates
    // returns results for single aggregates that were fully computed
    // updates aggregates variable to remove fully computed single aggregates
    protected ArrayList<MultidimensionalSingleAggregate> evaluateWithEarlyStop() {
        long time = System.currentTimeMillis();

        TopKSelectorForMultidimensionalSingleAggregateEstimates earlyStopTopK = new TopKSelectorForMultidimensionalSingleAggregateEstimates(handler, aggregates, globalTopK.getK(), interestingnessFunction, singleAggregates);
        LOGGER.info("Starting early-stop evaluation with aggregates.");//:\n" + earlyStopTopK.toString());
        HashSet<MultidimensionalAggregateEstimate> aggregateEstimates = new HashSet<>();
        boolean allEarlyStopEligibleAggregatesEvaluated = false;
        MultidimensionalSingleAggregateEstimate singleAggregateEstimate;
        MultidimensionalAggregateEstimate aggregateEstimate;
        MultidimensionalSingleAggregateEstimate.EstimationStage stage;
        MultidimensionalSingleAggregateEstimate kthAggregate;
        int iterationsWithoutStoppage = 0;
        int lambdaThreshold = 100;
        String reason;
        Double aggregateUpperBound;
        Double kthLocalTopKAggregateLowerBound;
        Double aggregateFinalScore;
        MultidimensionalSingleAggregateInTopK kthGlobalTopKMultidimensionalSingleAggregateInTopK = globalTopK.getKthAggregate();
        Double kthGlobalTopKAggregateScore = null;
        if (kthGlobalTopKMultidimensionalSingleAggregateInTopK != null) {
            MultidimensionalSingleAggregate kthGlobalTopKMultidimensionalSingleAggregate = kthGlobalTopKMultidimensionalSingleAggregateInTopK.getMultidimensionalSingleAggregate();
            if (kthGlobalTopKMultidimensionalSingleAggregate != null) {
                kthGlobalTopKAggregateScore = kthGlobalTopKMultidimensionalSingleAggregate.getInterestingnessFunctionScore(interestingnessFunction);
            }
        }

        do { // do-while to save one condition evaluation in the beginning (always true)
            aggregateEstimates.clear();
            iterationsWithoutStoppage++;
            kthAggregate = earlyStopTopK.getKthAggregate(); // in the first iteration of the do-while loop the local top-k doesn't need to be sorted
            if (kthAggregate != null && iterationsWithoutStoppage < lambdaThreshold && !allEarlyStopEligibleAggregatesEvaluated) { // there are at most k-1 aggregates left or we reached maximum number of iterations without stoppage, and not all aggregates have been evaluated
                allEarlyStopEligibleAggregatesEvaluated = true;
                for (Iterator<MultidimensionalSingleAggregateEstimate> iter = earlyStopTopK.getAggregates().iterator(); iter.hasNext();) {
                    singleAggregateEstimate = iter.next();
                    aggregateEstimate = singleAggregateEstimate.getMultidimensionalAggregateEstimate();
                    if (!aggregateEstimates.contains(aggregateEstimate)) { // sample only once per multidimensional aggregate (and not for each single aggregate)
                        aggregateEstimates.add(aggregateEstimate);
                        singleAggregateEstimate.sampleFromEachGroup();
                    }
                    stage = singleAggregateEstimate.updateEstimates();

                    if (!stage.equals(MultidimensionalSingleAggregateEstimate.EstimationStage.FULLY_EVALUATED)) {
                        allEarlyStopEligibleAggregatesEvaluated = false;
                        if (stage.equals(MultidimensionalSingleAggregateEstimate.EstimationStage.COMPARABLE_ESTIMATE)) {
                            // compare with kth aggregate in local top-k
                            if (!kthAggregate.getStage().equals(MultidimensionalSingleAggregateEstimate.EstimationStage.INCOMPARABLE_ESTIMATE)) { // both comparable
                                aggregateUpperBound = singleAggregateEstimate.getUpperBoundEstimate();
                                kthLocalTopKAggregateLowerBound = kthAggregate.getLowerBoundEstimate();
                                if (aggregateUpperBound < kthLocalTopKAggregateLowerBound) {
                                    reason = "due to earlyStopAggregateUpperBound: " + aggregateUpperBound + ", kthLocalTopKAggregateLowerBound: " + kthLocalTopKAggregateLowerBound + "\n";
                                    stopSingleAggregateFromLocalTopK(aggregateEstimate, singleAggregateEstimate, reason, time, System.currentTimeMillis());
                                    iter.remove();
                                    iterationsWithoutStoppage = 0;
                                }
                            }
                        }
                    }
                    else { // EstimationStage.FULLY_EVALUATED
                        if (kthGlobalTopKAggregateScore != null) {
                            // compare with kth aggregate in global top-k
                            aggregateFinalScore = singleAggregateEstimate.getFinalScoreNoStageCheck();
                            if (aggregateFinalScore < kthGlobalTopKAggregateScore) {
                                reason = "due to earlyStopAggregateFinalScore: " + aggregateFinalScore + ", kthGlobalTopKAggregateScore: " + kthGlobalTopKAggregateScore + "\n";
                                stopSingleAggregateFromLocalTopK(aggregateEstimate, singleAggregateEstimate, reason, time, System.currentTimeMillis());
                                iter.remove();
                                iterationsWithoutStoppage = 0;
                            }
                        }
                    }
                }
                earlyStopTopK.sortAggregates(); // we must sort the local top-k
            }
            else { // last iteration of the do-while loop
                //LOGGER.info("Terminating early-stop evaluation. The local top-k is:\n" + earlyStopTopK.toString());
                LOGGER.info("Terminating early-stop evaluation. The local top-k is:\n" + earlyStopTopK.toShortString(time, System.currentTimeMillis()));
                // remove unfinished single aggregates from the local top-k and remove the finished ones from the aggregates hash map
                updateLocalTopKAndAggregatesToBeFullyEvaluated(earlyStopTopK);
                break;
            }
        }
        while (true);

        LOGGER.info("Early-stop evaluation time: " + (System.currentTimeMillis() - time) + " ms");

        return earlyStopTopK.getTopKFullyEvaluatedAggregates(); // the local top-k was sorted in the end of the do-while loop
    }

    public static String constructBodyOfAggregateQuery(
            MultidimensionalAggregate aggregate, String selectPart,
            String fromPart) {
        // selectPart is expected to be at least "select column as c", where column and c are variables

        String tableName = aggregate.getAuxiliaryTableName();
        String dimensionList = aggregate.getDimensionList();

        StringBuilder selectPartBuilder = new StringBuilder();
        StringBuilder fromPartBuilder = new StringBuilder();
        selectPartBuilder.append(selectPart);
        fromPartBuilder.append(fromPart);
        if (tableName != null) {
            selectPartBuilder.append(", ").append(dimensionList);
            fromPartBuilder.append(" from ").append(tableName);
        }
        else {
            Long measure;
            fromPartBuilder.append(" from ").append(aggregate.getCandidateFactSet().getTableName()).append(" as cfs");

            long dimensionCode;
            int i = 1;
            for (AnalyzedAttribute dim: aggregate.getDimensions()) {
                dimensionCode = dim.getEncoding();
                selectPartBuilder.append(", t_").append(dimensionCode).append(".o as d").append(i);
                fromPartBuilder.append(" join t_").append(dimensionCode).append(" on t_").append(dimensionCode).append(".s = cfs.s");
                i++;
            }
            if (aggregate.getMeasure() != null) {
                measure = aggregate.getMeasure().getEncoding();
                fromPartBuilder.append(" join t_").append(measure).append(" on t_").append(measure).append(".s = cfs.s");

                if (aggregate.getMeasure().getDerivedFrom() == null
                    || aggregate.getMeasure().getDerivationType() != SuggestedDerivation.COUNT_ATTR) { // not derived or not count
                    fromPartBuilder.append(" join dictionary dict on dict.key = t_").append(measure).append(".o");
                }
            }
        }

        return selectPartBuilder.toString() + fromPartBuilder.toString();
    }

    protected static String constructGroupByPartOfAggregateQuery(
            MultidimensionalAggregate aggregate, String groupByPart) {
        String tableName = aggregate.getAuxiliaryTableName();

        StringBuilder groupByPartBuilder = new StringBuilder();
        groupByPartBuilder.append(" group by (");

        if (tableName != null) {
            groupByPartBuilder.append(aggregate.getDimensionList());
        }
        else {
            long dimensionCode;
            String prefix = "";
            for (AnalyzedAttribute dim: aggregate.getDimensions()) {
                dimensionCode = dim.getEncoding();
                groupByPartBuilder.append(prefix).append("t_").append(dimensionCode).append(".o");

                prefix = ", ";
            }
        }

        groupByPartBuilder.append(")");

        return groupByPartBuilder.toString();
    }

    protected static String constructGroupByDimensionsWithoutMeasureAggregateQuery(
            MultidimensionalAggregate aggregate) {
        String selectPart = "select count(*) as count_star";
        String fromPart = "";
        String groupByPart = "";

        return constructBodyOfAggregateQuery(aggregate, selectPart, fromPart) + constructGroupByPartOfAggregateQuery(aggregate, groupByPart);
    }

    protected String constructGroupByDimensionsWithMeasureAggregateQuery(
            MultidimensionalAggregate aggregate) {
        StringBuilder aggregationFunctionsBuilder = new StringBuilder();
        aggregationFunctionsBuilder.append("select ");

        String tableName = aggregate.getAuxiliaryTableName();
        String measureAlias;
        if (tableName != null) {
            measureAlias = "m";
        }
        else {
            if (aggregate.getMeasure().getDerivedFrom() == null
                || aggregate.getMeasure().getDerivationType() != SuggestedDerivation.COUNT_ATTR) { // not derived or not count
                measureAlias = "dict.value";
            }
            else {
                measureAlias = "t_" + aggregate.getMeasure().getEncoding() + ".o";
            }
        }

        String prefix = "";
        AggregationFunction af;
        for (MultidimensionalSingleAggregate agg: aggregates.get(aggregate)) {
            aggregationFunctionsBuilder.append(prefix);

            af = agg.getAggregationFunction();
            switch (af) {
                case COUNT_STAR:
                    aggregationFunctionsBuilder.append("count(*) as count_star");
                    break;
                case COUNT:
                    aggregationFunctionsBuilder.append("count(").append(measureAlias).append(") as count");
                    break;
                case COUNT_DISTINCT:
                    aggregationFunctionsBuilder.append("count(distinct ").append(measureAlias).append(") as count_distinct");
                    break;
                case MIN:
                    aggregationFunctionsBuilder.append("min(cast(").append(measureAlias).append(" as integer)) as min");
                    break;
                case MAX:
                    aggregationFunctionsBuilder.append("max(cast(").append(measureAlias).append(" as integer)) as max");
                    break;
                case SUM:
                    aggregationFunctionsBuilder.append("sum(cast(").append(measureAlias).append(" as integer)) as sum");
                    break;
                case AVERAGE:
                    aggregationFunctionsBuilder.append("avg(cast(").append(measureAlias).append(" as integer)) as average");
                    break;
                /*case AVERAGE_DISTINCT:
					aggregationFunctionsBuilder.append("avg(distinct cast(").append(measureAlias).append(" as integer)) as average_distinct");
					break;*/
                default:
                    break;
            }

            prefix = ", ";
        }

        String selectPart = aggregationFunctionsBuilder.toString();
        String fromPart = "";
        String groupByPart = "";

        return constructBodyOfAggregateQuery(aggregate, selectPart, fromPart) + constructGroupByPartOfAggregateQuery(aggregate, groupByPart);
    }

    protected String constructAggregateQuery(MultidimensionalAggregate aggregate) {
        AnalyzedAttribute measure = aggregate.getMeasure();
        if (measure == null) {
            return constructGroupByDimensionsWithoutMeasureAggregateQuery(aggregate);
        }

        return constructGroupByDimensionsWithMeasureAggregateQuery(aggregate);
    }

    protected Double computeInterestingnessScore(
            ArrayList<ResultRow> aggregationResults) {
        int numberOfAggregationResults;
        Double[] aggregateValues = null;
        Double[] aggregateValuesNormalizedByFeatureScaling;
        String[] nonNumericalValues = null;
        Double score;

        numberOfAggregationResults = aggregationResults.size();
        switch (interestingnessFunction) {
            case VAR_FEATURE_SCALING:
            case VAR_SUM:
            case VAR_MEAN:
            case SKEWNESS:
            case KURTOSIS:
                aggregateValues = new Double[numberOfAggregationResults];
                for (int i = 0; i < numberOfAggregationResults; i++) {
                    aggregateValues[i] = aggregationResults.get(i).getAggregatedValue();
                }
                break;
            case ENTROPY:
            case NORMALIZED_ENTROPY:
            case CUSTOM_ENTROPY:
                nonNumericalValues = new String[numberOfAggregationResults];
                for (int i = 0; i < numberOfAggregationResults; i++) {
                    nonNumericalValues[i] = Double.toString(aggregationResults.get(i).getAggregatedValue());
                }
                break;
        }

        switch (interestingnessFunction) {
            case VAR_FEATURE_SCALING:
                aggregateValuesNormalizedByFeatureScaling = EfficientComputations.normalizeByFeatureScaling(aggregateValues);
                score = EfficientComputations.variance(aggregateValuesNormalizedByFeatureScaling);
                break;
            case VAR_SUM:
                score = EfficientComputations.varianceOnRawValuesNormalizedByDivisionBySum(aggregateValues);
                break;
            case VAR_MEAN:
                score = EfficientComputations.varianceOnRawValuesNormalizedByDivisionByMean(aggregateValues);
                break;
            case SKEWNESS:
                score = EfficientComputations.skewness(aggregateValues);
                break;
            case KURTOSIS:
                score = EfficientComputations.kurtosis(aggregateValues);
                break;
            case ENTROPY:
                score = EfficientComputations.shannonEntropy(nonNumericalValues);
                break;
            case NORMALIZED_ENTROPY:
                score = EfficientComputations.normalizedShannonEntropy(nonNumericalValues);
                break;
            case CUSTOM_ENTROPY:
                score = EfficientComputations.normalizedShannonEntropy(nonNumericalValues);
                score = 2 * (1 - Math.abs(score - 0.5) - 0.5); // custom adjustment of the normalized  score
                break;
            default:
                score = null;
                break;
        }

        return score;
    }

    protected ArrayList<MultidimensionalSingleAggregate> evaluateInPostgres() {
        long time = System.currentTimeMillis();

        String sqlQuery = null;
        HashMap<AggregationFunction, ArrayList<ResultRow>> resultsOfQuery;
        ArrayList<ResultRow> aggregationResults;
        Double score;
        ArrayList<MultidimensionalSingleAggregate> result = new ArrayList<>();

        for (MultidimensionalAggregate aggregate: aggregates.keySet()) {
            try {
                sqlQuery = constructAggregateQuery(aggregate);
                resultsOfQuery = handler.getResultsForMultidimensionalCombinedAggregates(sqlQuery, aggregate.getDimensions().size());

                for (MultidimensionalSingleAggregate singleAggregate: aggregates.get(aggregate)) {
                    aggregationResults = resultsOfQuery.get(singleAggregate.getAggregationFunction());
                    if (aggregationResults.isEmpty()) {
                        continue; // skip this single aggregate
                    }
                    singleAggregate.setAggregationResults(aggregationResults);

                    score = computeInterestingnessScore(aggregationResults);
                    singleAggregate.setInterestingnessFunctionScore(interestingnessFunction, score);

                    result.add(singleAggregate);
                }
            }
            catch (SQLException ex) {
                throw new IllegalStateException("sqlQuery: " + sqlQuery + "; " + ex);
            }
        }

        LOGGER.info("Regular evaluation time: " + (System.currentTimeMillis() - time) + " ms");

        return result;
    }

    public void evaluate() {
        // early-stop evaluation
        ArrayList<MultidimensionalSingleAggregate> earlyStopResults = evaluateWithEarlyStop();

        // update global top-k with early-stop results
        globalTopK.addEarlyStopAggregatesAndUpdateTopK(earlyStopResults);

        // regular evaluation
        ArrayList<MultidimensionalSingleAggregate> regularEvaluationResults = evaluateInPostgres();

        // drop tables
        for (MultidimensionalSingleAggregate aggregate: regularEvaluationResults) {
            aggregate.getAggregate().dropAuxiliaryTable();
        }

        // update global top-k with regular evaluation results
        globalTopK.addEarlyStopAggregatesAndUpdateTopK(regularEvaluationResults);
    }
}
