//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.molap;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import fr.inria.cedar.spade.datastructures.SuggestedDerivation;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MOLAPTranslator {
    private static final Logger LOGGER = Logger.getLogger(MOLAPTranslator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected ArrayList<AnalyzedAttribute> dimensions;
    protected ArrayList<AnalyzedAttribute> measures;
    protected HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsPerEachAttribute;
    protected HashMap<AnalyzedAttribute, Boolean> avgForEachAttribute;
    protected CandidateFactSet candidateFactSet;
    protected DatabaseHandler handler;
    protected HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesInChunkPerDimension;
    protected HashMap<AnalyzedAttribute, ArrayList<String>> distinctValuesOfDimensions;
    protected HashMap<AnalyzedAttribute, Integer> nrOfGroupsPerDimension;
    protected int nrOfChunks;
    protected HashMap<Integer, BufferedWriter> referencesToChunks;
    protected HashMap<Integer, String> chunksFileNames;
    protected String directoryName;
    int[] multiIdxChunk;
    int[] multiIdxOffset;

    public MOLAPTranslator(ArrayList<AnalyzedAttribute> dimensions,
                           ArrayList<AnalyzedAttribute> measures,
                           HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsPerEachAttribute,
                           CandidateFactSet candidateFactSet,
                           DatabaseHandler handler,
                           HashMap<AnalyzedAttribute, Integer> nrOfDistinctValuesInChunkPerDimension) {
        this.dimensions = dimensions;
        this.measures = measures;
        avgForEachAttribute = new HashMap<>();
        for (Map.Entry<AnalyzedAttribute, LinkedList<AggregationFunction>> entry: aggregationFunctionsPerEachAttribute.entrySet()) {
            if (entry.getValue().contains(AggregationFunction.AVERAGE)) {
                avgForEachAttribute.put(entry.getKey(), true);
                entry.getValue().remove(entry.getValue().indexOf(AggregationFunction.AVERAGE));
            }
        }
        this.aggregationFunctionsPerEachAttribute = aggregationFunctionsPerEachAttribute;

        this.candidateFactSet = candidateFactSet;
        this.handler = handler;
        this.nrOfDistinctValuesInChunkPerDimension = nrOfDistinctValuesInChunkPerDimension;

        distinctValuesOfDimensions = new HashMap<>();
        referencesToChunks = new HashMap<>();
        chunksFileNames = new HashMap<>();

        orderDimensionsAccordingToNrOfDistinctValues();
        getDistinctValuesOfDimensions();

        this.nrOfGroupsPerDimension = new HashMap<>();
        nrOfChunks = 1;
        for (AnalyzedAttribute ap: dimensions) {
            int nrDisVals = distinctValuesOfDimensions.get(ap).size();
            int nrDisValsPerChunk = nrOfDistinctValuesInChunkPerDimension.get(ap);
            int nrOfGroups = nrDisVals / nrDisValsPerChunk;
            if ((nrDisVals % nrDisValsPerChunk) != 0) {
                nrOfGroups = nrOfGroups + 1;
            }
            nrOfChunks = nrOfChunks * nrOfGroups;
            nrOfGroupsPerDimension.put(ap, nrOfGroups);
        }

        multiIdxChunk = new int[dimensions.size()];
        multiIdxOffset = new int[dimensions.size()];

        directoryName = GlobalSettings.MOLAP_FOLDER_PATH + GlobalSettings.DATABASE_NAME + "/data/chunked_" + candidateFactSet.getEncoding() + "_using";
        for (AnalyzedAttribute d: dimensions) {
            directoryName = directoryName + "_" + d.getEncoding();
        }
        directoryName = directoryName + "/chunks/";
        File directory = new File(directoryName);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    /*
     * chunk the data, store the chunks in the db and return a Catalog with all the information
     * */
    public Catalog translate() {
        /*for (AnalyzedAttribute d: dimensions) {
			LOGGER.info(d.getEncoding() + ": " + distinctValuesOfDimensions.get(d));
		}*/
        chunkData();
        String tableName = dumpChunksToDB();
        return new Catalog(candidateFactSet, dimensions, measures, nrOfChunks, tableName, directoryName, handler, nrOfDistinctValuesInChunkPerDimension, distinctValuesOfDimensions, aggregationFunctionsPerEachAttribute, avgForEachAttribute, nrOfGroupsPerDimension);
    }

    /*
     * get the data from the database, assign each retrieved tuple to the correct chunk
     * */
    private void chunkData() {
        String sqlQuery = constructQueryToGetData();
        //LOGGER.info(sqlQuery);
        long start = System.currentTimeMillis();
        ResultSet rs = handler.getResultSet(sqlQuery);
        long totalTimeToGetData = System.currentTimeMillis() - start;
        LOGGER.info("Total time to get the data: " + totalTimeToGetData + " ms");
        //LOGGER.info(sqlQuery);

        start = System.currentTimeMillis();
        ArrayList<String> tuple;
        ArrayList<String> measures;
        try {
            ResultSetMetaData metadata = rs.getMetaData();
            int nrOfColumns = metadata.getColumnCount();

            while (rs.next()) {
                tuple = new ArrayList<>();
                measures = new ArrayList<>();
                int i;
                // read the values of the dimensions
                for (i = 1; i <= dimensions.size(); i++) {
                    String s = rs.getString(i);
                    if (s == null) {
                        s = "null";
                    }
                    tuple.add(s);
                }

                for (; i <= nrOfColumns; i++) {
                    String s = rs.getString(i);
                    if (s == null) {
                        s = "null";
                    }
                    measures.add(s);
                }
                addTupleToChunk(tuple, measures);
            }
            for (Map.Entry<Integer, BufferedWriter> entry: referencesToChunks.entrySet()) {
                entry.getValue().close();
            }
            rs.close();
        }
        catch (SQLException | IOException ex) {
            throw new IllegalStateException(ex);
        }
        long totalTimeToChunkData = System.currentTimeMillis() - start;
        LOGGER.info("Total time to chunk the data: " + totalTimeToChunkData + " ms");
    }

    private void addTupleToChunk(ArrayList<String> dims, ArrayList<String> meas) {
        /*
         * given the values of the dimensions find:
         * - the position of each value in the global ordered list of the distinct values of the considered dimension
         * - the group to which each value belongs
         * - the position of the value inside the group
         * */
        ArrayList<Integer> globalOrder = new ArrayList<>();
        ArrayList<Integer> groupOrder = new ArrayList<>();
        ArrayList<Integer> localOrder = new ArrayList<>();
        for (int i = 0; i < dims.size(); i++) {
            ArrayList<String> allDistValues = distinctValuesOfDimensions.get(dimensions.get(i));
            int globalIdx = allDistValues.indexOf(dims.get(i));
            int groupIdx = globalIdx / nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));
            int localIdx = globalIdx % nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));
            globalOrder.add(globalIdx);
            groupOrder.add(groupIdx);
            localOrder.add(localIdx);
        }

        int chunkId = getChunkId(groupOrder);
        int offset = getOffsetInChunk(localOrder);

        String strTuple = "" + offset;
        for (String m: meas) {
            strTuple = strTuple + " " + m;
        }
        strTuple = strTuple + "\n";

        //LOGGER.info(strTuple);
        if (referencesToChunks.containsKey(chunkId)) {
            try {
                referencesToChunks.get(chunkId).write(strTuple);
            }
            catch (IOException ex) {
                throw new IllegalStateException(ex);
            }
        }
        else {
            try {
                chunksFileNames.put(chunkId, directoryName + "CHUNK_" + chunkId);
                BufferedWriter writer = new BufferedWriter(new FileWriter(directoryName + "CHUNK_" + chunkId));
                writer.write(strTuple);
                referencesToChunks.put(chunkId, writer);
            }
            catch (IOException ex) {
                throw new IllegalStateException(ex);
            }
        }
        //printDecodedValue(chunkId, offset);
    }

    private String dumpChunksToDB() {
        String tableName = "chunked_" + candidateFactSet.getEncoding() + "_using";
        for (AnalyzedAttribute d: dimensions) {
            tableName = tableName + "_" + d.getEncoding();
        }
        String sqlQuery = "CREATE TABLE IF NOT EXISTS " + tableName + " ( chunkid INTEGER PRIMARY KEY, chunkdata BYTEA );";
        try {
            handler.executeUpdate(sqlQuery);

            sqlQuery = "INSERT INTO " + tableName + " VALUES (?, ?)";
            try (PreparedStatement ps = handler.getPreparedStatement(sqlQuery)) {
                for (int i = 0; i < nrOfChunks; i++) {
                    if (chunksFileNames.containsKey(i)) {
                        ps.setInt(1, i);
                        File file = new File(chunksFileNames.get(i));
                        ps.setBinaryStream(2, new FileInputStream(file), (int) file.length());
                        ps.executeUpdate();
                    }
                }
                if (!GlobalSettings.DATABASE_AUTOCOMMIT) {
                    handler.commit();
                }
            }
        }
        catch (SQLException | FileNotFoundException ex) {
            throw new IllegalStateException(ex);
        }
        return tableName;
    }

    /*
     * ordering ABC here means that A is the one that changes more frequently while is the one that changes slowly.
     * */
    private int getChunkId(ArrayList<Integer> groupOrderMultiIdx) {
        int currentDenom = 1, chunkId = 0;

        for (int i = 0; i < dimensions.size() - 1; i++) {
            currentDenom = currentDenom * nrOfGroupsPerDimension.get(dimensions.get(i));
        }

        for (int i = groupOrderMultiIdx.size() - 1; i >= 0; i--) {
            chunkId = chunkId + (groupOrderMultiIdx.get(i) * currentDenom);
            if (i > 0) {
                currentDenom = currentDenom / nrOfGroupsPerDimension.get(dimensions.get(i - 1));
            }
        }
        return chunkId;
    }

    private int getOffsetInChunk(ArrayList<Integer> multiDimIdx) {
        int currentDenom = 1, offset = 0;

        for (int i = 0; i < dimensions.size() - 1; i++) {
            currentDenom = currentDenom * nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));
        }

        for (int i = multiDimIdx.size() - 1; i >= 0; i--) {
            offset = offset + (multiDimIdx.get(i) * currentDenom);
            if (i > 0) {
                currentDenom = currentDenom / nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i - 1));
            }
        }
        return offset;
    }

    private void printDecodedValue(int chunkID, int offset) {
        LOGGER.info("\nchunkId " + chunkID + " and offset " + offset + " correspond to values: ");
        int currentDenom = 1;
        for (int i = 0; i < dimensions.size() - 1; i++) {
            currentDenom = currentDenom * nrOfGroupsPerDimension.get(dimensions.get(i));
        }

        for (int i = multiIdxChunk.length - 1; i >= 0; i--) {
            multiIdxChunk[i] = chunkID / currentDenom;
            chunkID = chunkID % currentDenom;
            if (i > 0) {
                currentDenom = currentDenom / nrOfGroupsPerDimension.get(dimensions.get(i - 1));
            }
        }

        currentDenom = 1;
        for (int i = 0; i < dimensions.size() - 1; i++) {
            currentDenom = currentDenom * nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i));
        }

        for (int i = multiIdxOffset.length - 1; i >= 0; i--) {
            multiIdxOffset[i] = offset / currentDenom;
            offset = offset % currentDenom;
            if (i > 0) {
                currentDenom = currentDenom / nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i - 1));
            }
        }

        for (int i = 0; i < dimensions.size(); i++) {
            ArrayList<String> distValues = distinctValuesOfDimensions.get(dimensions.get(i));
            int idx = multiIdxChunk[i] * nrOfDistinctValuesInChunkPerDimension.get(dimensions.get(i)) + multiIdxOffset[i];
            LOGGER.info(distValues.get(idx) + ", ");
        }
    }

    private class AttributeComparator implements
            Comparator<AnalyzedAttribute> {
        @Override
        public int compare(AnalyzedAttribute attribute1,
                           AnalyzedAttribute attribute2) {
            Integer value1 = (Integer) attribute1.getNumberOfDistinctObjects();
            Integer value2 = (Integer) attribute2.getNumberOfDistinctObjects();

            if (value1 == null && value2 == null) {
                return 0;
            }
            if (value1 == null) {
                return 1;
            }
            if (value2 == null) {
                return -1;
            }

            return value1.compareTo(value2);
        }
    }

    public void orderDimensionsAccordingToNrOfDistinctValues() {
        Collections.sort(dimensions, new AttributeComparator());
    }

    /*
     * given an attribute, queries the database to retrieve all its distinct values
     * and returns them in ascending order
     * */
    private void getDistinctValuesOfDimensions() {
        ArrayList<String> result;
        String sqlQuery = "";
        for (AnalyzedAttribute ap: dimensions) {
            try {
                if (ap.getDerivationType() != null && ap.getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
                    sqlQuery = "select distinct o from " + candidateFactSet.getTableName() + " as cfs join t_" + ap.getEncoding() + " as T on cfs.s=T.s order by o asc";
                    result = handler.getResultSelectFromObjectColumn(sqlQuery);
                }
                else {
                    sqlQuery = "select distinct value from " + candidateFactSet.getTableName() + " as cfs join t_" + ap.getEncoding() + " as T on cfs.s=T.s join dictionary as D on T.o=D.key order by value asc";
                    result = handler.getResultSelectFromValueColumn(sqlQuery);
                }

            }
            catch (SQLException ex) {
                throw new IllegalStateException("Error occured when reading/writing to files. " + sqlQuery + "\n" + ex);
            }
            result.add("null");
            distinctValuesOfDimensions.put(ap, result);
        }
    }

    private String constructQueryToGetData() {
        StringBuilder sqlQuery = new StringBuilder("select ");

        // select clause: first consider the dimensions, then the measures
        for (int i = 0; i < dimensions.size(); i++) {
            if (dimensions.get(i).getDerivedFrom() != null && dimensions.get(i).getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
                sqlQuery.append("d").append(i + 1).append(".o, ");
            }
            else {
                sqlQuery.append("DD").append(i + 1).append(".value, ");
            }
        }
        sqlQuery.append("count(*) as COUNT_STAR "); // count star is always considered

        if (measures != null) {
            for (int i = 0; i < measures.size(); i++) {
                LinkedList<AggregationFunction> afs = aggregationFunctionsPerEachAttribute.get(measures.get(i));
                if (afs != null) {
                    for (AggregationFunction af: afs) {
                        switch (af) {
                            case COUNT:
                                sqlQuery.append(", SUM(M").append(i).append(".COUNT_M)");
                                break;
                            case MAX:
                                sqlQuery.append(", MAX(M").append(i).append(".MAX_M)");
                                break;
                            case MIN:
                                sqlQuery.append(", MIN(M").append(i).append(".MIN_M)");
                                break;
                            case SUM:
                                sqlQuery.append(", SUM(M").append(i).append(".SUM_M)");
                                break;
                        }
                    }
                }
            }
        }

        sqlQuery.append(" from ").append(candidateFactSet.getTableName()).append(" as cfs ");
        // dimensions and measures derived from counting do not need to be decoded using the dictionary
        for (int i = 0; i < dimensions.size(); i++) {
            if (dimensions.get(i).getDerivedFrom() != null && dimensions.get(i).getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
                sqlQuery.append(" left join t_").append(dimensions.get(i).getEncoding()).append(" d").append(i + 1).append(" on cfs.s=d").append(i + 1).append(".s ");
            }
            else {
                sqlQuery.append(" left join (t_").append(dimensions.get(i).getEncoding()).append(" d").append(i + 1).append(" join dictionary DD").append(i + 1).append(" on d").append(i + 1).append(".o=DD").append(i + 1).append(".key) on cfs.s=d").append(i + 1).append(".s ");
            }
        }

        if (measures != null) {
            for (int i = 0; i < measures.size(); i++) {
                sqlQuery.append(" left join ( select s ");
                if (measures.get(i).getDerivedFrom() != null && measures.get(i).getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
                    for (AggregationFunction af: aggregationFunctionsPerEachAttribute.get(measures.get(i))) {
                        switch (af) {
                            case COUNT:
                                sqlQuery.append(", ").append("count(o) as COUNT_M");
                                break;
                            case MAX:
                                sqlQuery.append(", ").append("max(cast(o as double precision)) as MAX_M");
                                break;
                            case MIN:
                                sqlQuery.append(", ").append("min(cast(o as double precision)) as MIN_M");
                                break;
                            case SUM:
                                sqlQuery.append(", ").append("sum(cast(o as double precision)) as SUM_M");
                                break;
                        }
                    }
                }
                else {
                    for (AggregationFunction af: aggregationFunctionsPerEachAttribute.get(measures.get(i))) {
                        switch (af) {
                            case COUNT:
                                sqlQuery.append(", ").append("count(value) as COUNT_M");
                                break;
                            case MAX:
                                sqlQuery.append(", ").append("max(cast(value as double precision)) as MAX_M");
                                break;
                            case MIN:
                                sqlQuery.append(", ").append("min(cast(value as double precision)) as MIN_M");
                                break;
                            case SUM:
                                sqlQuery.append(", ").append("sum(cast(value as double precision)) as SUM_M");
                                break;
                        }
                    }
                }
                sqlQuery.append(" from t_").append(measures.get(i).getEncoding());
                if (!(measures.get(i).getDerivedFrom() != null && measures.get(i).getDerivationType() == SuggestedDerivation.COUNT_ATTR)) {
                    sqlQuery.append(" join dictionary on key=o ");
                }
                sqlQuery.append(" group by s ) as M").append(i).append(" on cfs.s=M").append(i).append(".s");
            }
        }

        sqlQuery.append(" group by ");
        String prefix = " ";
        for (int i = 0; i < dimensions.size(); i++) {
            if (dimensions.get(i).getDerivedFrom() != null && dimensions.get(i).getDerivationType() == SuggestedDerivation.COUNT_ATTR) {
                sqlQuery.append(prefix).append("d").append(i + 1).append(".o");
            }
            else {
                sqlQuery.append(prefix).append("DD").append(i + 1).append(".value");
            }
            prefix = ",";
        }

        return sqlQuery.toString();
    }
}
