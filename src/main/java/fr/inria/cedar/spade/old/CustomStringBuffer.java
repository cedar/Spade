//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public final class CustomStringBuffer {
    private static final Logger LOGGER = Logger.getLogger(CustomStringBuffer.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    final static int[] SIZE_TABLE = {9, 99, 999, 9999, 99999, 999999, 9999999,
                                     99999999, 999999999, Integer.MAX_VALUE};
    private int capacity;
    private int size;
    private char[] buffer;

    public CustomStringBuffer(String initialContent, int capacity) {
        this.capacity = capacity;
        size = 0;
        buffer = new char[capacity];
        append(initialContent);
    }

    public int getCapacity() {
        return capacity;
    }

    public int getSize() {
        return size;
    }

    private void changeCapacity(int newCapacity, boolean preserveBufferContent) {
        char[] newBuffer = new char[newCapacity];
        if (preserveBufferContent) {
            System.arraycopy(buffer, 0, newBuffer, 0, capacity);
        }
        capacity = newCapacity;
        buffer = newBuffer;
    }

    public void resize(int newSize, boolean preserveBufferContent) {
        if (newSize > capacity) {
            changeCapacity(capacity, preserveBufferContent);
        }
        else {
            size = newSize;
        }
    }

    // copied from Integer class -->
    final static char[] DIGIT_TENS = {
        '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
        '1', '1', '1', '1', '1', '1', '1', '1', '1', '1',
        '2', '2', '2', '2', '2', '2', '2', '2', '2', '2',
        '3', '3', '3', '3', '3', '3', '3', '3', '3', '3',
        '4', '4', '4', '4', '4', '4', '4', '4', '4', '4',
        '5', '5', '5', '5', '5', '5', '5', '5', '5', '5',
        '6', '6', '6', '6', '6', '6', '6', '6', '6', '6',
        '7', '7', '7', '7', '7', '7', '7', '7', '7', '7',
        '8', '8', '8', '8', '8', '8', '8', '8', '8', '8',
        '9', '9', '9', '9', '9', '9', '9', '9', '9', '9',};

    final static char[] DIGIT_ONES = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',};

    final static char[] DIGITS = {
        '0', '1', '2', '3', '4', '5',
        '6', '7', '8', '9', 'a', 'b',
        'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't',
        'u', 'v', 'w', 'x', 'y', 'z'
    };

    static void integerGetChars(int i, int index, char[] buf) {
        int q, r;
        int charPos = index;
        char sign = 0;

        if (i < 0) {
            sign = '-';
            i = -i;
        }

        // Generate two digits per iteration
        while (i >= 65536) {
            q = i / 100;
            // really: r = i - (q * 100);
            r = i - ((q << 6) + (q << 5) + (q << 2));
            i = q;
            buf[--charPos] = DIGIT_ONES[r];
            buf[--charPos] = DIGIT_TENS[r];
        }

        // Fall thru to fast mode for smaller numbers
        // assert(i <= 65536, i);
        for (;;) {
            q = (i * 52429) >>> (16 + 3);
            r = i - ((q << 3) + (q << 1));  // r = i-(q*10) ...
            buf[--charPos] = DIGITS[r];
            i = q;
            if (i == 0) {
                break;
            }
        }
        if (sign != 0) {
            buf[--charPos] = sign;
        }
    }
    // <-- copied from Integer class

    // inspired by Integer class
    static int stringSize(int x) {
        int i = 0;
        while (x > SIZE_TABLE[i]) {
            i++;
        }

        return i + 1;
    }

    public CustomStringBuffer append(int i) {
        if (i == Integer.MIN_VALUE) {
            append("-2147483648");

            return this;
        }

        int appendedLength = (i < 0) ? stringSize(-i) + 1 : stringSize(i);
        int newSize = size + appendedLength;
        if (newSize > capacity) {
            changeCapacity(capacity, true);
        }

        // copy int characters to the buffer starting from newSize position back
        integerGetChars(i, newSize, buffer);
        // change the logical content size
        size = newSize;

        return this;
    }

    public CustomStringBuffer append(String string) {
        if (string == null) {
            throw new IllegalStateException("The string is null");
        }
        int appendedLength = string.length();
        if (appendedLength == 0) {
            return this;
        }
        int newSize = size + appendedLength;
        if (newSize > capacity) {
            changeCapacity(capacity, true);
        }

        // copy string characters to the buffer after the current logical content
        string.getChars(0, appendedLength, buffer, size);
        // change the logical content size
        size = newSize;

        return this;
    }

    public CustomStringBuffer append(Object object) {
        return append(String.valueOf(object));
    }
}

