//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.old.datastructures;

import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.database.ResultRow;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.inria.cedar.spade.datastructures.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MultidimensionalSingleAggregate {
    private static final Logger LOGGER = Logger.getLogger(MultidimensionalSingleAggregate.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected DatabaseHandler handler;
    protected CandidateFactSet candidateFactSet;
    protected ArrayList<AnalyzedAttribute> dimensions;
    protected AnalyzedAttribute measure;
    protected AggregationFunction aggregationFunction;
    protected MultidimensionalAggregate aggregate;
    protected ArrayList<ResultRow> aggregationResults;
    protected HashMap<InterestingnessFunction, Double> interestingnessFunctionScores;

    public MultidimensionalSingleAggregate(DatabaseHandler handler,
                                           CandidateFactSet candidateFactSet,
                                           ArrayList<AnalyzedAttribute> dimensions,
                                           AnalyzedAttribute measure,
                                           AggregationFunction aggregationFunction) {
        this.handler = handler;
        this.candidateFactSet = candidateFactSet;
        this.dimensions = dimensions;
        this.measure = measure;
        this.aggregationFunction = aggregationFunction;
        this.aggregationResults = new ArrayList<>();
        this.interestingnessFunctionScores = new HashMap<>();
        this.aggregate = new MultidimensionalAggregate(handler, this);
    }

    public CandidateFactSet getCandidateFactSet() {
        return candidateFactSet;
    }

    public ArrayList<AnalyzedAttribute> getDimensions() {
        return dimensions;
    }

    public AnalyzedAttribute getMeasure() {
        return measure;
    }

    public AggregationFunction getAggregationFunction() {
        return aggregationFunction;
    }

    public void setAggregationResults(ArrayList<ResultRow> ar) {
        aggregationResults = ar;
    }

    public ArrayList<ResultRow> getAggregationResults() {
        return aggregationResults;
    }

    public void setInterestingnessFunctionScore(
            InterestingnessFunction interestingnessFunction,
            Double score) {
        interestingnessFunctionScores.put(interestingnessFunction, score);
    }

    public HashMap<InterestingnessFunction, Double> getInterestingnessFunctionScores() {
        return interestingnessFunctionScores;
    }

    public Double getInterestingnessFunctionScore(
            InterestingnessFunction interestingnessFunction) {
        return interestingnessFunctionScores.getOrDefault(interestingnessFunction, null);
    }

    public void setAggregate(MultidimensionalAggregate agg) {
        aggregate = agg;
    }

    public MultidimensionalAggregate getAggregate() {
        return aggregate;
    }

    protected String decodeCandidateFactSet(boolean shortName) {
        String candidateFactSetName;
        String sqlQuery = "select value from dictionary where key = " + aggregate.getCandidateFactSet().getEncoding();
        try {
            candidateFactSetName = handler.getSQLSelectSingleResult(sqlQuery);
            if (shortName) {
                return Utils.getShorterAttributeName(candidateFactSetName);
            }
            return candidateFactSetName;
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Error in decoding the candidate fact set of single attribute: " + sqlQuery + "; " + ex);
        }
    }

    protected String getValueFromDictionary(long code) throws SQLException {
        String sqlQuery = "select value from dictionary where key = " + code;
        return handler.getSQLSelectSingleResult(sqlQuery);
    }

    protected String decodeAttributeName(AnalyzedAttribute attribute,
                                         boolean shortName) {
        Pattern p = Pattern.compile("\\d+");
        Matcher m;

        String decoded = null;
        try {
            decoded = getValueFromDictionary(attribute.getEncoding());
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }

        AnalyzedAttribute derivedFrom = attribute.getDerivedFrom();
        if (derivedFrom != null) {
            SuggestedDerivation derivationType = attribute.getDerivationType();
            if (derivationType != SuggestedDerivation.PATH) {
                try {
                    decoded = getValueFromDictionary(derivedFrom.getEncoding());
                    if (shortName) {
                        decoded = Utils.getShorterAttributeName(decoded);
                    }
                }
                catch (SQLException ex) {
                    throw new IllegalStateException(ex);
                }
                switch (derivationType) {
                    case COUNT_ATTR:
                        decoded = "count(" + decoded + ")";
                        break;
                    case KWD_IN_TEXT:
                        decoded = "keywords(" + decoded + ")";
                        break;
                    case LANG_DET:
                        decoded = "language(" + decoded + ")";
                        break;
                }

                return decoded;
            }

            // for paths need to decode all the attributes in the path
            boolean analyze = false;
            StringTokenizer multiTokenizer = new StringTokenizer(decoded, "/>");
            StringBuilder decodedPath = new StringBuilder();

            while (multiTokenizer.hasMoreTokens()) {
                String elem = multiTokenizer.nextToken();
                if (analyze) {
                    try {
                        decoded = getValueFromDictionary(Long.parseLong(elem));
                        if (shortName) {
                            decoded = Utils.getShorterAttributeName(decoded);
                        }
                        decodedPath.append("/").append(decoded);
                    }
                    catch (SQLException ex) {
                        throw new IllegalStateException(ex);
                    }
                }

                if (elem.startsWith("Summary")) {
                    m = p.matcher(elem);
                    while (m.find()) {
                        try {
                            decoded = getValueFromDictionary(Long.parseLong(m.group()));
                            if (shortName) {
                                decoded = Utils.getShorterAttributeName(decoded);
                            }
                            decodedPath.append(decoded);
                        }
                        catch (SQLException ex) {
                            throw new IllegalStateException(ex);
                        }
                    }
                    analyze = true;
                }
            }

            return decodedPath.toString();
        }

        if (shortName) {
            return Utils.getShorterAttributeName(decoded);
        }

        return decoded;
    }

    protected String decodeDimensions(boolean shortName) {
        String dimensionName;
        String dimensionsName = "";
        String prefix = "";
        for (AnalyzedAttribute dimension: dimensions) {
            dimensionName = decodeAttributeName(dimension, shortName);
            /*AnalyzedAttribute attributeDimensionWasDerivedFrom = dimension.getDerivedFrom();
			dimensionCode = dimension.At();
			if (attributeDimensionWasDerivedFrom != null && dimension.getDerivationType() != SuggestedDerivation.PATH) { // dimension is derived
				dimensionCode = attributeDimensionWasDerivedFrom.getAttribute();
			}

			sqlQuery = "select value from dictionary where key = " + dimensionCode;
			try {
				dimensionName = handler.getSQLSelectSingleResult(sqlQuery);
			}
			catch (SQLException ex) {
				throw new IllegalStateException("Error in decoding the dimension of single attribute: " + sqlQuery + "; " + ex);
			}

			if (attributeDimensionWasDerivedFrom != null) {
				SuggestedDerivation sd = dimension.getDerivationType();
				if (sd != SuggestedDerivation.PATH) {
					dimensionName = sd.toString() + " " + dimensionName;
				}
				else {
					sqlQuery = "SELECT value from dictionary where key = " + attributeDimensionWasDerivedFrom.getAttribute();
					try {
						dimensionName = sd.toString() + handler.getSQLSelectSingleResult(sqlQuery);
					}
					catch (SQLException ex) {
						throw new IllegalStateException("Error in decoding the dimension of single attribute: " + sqlQuery + "; " + ex);
					}

					String pattern = "/([0-9]+)";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(dimensionName);

					boolean skipfrist = true;
					String code = null;
					while (m.find()) {
						if (skipfrist) {
							skipfrist = false;
						}
						else {
							sqlQuery = "select value from dictionary where key = " + m.group(1);
							try {
								code = handler.getSQLSelectSingleResult(sqlQuery);
							} catch (SQLException ex) {
								throw new IllegalStateException("Error in decoding the dimension of single attribute: " + sqlQuery + "; " + ex);
							}
							dimensionName = dimensionName + "/" + code;
						}
					}
				}
			}*/
            dimensionsName += prefix + dimensionName;
            prefix = ", ";
        }

        return dimensionsName;
    }

    protected String decodeMeasure(boolean shortName) {
        if (measure == null) {
            return "No measure";
        }

        return decodeAttributeName(measure, shortName);

        /*Long measureCode = aggregate.getMeasure().getAttribute();
		AnalyzedAttribute attributeMeasureWasDerivedFrom = aggregate.getMeasure().getDerivedFrom();
		String measureName = null;
		if (attributeMeasureWasDerivedFrom != null && measure.getDerivationType() != SuggestedDerivation.PATH) {
			measureCode = measure.getAttribute();
		}
		String sqlQuery = "select value from dictionary where key = " + measureCode;
		try {
			measureName = handler.getSQLSelectSingleResult(sqlQuery);
		}
		catch (SQLException ex) {
			throw new IllegalStateException("Error in decoding the measure of single attribute. " + sqlQuery + "; " + ex);
		}
		if (attributeMeasureWasDerivedFrom != null) {
			SuggestedDerivation sd = aggregate.getMeasure().getDerivationType();
			measureName = sd.toString() + " " + measureName;
		}

		return measureName;*/
    }

    @Override
    public String toString() {
        String candidateFactSetName = decodeCandidateFactSet(false);
        String dimensionsName = decodeDimensions(false);
        String measureName = decodeMeasure(false);

        String stringRepresentation = "\n-\tCandidateFactSet: " + candidateFactSetName
                                      + "\n \tDimensions: " + dimensionsName
                                      + "\n \tMeasure: " + measureName
                                      + "\n \tAggregate: " + aggregationFunction.toString().toLowerCase();

        // get interestingness scores
        for (InterestingnessFunction interestingnessFunction: interestingnessFunctionScores.keySet()) {
            stringRepresentation += "\n \t" + interestingnessFunction + ": " + interestingnessFunctionScores.get(interestingnessFunction);
        }

        return stringRepresentation;
    }

    public String toShortString(long startTime, long endTime) {
        String candidateFactSetName = decodeCandidateFactSet(true);
        String dimensionsName = decodeDimensions(true);
        String measureName = decodeMeasure(true);

        String startTimeDate = (new SimpleDateFormat("HH:mm:ss.SSS")).format(new Date(startTime));
        String endTimeDate = (new SimpleDateFormat("HH:mm:ss.SSS")).format(new Date(endTime));

        return "(" + candidateFactSetName + ", (" + dimensionsName + "), " + measureName + ", " + aggregationFunction.toString().toLowerCase() + ") " + startTimeDate + " " + endTimeDate;
    }
}
