//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.database;

import java.util.ArrayList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ResultRow {
    private static final Logger LOGGER = Logger.getLogger(ResultRow.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected final ArrayList<String> dimensions;
    protected final Double aggregatedValue;

    public ResultRow(ArrayList<String> dimensions, Double aggregatedValue) {
        this.dimensions = dimensions;
        this.aggregatedValue = aggregatedValue;
    }

    public ArrayList<String> getDimensions() {
        return dimensions;
    }

    public Double getAggregatedValue() {
        return aggregatedValue;
    }
}
