//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.database;

import fr.inria.cedar.ontosql.db.DatabaseUtils;
import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.SuggestedDerivation;
import fr.inria.cedar.spade.datastructures.Utils;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class DatabaseHandler {
    private static final Logger LOGGER = Logger.getLogger(DatabaseHandler.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected Connection conn;
    protected int fetchSize;
    protected PreparedStatement lastPreparedStatement;

    public DatabaseHandler(Connection conn) {
        this.conn = conn;
        this.lastPreparedStatement = null;
    }

    public Connection getConnection(){
        return conn;
    }

    public void setFetchSize(int fetchSize) {
        this.fetchSize = fetchSize;
    }

    public void setAutoCommit(boolean value) throws SQLException {
        conn.setAutoCommit(value);
    }

    public void commit() throws SQLException {
        conn.commit();
    }

    protected void closeLastPreparedStatement() {
        if (lastPreparedStatement != null) {
            try {
                if (!lastPreparedStatement.isClosed()) {
                    lastPreparedStatement.close();
                }
            }
            catch (SQLException ex) {
                throw new IllegalStateException(ex);
            }
        }
    }

    public PreparedStatement getPreparedStatementWithoutClosingLastOne(
            String statement) {
        try {
            PreparedStatement prep = conn.prepareStatement(statement, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            prep.setFetchSize(fetchSize);

            return prep;
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Error occured when preparing the statement. " + ex);
        }
    }

    public PreparedStatement getPreparedStatement(String statement) {
        closeLastPreparedStatement();

        try {
            lastPreparedStatement = conn.prepareStatement(statement, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            lastPreparedStatement.setFetchSize(fetchSize);

            return lastPreparedStatement;
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Error occured when preparing the statement. " + ex);
        }
    }

    public void rollback() throws SQLException {
        conn.rollback();
    }

    public void closeConnection() throws SQLException {
        closeLastPreparedStatement();

        conn.close();
    }

    public void executeUpdate(String sqlQuery) throws SQLException {
        try (PreparedStatement stmt = getPreparedStatement(sqlQuery)) {
            stmt.executeUpdate();
        }
        if (!GlobalSettings.DATABASE_AUTOCOMMIT) {
            conn.commit();
        }
    }

    /**
     * Execute a query without returning the results
     *
     * //@param sqlQuery sql query to execute
     *
     * //@throws SQLException Error Code: 02000 Condition Name: no_data => This error is normal (and expected) for this function.
     * @param encoding
     * @return
     */
    /*public void executeQuery(String sqlQuery) throws SQLException {
		try (PreparedStatement stmt = getPreparedStatement(sqlQuery)) {
			stmt.executeQuery();
		}
		catch (SQLException ex) {
			if (!ex.getSQLState().equals("02000")) {
				throw new SQLException(ex);
			}
		}
	}*/

    /**
     * Generic method for getting a result set out of the database
     *
     * @param sqlQuery
     *
     * @return the result set
     */
    public ResultSet getResultSet(String sqlQuery) {
        PreparedStatement stmt;
        ResultSet rs = null;
        try {
            stmt = getPreparedStatement(sqlQuery);
            rs = stmt.executeQuery();
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Could not open result set: " + sqlQuery + "; " + ex);
        }

        return rs;
    }

    public String getValue(String encoding) {
        String value = null;
        String sqlQuery = "select cleanvalue from dictionary where key = " + encoding;

        try { // retrieve the result of the query
            value = getSQLSelectSingleResult(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Error in getting the value of an encoding: " + sqlQuery + "; " + ex);
        }

        return value;
    }

    public String decodeAttributeName(AnalyzedAttribute attribute) {
        if (attribute == null) {
            return null;
        }

        Pattern p = Pattern.compile("\\d+");
        Matcher m;
        String decoded;

        AnalyzedAttribute derivedFrom = attribute.getDerivedFrom();
        if (derivedFrom != null) {
            SuggestedDerivation derivationType = attribute.getDerivationType();
            if (derivationType != SuggestedDerivation.PATH) {
                decoded = Utils.getShorterAttributeName(getValue(Long.toString(derivedFrom.getEncoding())));
                switch (derivationType) {
                    case COUNT_ATTR:
                        decoded = "count(" + decoded + ")";
                        break;
                    case KWD_IN_TEXT:
                        decoded = "keywords(" + decoded + ")";
                        break;
                    case LANG_DET:
                        decoded = "language(" + decoded + ")";
                        break;
                }

                return decoded;
            }

            // for paths need to decode all the attributes in the path
            boolean analyze = false;
            decoded = getValue(Long.toString(attribute.getEncoding()));
            StringTokenizer multiTokenizer = new StringTokenizer(decoded, "/>");
            StringBuilder decodedPath = new StringBuilder();

            String elem;
            while (multiTokenizer.hasMoreTokens()) {
                elem = multiTokenizer.nextToken();
                if (analyze) {
                    decodedPath.append("/").append(Utils.getShorterAttributeName(getValue(elem)));
                }

                if (elem.startsWith("Summary")) {
                    m = p.matcher(elem);
                    while (m.find()) {
                        decodedPath.append(Utils.getShorterAttributeName(getValue(m.group())));
                    }
                    analyze = true;
                }
            }

            return decodedPath.toString();
        }

        return Utils.getShorterAttributeName(getValue(Long.toString(attribute.getEncoding())));
    }

    /*
	* Select from object column
	* */
    public ArrayList<String> getResultSelectFromObjectColumn(
            final String sqlQuery) throws SQLException {
        ArrayList<String> result = new ArrayList<>();
        PreparedStatement stmt;
        ResultSet rs = null;
        try {
            stmt = getPreparedStatement(sqlQuery);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(rs.getString("o"));
            }
        }
        finally {
            DatabaseUtils.tryClose(rs);
        }
        return result;
    }

    /*
	* Select from object column
	* */
    public ArrayList<String> getResultSetSelectFromSummaryNodesColumn(
            final String sqlQuery) throws SQLException {
        ArrayList<String> result = new ArrayList<>();
        PreparedStatement stmt;
        ResultSet rs = null;
        try {
            stmt = getPreparedStatement(sqlQuery);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(rs.getString("summarynode"));
            }
        }
        finally {
            DatabaseUtils.tryClose(rs);
        }
        return result;
    }

    /**
     * Gets the results of two columns: subject, count
     *
     * @param sqlQuery SQL query to execute
     *
     * @return SQL query results HashMap<property (String), count(*) (Integer)>
     *
     * @throws java.sql.SQLException
     */
    public HashMap<String, Integer> getResultSelectFromSubjectAndCount(
            final String sqlQuery) throws SQLException {
        HashMap<String, Integer> result = new HashMap<>();
        PreparedStatement stmt;
        ResultSet rs = null;
        try {
            stmt = getPreparedStatement(sqlQuery);
            rs = stmt.executeQuery();
            while (rs.next()) {
                String property = rs.getString("s");
                int count = rs.getInt("count");
                result.put(property, count);
            }
        }
        finally {
            DatabaseUtils.tryClose(rs);
        }
        return result;
    }

    public HashMap<String, String> getResultSelectFromSubjectAndObject(
            final String sqlQuery) throws SQLException {
        HashMap<String, String> result = new HashMap<>();
        PreparedStatement stmt;
        ResultSet rs = null;
        try {
            stmt = getPreparedStatement(sqlQuery);
            rs = stmt.executeQuery();
            while (rs.next()) {
                String subject = rs.getString("s");
                String object = rs.getString("o");
                result.put(subject, object);
            }
        }
        finally {
            DatabaseUtils.tryClose(rs);
        }
        return result;
    }

    public HashMap<AggregationFunction, ArrayList<ResultRow>> getResultsForMultidimensionalCombinedAggregates(
            String sqlQuery, int numberOfdimensions) throws SQLException {
        HashMap<AggregationFunction, ArrayList<ResultRow>> results = new HashMap<>();

        PreparedStatement stmt;
        ResultSet rs = null;
        try {
            stmt = getPreparedStatement(sqlQuery);
            rs = stmt.executeQuery();

            ResultSetMetaData metaData = rs.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            // the initial hashmap contains each retrieved aggregate associated to an empty list of results
            for (int i = 1; i <= numberOfColumns - numberOfdimensions; i++) { // column numbering starts with 1, aggregatation functions first
                results.put(AggregationFunction.getAggregate(metaData.getColumnName(i)), new ArrayList<>());
            }

            ArrayList<String> group;
            while (rs.next()) {
                group = new ArrayList<>();
                // dimensions are after aggregatation functions
                for (int i = numberOfColumns - numberOfdimensions + 1; i <= numberOfColumns; i++) {
                    group.add(rs.getString(i));
                }
                // aggregatation function are first
                for (int i = 1; i <= numberOfColumns - numberOfdimensions; i++) {
                    results.get(AggregationFunction.getAggregate(metaData.getColumnName(i))).add(new ResultRow(group, rs.getDouble(i)));
                }
            }
        }
        finally {
            DatabaseUtils.tryClose(rs);
        }

        return results;
    }

    // --------------- WHAT FOLLOWS HAS BEEN ADDED IN ORDER TO USE THE SUMMARY --------
    public String getSQLSelectSingleResult(String sqlQuery) throws SQLException {
        String result = "-1";
        PreparedStatement stmt;
        ResultSet rs = null;
        try {
            stmt = getPreparedStatement(sqlQuery);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result = rs.getString(1);
            }
        }
        finally {
            DatabaseUtils.tryClose(rs);
        }
        return result;
    }

    public String getSQLSelectFirstResult(String sqlQuery) throws SQLException {
        String result = "-1";
        PreparedStatement stmt;
        ResultSet rs = null;
        try {
            stmt = getPreparedStatement(sqlQuery);
            rs = stmt.executeQuery();
            if (rs.next()) {
                result = rs.getString(1);
            }
        }
        finally {
            DatabaseUtils.tryClose(rs);
        }
        return result;
    }

    public ArrayList<String> getResultSelectFromValueColumn(
            final String sqlQuery) throws SQLException {
        ArrayList<String> result = new ArrayList<>();
        PreparedStatement stmt;
        ResultSet rs = null;
        try {
            stmt = getPreparedStatement(sqlQuery);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(rs.getString("value"));
            }
        }
        finally {
            DatabaseUtils.tryClose(rs);
        }
        return result;
    }

    public ArrayList<String> getResultSelectFromKeyColumn(final String sqlQuery) throws SQLException {
        ArrayList<String> result = new ArrayList<>();
        PreparedStatement stmt;
        ResultSet rs = null;
        try {
            stmt = getPreparedStatement(sqlQuery);
            rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(rs.getString("key"));
            }
        }
        finally {
            DatabaseUtils.tryClose(rs);
        }
        return result;
    }
}
