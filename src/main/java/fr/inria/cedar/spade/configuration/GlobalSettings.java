//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.configuration;

import fr.inria.cedar.spade.datastructures.CandidateFactSetSelectionMethod;
import java.io.*;
import java.util.Properties;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class GlobalSettings {
    private static final Logger LOGGER = Logger.getLogger(GlobalSettings.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    // configuration folder
    public static final String CONFIGURATION_FOLDER_PATH = "conf/";

    // Postgres server setting
    public static String DATABASE_USER;
    public static String DATABASE_PASSWORD;
    public static String DATABASE_URL;
    public static String DATABASE_PORT;
    public static String DATABASE_NAME;
    public static final boolean DATABASE_AUTOCOMMIT = false;
    public static final int DATABASE_FETCHSIZE = 10000;

    // RDF class property constants
    public static final String TYPE_PROPERTY = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";
    public static int TYPE_PROPERTY_ENCODING;

    // table names
    public static String SUMMARY_TABLE_NAME;
    public static String SUMMARY_REP_TABLE_NAME;

    // attibute derivations
    public static boolean ENABLE_COUNT_DERIVATION;
    public static boolean ENABLE_PATH_DERIVATION;
    public static boolean ENABLE_KWD_EXTRACTION;
    public static boolean ENABLE_LANGUAGE_DETECTION;
    public static String DERIVED_ATTRIBUTE_PREFIX;
    public static int MAX_DERIVATION_DEPTH;
    public static int PATH_DERIVATION_LENGTH;
    public static int LENGTH_FOR_KWD_DETECTION;
    public static int LENGTH_FOR_LANGUAGE_DETECTION;

    // thresholds
    public static double DISTINCT_OBJECTS_TO_SUBJECTS_RATIO_THRESHOLD;
    public static double SUPPORT_THRESHOLD;
    public static double ALMOST_ONE_THRESHOLD;
    public static long MAX_NUMBER_OF_DISTINCT_VALUES_PER_DIMENSION;

    // frequent itemset mining
    public static int NUMBER_OF_TUPLES_TO_SAMPLE_FOR_FREQUENT_ITEMSET_MINING;

    // fundamental parameters
    public static CandidateFactSetSelectionMethod CANDIDATE_FACT_SET_SELECTION;
    public static boolean RECOMPUTE_STATISTICS_WITHIN_CANDIDATE_FACT_SET;
    public static int K_FOR_TOP_K;
    public static String INTERESTINGNESS_FUNCTION;

    // MOLAP
    public static int MAX_NUMBER_OF_DIMENSIONS = 4;
    public static int DIMENSION_OF_CHUNK = -1;

    // Early-stop
    public static int NUMBER_OF_TUPLES_TO_SAMPLE_FROM_EACH_GROUP;
    public static int EARLY_STOP_RETRIEVAL_BATCH_SIZE;
    public static int EARLY_STOP_MAX_NUMBER_OF_ITERATIONS_WITHOUT_STOPPAGE;
    public static boolean ONLY_AVERAGE_MODE;

    // aggregate evaluation module: molap, cube
    public static String AGGREGATE_EVALUATION_MODULE;
    public static boolean ALLOW_USE_OF_PREAGGREGATED_MEASURES_TABLES;
    public static boolean STORE_ALL_AGGREGATES_AMONG_THOSE_COMPUTED_MORE_THAN_ONCE;
    public static boolean OPTIMIZE_MMSTS;
    public static String CUBE_TYPE_OF_COUNT;

    // input/output folder paths
    public static String MOLAP_FOLDER_PATH;
    public static String FLASK_FOLDER_PATH;
    public static String PERFORMANCE_MEASUREMENTS_FILE_NAME;
    public static String TOP_K_RESULTS_FILE_NAME;

    public static String KEY_VALUE_STORE;
    public static int REDIS_MAX_PIPELINE_SIZE;
    public static String BERKELEYDB_ENVIRONMEMT;
    public static String BERKELEYDB_FEATURES;
    public static int NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME;

    // profiling the size of bitmaps
    public static boolean PROFILE_BITMAP_SIZE;
    public static String PROFILE_BITMAP_SIZE_FILE_NAME;

    public static void setUp() {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            String filename = CONFIGURATION_FOLDER_PATH + "configuration.properties";
            input = new FileInputStream(filename);
            prop.load(input);

            // database
            DATABASE_URL = prop.getProperty("DATABASE_URL");
            DATABASE_PORT = prop.getProperty("DATABASE_PORT");
            DATABASE_USER = prop.getProperty("DATABASE_USER");
            DATABASE_PASSWORD = prop.getProperty("DATABASE_PASSWORD");
            DATABASE_NAME = prop.getProperty("DATABASE_NAME");

            // attibute derivations
            ENABLE_COUNT_DERIVATION = Boolean.parseBoolean(prop.getProperty("ENABLE_COUNT_DERIVATION"));
            ENABLE_PATH_DERIVATION = Boolean.parseBoolean(prop.getProperty("ENABLE_PATH_DERIVATION"));
            ENABLE_KWD_EXTRACTION = Boolean.parseBoolean(prop.getProperty("ENABLE_KWD_EXTRACTION"));
            ENABLE_LANGUAGE_DETECTION = Boolean.parseBoolean(prop.getProperty("ENABLE_LANGUAGE_DETECTION"));
            DERIVED_ATTRIBUTE_PREFIX = "<http://derived.org/";
            MAX_DERIVATION_DEPTH = Integer.parseInt(prop.getProperty("MAX_DERIVATION_DEPTH"));
            PATH_DERIVATION_LENGTH = Integer.parseInt(prop.getProperty("PATH_DERIVATION_LENGTH"));
            LENGTH_FOR_KWD_DETECTION = Integer.parseInt(prop.getProperty("LENGTH_FOR_KWD_DETECTION"));
            LENGTH_FOR_LANGUAGE_DETECTION = Integer.parseInt(prop.getProperty("LENGTH_FOR_LANGUAGE_DETECTION"));

            // thresholds
            DISTINCT_OBJECTS_TO_SUBJECTS_RATIO_THRESHOLD = Double.parseDouble(prop.getProperty("DISTINCT_OBJECTS_TO_SUBJECTS_RATIO_THRESHOLD"));
            SUPPORT_THRESHOLD = Double.parseDouble(prop.getProperty("SUPPORT_THRESHOLD"));
            ALMOST_ONE_THRESHOLD = Double.parseDouble(prop.getProperty("ALMOST_ONE_THRESHOLD"));
            MAX_NUMBER_OF_DISTINCT_VALUES_PER_DIMENSION = Long.parseLong(prop.getProperty("MAX_NUMBER_OF_DISTINCT_VALUES_PER_DIMENSION"));

            // fundamental parameters
            CANDIDATE_FACT_SET_SELECTION = CandidateFactSetSelectionMethod.convert(prop.getProperty("CANDIDATE_FACT_SET_SELECTION"));
            RECOMPUTE_STATISTICS_WITHIN_CANDIDATE_FACT_SET = Boolean.parseBoolean(prop.getProperty("RECOMPUTE_STATISTICS_WITHIN_CANDIDATE_FACT_SET"));
            K_FOR_TOP_K = Integer.parseInt(prop.getProperty("K_FOR_TOP_K"));
            INTERESTINGNESS_FUNCTION = prop.getProperty("INTERESTINGNESS_FUNCTION");

            // MOLAP
            MOLAP_FOLDER_PATH = prop.getProperty("MOLAP_FOLDER_PATH");
            MAX_NUMBER_OF_DIMENSIONS = Integer.parseInt(prop.getProperty("MAX_NUMBER_OF_DIMENSIONS"));
            DIMENSION_OF_CHUNK = Integer.parseInt(prop.getProperty("DIMENSION_OF_CHUNK"));
            OPTIMIZE_MMSTS = Boolean.parseBoolean(prop.getProperty("OPTIMIZE_MMSTS"));

            NUMBER_OF_TUPLES_TO_SAMPLE_FOR_FREQUENT_ITEMSET_MINING = Integer.parseInt(prop.getProperty("NUMBER_OF_TUPLES_TO_SAMPLE_FOR_FREQUENT_ITEMSET_MINING"));

            // Early-stop
            NUMBER_OF_TUPLES_TO_SAMPLE_FROM_EACH_GROUP = Integer.parseInt(prop.getProperty("NUMBER_OF_TUPLES_TO_SAMPLE_FROM_EACH_GROUP"));
            EARLY_STOP_RETRIEVAL_BATCH_SIZE = Integer.parseInt(prop.getProperty("EARLY_STOP_RETRIEVAL_BATCH_SIZE"));
            EARLY_STOP_MAX_NUMBER_OF_ITERATIONS_WITHOUT_STOPPAGE = Integer.parseInt(prop.getProperty("EARLY_STOP_MAX_NUMBER_OF_ITERATIONS_WITHOUT_STOPPAGE"));
            ONLY_AVERAGE_MODE = Boolean.parseBoolean(prop.getProperty("ONLY_AVERAGE_MODE"));

            AGGREGATE_EVALUATION_MODULE = prop.getProperty("AGGREGATE_EVALUATION_MODULE");
            ALLOW_USE_OF_PREAGGREGATED_MEASURES_TABLES = Boolean.parseBoolean(prop.getProperty("ALLOW_USE_OF_PREAGGREGATED_MEASURES_TABLES"));
            STORE_ALL_AGGREGATES_AMONG_THOSE_COMPUTED_MORE_THAN_ONCE = Boolean.parseBoolean(prop.getProperty("STORE_ALL_AGGREGATES_AMONG_THOSE_COMPUTED_MORE_THAN_ONCE"));
            CUBE_TYPE_OF_COUNT = prop.getProperty("CUBE_TYPE_OF_COUNT");

            FLASK_FOLDER_PATH = prop.getProperty("FLASK_FOLDER_PATH");
            TOP_K_RESULTS_FILE_NAME = prop.getProperty("TOP_K_RESULTS_FILE_NAME");
            PERFORMANCE_MEASUREMENTS_FILE_NAME = prop.getProperty("PERFORMANCE_MEASUREMENTS_FILE_NAME");

            KEY_VALUE_STORE = prop.getProperty("KEY_VALUE_STORE");
            REDIS_MAX_PIPELINE_SIZE = Integer.parseInt(prop.getProperty("REDIS_MAX_PIPELINE_SIZE"));
            BERKELEYDB_ENVIRONMEMT = prop.getProperty("BERKELEYDB_ENVIRONMEMT");
            BERKELEYDB_FEATURES = prop.getProperty("BERKELEYDB_FEATURES");
            NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME = Integer.parseInt(prop.getProperty("NUMBER_OF_TUPLES_TO_RETRIEVE_FROM_KEY_VALUE_STORE_AT_THE_TIME"));

            PROFILE_BITMAP_SIZE = Boolean.parseBoolean(prop.getProperty("PROFILE_BITMAP_SIZE"));
            PROFILE_BITMAP_SIZE_FILE_NAME = prop.getProperty("PROFILE_BITMAP_SIZE_FILE_NAME");
        }
        catch (IOException | NullPointerException ex) {
            throw new IllegalStateException(ex);
        }
        finally {
            if (input != null) {
                try {
                    input.close();
                }
                catch (IOException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }
    }
}
