//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.configuration;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class PerformanceMeasurements {
    private static final Logger LOGGER = Logger.getLogger(PerformanceMeasurements.class.getName());

    // candidate fact set number -> lattice number -> field -> value
    protected static HashMap<Integer, HashMap<Integer, HashMap<String, Object[]>>> resultsPerCandidateFactSetPerLattice;

    protected static int currentCandidateFactSetNumber;

    // global
    public static Long[] timeToSelectCandidateFactSets;
    public static Long[] totalTimeToExportAggregateResultsToCSVFiles;
    public static Long[] timeToComputeTopK;
    public static Long[] totalExecutionTime;

    public static Long[] totalNumberOfAggregates;
    public static Long[] totalNumberOfStoppedAggregates;

    // for each candidate fact set
    public static String[] candidateFactSetEncoding;
    public static String[] candidateFactSetDecoded;

    public static Long[] timeToRetrieveFactsAndAttributesOfCandidateFactSet;
    public static Long[] timeToAnalyzeAttributes;
    public static Long[] timeToSelectLattices;
    public static Long[] timeToComputeMMSTs;

    public static Long[] numberOfFacts;

    // for each candidate fact set and for each lattice
    public static Long[] timeToLoadPreaggregatedMeasuresValues;
    public static Long[] timeToSetUp;
    public static Long[] timeToRetrieveData;
    public static Long[] timeToChunkData;
    public static Long[] timeToPerformReservoirSampling;
    public static Long[] timeToPostprocessSample;
    public static Long[] timeToChunkDataAndSample;
    public static Long[] timeToDumpChunksToDB;
    public static Long[] timeToTranslateData;
    public static Long[] timeToPropagateEarlyStopSample;
    public static Long[] timeToInstantiateMMST;
    public static Long[] totalWallTimeToRunThreadsInParallel;
    public static Long[] timeToRunEarlyStopPruning;
    public static Long[] timeToLoadChunkToRoot;
    public static Long[] timeToUpdateBitmaps;
    public static Long[] timeToEmptyMemoryAllocation;
    public static Long[] timeToComputeAndStoreAggregateResults;
    public static Long[] timeToEvaluateAllChunksInMOLAP;
    public static Long[] totalEvaluationTime;

    public static String[] latticeDimensions;
    public static String[] latticeMeasures;

    public static Long[] numberOfAggregates;
    public static Long[] numberOfStoppedAggregates;

    static {
        LOGGER.setLevel(Level.INFO);

        resultsPerCandidateFactSetPerLattice = new HashMap<>();

        timeToSelectCandidateFactSets = new Long[1];
        totalTimeToExportAggregateResultsToCSVFiles = new Long[1];
        timeToComputeTopK = new Long[1];
        totalExecutionTime = new Long[1];

        totalNumberOfAggregates = new Long[1];
        totalNumberOfStoppedAggregates = new Long[1];

        timeToSelectCandidateFactSets[0] = 0L;
        totalTimeToExportAggregateResultsToCSVFiles[0] = 0L;
        timeToComputeTopK[0] = 0L;
        totalExecutionTime[0] = 0L;

        totalNumberOfAggregates[0] = 0L;
        totalNumberOfStoppedAggregates[0] = 0L;
    }

    public static void setCandidateFactSet(int candidateFactSetNumber) {
        currentCandidateFactSetNumber = candidateFactSetNumber;
        resultsPerCandidateFactSetPerLattice.put(candidateFactSetNumber, new HashMap<>());

        candidateFactSetEncoding = new String[1];
        candidateFactSetDecoded = new String[1];

        timeToRetrieveFactsAndAttributesOfCandidateFactSet = new Long[1];
        timeToAnalyzeAttributes = new Long[1];
        timeToSelectLattices = new Long[1];
        timeToComputeMMSTs = new Long[1];

        numberOfFacts = new Long[1];

        timeToRetrieveFactsAndAttributesOfCandidateFactSet[0] = 0L;
        timeToAnalyzeAttributes[0] = 0L;
        timeToSelectLattices[0] = 0L;
        timeToComputeMMSTs[0] = 0L;

        numberOfFacts[0] = 0L;

        HashMap<String, Object[]> measurements = new HashMap<>();
        measurements.put("candidateFactSetEncoding", candidateFactSetEncoding);
        measurements.put("candidateFactSetDecoded", candidateFactSetDecoded);

        measurements.put("timeToRetrieveFactsAndAttributesOfCandidateFactSet", timeToRetrieveFactsAndAttributesOfCandidateFactSet);
        measurements.put("timeToAnalyzeAttributes", timeToAnalyzeAttributes);
        measurements.put("timeToSelectLattices", timeToSelectLattices);
        measurements.put("timeToComputeMMSTs", timeToComputeMMSTs);

        measurements.put("numberOfFacts", numberOfFacts);

        resultsPerCandidateFactSetPerLattice.get(currentCandidateFactSetNumber).put(0, measurements); // 0 means common for all the lattices (lattice numbering starts with 1)
    }

    public static void setLattice(Integer latticeNumber) {
        timeToLoadPreaggregatedMeasuresValues = new Long[1];
        timeToSetUp = new Long[1];
        timeToRetrieveData = new Long[1];
        timeToChunkData = new Long[1];
        timeToPerformReservoirSampling = new Long[1];
        timeToPostprocessSample = new Long[1];
        timeToChunkDataAndSample = new Long[1];
        timeToDumpChunksToDB = new Long[1];
        timeToTranslateData = new Long[1];
        timeToPropagateEarlyStopSample = new Long[1];
        timeToInstantiateMMST = new Long[1];
        totalWallTimeToRunThreadsInParallel = new Long[1];
        timeToRunEarlyStopPruning = new Long[1];
        timeToLoadChunkToRoot = new Long[1];
        timeToUpdateBitmaps = new Long[1];
        timeToEmptyMemoryAllocation = new Long[1];
        timeToComputeAndStoreAggregateResults = new Long[1];
        timeToEvaluateAllChunksInMOLAP = new Long[1];
        totalEvaluationTime = new Long[1];

        latticeDimensions = new String[1];
        latticeMeasures = new String[1];

        numberOfAggregates = new Long[1];
        numberOfStoppedAggregates = new Long[1];

        // setting the default values to 0
        timeToLoadPreaggregatedMeasuresValues[0] = 0L;
        timeToSetUp[0] = 0L;
        timeToRetrieveData[0] = 0L;
        timeToChunkData[0] = 0L;
        timeToPerformReservoirSampling[0] = 0L;
        timeToPostprocessSample[0] = 0L;
        timeToChunkDataAndSample[0] = 0L;
        timeToDumpChunksToDB[0] = 0L;
        timeToTranslateData[0] = 0L;
        timeToPropagateEarlyStopSample[0] = 0L;
        timeToInstantiateMMST[0] = 0L;
        totalWallTimeToRunThreadsInParallel[0] = 0L;
        timeToRunEarlyStopPruning[0] = 0L;
        timeToLoadChunkToRoot[0] = 0L;
        timeToUpdateBitmaps[0] = 0L;
        timeToEmptyMemoryAllocation[0] = 0L;
        timeToComputeAndStoreAggregateResults[0] = 0L;
        timeToEvaluateAllChunksInMOLAP[0] = 0L;
        totalEvaluationTime[0] = 0L;

        latticeDimensions[0] = "";
        latticeMeasures[0] = "";

        numberOfAggregates[0] = 0L;
        numberOfStoppedAggregates[0] = 0L;

        // for this lattice
        // field name -> value
        HashMap<String, Object[]> measurements = new HashMap<>();

        measurements.put("timeToLoadPreaggregatedMeasuresValues", timeToLoadPreaggregatedMeasuresValues);
        measurements.put("timeToSetUp", timeToSetUp);
        measurements.put("timeToRetrieveData", timeToRetrieveData);
        measurements.put("timeToChunkData", timeToChunkData);
        measurements.put("timeToPerformReservoirSampling", timeToPerformReservoirSampling);
        measurements.put("timeToPostprocessSample", timeToPostprocessSample);
        measurements.put("timeToChunkDataAndSample", timeToChunkDataAndSample);
        measurements.put("timeToDumpChunksToDB", timeToDumpChunksToDB);
        measurements.put("timeToTranslateData", timeToTranslateData);
        measurements.put("timeToPropagateEarlyStopSample", timeToPropagateEarlyStopSample);
        measurements.put("timeToInstantiateMMST", timeToInstantiateMMST);
        measurements.put("totalWallTimeToRunThreadsInParallel", totalWallTimeToRunThreadsInParallel);
        measurements.put("timeToRunEarlyStopPruning", timeToRunEarlyStopPruning);
        measurements.put("timeToLoadChunkToRoot", timeToLoadChunkToRoot);
        measurements.put("timeToUpdateBitmaps", timeToUpdateBitmaps);
        measurements.put("timeToEmptyMemoryAllocation", timeToEmptyMemoryAllocation);
        measurements.put("timeToComputeAndStoreAggregateResults", timeToComputeAndStoreAggregateResults);
        measurements.put("timeToEvaluateAllChunksInMOLAP", timeToEvaluateAllChunksInMOLAP);
        measurements.put("totalEvaluationTime", totalEvaluationTime);

        measurements.put("latticeDimensions", latticeDimensions);
        measurements.put("latticeMeasures", latticeMeasures);

        measurements.put("numberOfAggregates", numberOfAggregates);
        measurements.put("numberOfStoppedAggregates", numberOfStoppedAggregates);

        resultsPerCandidateFactSetPerLattice.get(currentCandidateFactSetNumber).put(latticeNumber, measurements);
    }

    public static void outputToJSONFile(String filename) {
        try (FileWriter writer = new FileWriter(filename); BufferedWriter json = new BufferedWriter(writer)) {
            json.write("{ \"timeToSelectCandidateFactSets\":");
            json.write(timeToSelectCandidateFactSets[0].toString());
            json.write(", \"candidateFactSets\":[ ");

            int candidateFactSetNumber;
            HashMap<Integer, HashMap<String, Object[]>> candidateFactSet;
            HashMap<String, Object[]> lattice;
            String prefix = "";
            String prefix2;
            Integer latticeNumber;
            for (Map.Entry<Integer, HashMap<Integer, HashMap<String, Object[]>>> entry: resultsPerCandidateFactSetPerLattice.entrySet()) {
                candidateFactSetNumber = entry.getKey();
                candidateFactSet = entry.getValue();
                json.write(prefix);
                json.write("{ \"candidateFactSetNumber\":");
                json.write(Integer.toString(candidateFactSetNumber));

                lattice = candidateFactSet.get(0);

                json.write(", \"candidateFactSetEncoding\":");
                json.write((String) lattice.get("candidateFactSetEncoding")[0]);
                json.write(", \"candidateFactSetDecoded\":\"");
                json.write((String) lattice.get("candidateFactSetDecoded")[0]);

                json.write("\", \"numberOfFacts\":");
                json.write(((Long) lattice.get("numberOfFacts")[0]).toString());

                json.write(", \"timeToRetrieveFactsAndAttributesOfCandidateFactSet\":");
                json.write(((Long) lattice.get("timeToRetrieveFactsAndAttributesOfCandidateFactSet")[0]).toString());
                json.write(", \"timeToAnalyzeAttributes\":");
                json.write(((Long) lattice.get("timeToAnalyzeAttributes")[0]).toString());
                json.write(", \"timeToSelectLattices\":");
                json.write(((Long) lattice.get("timeToSelectLattices")[0]).toString());
                json.write(", \"timeToComputeMMSTs\":");
                json.write(((Long) lattice.get("timeToComputeMMSTs")[0]).toString());

                json.write(", \"lattices\":[ ");

                prefix2 = "";
                for (Map.Entry<Integer, HashMap<String, Object[]>> entry2: candidateFactSet.entrySet()) {
                    latticeNumber = entry2.getKey();
                    if (latticeNumber == 0) {
                        continue;
                    }

                    lattice = entry2.getValue();
                    json.write(prefix2);
                    json.write("{ \"latticeNumber\":");
                    json.write(latticeNumber.toString());
                    json.write(", \"latticeDimensions\":\"");
                    json.write((String) lattice.get("latticeDimensions")[0]);
                    json.write("\", \"latticeMeasures\":\"");
                    json.write((String) lattice.get("latticeMeasures")[0]);

                    json.write("\", \"timeToLoadPreaggregatedMeasuresValues\":");
                    json.write(((Long) lattice.get("timeToLoadPreaggregatedMeasuresValues")[0]).toString());
                    json.write(", \"timeToTranslateDataBreakdown\":{ ");
                    json.write("\"timeToSetUp\":");
                    json.write(((Long) lattice.get("timeToSetUp")[0]).toString());
                    json.write(", \"timeToChunkDataAndSampleBreakdown\":{ ");
                    json.write("\"timeToRetrieveData\":");
                    json.write(((Long) lattice.get("timeToRetrieveData")[0]).toString());
                    json.write(", \"timeToChunkData\":");
                    json.write(((Long) lattice.get("timeToChunkData")[0]).toString());
                    json.write(", \"timeToPerformReservoirSampling\":");
                    json.write(((Long) lattice.get("timeToPerformReservoirSampling")[0]).toString());
                    json.write(", \"timeToPostprocessSample\":");
                    json.write(((Long) lattice.get("timeToPostprocessSample")[0]).toString());
                    json.write(" }, \"timeToChunkDataAndSample\":");
                    json.write(((Long) lattice.get("timeToChunkDataAndSample")[0]).toString());
                    json.write(", \"timeToDumpChunksToDB\":");
                    json.write(((Long) lattice.get("timeToDumpChunksToDB")[0]).toString());
                    json.write(" }, \"timeToTranslateData\":");
                    json.write(((Long) lattice.get("timeToTranslateData")[0]).toString());
                    json.write(", \"timeToPropagateEarlyStopSample\":");
                    json.write(((Long) lattice.get("timeToPropagateEarlyStopSample")[0]).toString());
                    json.write(", \"timeToInstantiateMMST\":");
                    json.write(((Long) lattice.get("timeToInstantiateMMST")[0]).toString());
                    json.write(", \"totalWallTimeToRunThreadsInParallel\":");
                    json.write(((Long) lattice.get("totalWallTimeToRunThreadsInParallel")[0]).toString());
                    json.write(", \"timeToRunEarlyStopPruning\":");
                    json.write(((Long) lattice.get("timeToRunEarlyStopPruning")[0]).toString());
                    json.write(", \"timeToEvaluateAllChunksInMOLAPBreakdown\":{ ");
                    json.write("\"timeToLoadChunkToRoot\":");
                    json.write(((Long) lattice.get("timeToLoadChunkToRoot")[0]).toString());
                    json.write(", \"timeToUpdateBitmaps\":");
                    json.write(((Long) lattice.get("timeToUpdateBitmaps")[0]).toString());
                    json.write(", \"timeToEmptyMemoryAllocation\":");
                    json.write(((Long) lattice.get("timeToEmptyMemoryAllocation")[0]).toString());
                    json.write(", \"timeToComputeAndStoreAggregateResults\":");
                    json.write(((Long) lattice.get("timeToComputeAndStoreAggregateResults")[0]).toString());
                    json.write(" }, \"timeToEvaluateAllChunksInMOLAP\":");
                    json.write(((Long) lattice.get("timeToEvaluateAllChunksInMOLAP")[0]).toString());
                    json.write(", \"totalEvaluationTime\":");
                    json.write(((Long) lattice.get("totalEvaluationTime")[0]).toString());

                    json.write(", \"numberOfAggregates\":");
                    json.write(((Long) lattice.get("numberOfAggregates")[0]).toString());
                    json.write(", \"numberOfStoppedAggregates\":");
                    json.write(((Long) lattice.get("numberOfStoppedAggregates")[0]).toString());

                    json.write(" }");

                    prefix2 = ", ";
                }

                json.write(" ] }");

                prefix = ", ";
            }

            json.write(" ], \"totalNumberOfAggregates\":");
            json.write((totalNumberOfAggregates[0]).toString());
            json.write(", \"totalNumberOfStoppedAggregates\":");
            json.write((totalNumberOfStoppedAggregates[0]).toString());
            json.write(", \"totalTimeToExportAggregateResultsToCSVFiles\":");
            json.write(totalTimeToExportAggregateResultsToCSVFiles[0].toString());
            json.write(", \"timeToComputeTopK\":");
            json.write(timeToComputeTopK[0].toString());
            json.write(", \"totalExecutionTime\":");
            json.write(totalExecutionTime[0].toString());
            json.write(" }");
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public PerformanceMeasurements() {
    }
}
