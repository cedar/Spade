//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.controller;

import fr.inria.cedar.RDFQuotient.controller.Interface;
import fr.inria.cedar.ontosql.db.PostgresDataSource;
import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.configuration.PerformanceMeasurements;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.CandidateFactSet;
import fr.inria.cedar.spade.datastructures.CandidateFactSetSelectionMethod;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.datastructures.Utils;
import fr.inria.cedar.spade.datastructures.estimation.EarlyStopSample;
import fr.inria.cedar.spade.molapForRDF.*;
import fr.inria.cedar.spade.operations.AggregateDimensionsAndMeasuresSelector;
import fr.inria.cedar.spade.operations.AttributeAnalysis;
import fr.inria.cedar.spade.operations.CandidateFactSetSelector;
import fr.inria.cedar.spade.operations.DirectOutgoingAttributeEnumeration;
import fr.inria.cedar.spade.operations.MultidimensionalAggregateEvaluationWithEarlyStop;
import fr.inria.cedar.spade.operations.OfflinePrecomputation;
import fr.inria.cedar.spade.operations.SummaryBasedCandidateFactSetSelector;
import fr.inria.cedar.spade.operations.TypeBasedCandidateFactSetSelector;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class SystemInterface {
    private static final Logger LOGGER = Logger.getLogger(SystemInterface.class.getName());
    protected static DatabaseHandler handler;

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public SystemInterface() {
    }

    protected static void loadAndSummarize() {
        LOGGER.info("Loading the dataset into the database");
        Interface.load("conf/loading.properties", null, false); // uses conf/loading.properties file for the parameters
        LOGGER.info("The dataset loaded into the database");
        LOGGER.info("Summarizing the dataset");
        Interface.summarize("conf/summarization.properties", null, true); // uses conf/summarization.properties file for the parameters
        LOGGER.info("The summary computed");
    }

    // accesses the dictionary to find and return the encoding of a given value
    protected static String getEncoding(DatabaseHandler databaseHandler,
                                        String value) {
        String encoding;
        String sqlQuery = "SELECT key from dictionary where value = $dagger$" + value + "$dagger$";

        try { // retrieve the result of the query
            encoding = databaseHandler.getSQLSelectSingleResult(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Error in getting the encoding of a value. " + sqlQuery + "\n" + ex);
        }

        return encoding;
    }

    protected static DatabaseHandler setUpConfiguration() {
        try {
            GlobalSettings.setUp(); // read the configuration file (provided by user in the input file)
        }
        catch (IllegalStateException ex) {
            LOGGER.error(ex);
            System.exit(1);
        }

        LOGGER.info("Connecting to the database");
        PostgresDataSource pgds = new PostgresDataSource(GlobalSettings.DATABASE_URL, Integer.valueOf(GlobalSettings.DATABASE_PORT), GlobalSettings.DATABASE_NAME, GlobalSettings.DATABASE_USER, GlobalSettings.DATABASE_PASSWORD);
        DatabaseHandler databaseHandler;
        try {
            Connection conn = pgds.getConnection();
            databaseHandler = new DatabaseHandler(conn);

            if (!GlobalSettings.DATABASE_AUTOCOMMIT) {
                databaseHandler.setAutoCommit(false);
                databaseHandler.setFetchSize(GlobalSettings.DATABASE_FETCHSIZE);
                conn.setHoldability(ResultSet.CLOSE_CURSORS_AT_COMMIT);
            }
            String sqlQuery = "select name from saved_summary_table_names where name like '%_edges' order by name desc";
            GlobalSettings.SUMMARY_TABLE_NAME = databaseHandler.getSQLSelectFirstResult(sqlQuery);
            sqlQuery = "select name from saved_summary_table_names where name like '%_rep' order by name desc";
            GlobalSettings.SUMMARY_REP_TABLE_NAME = databaseHandler.getSQLSelectFirstResult(sqlQuery);

            // find and store the encoding of the type property
            GlobalSettings.TYPE_PROPERTY_ENCODING = Integer.valueOf(getEncoding(databaseHandler, GlobalSettings.TYPE_PROPERTY));
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }
        LOGGER.info("A new database connection established");

        return databaseHandler;
    }

    protected static void findCleanValuesAndUpdateDictionary() {
        LOGGER.info("Adding cleanvalue column to the dictionary");
        String sqlQuery = null;
        try {
            //String sqlQuery = "update dictionary set value = regexp_replace(value, '\"(.*)\"\\^\\^<http://www.w3.org/2001/XMLSchema#.*>', '\\1')";
            sqlQuery = "alter table dictionary add column cleanvalue text;"
                       + "update dictionary set cleanvalue = regexp_replace(value, '\"(.*)\".*', '\\1');"
                       + "create unique index dictionary_i_cleanvalue on dictionary using btree(md5(cleanvalue), key) tablespace pg_default";
            handler.executeUpdate(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Error in adding cleanvalue column to the dictionary; sqlQuery: " + sqlQuery + "\n" + ex);
        }
        LOGGER.info("The dictionary updated");
    }

    protected static void deriveNewAttributesAndPreaggregateAll() {
        OfflinePrecomputation offlinePrecomputation = new OfflinePrecomputation(handler);

        LOGGER.info("Analyzing original attributes and deriving new ones (if derivations are enabled and original properties permit)");
        int numberOfNewAttributes = offlinePrecomputation.deriveNewAttributesFromInputGraph();
        LOGGER.info("All the attributes (including " + numberOfNewAttributes + " new derived attributes) analyzed");

        LOGGER.info("Pre-aggregating all the attributes");
        offlinePrecomputation.preaggregateAttributes(offlinePrecomputation.getAllAttributes());
        LOGGER.info("All the attributes pre-aggregated");

        LOGGER.info("Running ANALYZE over the whole database");
        offlinePrecomputation.runAnalyze();
        LOGGER.info("ANALYZE finished");
    }

    protected static void dropSubjectIdColumn(String tableName) {
        StringBuilder sqlQuery = new StringBuilder("alter table ");
        sqlQuery.append(tableName)
                .append(" drop column if exists subject_id");
        try {
            handler.executeUpdate(sqlQuery.toString());
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }
    }

    protected static void dropTable(String tableName) {
        StringBuilder sqlQuery = new StringBuilder("drop table if exists ");
        sqlQuery.append(tableName);
        try {
            handler.executeUpdate(sqlQuery.toString());
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }
    }

    protected static void dropAttributeProfilesAndCFSsTables() {
        dropTable("attributeprofiles");

        CandidateFactSetSelector candidateFactSetSelector = new TypeBasedCandidateFactSetSelector(handler);
        for (String candidateFactSetEncoding: candidateFactSetSelector.getAllCandidateFactSets()) {
            dropSubjectIdColumn("t_" + candidateFactSetEncoding);
        }

        LinkedList<String> candidateFactSetTables = new LinkedList<>();
        String sqlQuery = "select distinct value from dictionary where value like 'generated_cfs_%'";
        try (ResultSet rs = handler.getResultSet(sqlQuery)) {
            while (rs.next()) {
                candidateFactSetTables.add(rs.getString(1));
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }

        for (String candidateFactSetTable: candidateFactSetTables) {
            dropTable(candidateFactSetTable);
        }

        sqlQuery = "delete from dictionary where value like 'generated_cfs_%'";
        try {
            handler.executeUpdate(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("SQL query: " + sqlQuery + "; " + ex);
        }
    }

    // removes all tables representing derived attributes from the database
    protected static void cleanDatabaseFromDerivedAttributes() {
        ArrayList<String> derived;
        String sqlQuery;

        // the name of each derived attribute begins with DERIVED_ATTRIBUTE_PREFIX
        sqlQuery = "select key from dictionary where value like '" + GlobalSettings.DERIVED_ATTRIBUTE_PREFIX + "%'";
        try {
            derived = handler.getResultSelectFromKeyColumn(sqlQuery);

            // drop all tables that represent derived attributes
            for (String d: derived) {
                dropTable("t_" + d);
            }

            // delete all entries that represent derived attributes in attributeprofiles table
            for (String d: derived) {
                sqlQuery = "delete from attributeprofiles where attributeencoding=" + d + ";";
                handler.executeUpdate(sqlQuery);
            }
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Error in getting the keys of derived properties. " + sqlQuery + "\n" + ex);
        }

        // delete all references to derived attributes from the dictionary
        sqlQuery = "delete from dictionary where value like '" + GlobalSettings.DERIVED_ATTRIBUTE_PREFIX + "%'";
        try {
            handler.executeUpdate(sqlQuery);
        }
        catch (SQLException ex) {
            throw new IllegalStateException("Error in deleting the references to derived properties from the dictionary. " + sqlQuery + "\n" + ex);
        }

        // TODO: delete from allattributes table
    }

    protected static InterestingnessFunction getInterestingnessFunction() {
        InterestingnessFunction interestingnessFunction;
        String interestingnessFunctionString = GlobalSettings.INTERESTINGNESS_FUNCTION;
        switch (interestingnessFunctionString) {
            case "variance_feature_scaling":
                interestingnessFunction = InterestingnessFunction.VAR_FEATURE_SCALING;
                break;
            case "variance_sum":
                interestingnessFunction = InterestingnessFunction.VAR_SUM;
                break;
            case "variance_mean":
                interestingnessFunction = InterestingnessFunction.VAR_MEAN;
                break;
            case "skewness":
                interestingnessFunction = InterestingnessFunction.SKEWNESS;
                break;
            case "kurtosis":
                interestingnessFunction = InterestingnessFunction.KURTOSIS;
                break;
            default:
                throw new IllegalStateException("Interestingness function " + interestingnessFunctionString + " not supported");
        }

        return interestingnessFunction;
    }

    protected static CandidateFactSetSelector getCandidateFactSetSelector() {
        CandidateFactSetSelectionMethod candidateFactSetSelectionMethod = GlobalSettings.CANDIDATE_FACT_SET_SELECTION;
        CandidateFactSetSelector candidateFactSetSelector;
        switch (candidateFactSetSelectionMethod) {
            case TYPED_BASED_CANDIDATE_FACT_SET_SELECTION:
                candidateFactSetSelector = new TypeBasedCandidateFactSetSelector(handler);
                break;
            case SUMMARY_BASED_CANDIDATE_FACT_SET_SELECTION:
                candidateFactSetSelector = new SummaryBasedCandidateFactSetSelector(handler);
                break;
            case ATTRIBUTE_BASED_CANDIDATE_FACT_SET_SELECTION:
            case SAMPLE_BASED_CANDIDATE_FACT_SET_SELECTION:
                // TODO: add attribute and sample based CFS selection
                throw new UnsupportedOperationException("Not implemented yet");
            default:
                throw new IllegalStateException("No value corresponding to " + candidateFactSetSelectionMethod);
        }

        return candidateFactSetSelector;
    }

    protected static CandidateFactSet getFactsFromCandidateFactSet(
            CandidateFactSetSelector candidateFactSetSelector,
            String candidateFactSetEncoding) {
        CandidateFactSetSelectionMethod candidateFactSetSelectionMethod = GlobalSettings.CANDIDATE_FACT_SET_SELECTION;
        switch (candidateFactSetSelectionMethod) {
            case TYPED_BASED_CANDIDATE_FACT_SET_SELECTION:
                LOGGER.info("Considering type " + candidateFactSetEncoding + " / " + handler.getValue(candidateFactSetEncoding));
                break;
            case SUMMARY_BASED_CANDIDATE_FACT_SET_SELECTION:
                LOGGER.info("Considering summary node " + candidateFactSetEncoding + " / " + handler.getValue(candidateFactSetEncoding));
                break;
            case ATTRIBUTE_BASED_CANDIDATE_FACT_SET_SELECTION:
            case SAMPLE_BASED_CANDIDATE_FACT_SET_SELECTION:
                // TODO: add attribute and sample based CFS selection
                throw new UnsupportedOperationException("Not implemented yet");
            default:
                throw new IllegalStateException("No value corresponding to " + candidateFactSetSelectionMethod);
        }

        CandidateFactSet candidateFactSet = candidateFactSetSelector.getFactsFromCandidateFactSet(candidateFactSetEncoding);
        PerformanceMeasurements.numberOfFacts[0] = (long) candidateFactSet.getSize();
        LOGGER.info("\tfound " + PerformanceMeasurements.numberOfFacts[0] + " facts");

        return candidateFactSet;
    }

    protected static boolean containsMultiValuedAttributes(
            ArrayList<AnalyzedAttribute> dimensions) {
        for (AnalyzedAttribute dimension: dimensions) {
            if (!dimension.isAtMostOne()) {
                return true;
            }
        }
        return false;
    }

    protected static LinkedList<MinimumMemorySpanningTree> computeMMSTsAndOptimizeThemGlobally(
            HashMap<Integer, ArrayList<AnalyzedAttribute>> setsOfDimensions,
            HashMap<Integer, ArrayList<AnalyzedAttribute>> setsOfMeasures,
            HashMap<AnalyzedAttribute, Integer> numberOfDistinctValuesPerAttribute,
            HashMap<AnalyzedAttribute, Integer> chunkSizePerAttribute,
            HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsPerAttribute,
            String candidateFactSetDecoded) {
        LinkedList<MinimumMemorySpanningTree> MMSTs = new LinkedList<>();
        MMSTsOptimizer intraMMSTOptimizer = null;
        if (GlobalSettings.OPTIMIZE_MMSTS == true) {
            intraMMSTOptimizer = new MMSTsOptimizer();
        }
        ArrayList<AnalyzedAttribute> dimensions;
        ArrayList<AnalyzedAttribute> measures;
        StringBuilder latticeDescription = new StringBuilder();
        MinimumMemorySpanningTree MMST;
        AggregateInLattice root;
        int latticeNumber = 1;
        for (Map.Entry<Integer, ArrayList<AnalyzedAttribute>> entry: setsOfDimensions.entrySet()) {
            dimensions = entry.getValue();
            measures = setsOfMeasures.get(entry.getKey());
            latticeDescription.setLength(0);
            latticeDescription.append("Lattice ").append(latticeNumber).append(": ");
            for (AnalyzedAttribute aa: dimensions) {
                latticeDescription.append(aa.getEncoding()).append(", ");
            }
            latticeDescription.append(measures.size()).append(" measures");

            if (containsMultiValuedAttributes(dimensions)) {
                latticeDescription.append(" --> CONTAINS multivalued attributes");
            }
            else {
                latticeDescription.append(" --> DOES NOT CONTAIN multivalued attributes");
            }
            LOGGER.info(latticeDescription.toString());
            MMST = new MinimumMemorySpanningTree(handler, dimensions, numberOfDistinctValuesPerAttribute, chunkSizePerAttribute, measures, aggregationFunctionsPerAttribute, candidateFactSetDecoded);
            root = MMST.computeMMST();
            MMSTs.add(MMST);

            latticeNumber++;

            if (GlobalSettings.OPTIMIZE_MMSTS) {
                intraMMSTOptimizer.addMMST(root);
            }
        }
        if (GlobalSettings.OPTIMIZE_MMSTS) {
            intraMMSTOptimizer.computeGlobalSolution();
        }

        return MMSTs;
    }

    protected static String listOfAttributeEncodings(
            ArrayList<AnalyzedAttribute> attributes) {
        StringBuilder attributeEncodings = new StringBuilder();
        attributeEncodings.append("(");
        String prefix = "";
        for (AnalyzedAttribute attribute: attributes) {
            attributeEncodings.append(prefix).append(attribute.getEncoding());
            prefix = ", ";
        }
        attributeEncodings.append(")");

        return attributeEncodings.toString();
    }

    protected static int numberOfAggregates(
            ArrayList<AnalyzedAttribute> dimensions,
            ArrayList<AnalyzedAttribute> measures,
            HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsPerAttribute) {
        int numberOfNodesInLattice = (int) (Math.pow(2, dimensions.size()) - 1); // -1 for ALL/bottom/no-dimensions node
        int numberOfMeasureAggregationFunctions = 0;
        for (AnalyzedAttribute measure: measures) {
            if (GlobalSettings.ONLY_AVERAGE_MODE) {
                if (aggregationFunctionsPerAttribute.get(measure).contains(AggregationFunction.AVERAGE)) {
                    numberOfMeasureAggregationFunctions += 1;
                }
            }
            else {
                numberOfMeasureAggregationFunctions += aggregationFunctionsPerAttribute.get(measure).size();
            }
        }

        // each node in the lattice is assigned the same set of measure-aggregation function pairs
        // we add +1 to account for count(*) assigned to each node regardless of the measures
        return numberOfNodesInLattice * (numberOfMeasureAggregationFunctions + 1);
    }

    public static void main(String[] args) {
        if (args.length > 0) {
            String argument = args[0];

            switch (argument) {
                case "offline":
                    loadAndSummarize();
                    handler = setUpConfiguration();
                    findCleanValuesAndUpdateDictionary();
                    deriveNewAttributesAndPreaggregateAll();
                    return;
                case "drop-online-tables":
                    handler = setUpConfiguration();
                    dropAttributeProfilesAndCFSsTables();
                    return;
                default:
                    LOGGER.error("Unsupported argument " + argument);
                    return;
            }
        }

        long startTime = System.currentTimeMillis();

        // DATABASE CONFIGURATION SETUP
        handler = setUpConfiguration();
        DatabaseHandler additionalHandler = null;
        if (GlobalSettings.AGGREGATE_EVALUATION_MODULE.equals("molap")) {
            additionalHandler = setUpConfiguration();
        }

        final InterestingnessFunction interestingnessFunction = getInterestingnessFunction();
        final boolean materializeTableWithCollectedData = false;
        final boolean useEarlyStop = GlobalSettings.NUMBER_OF_TUPLES_TO_SAMPLE_FROM_EACH_GROUP > 0;
        final boolean inMemory = GlobalSettings.DIMENSION_OF_CHUNK == 0;

        AggregateResultStorage aggregateResultStorage;
        switch (GlobalSettings.KEY_VALUE_STORE) {
            case "redis":
                aggregateResultStorage = new RedisStorage(interestingnessFunction);
                break;
            case "berkeleydb":
                aggregateResultStorage = new BerkeleyDBStorage(interestingnessFunction);
                break;
            case "postgres":
                aggregateResultStorage = new PostgresStorage(handler, interestingnessFunction);
                break;
            case "csv":
                aggregateResultStorage = new CSVStorage();
                break;
            default:
                throw new IllegalStateException("Unsupported store: " + GlobalSettings.KEY_VALUE_STORE);
        }

        AggregateResultManager aggregateResultManager = new AggregateResultManager(aggregateResultStorage, interestingnessFunction);

        String directoryName = GlobalSettings.FLASK_FOLDER_PATH;
        File directory = new File(directoryName);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        // opening and closing the file to empty it.
        if (GlobalSettings.PROFILE_BITMAP_SIZE) {
            try {
                BufferedWriter bwBitmapSize = new BufferedWriter(new FileWriter(GlobalSettings.PROFILE_BITMAP_SIZE_FILE_NAME));
                bwBitmapSize.close();
            }
            catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        // CANDIDATE FACT SET SELECTION
        LOGGER.info("Selecting candidate fact sets");
        long time = System.currentTimeMillis();
        CandidateFactSetSelector candidateFactSetSelector = getCandidateFactSetSelector();
        ArrayList<String> allCandidateFactSets = candidateFactSetSelector.getAllCandidateFactSets();
        PerformanceMeasurements.timeToSelectCandidateFactSets[0] = Utils.calculateRunTimeInMilliseconds(time);
        LOGGER.info("The candidate fact sets selected");
        LOGGER.info("Time to select the candidate fact sets: " + PerformanceMeasurements.timeToSelectCandidateFactSets[0] + " ms");

        int candidateFactSetNumber = 1;
        String candidateFactSetDecoded;
        CandidateFactSet candidateFactSet;

        // TODO: add a switch for different methods of attribute enumeration
        DirectOutgoingAttributeEnumeration attributeEnumerator = new DirectOutgoingAttributeEnumeration(handler);
        LinkedList<Long> candidateFactSetAttributes;
        AttributeAnalysis attributeAnalyzer = new AttributeAnalysis(handler);
        LinkedList<AnalyzedAttribute> allAnalyzedAttributes;

        AggregateDimensionsAndMeasuresSelector dimensionsAndMeasuresSelector;
        HashMap<Integer, ArrayList<AnalyzedAttribute>> setsOfDimensions;
        HashMap<Integer, ArrayList<AnalyzedAttribute>> setsOfMeasures;
        HashMap<AnalyzedAttribute, Integer> numberOfDistinctValuesPerAttribute;
        HashMap<AnalyzedAttribute, Integer> chunkSizePerAttribute;
        HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsPerAttribute;
        LinkedList<MinimumMemorySpanningTree> MMSTs;

        long evaluationTime;
        int latticeNumber;
        ArrayList<AnalyzedAttribute> dimensions;
        ArrayList<AnalyzedAttribute> measures;

        MOLAPMeasures preaggregatedMeasures;
        String tableName;
        String directoryWithData;

        double globalKthBestScore = 0; // initially the lowest possible score

        // dictionaries to keep track of the aggregates computed so far
        HashMap<String, String> dictionaryFromFilenameToSHA1;
        //HashMap<String, String> dictionaryFromSHA1ToFilename;

        int globalMMSTNumber = 1;

        // LOOP OVER CANDIDATE FACT SETS
        for (String candidateFactSetEncoding: allCandidateFactSets) {
            PerformanceMeasurements.setCandidateFactSet(candidateFactSetNumber);
            candidateFactSetDecoded = Utils.getShorterAttributeName(handler.getValue(candidateFactSetEncoding));
            PerformanceMeasurements.candidateFactSetEncoding[0] = candidateFactSetEncoding;
            PerformanceMeasurements.candidateFactSetDecoded[0] = candidateFactSetDecoded;

            // RETRIEVING FACTS AND ATTRIBUTES OF THE CANDIDATE FACT SET
            LOGGER.info("Retrieving facts and enumerating attributes of Candidate Fact Set " + candidateFactSetNumber);
            time = System.currentTimeMillis();
            candidateFactSet = getFactsFromCandidateFactSet(candidateFactSetSelector, candidateFactSetEncoding);
            candidateFactSetAttributes = attributeEnumerator.getAllAttributesUsingSummary(candidateFactSet);
            LOGGER.info("\tfound " + candidateFactSetAttributes.size() + " properties");
            PerformanceMeasurements.timeToRetrieveFactsAndAttributesOfCandidateFactSet[0] = Utils.calculateRunTimeInMilliseconds(time);
            LOGGER.info("The facts and attributes retrieved");
            LOGGER.info("Time to retrieve the facts and attributes of the candidate fact set: " + PerformanceMeasurements.timeToRetrieveFactsAndAttributesOfCandidateFactSet[0] + " ms");

            // ATTRIBUTE ANALYSIS
            LOGGER.info("Analyzing frequent attributes (support >= " + GlobalSettings.SUPPORT_THRESHOLD + ")");
            time = System.currentTimeMillis();
            allAnalyzedAttributes = attributeAnalyzer.getAnalysisResult(candidateFactSet, candidateFactSetAttributes);
            LOGGER.info("The total of " + allAnalyzedAttributes.size() + " frequent attributes analyzed");
            for (AnalyzedAttribute analyzedAttribute: allAnalyzedAttributes) {
                LOGGER.info(analyzedAttribute.toString());
            }
            PerformanceMeasurements.timeToAnalyzeAttributes[0] = Utils.calculateRunTimeInMilliseconds(time);
            LOGGER.info("Time to analyze the attributes: " + PerformanceMeasurements.timeToAnalyzeAttributes[0] + " ms");

            preaggregatedMeasures = new MOLAPMeasures(additionalHandler, candidateFactSet, useEarlyStop);

            // LATTICE SELECTION
            LOGGER.info("Selecting lattices");
            time = System.currentTimeMillis();
            dimensionsAndMeasuresSelector = new AggregateDimensionsAndMeasuresSelector(candidateFactSet, allAnalyzedAttributes, handler);
            dimensionsAndMeasuresSelector.findFrequentSetsOfDimensions(GlobalSettings.NUMBER_OF_TUPLES_TO_SAMPLE_FOR_FREQUENT_ITEMSET_MINING);
            setsOfDimensions = dimensionsAndMeasuresSelector.getSetsOfDimensions();
            setsOfMeasures = dimensionsAndMeasuresSelector.getSetsOfMeasures();
            numberOfDistinctValuesPerAttribute = dimensionsAndMeasuresSelector.getNumberOfDistinctValuesPerAttribute();
            chunkSizePerAttribute = dimensionsAndMeasuresSelector.getSuggestedChunkSizePerAttribute();
            aggregationFunctionsPerAttribute = dimensionsAndMeasuresSelector.getSuggestedAggFunctionsPerAttribute();
            PerformanceMeasurements.timeToSelectLattices[0] = Utils.calculateRunTimeInMilliseconds(time);
            LOGGER.info("The lattices selected");
            LOGGER.info("Time to select the lattices: " + PerformanceMeasurements.timeToSelectLattices[0] + " ms");

            dictionaryFromFilenameToSHA1 = new HashMap<>();
            //dictionaryFromSHA1ToFilename = new HashMap<>();

            // MMSTs COMPUTATION
            LOGGER.info("Computing MMSTs");
            time = System.currentTimeMillis();
            MMSTs = computeMMSTsAndOptimizeThemGlobally(setsOfDimensions, setsOfMeasures, numberOfDistinctValuesPerAttribute, chunkSizePerAttribute, aggregationFunctionsPerAttribute, candidateFactSetDecoded);
            PerformanceMeasurements.timeToComputeMMSTs[0] = Utils.calculateRunTimeInMilliseconds(time);
            LOGGER.info("The MMSTs computed");
            LOGGER.info("Time to compute the MMSTs: " + PerformanceMeasurements.timeToComputeMMSTs[0] + " ms");

            latticeNumber = 1;
            // LOOP OVER LATTICES WITHIN A CANDIDATE FACT SET
            for (MinimumMemorySpanningTree currentMMST: MMSTs) { // for each pair of dimensions and measures that correspond to one lattice
                PerformanceMeasurements.setLattice(latticeNumber);
                LOGGER.info("Processing Lattice " + latticeNumber);
                PerformanceMeasurements.numberOfAggregates[0] = 0L;

                dimensions = currentMMST.getDimensions();
                measures = currentMMST.getMeasures();
                PerformanceMeasurements.latticeDimensions[0] = listOfAttributeEncodings(dimensions);
                PerformanceMeasurements.latticeMeasures[0] = listOfAttributeEncodings(measures);
                LOGGER.info("Lattice dimensions: " + PerformanceMeasurements.latticeDimensions[0]);
                LOGGER.info("Lattice measures: " + PerformanceMeasurements.latticeMeasures[0]);

                switch (GlobalSettings.AGGREGATE_EVALUATION_MODULE) {
                    case "cube": // EVALUATION USING POSTGRES CUBE
                        LOGGER.info("Evaluating aggregates using Postgres CUBE");
                        evaluationTime = System.currentTimeMillis();
                        if (GlobalSettings.STORE_ALL_AGGREGATES_AMONG_THOSE_COMPUTED_MORE_THAN_ONCE == false) {
                            globalMMSTNumber = 0;
                        }
                        PostgresCubeEvaluator evaluator = new PostgresCubeEvaluator(globalMMSTNumber, candidateFactSet, candidateFactSetDecoded, dimensions, measures, aggregationFunctionsPerAttribute, handler, aggregateResultManager);
                        evaluator.evaluate(GlobalSettings.ALLOW_USE_OF_PREAGGREGATED_MEASURES_TABLES);
                        LOGGER.info("The aggregates evaluated");

                        PerformanceMeasurements.totalEvaluationTime[0] = Utils.calculateRunTimeInMilliseconds(evaluationTime);
                        LOGGER.info("Total evaluation time: " + PerformanceMeasurements.totalEvaluationTime[0] + " ms");
                        globalMMSTNumber++;
                        break;
                    case "molap": // EVALUATION USING MVDCube AND EARLY-STOP (if enabled)
                        LOGGER.info("Evaluating aggregates using MVDCube");
                        evaluationTime = System.currentTimeMillis();

                        final ArrayList<AnalyzedAttribute> measuresFinal = measures;
                        final HashMap<AnalyzedAttribute, LinkedList<AggregationFunction>> aggregationFunctionsPerAttributeFinal = aggregationFunctionsPerAttribute;
                        final MOLAPMeasures preaggregatedMeasuresFinal = preaggregatedMeasures;

                        final RDFToMOLAPTranslator translator = new RDFToMOLAPTranslator(dimensions, measures, candidateFactSet, handler, chunkSizePerAttribute);
                        final AggregateInLattice root = currentMMST.getRoot();

                        final HashMap<String, String> dictionaryFromFilenameToSHA1Final = dictionaryFromFilenameToSHA1;
                        //final HashMap<String, String> dictionaryFromSHA1ToFilenameFinal = dictionaryFromSHA1ToFilename;
                        final AggregateResultManager aggregateResultManagerFinal = aggregateResultManager;

                        final HashMap<AnalyzedAttribute, Integer> chunkSizePerAttributeFinal = chunkSizePerAttribute;

                        LOGGER.info("Running two threads in parallel");
                        time = System.currentTimeMillis();

                        CompletableFuture<Void> thread1 = CompletableFuture.runAsync(() -> {
                            // PRE-AGGREGATED MEASURE VALUES LOADING (only the measures that hasn't been loaded so far for this candidate fact set)
                            LOGGER.info("Thread 1: Loading necessary pre-aggregated measures values from Postgres to in-memory arrays");
                            long localTime = System.currentTimeMillis();
                            preaggregatedMeasuresFinal.updateMeasures(measuresFinal, aggregationFunctionsPerAttributeFinal);
                            PerformanceMeasurements.timeToLoadPreaggregatedMeasuresValues[0] = Utils.calculateRunTimeInMilliseconds(localTime);
                            LOGGER.info("Thread 1: The pre-aggregated measures values loaded to in-memory arrays");
                            LOGGER.info("Thread 1: Time to load pre-aggregated measures values: " + PerformanceMeasurements.timeToLoadPreaggregatedMeasuresValues[0] + " ms");
                        });

                        CompletableFuture<Void> thread2 = CompletableFuture.runAsync(() -> {
                            // MOLAP DATA TRANSLATION AND EARLY-STOP SAMPLING (if enabled)
                            if (useEarlyStop) {
                                LOGGER.info("Thread 2: Translating relational data to MOLAP sparse array format and early-stop sampling");
                            }
                            else {
                                LOGGER.info("Thread 2: Translating relational data to MOLAP sparse array format");
                            }
                            long localTime = System.currentTimeMillis();
                            translator.translate(materializeTableWithCollectedData, GlobalSettings.NUMBER_OF_TUPLES_TO_SAMPLE_FROM_EACH_GROUP, inMemory, root);
                            PerformanceMeasurements.timeToTranslateData[0] = Utils.calculateRunTimeInMilliseconds(localTime);
                            LOGGER.info("Thread 2: The data translated");
                            LOGGER.info("Thread 2: Time to translate the data: " + PerformanceMeasurements.timeToTranslateData[0] + " ms");

                            if (GlobalSettings.ONLY_AVERAGE_MODE) {
                                currentMMST.removeNonAverageAggregationFunctions();
                            }

                            if (useEarlyStop) {
                                // EARLY-STOP SAMPLE PROPAGATION
                                LOGGER.info("Thread 2: Propagating the early-stop sample");
                                localTime = System.currentTimeMillis();
                                translator.setEarlyStopEvaluator(
                                        new MultidimensionalAggregateEvaluationWithEarlyStop(
                                                handler,
                                                EarlyStopSample.collectAggregatesWithSamples(translator.getSample(), chunkSizePerAttributeFinal),
                                                chunkSizePerAttributeFinal,
                                                translator.getDistinctValuesOfDimensions(),
                                                interestingnessFunction,
                                                dictionaryFromFilenameToSHA1Final,
                                                aggregateResultManagerFinal
                                        )
                                );
                                PerformanceMeasurements.timeToPropagateEarlyStopSample[0] = Utils.calculateRunTimeInMilliseconds(localTime);
                                LOGGER.info("Thread 2: The early-stop sample propagated");
                                LOGGER.info("Thread 2: Time to propagate the early-stop sample: " + PerformanceMeasurements.timeToPropagateEarlyStopSample[0] + " ms");
                            }

                            // INSTANTIATING THE MMST
                            LOGGER.info("Thread 2: Instantiating the MMST");
                            localTime = System.currentTimeMillis();
                            MinimumMemorySpanningTree.setFieldsInAggregates(root, handler, translator.getDistinctValuesOfDimensions(), translator.getNumberOfGroupsPerDimension(), dictionaryFromFilenameToSHA1Final, /*dictionaryFromSHA1ToFilenameFinal, */aggregateResultManagerFinal);
                            currentMMST.instantiateMMST();
                            PerformanceMeasurements.timeToInstantiateMMST[0] = Utils.calculateRunTimeInMilliseconds(localTime);
                            LOGGER.info("Thread 2: The MMST instantiated");
                            LOGGER.info("Thread 2: Time to instantiate the MMST: " + PerformanceMeasurements.timeToInstantiateMMST[0] + " ms");
                        });

                        CompletableFuture<Void> future = CompletableFuture.allOf(thread1, thread2);
                        try {
                            future.get(); // wait for both threads to complete
                        }
                        catch (InterruptedException | ExecutionException ex) {
                            throw new IllegalStateException(ex);
                        }

                        PerformanceMeasurements.totalWallTimeToRunThreadsInParallel[0] = Utils.calculateRunTimeInMilliseconds(time);
                        LOGGER.info("Total wall time to run the two threads in parallel: " + PerformanceMeasurements.totalWallTimeToRunThreadsInParallel[0] + " ms");

                        if (useEarlyStop) {
                            // EARLY-STOPPING
                            LOGGER.info("Aggregate pruning with early-stop");
                            time = System.currentTimeMillis();
                            globalKthBestScore = translator.getEarlyStopEvaluator().runEarlyStop(preaggregatedMeasures, globalKthBestScore);
                            //LOGGER.info("Global kth best score: " + globalKthBestScore);
                            PerformanceMeasurements.timeToRunEarlyStopPruning[0] = Utils.calculateRunTimeInMilliseconds(time);
                            LOGGER.info("Early-stop terminated");
                            LOGGER.info("Time to run the early-stop pruning: " + PerformanceMeasurements.timeToRunEarlyStopPruning[0] + " ms");
                        }

                        // AGGREGATE EVALUATION
                        LOGGER.info("Computing aggregate results using MVDCube");
                        time = System.currentTimeMillis();
                        tableName = translator.getTableName();
                        currentMMST.evaluate(translator.getNumberOfChunks(), tableName, preaggregatedMeasures, inMemory);
                        PerformanceMeasurements.timeToEvaluateAllChunksInMOLAP[0] = Utils.calculateRunTimeInMilliseconds(time);
                        LOGGER.info("The aggregate results computed");
                        LOGGER.info("Time to compute the aggregate results: " + PerformanceMeasurements.timeToEvaluateAllChunksInMOLAP[0] + " ms");

                        PerformanceMeasurements.totalEvaluationTime[0] = Utils.calculateRunTimeInMilliseconds(evaluationTime);
                        LOGGER.info("Total evaluation time: " + PerformanceMeasurements.totalEvaluationTime[0] + " ms");

                        if (!inMemory) {
                            // DROP MOLAP TABLES
                            directoryWithData = translator.getDirectoryWithData();
                            currentMMST.clean(tableName, directoryWithData);
                        }
                        break;
                    default:
                        LOGGER.error("Aggregate module " + GlobalSettings.AGGREGATE_EVALUATION_MODULE + " not supported");
                        return;
                }

                PerformanceMeasurements.numberOfAggregates[0] += (long) numberOfAggregates(dimensions, measures, aggregationFunctionsPerAttribute);
                PerformanceMeasurements.totalNumberOfAggregates[0] += PerformanceMeasurements.numberOfAggregates[0];

                latticeNumber++;
            }
            candidateFactSetNumber++;
        }

        LOGGER.info("Total time to export " + GlobalSettings.KEY_VALUE_STORE + " data to CSV files: " + PerformanceMeasurements.totalTimeToExportAggregateResultsToCSVFiles[0] + " ms");

        // COMPUTING THE TOP-K MOST INTERESTING AGGREGATES ACCORDING TO THE INTERESTINGNESS FUNCTION
        LOGGER.info("Computing the top-k most interesting aggregates");
        time = System.currentTimeMillis();
        aggregateResultManager.computeAndPrintRankedAggregatesAndOutputTopK(interestingnessFunction);
        LOGGER.info("The top-k results computed");

        PerformanceMeasurements.timeToComputeTopK[0] = Utils.calculateRunTimeInMilliseconds(time);
        LOGGER.info("Time to compute the top-k results: " + PerformanceMeasurements.timeToComputeTopK[0] + " ms");

        PerformanceMeasurements.totalExecutionTime[0] = Utils.calculateRunTimeInMilliseconds(startTime);

        aggregateResultManager.removeData();
        aggregateResultManager.closeConnection();

        //cleanDatabaseFromDerivedAttributes();
        try {
            handler.closeConnection();
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }

        PerformanceMeasurements.outputToJSONFile(GlobalSettings.PERFORMANCE_MEASUREMENTS_FILE_NAME);

        LOGGER.info("Total execution time: " + PerformanceMeasurements.totalExecutionTime[0] + " ms");
    }
}
