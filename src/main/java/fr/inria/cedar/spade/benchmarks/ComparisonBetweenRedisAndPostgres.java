//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.benchmarks;

import fr.inria.cedar.ontosql.db.PostgresDataSource;
import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.Utils;
import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.KeyValue;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.jdbc.PgConnection;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanParams;
import static redis.clients.jedis.ScanParams.SCAN_POINTER_START;
import redis.clients.jedis.ScanResult;


public class ComparisonBetweenRedisAndPostgres {
    private static final org.apache.log4j.Logger LOGGER = Logger.getLogger(ComparisonBetweenRedisAndPostgres.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    PostgresDataSource pgds;
    Connection connectionToPosgres;
    private DatabaseHandler handler;

    private RedisClient redisClient;
    private StatefulRedisConnection<String, String> connection;
    private RedisCommands<String, String> syncCommands;
    Jedis jedis;

    private static int totalNrOfTuples = 2000000;
    private static boolean postgres = true;
    private static boolean redis = true;
    private static String datasetFilename = "/Users/mirjanamazuran/Desktop/test.csv";
    private static boolean useJedis = true;


    public static void main(String[] args){
        ComparisonBetweenRedisAndPostgres controller = new ComparisonBetweenRedisAndPostgres();

        createDataset();

        if(postgres) {
            long timeP = System.currentTimeMillis();
            controller.postgresTests();
            LOGGER.info("Total time for Postgres: " + Utils.calculateRunningTime(timeP) + " sec");
        }
        if(redis) {
            long timeR = System.currentTimeMillis();
            controller.redisTests();
            LOGGER.info("Total time for Redis: " + Utils.calculateRunningTime(timeR) + " sec");
        }

    }

    private static void createDataset(){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(datasetFilename));

            for(int tupleID = 1; tupleID <= totalNrOfTuples; tupleID++){
                bw.write(String.valueOf(tupleID));
                bw.write(",");
                bw.write(String.valueOf(Math.random()));
                bw.write("\n");
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    * ######## POSTGRESQL
    * */
    private void postgresTests(){

        try {
            long time;

            time = System.currentTimeMillis();
            establishPostgresConnection();
            LOGGER.info("\t time for establishPostgresConnection(): " + Utils.calculateRunningTime(time) + " sec");

            time = System.currentTimeMillis();
            createPostgresTable();
            LOGGER.info("\t time for createPostgresTable(): " + Utils.calculateRunningTime(time) + " sec");

            time = System.currentTimeMillis();
            insertIntoPostgresTableWithCopy();
            LOGGER.info("\t time for insertIntoPostgresTableWithCopy(): " + Utils.calculateRunningTime(time) + " sec");

            time = System.currentTimeMillis();
            retrieveAllTuplesFromPostgresTable();
            LOGGER.info("\t time for retrieveAllTuplesFromPostgresTable(): " + Utils.calculateRunningTime(time) + " sec");

            time = System.currentTimeMillis();
            dropPostgresTable();
            LOGGER.info("\t time for dropPostgresTable(): " + Utils.calculateRunningTime(time) + " sec");

            handler.commit(); // does it store on disk without the commit? there is no data without it
            handler.closeConnection();
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }

    }

    private void establishPostgresConnection(){
        pgds = new PostgresDataSource("localhost", 5432, "redisvspostgres", "postgres", "p4ssP0stgr3s");
        try {
            connectionToPosgres = pgds.getConnection();
            handler = new DatabaseHandler(connectionToPosgres);
            handler.setAutoCommit(false);
            handler.setFetchSize(GlobalSettings.DATABASE_FETCHSIZE);
            connectionToPosgres.setHoldability(ResultSet.CLOSE_CURSORS_AT_COMMIT);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void createPostgresTable() throws SQLException {
        String sqlQuery = "create table test (id integer not null, a1 double precision, primary key(id))";
        handler.executeUpdate(sqlQuery);
    }

    private void insertIntoPostgresInBatch() throws SQLException {
        StringBuilder sqlQuery = new StringBuilder("insert into test values ");
        String prefix = " ";
        for(int tupleID = 1; tupleID <= totalNrOfTuples; tupleID++){
            sqlQuery.append(prefix).append("(").append(tupleID).append(",'").append(Math.random()).append("')");
            prefix = ", ";
        }
        handler.executeUpdate(sqlQuery.toString());
    }

    private void insertIntoPostgresTableWithCopy(){
        try {
            new CopyManager((PgConnection) connectionToPosgres)
                    .copyIn(
                            "COPY test FROM STDIN WITH DELIMITER ','",
                            new FileReader(datasetFilename)
                    );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void retrieveAllTuplesFromPostgresTable() throws SQLException {
        String sqlQuery = "select * from test";
        ResultSet rs = handler.getResultSet(sqlQuery);

        while (rs.next()) {
            rs.getInt(1);
            rs.getDouble(2);
        }
    }

    private void dropPostgresTable() throws SQLException {
        String sqlQuery = "drop table test";
        handler.executeUpdate(sqlQuery);
    }


    /*
     * ######## REDIS
     * */
    private void redisTests(){
        long time;

        time = System.currentTimeMillis();
        massInsertionIntoRedis();
        LOGGER.info("\t time for massInsertionIntoRedis(): " + Utils.calculateRunningTime(time) + " sec");

        //insertIntoRedisInBatch();
        //LOGGER.info("\t time for insertIntoRedisInBatch(): " + Utils.calculateRunningTime(time) + " sec");

        time = System.currentTimeMillis();
        establishRedisConnection();
        LOGGER.info("\t time for establishConnection(): " + Utils.calculateRunningTime(time) + " sec");

        time = System.currentTimeMillis();
        retrieveAllTuplesFromRedisUsingScan();
        LOGGER.info("\t time for retrieveAllTuplesFromRedisUsingScan(): " + Utils.calculateRunningTime(time) + " sec");

        time = System.currentTimeMillis();
        //retrieveAllTuplesFromRedisUsingMget();
        LOGGER.info("\t time for retrieveAllTuplesFromRedisUsingMget(): " + Utils.calculateRunningTime(time) + " sec");

        time = System.currentTimeMillis();
        dropDataFromRedis();
        LOGGER.info("\t time for dropDataFromRedis(): " + Utils.calculateRunningTime(time) + " sec");

        if(useJedis) {
            jedis.close();
        }
        else {
            connection.close();
            redisClient.shutdown();
        }
    }

    private void establishRedisConnection(){
        if(useJedis){
            jedis = new Jedis("localhost");
        }
        else {
            redisClient = RedisClient.create("redis://localhost");
            connection = redisClient.connect();
            syncCommands = connection.sync();
        }
    }

    private void massInsertionIntoRedis(){
        String command = "awk -F, 'NR > 0{ print \"SET\", \"\\\"test:\"$1\"\\\"\", \"\\\"\"$2\"\\\"\" }' " + datasetFilename + " | redis-cli --pipe";
        //LOGGER.info(command);
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash", "-c", command);

        try {
            //Process process =
                    processBuilder.start();
            /*StringBuilder output = new StringBuilder();

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

            int exitVal = process.waitFor();
            if (exitVal == 0) {
                System.out.println("Success!");
                System.out.println(output);
            }*/

        } catch (IOException e) {
            e.printStackTrace();
        //} catch (InterruptedException e) {
        //    e.printStackTrace();
        }
    }

    private void insertIntoRedisInBatch(){
        if(useJedis){
            String[] data = new String[2*totalNrOfTuples];
            for(int tupleID = 0; tupleID < totalNrOfTuples; tupleID++){
                data[2 * tupleID] = "test:" + (tupleID+1);
                data[2 * tupleID + 1] = String.valueOf(Math.random());
            }
            jedis.mset(data);
        }
        else {
            HashMap<String,String> data = new HashMap<>();
            for(int tupleID = 1; tupleID <= totalNrOfTuples; tupleID++){
                data.put("test:" + tupleID, String.valueOf(Math.random()));
            }
            syncCommands.mset(data);
        }
    }

    private void retrieveAllTuplesFromRedisUsingScan(){
        if(useJedis){
            int i=0;
            ScanParams scanParams = new ScanParams().count(GlobalSettings.DATABASE_FETCHSIZE).match("*");
            String cur = SCAN_POINTER_START;
            do {
                ScanResult<String> scanResult = jedis.scan(cur, scanParams);
                for(String s: scanResult.getResult()){
                    jedis.get(s);
                    i++;
                }
                cur = scanResult.getCursor();
            } while (!cur.equals(SCAN_POINTER_START));
            LOGGER.info(i);
        }
        else {
            KeyScanCursor<String> cursor = syncCommands.scan();
            int i = 1;

            while (!cursor.isFinished()) {
                List<String> keys = cursor.getKeys();
                for (String key : keys) {
                    syncCommands.get(key);
                    i++;
                }
                cursor = syncCommands.scan(cursor);
            }
            LOGGER.info(i);
            LOGGER.info(cursor.getCursor());
            List<String> keys = cursor.getKeys();
            for (String key : keys) {
                syncCommands.get(key);
                i++;
            }
            LOGGER.info(i);
        }
    }

    private void retrieveAllTuplesFromRedisUsingMget(){
        String allKeys[] = new String[totalNrOfTuples];
        for (int tupleID = 1; tupleID <= totalNrOfTuples; tupleID++) {
            allKeys[tupleID - 1] = "test:" + tupleID;
        }
        if (useJedis){
            //int i=0;
            List<String> res = jedis.mget(allKeys);
            for(String s: res){
                //i++;
                //LOGGER.info(s);
            }
            //LOGGER.info(i);
        }
        else {
            for (KeyValue<String, String> val : syncCommands.mget(allKeys)) {
                LOGGER.info(val.getKey());
                LOGGER.info(val.getValue());
            }
        }

    }

    private void dropDataFromRedis(){
        /*KeyScanCursor<String> cursor = syncCommands.scan(ScanArgs.Builder.limit(200).match("test:*"));
        while (!cursor.isFinished()) {
            for (String key : cursor.getKeys()) {
                syncCommands.del(key);
            }
            cursor = syncCommands.scan(cursor, ScanArgs.Builder.limit(200).match("test:*"));
        }*/
        /*String[] keys = new String[totalNrOfTuples];
        int i=0;
        List<String> tuples = syncCommands.keys("test:*");
        for(String t: tuples) {
            keys[i] = t;
            i++;
        }
        syncCommands.del(keys);*/
        if(useJedis){
            jedis.flushDB();
        }
        else {
            syncCommands.flushdb();
        }
    }

}
