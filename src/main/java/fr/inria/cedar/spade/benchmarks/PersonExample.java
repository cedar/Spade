//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.benchmarks;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;
import com.sleepycat.persist.StoreConfig;
import static com.sleepycat.persist.model.DeleteAction.NULLIFY;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.Persistent;
import com.sleepycat.persist.model.PrimaryKey;
import static com.sleepycat.persist.model.Relationship.MANY_TO_MANY;
import static com.sleepycat.persist.model.Relationship.MANY_TO_ONE;
import static com.sleepycat.persist.model.Relationship.ONE_TO_MANY;
import static com.sleepycat.persist.model.Relationship.ONE_TO_ONE;
import com.sleepycat.persist.model.SecondaryKey;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

public class PersonExample {
    private static final Logger LOGGER = Logger.getLogger(PersonExample.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    /* An entity class. */
    @Entity
    static class Person {

        @PrimaryKey
        String ssn;

        String name;
        Address address;

        @SecondaryKey(relate=MANY_TO_ONE, relatedEntity=Person.class)
        String parentSsn;

        @SecondaryKey(relate=ONE_TO_MANY)
        Set<String> emailAddresses = new HashSet<String>();

        @SecondaryKey(relate=MANY_TO_MANY,
                relatedEntity=Employer.class,
                onRelatedEntityDelete=NULLIFY)
        Set<Long> employerIds = new HashSet<Long>();

        Person(String name, String ssn, String parentSsn) {
            this.name = name;
            this.ssn = ssn;
            this.parentSsn = parentSsn;
        }

        private Person() {} // For deserialization
    }

    /* Another entity class. */
    @Entity
    static class Employer {

        @PrimaryKey(sequence="ID")
        long id;

        @SecondaryKey(relate=ONE_TO_ONE)
        String name;

        Address address;

        Employer(String name) {
            this.name = name;
        }

        private Employer() {} // For deserialization
    }

    /* A persistent class used in other classes. */
    @Persistent
    static class Address {
        String street;
        String city;
        String state;
        int zipCode;
        private Address() {} // For deserialization
    }

    /* The data accessor class for the entity model. */
    static class PersonAccessor {

        /* Person accessors */
        PrimaryIndex<String,Person> personBySsn;
        SecondaryIndex<String,String,Person> personByParentSsn;
        SecondaryIndex<String,String,Person> personByEmailAddresses;
        SecondaryIndex<Long,String,Person> personByEmployerIds;

        /* Employer accessors */
        PrimaryIndex<Long,Employer> employerById;
        SecondaryIndex<String,Long,Employer> employerByName;

        /* Opens all primary and secondary indices. */
        public PersonAccessor(EntityStore store)
                throws DatabaseException {

            personBySsn = store.getPrimaryIndex(
                    String.class, Person.class);

            personByParentSsn = store.getSecondaryIndex(
                    personBySsn, String.class, "parentSsn");

            personByEmailAddresses = store.getSecondaryIndex(
                    personBySsn, String.class, "emailAddresses");

            personByEmployerIds = store.getSecondaryIndex(
                    personBySsn, Long.class, "employerIds");

            employerById = store.getPrimaryIndex(
                    Long.class, Employer.class);

            employerByName = store.getSecondaryIndex(
                    employerById, String.class, "name");
        }
    }

    public static void main(String[] args) throws DatabaseException {
        //boolean berkeleydb=true;
        boolean berkeleydb=false;

        File file = new File("/Users/mirjanamazuran/Desktop/SpadeAggregates/dbEnv");
        if (file.exists() || file.mkdirs()) {
            PersonExample example = new PersonExample(file);
            if (berkeleydb) {
                example.runBerkeleyDB();
                example.close();
            } else {
                example.runRedis();
            }
        }
    }

    private Environment env;
    private EntityStore store;
    private PersonAccessor dao;
    private long peopleNo=10000;

    private PersonExample(File envHome) throws DatabaseException {
        /* Open a transactional Berkeley DB engine environment. */
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        envConfig.setTransactional(true);
        env = new Environment(envHome, envConfig);

        /* Open a transactional entity store. */
        StoreConfig storeConfig = new StoreConfig();
        storeConfig.setAllowCreate(true);
        storeConfig.setTransactional(true);
        store = new EntityStore(env, "PersonStore", storeConfig);

        /* Initialize the data access object. */
        dao = new PersonAccessor(store);
    }

    private void runBerkeleyDB() throws DatabaseException {
        long start = System.currentTimeMillis();
        for (int i = 0; i < peopleNo; i ++) {
            Person p = new Person("Person" + i, "111-111-"+i, null);
            dao.personBySsn.put(p);
        }
        long stop = System.currentTimeMillis();
        System.out.println(peopleNo + " people stored in " + (stop-start) + " ms.");
    }

    private void close() throws DatabaseException {
        store.close();
        env.close();
    }

    private void runRedis(){
        final Jedis jedis;
        Pipeline jedisPipeline;
        final int MAX_PIPELINE_SIZE = 100000;
        int currentPipelineSize;
        HashMap<String,String> myPerson;

        jedis = new Jedis("localhost", 6379, 10000000);
        jedisPipeline = jedis.pipelined();
        currentPipelineSize = 0;

        long start = System.currentTimeMillis();
        for (int i = 0; i < peopleNo; i ++) {
            myPerson = new HashMap();
            myPerson.put("name", "Person" + 1);
            myPerson.put("ssn", "111-111-"+i);
            myPerson.put("parentSsn", "null");
            jedisPipeline.hmset("person:" + i, myPerson);
            currentPipelineSize++;
            if(currentPipelineSize >= MAX_PIPELINE_SIZE){
                jedisPipeline.sync();
                currentPipelineSize=0;
            }
        }
        if(currentPipelineSize>0){
            jedisPipeline.sync();
        }
        long stop = System.currentTimeMillis();
        System.out.println(peopleNo + " people stored in " + (stop-start) + " ms.");
    }
}

