//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.benchmarks;

import fr.inria.cedar.ontosql.db.PostgresDataSource;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.Pair;
import fr.inria.cedar.spade.datastructures.Utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class BenchmarkForClustering {
    private static final int NCFS = 1000000;
    private static final double percentageAdditionalSubjects = 0.3;
    private static final double measureSupport = 0.7;

    private static final int NcfsWithMeasure = (int)(NCFS * measureSupport);
    private static final int NcfsWithoutMeasure = NCFS - NcfsWithMeasure;
    private static final int NAdditionalSubjects = (int)(percentageAdditionalSubjects * NCFS);
    private static final int totalSubjects = NCFS + NAdditionalSubjects;


    public static void main (String[] args) {
        ArrayList<Integer> allSubjects = new ArrayList<>();
        ArrayList<Integer> CFS = new ArrayList<>();
        ArrayList<Pair<Integer, Double>> m = new ArrayList<>();
        Integer subject;
        int idxAllSubjects;

        for (idxAllSubjects = 0; idxAllSubjects < totalSubjects; idxAllSubjects++) {
            allSubjects.add(idxAllSubjects + 1);
        }
        Collections.shuffle(allSubjects);

        for (idxAllSubjects = 0; idxAllSubjects < NcfsWithMeasure; idxAllSubjects++) {
            subject = allSubjects.get(idxAllSubjects);
            CFS.add(subject);
            m.add(new Pair<>(subject, Math.random()));
        }
        for (; idxAllSubjects < NcfsWithMeasure + NcfsWithoutMeasure; idxAllSubjects++) {
            CFS.add(allSubjects.get(idxAllSubjects));
        }
        for (; idxAllSubjects < totalSubjects; idxAllSubjects++) {
            m.add(new Pair<>(allSubjects.get(idxAllSubjects), Math.random()));
        }

        long startTime;
        StringBuilder sqlQuery = new StringBuilder();
        ResultSet rs;
        String prefix = "";
        PostgresDataSource pgds = new PostgresDataSource("localhost", 5432, "benchcluster", "postgres", "p4ssP0stgr3s");
        DatabaseHandler databaseHandler;
        try {
            Connection conn = pgds.getConnection();
            databaseHandler = new DatabaseHandler(conn);

            databaseHandler.setAutoCommit(false);
            databaseHandler.setFetchSize(10000);
            conn.setHoldability(ResultSet.CLOSE_CURSORS_AT_COMMIT);

            prefix = "";
            sqlQuery.setLength(0);
            sqlQuery.append("create table t_123456 (s integer not null); create unique index t_123456_i_s on t_123456 using btree(s); insert into t_123456 values ");
            for(int i=0; i<CFS.size(); i++){
                sqlQuery.append(prefix).append("(").append(CFS.get(i)).append(")");
                prefix = ",";
            }
            sqlQuery.append(";");
            //System.out.println(sqlQuery.toString());

            startTime = System.currentTimeMillis();
            databaseHandler.executeUpdate(sqlQuery.toString());
            System.out.println("Time to create, index and batch insert into CFS: " + Utils.calculateRunTimeInMilliseconds(startTime) + " ms");

            prefix = "";
            sqlQuery.setLength(0);
            sqlQuery.append("create table t_789012_preaggregated (s integer, meas double precision); create unique index t_789012_preaggregated_i_sm on t_789012_preaggregated using btree(s,meas); insert into t_789012_preaggregated values ");
            for(int i=0; i<m.size(); i++){
                sqlQuery.append(prefix).append("(").append(m.get(i).getKey()).append(",").append(m.get(i).getValue()).append(")");
                prefix = ",";
            }
            sqlQuery.append(";");
            //System.out.println(sqlQuery.toString());

            startTime = System.currentTimeMillis();
            databaseHandler.executeUpdate(sqlQuery.toString());
            System.out.println("Time to create, index and batch insert into Measure: " + Utils.calculateRunTimeInMilliseconds(startTime) + " ms");

            sqlQuery.setLength(0);
            sqlQuery.append("select * from t_123456 as cfs left join t_789012_preaggregated as m on cfs.s=m.s;");
            startTime = System.currentTimeMillis();
            rs = databaseHandler.getResultSet(sqlQuery.toString());
            while(rs.next()){

            }
            System.out.println("Time to join CFS and Measure " + Utils.calculateRunTimeInMilliseconds(startTime) + " ms");

            sqlQuery.setLength(0);
            sqlQuery.append("cluster t_123456 using t_123456_i_s; ");
            startTime = System.currentTimeMillis();
            databaseHandler.executeUpdate(sqlQuery.toString());
            System.out.println("Time to cluster CFS " + Utils.calculateRunTimeInMilliseconds(startTime) + " ms");

            sqlQuery.setLength(0);
            sqlQuery.append("cluster t_789012_preaggregated using t_789012_preaggregated_i_sm; ");
            startTime = System.currentTimeMillis();
            databaseHandler.executeUpdate(sqlQuery.toString());
            System.out.println("Time to cluster Measure " + Utils.calculateRunTimeInMilliseconds(startTime) + " ms");

            sqlQuery.setLength(0);
            sqlQuery.append("set enable_hashjoin=off;");
            databaseHandler.executeUpdate(sqlQuery.toString());
            sqlQuery.setLength(0);
            sqlQuery.append("select * from t_123456 as cfs left join t_789012_preaggregated as m on cfs.s=m.s;");
            startTime = System.currentTimeMillis();
            rs = databaseHandler.getResultSet(sqlQuery.toString());
            while(rs.next()){

            }
            sqlQuery.setLength(0);
            sqlQuery.append("set enable_hashjoin=on;");
            databaseHandler.executeUpdate(sqlQuery.toString());
            System.out.println("Time to join CFS and Measure " + Utils.calculateRunTimeInMilliseconds(startTime) + " ms");


            sqlQuery.setLength(0);
            sqlQuery.append("drop table t_123456; drop table t_789012_preaggregated;");
            databaseHandler.executeUpdate(sqlQuery.toString());

            databaseHandler.closeConnection();
        }
        catch (SQLException ex) {
            throw new IllegalStateException(ex);
        }

    }
}
