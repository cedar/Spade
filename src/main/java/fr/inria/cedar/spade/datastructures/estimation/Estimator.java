//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures.estimation;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class Estimator {
    private static final Logger LOGGER = Logger.getLogger(Estimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected DatabaseHandler handler;
    protected MultidimensionalAggregateEstimate aggregateEstimate;
    protected AnalyzedAttribute measure;
    protected AggregationFunction aggregationFunction;
    protected InterestingnessFunction interestingnessFunction;
    protected double confidenceLevel;

    // array[aggregationFunction][groupID] = pre-aggregated value
    double[][] retrievedPreaggregatedValuesByMeasure;

    protected int numberOfGroups;
    protected int[] sizeOfGroupInSample;

    protected int lastIteration;
    protected boolean sampleExhausted;

    protected double pointEstimate;
    protected double upperBoundEstimate;
    protected double lowerBoundEstimate;

    public Estimator(
            DatabaseHandler handler,
            MultidimensionalAggregateEstimate aggregateEstimate,
            AnalyzedAttribute measure,
            AggregationFunction aggregationFunction,
            InterestingnessFunction interestingnessFunction,
            double confidenceLevel,
            double[][] retrievedPreaggregatedValuesByMeasure,
            int numberOfGroups,
            int[] sizeOfGroupInSample) {
        this.handler = handler;
        this.aggregateEstimate = aggregateEstimate;
        this.measure = measure;
        this.aggregationFunction = aggregationFunction;
        this.interestingnessFunction = interestingnessFunction;
        this.confidenceLevel = confidenceLevel;
        this.retrievedPreaggregatedValuesByMeasure = retrievedPreaggregatedValuesByMeasure;

        this.numberOfGroups = numberOfGroups;
        this.sizeOfGroupInSample = sizeOfGroupInSample;

        this.lastIteration = -1;
        this.sampleExhausted = false;

        this.pointEstimate = Double.NaN;
        this.upperBoundEstimate = Double.NaN;
        this.lowerBoundEstimate = Double.NaN;
    }

    public MultidimensionalAggregateEstimate getAggregateEstimate() {
        return aggregateEstimate;
    }

    public AnalyzedAttribute getMeasure() {
        return measure;
    }

    public AggregationFunction getAggregationFunction() {
        return aggregationFunction;
    }

    public int getLastIteration() {
        return lastIteration;
    }

    /*
        Returns true if estimation of interestingness score is statistically
        meaningful: sufficiently many tuples have been retrieved.
     */
    public boolean isComparable() {
        return (lastIteration + 1) * GlobalSettings.EARLY_STOP_RETRIEVAL_BATCH_SIZE >= 30;
    }

    protected abstract void updateEstimatesForFeatureScalingNormalizedVariance(
            int iteration);

    protected abstract void updateEstimatesForSumNormalizedVariance(
            int iteration);

    protected abstract void updateEstimatesForMeanNormalizedVariance(
            int iteration);

    protected abstract void updateEstimatesForSkewness(int iteration);

    protected abstract void updateEstimatesForKurtosis(int iteration);

    protected void updateEstimates(int iteration) {
        switch (interestingnessFunction) {
            case VAR_FEATURE_SCALING:
                updateEstimatesForFeatureScalingNormalizedVariance(iteration);
                break;
            case VAR_SUM:
                updateEstimatesForSumNormalizedVariance(iteration);
                break;
            case VAR_MEAN:
                updateEstimatesForMeanNormalizedVariance(iteration);
                break;
            case SKEWNESS:
                updateEstimatesForSkewness(iteration);
                break;
            case KURTOSIS:
                updateEstimatesForKurtosis(iteration);
                break;
            default:
                throw new IllegalStateException("Unsupported interestingness function");
        }
    }

    public double getPointEstimate() {
        return pointEstimate;
    }

    public double getLowerBoundEstimate() {
        return lowerBoundEstimate;
    }

    public double getUpperBoundEstimate() {
        return upperBoundEstimate;
    }

    public double getConfidenceIntervalLength() {
        return getUpperBoundEstimate() - getLowerBoundEstimate();
    }

    public double getConfidenceLevel() {
        return confidenceLevel;
    }

    public void updateEstimatesAndGetStage(int iteration) {
        if (sampleExhausted) {
            return;
        }

        if (!aggregateEstimate.someValuesWereRetrievedInLastIteration()) {
            sampleExhausted = true;
        }

        if (!sampleExhausted) {
            updateEstimates(iteration);
        }
    }

    public boolean sampleExhausted() {
        return sampleExhausted;
    }

    @Override
    public String toString() {
        String candidateFactSetName = aggregateEstimate.getAggregate().getDecodedCandidateFactSet();

        String dimensionNames = decodeDimensions();
        String measureName = decodeMeasure();

        double intervalLength = getConfidenceIntervalLength();
        double confidenceLevelPercentage = confidenceLevel * 100;

        return "Candidate fact set " + candidateFactSetName
               + ", dimensions: " + dimensionNames
               + ", aggregation function: "
               + aggregationFunction.toString().toLowerCase()
               + ", measure: " + measureName
               + ", interestingness function: "+ interestingnessFunction
               + "\n\tpoint estimate: " + pointEstimate
               + ", " + confidenceLevelPercentage + "% confidence interval: ["
               + lowerBoundEstimate + ", " + upperBoundEstimate + "], length: "
               + intervalLength + ", stage: "
               + (isComparable() ? "comparable" : "incomparable");
    }

    protected String encodedDimensions() {
        String dimensionNames = "";
        String prefix = "";
        for (AnalyzedAttribute dimension: aggregateEstimate.getAggregate().getDimensions()) {
            dimensionNames += prefix + dimension.getEncoding();
            prefix = ", ";
        }

        return dimensionNames;
    }

    protected String encodedMeasure() {
        if (measure == null) {
            return "No measure";
        }

        return Long.toString(measure.getEncoding());
    }

    protected String decodeDimensions() {
        String dimensionName;
        String dimensionNames = "";
        String prefix = "";
        for (AnalyzedAttribute dimension: aggregateEstimate.getAggregate().getDimensions()) {
            dimensionName = handler.decodeAttributeName(dimension);
            dimensionNames += prefix + dimensionName;
            prefix = ", ";
        }

        return dimensionNames;
    }

    protected String decodeMeasure() {
        if (measure == null) {
            return "No measure";
        }

        return handler.decodeAttributeName(measure);
    }

    public StringBuilder toShortString(long startTime, long endTime,
                                       int iteration,
                                       int iterationsWithoutStoppage,
                                       boolean useFilename) {
        String candidateFactSetName = aggregateEstimate.getAggregate().getDecodedCandidateFactSet();

        //String dimensionNames = decodeDimensions(true);
        //String measureName = decodeMeasure(true);
        String dimensionNames = encodedDimensions();
        String measureName = encodedMeasure();

        double intervalLength = getConfidenceIntervalLength();

        String startTimeDate = (new SimpleDateFormat("HH:mm:ss.SSS")).format(new Date(startTime));
        String endTimeDate = (new SimpleDateFormat("HH:mm:ss.SSS")).format(new Date(endTime));

        String duration = Long.toString(endTime - startTime);

        StringBuilder output = new StringBuilder();
        if (useFilename) {
            output.append(aggregateEstimate.getAggregate().getAggregateResultsFilename(measure, aggregationFunction));
        }
        else {
            output.append(candidateFactSetName).append("; (")
                    .append(dimensionNames).append("); ")
                    .append(aggregationFunction.toString().toLowerCase())
                    .append(" over ").append(measureName).append("; ")
                    .append(interestingnessFunction);
        }

        return output.append("\n\t")
                .append(pointEstimate)
                .append(", [").append(lowerBoundEstimate).append(", ")
                .append(upperBoundEstimate).append("], ")
                .append(intervalLength).append(", ")
                .append(isComparable() ? "comparable" : "incomparable")
                .append("\n\t")
                .append(startTimeDate).append(" - ").append(endTimeDate)
                .append(", ").append(duration).append(" ms; iteration: ")
                .append(iteration).append("; iterations without stoppage: ")
                .append(iterationsWithoutStoppage);
    }
}
