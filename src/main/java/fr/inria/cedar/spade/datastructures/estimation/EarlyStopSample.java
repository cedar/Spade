//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures.estimation;

import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.molapForRDF.AggregateInLattice;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.roaringbitmap.RoaringBitmap;

public abstract class EarlyStopSample {
    private static final Logger LOGGER = Logger.getLogger(EarlyStopSample.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected AggregateInLattice root;
    protected int numberOfSubjectsToSampleInEachGroup;

    public static class Group {
        public int totalGroupSize;
        public int[] subjectIDsArray;
        public RoaringBitmap subjectIDsBitmap;

        public Group() {
            totalGroupSize = 1;
            subjectIDsArray = null;
            subjectIDsBitmap = null;
        }
    }

    // groupID -> Group object; the order of keys is fixed: can be reiterated
    protected LinkedHashMap<Long, Group> groups;

    public EarlyStopSample(AggregateInLattice root,
                           int numberOfSubjectsToSampleInEachGroup) {
        this.root = root;
        this.numberOfSubjectsToSampleInEachGroup = numberOfSubjectsToSampleInEachGroup;
        groups = new LinkedHashMap<>();
    }

    public abstract void addTupleToSample(long globalIndexOfGroup, int subjectID);

    public abstract void postprocess();

    public void printSample() {
        Long groupID;
        int[] subjectIDs;
        for (Map.Entry<Long, Group> entry: groups.entrySet()) {
            groupID = entry.getKey();
            System.out.println("GroupID: " + groupID);
            subjectIDs = entry.getValue().subjectIDsArray;
            for (Integer value: subjectIDs) {
                System.out.println("\tSubjectID: " + value);
            }
        }
        System.out.println();
    }

    protected final static Long translate(
            Long groupID,
            AggregateInLattice parent,
            AggregateInLattice child,
            HashMap<AnalyzedAttribute, Integer> chunkSizePerAttribute) {
        // unpack groupID to (chunkID, offset)
        int divisor = 1;
        int multiplier = 1;
        int chunkSize;
        AnalyzedAttribute dimensionMissingFromParent = child.getDimensionMissingFromParent();
        for (AnalyzedAttribute dimension: parent.getDimensions()) {
            chunkSize = chunkSizePerAttribute.get(dimension);
            divisor *= chunkSize;
            if (dimension != dimensionMissingFromParent) {
                multiplier *= chunkSize;
            }
        }
        int nodeChunkID = (int) (groupID / divisor);
        long nodeOffset = groupID % divisor;

        // translate
        int childChunkID = child.computeChunkId(nodeChunkID);
        long childOffsetID = child.computeOffset(nodeOffset);

        // pack (childChunkID, childOffsetID) to childGroupID
        return childChunkID * multiplier + childOffsetID;
    }

    @SuppressWarnings("ArraysAsListPrimitiveArray")
    protected final static EarlyStopSample propagateSampleToChild(
            EarlyStopSample sample,
            AggregateInLattice child,
            HashMap<AnalyzedAttribute, Integer> chunkSizePerAttribute) {
        EarlyStopSample childSample = null;
        if (sample instanceof EarlyStopSampleUsingNaiveSampling) {
            childSample = new EarlyStopSampleUsingNaiveSampling(child, sample.numberOfSubjectsToSampleInEachGroup);
        }
        else if (sample instanceof EarlyStopSampleUsingAlgorithmRReservoirSampling) {
            childSample = new EarlyStopSampleUsingAlgorithmRReservoirSampling(child, sample.numberOfSubjectsToSampleInEachGroup);
        }
        else if (sample instanceof EarlyStopSampleUsingReservoirSamplingWithRandomSort) {
            childSample = new EarlyStopSampleUsingReservoirSamplingWithRandomSort(child, sample.numberOfSubjectsToSampleInEachGroup);
        }
        else {
            throw new IllegalStateException("Early-stop sample type " + sample.getClass() + " not supported");
        }

        // translate IDs and create new groups (some groups will be merged)
        Long childGroupID;
        Group group;
        for (Map.Entry<Long, Group> entry: sample.groups.entrySet()) {
            final Long groupID = entry.getKey();
            group = entry.getValue();
            final int count = group.totalGroupSize;
            final RoaringBitmap bitmap = group.subjectIDsBitmap;

            childGroupID = translate(groupID, sample.root, child, chunkSizePerAttribute);

            childSample.groups.compute(childGroupID, (Long key, Group value) -> {
                if (value == null) {
                    Group newGroup = new Group();
                    newGroup.totalGroupSize = count;

                    newGroup.subjectIDsBitmap = new RoaringBitmap();
                    newGroup.subjectIDsBitmap.or(bitmap);

                    return newGroup;
                }
                else {
                    // summing the counts: results are upper bound on counts
                    value.totalGroupSize += count;

                    value.subjectIDsBitmap.or(bitmap);

                    return value;
                }
            });
        }

        for (Map.Entry<Long, Group> entry: childSample.groups.entrySet()) {
            group = entry.getValue();
            group.subjectIDsArray = group.subjectIDsBitmap.toArray();

            // shuffle the subjectIDs in place
            Collections.shuffle(Arrays.asList(group.subjectIDsArray));
        }

        return childSample;
    }

    // convert tree to list of aggregates with samples
    protected final static void collectAggregatesWithSamples(
            LinkedList<EarlyStopSample> aggregatesWithSamples,
            EarlyStopSample sample,
            HashMap<AnalyzedAttribute, Integer> chunkSizePerAttribute) {
        // add aggregates in node
        aggregatesWithSamples.add(sample);

        LinkedList<AggregateInLattice> children = sample.root.getChildrenAggregates();
        EarlyStopSample childSample;
        for (AggregateInLattice child: children) {
            // propagate the groups to child
            childSample = propagateSampleToChild(sample, child, chunkSizePerAttribute);

            // add results from child to the accumulated result
            collectAggregatesWithSamples(aggregatesWithSamples, childSample, chunkSizePerAttribute);
        }
    }

    public final static LinkedList<EarlyStopSample> collectAggregatesWithSamples(
            EarlyStopSample sample,
            HashMap<AnalyzedAttribute, Integer> chunkSizePerAttribute) {
        LinkedList<EarlyStopSample> aggregatesWithSamples = new LinkedList<>();
        collectAggregatesWithSamples(aggregatesWithSamples, sample, chunkSizePerAttribute);

        return aggregatesWithSamples;
    }

    public AggregateInLattice getAggregateInLattice() {
        return root;
    }

    public LinkedHashMap<Long, Group> getGroups() {
        return groups;
    }
}
