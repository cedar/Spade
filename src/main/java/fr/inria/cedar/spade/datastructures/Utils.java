//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Utils {
    private static final Logger LOGGER = Logger.getLogger(Utils.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    /*
	* Get short name of a URI (last substring containing only letters or digits)
	* For instance, http://sw.deri.org/~aharth/2004/07/dblp/dblp.owl#year ==> year
	* */
    public static String getShorterAttributeName(String typeName) {
        int lastSlash = typeName.lastIndexOf('/');
        int lastHash = typeName.lastIndexOf('#');

        int fromidx = lastSlash;
        if (lastHash > lastSlash){
            fromidx = lastHash;
        }
        return typeName.substring(fromidx + 1, typeName.length()-1);

        /*String result;
        String pattern = "([A-Za-z0-9]+)>?$";
        //String pattern = "([A-Za-z0-9]+)>? .";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(typeName);
        if (m.find()) {
            result = m.group(1);
        }

        return result;*/
    }

    /**
     * Used to calculate the program's running time
     *
     * @param timeStart time when the program started
     *
     * @return running time in second
     */
    public static double calculateRunningTime(long timeStart) {
        long timeEnd = System.currentTimeMillis();
        long tDelta = timeEnd - timeStart;
        return tDelta / 1000.0;
    }

    public static long calculateRunTimeInMilliseconds(long startTime) {
        return System.currentTimeMillis() - startTime;
    }

    // reference: https://stackoverflow.com/questions/1515489/compute-sha-1-of-byte-array
    protected static String bytesToHex(byte[] hash) {
        String hex;
        try (Formatter formatter = new Formatter()) {
            for (byte b: hash) {
                formatter.format("%02x", b);
            }
            hex = formatter.toString();
        }

        return hex;
    }

    // reference: https://www.baeldung.com/sha-256-hashing-java
    /*protected static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        String hex;

        for (int i = 0; i < hash.length / 2; i++) {
            hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) { // only one digit
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }*/

    public static String generateSHA1(String convertme) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");

        return bytesToHex(md.digest(convertme.getBytes()));
    }

    /*protected static String generateDimensionsAsHash(String dimensions) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(dimensions.getBytes());
        byte[] bytes = messageDigest.digest();

        return bytesToHex(bytes);
    }*/
}
