//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures;

public enum InterestingnessFunction {
    VAR_FEATURE_SCALING("Variance over data normalized by feature scaling"),
    VAR_SUM("Variance over data divided by sum"),
    VAR_MEAN("Variance over data divided by mean"),
    ENTROPY("Entropy"),
    NORMALIZED_ENTROPY("Normalized entropy"),
    CUSTOM_ENTROPY("Normalized entropy computed according to 2*(1-abs(entr-0.5)-0.5)"),
    SKEWNESS("Skewness"),
    KURTOSIS("Kurtosis"),;
    private final String text;

    InterestingnessFunction(String text, Object... args) {
        this.text = format(text, args);
    }

    private static String format(String text, Object... args) {
        return String.format(text, args);
    }

    @Override
    public String toString() {
        return text;
    }
}
