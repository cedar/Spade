//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures.estimation;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.datastructures.estimation.EarlyStopSample.Group;
import fr.inria.cedar.spade.molapForRDF.AggregateInLattice;
import fr.inria.cedar.spade.molapForRDF.MOLAPMeasures;
import static fr.inria.cedar.spade.molapForRDF.MOLAPMeasures.AVERAGE_ORDINAL;
import static fr.inria.cedar.spade.molapForRDF.MOLAPMeasures.COUNT_ORDINAL;
import static fr.inria.cedar.spade.molapForRDF.MOLAPMeasures.ESTIMATED_TOTAL_GROUP_SIZE;
import static fr.inria.cedar.spade.molapForRDF.MOLAPMeasures.EXTRAPOLATED_SUM_ORDINAL;
import static fr.inria.cedar.spade.molapForRDF.MOLAPMeasures.MAX_ORDINAL;
import static fr.inria.cedar.spade.molapForRDF.MOLAPMeasures.MIN_ORDINAL;
import static fr.inria.cedar.spade.molapForRDF.MOLAPMeasures.NUMBER_OF_PREAGGREGATED_VALUE_TYPES;
import static fr.inria.cedar.spade.molapForRDF.MOLAPMeasures.SUM_ORDINAL;
import static fr.inria.cedar.spade.molapForRDF.MOLAPMeasures.VARIANCE_ORDINAL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MultidimensionalAggregateEstimate {
    private static final Logger LOGGER = Logger.getLogger(MultidimensionalAggregateEstimate.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected final DatabaseHandler handler;
    protected final AggregateInLattice aggregate;
    protected final LinkedHashMap<Long, Group> sampleGroups;

    // we assume here that there will be at most 2^31 aggregation groups (existing, not all possible)
    protected final int numberOfGroups;

    protected MOLAPMeasures measures;

    // measure[aggregation function] -> estimator
    protected HashMap<AnalyzedAttribute, Estimator[]> estimators;

    // measure[aggregation function][groupID] = pre-aggregated value
    protected HashMap<AnalyzedAttribute, double[][]> retrievedPreaggregatedValues;

    // measure[groupID] -> raw values list
    protected HashMap<AnalyzedAttribute, ArrayList<ArrayList<Double>>> retrievedRawValues;

    protected int lastIteration;
    protected boolean someValuesWereRetrievedInLastIteration;
    protected HashMap<AnalyzedAttribute, int[]> sizeOfGroupInSample;

    public MultidimensionalAggregateEstimate(
            DatabaseHandler handler,
            AggregateInLattice aggregate,
            EarlyStopSample sample) {
        this.handler = handler;
        this.aggregate = aggregate;
        this.sampleGroups = sample.getGroups();
        numberOfGroups = sampleGroups.size();
        this.measures = null;

        estimators = new HashMap<>();
        retrievedPreaggregatedValues = new HashMap<>();
        retrievedRawValues = new HashMap<>();

        lastIteration = -1;
        someValuesWereRetrievedInLastIteration = false;
        sizeOfGroupInSample = new HashMap<>();
    }

    public void setMeasures(MOLAPMeasures measures) {
        this.measures = measures;
    }

    public AggregateInLattice getAggregate() {
        return aggregate;
    }

    protected void addEstimator(
            AnalyzedAttribute measure,
            AggregationFunction aggregationFunction,
            Estimator estimator) {
        if (!estimators.containsKey(measure)) {
            estimators.put(measure, new Estimator[5]);
        }
        estimators.get(measure)[MOLAPMeasures.getOrdinal(aggregationFunction)] = estimator;
    }

    public Estimator addEstimator(
            MultidimensionalAggregateEstimate aggregateEstimate,
            AnalyzedAttribute measure,
            AggregationFunction aggregationFunction,
            InterestingnessFunction interestingnessFunction,
            double confidenceLevel) {
        Estimator estimator = null;
        switch (interestingnessFunction) {
            case VAR_FEATURE_SCALING:
            case VAR_SUM:
            case VAR_MEAN:
            case SKEWNESS:
            case KURTOSIS:
                if (!retrievedPreaggregatedValues.containsKey(measure)) {
                    double[][] preaggregatedValuesByAggregationFunctionAndGroup = new double[NUMBER_OF_PREAGGREGATED_VALUE_TYPES][];
                    retrievedPreaggregatedValues.put(measure, preaggregatedValuesByAggregationFunctionAndGroup);
                    for (int i = 0; i < NUMBER_OF_PREAGGREGATED_VALUE_TYPES; i++) {
                        preaggregatedValuesByAggregationFunctionAndGroup[i] = new double[numberOfGroups];
                        Arrays.fill(preaggregatedValuesByAggregationFunctionAndGroup[i], Double.NaN);
                    }

                    int[] groupSampleSize = new int[1];
                    sizeOfGroupInSample.put(measure, groupSampleSize);

                    ArrayList<ArrayList<Double>> retrievedRawValuesByMeasure = new ArrayList<>(numberOfGroups);
                    retrievedRawValues.put(measure, retrievedRawValuesByMeasure);
                    for (int groupID = 0; groupID < numberOfGroups; groupID++) {
                        retrievedRawValuesByMeasure.add(new ArrayList<>());
                    }
                }
                switch (aggregationFunction) {
                    case MIN:
                        estimator = new InterestingnessFunctionOverMinEstimator(handler, aggregateEstimate, measure, aggregationFunction, interestingnessFunction, confidenceLevel, retrievedPreaggregatedValues.get(measure), numberOfGroups, sizeOfGroupInSample.get(measure));
                        break;
                    case MAX:
                        estimator = new InterestingnessFunctionOverMaxEstimator(handler, aggregateEstimate, measure, aggregationFunction, interestingnessFunction, confidenceLevel, retrievedPreaggregatedValues.get(measure), numberOfGroups, sizeOfGroupInSample.get(measure));
                        break;
                    case SUM:
                        estimator = new InterestingnessFunctionOverSumEstimator(handler, aggregateEstimate, measure, aggregationFunction, interestingnessFunction, confidenceLevel, retrievedPreaggregatedValues.get(measure), numberOfGroups, sizeOfGroupInSample.get(measure));
                        break;
                    case AVERAGE:
                        estimator = new InterestingnessFunctionOverAverageEstimator(handler, aggregateEstimate, measure, aggregationFunction, interestingnessFunction, confidenceLevel, retrievedPreaggregatedValues.get(measure), numberOfGroups, sizeOfGroupInSample.get(measure));
                        break;
                }
                break;
            default:
                throw new IllegalStateException("No estimator for intesteringness function: " + interestingnessFunction);
        }

        addEstimator(measure, aggregationFunction, estimator);

        return estimator;
    }

    public void removeEstimator(
            AnalyzedAttribute measure,
            AggregationFunction aggregationFunction) {
        estimators.get(measure)[MOLAPMeasures.getOrdinal(aggregationFunction)] = null;
    }

    public Estimator getEstimator(
            AnalyzedAttribute measure,
            AggregationFunction aggregationFunction) {
        return estimators.get(measure)[MOLAPMeasures.getOrdinal(aggregationFunction)];
    }

    public void updateGroup(AnalyzedAttribute measure,
                            double[][] preaggregatedValuesArrayPerMeasure,
                            double[][] retrievedPreaggregatedValuesByMeasure,
                            ArrayList<ArrayList<Double>> retrievedRawValuesByMeasure,
                            int groupID,
                            int[] subjectIDs,
                            int totalGroupSize,
                            int start,
                            int end,
                            boolean hasSumOrAverage,
                            boolean hasMin,
                            boolean hasMax) {
        if (end > totalGroupSize) {
            end = totalGroupSize;
        }
        if (end > subjectIDs.length) {
            end = subjectIDs.length;
        }

        ArrayList<Double> retrievedRawValuesByMeasureByGroup = retrievedRawValuesByMeasure.get(groupID);
        int subjectID;
        int minOrdinal;
        int maxOrdinal;
        double count;
        double value;

        for (int i = start; i < end; i++) {
            subjectID = subjectIDs[i];

            if (measure.isAtMostOne()) {
                minOrdinal = SUM_ORDINAL;
                maxOrdinal = SUM_ORDINAL;
            }
            else {
                minOrdinal = MIN_ORDINAL;
                maxOrdinal = MAX_ORDINAL;
            }

            if (preaggregatedValuesArrayPerMeasure[COUNT_ORDINAL] != null) {
                count = preaggregatedValuesArrayPerMeasure[COUNT_ORDINAL][subjectID - 1];
            }
            else {
                if (!Double.isNaN(preaggregatedValuesArrayPerMeasure[SUM_ORDINAL][subjectID - 1])) {
                    count = 1.0;
                }
                else {
                    count = Double.NaN;
                }
            }
            if (!Double.isNaN(count)) {
                if (Double.isNaN(retrievedPreaggregatedValuesByMeasure[COUNT_ORDINAL][groupID])) {
                    retrievedPreaggregatedValuesByMeasure[COUNT_ORDINAL][groupID] = count;
                }
                else {
                    retrievedPreaggregatedValuesByMeasure[COUNT_ORDINAL][groupID] += count;
                }

                if (hasSumOrAverage) {
                    value = preaggregatedValuesArrayPerMeasure[SUM_ORDINAL][subjectID - 1];

                    if (count > 1) {
                        // retrieve and add raw values
                        retrievedRawValuesByMeasureByGroup.addAll(measures.getRawValuesOfMeasure(measure).get(subjectID - 1));
                    }
                    else {
                        // add raw values if hasSumOrAverage
                        retrievedRawValuesByMeasureByGroup.add(value);
                    }

                    if (!Double.isNaN(value)) {
                        if (Double.isNaN(retrievedPreaggregatedValuesByMeasure[SUM_ORDINAL][groupID])) {
                            retrievedPreaggregatedValuesByMeasure[SUM_ORDINAL][groupID] = value;
                        }
                        else {
                            retrievedPreaggregatedValuesByMeasure[SUM_ORDINAL][groupID] += value;
                        }
                    }
                }
                if (hasMin) {
                    value = preaggregatedValuesArrayPerMeasure[minOrdinal][subjectID - 1];
                    if (!Double.isNaN(value)) {
                        if (Double.isNaN(retrievedPreaggregatedValuesByMeasure[MIN_ORDINAL][groupID])) {
                            retrievedPreaggregatedValuesByMeasure[MIN_ORDINAL][groupID] = value;
                        }
                        else {
                            if (retrievedPreaggregatedValuesByMeasure[MIN_ORDINAL][groupID] > value) {
                                retrievedPreaggregatedValuesByMeasure[MIN_ORDINAL][groupID] = value;
                            }
                        }
                    }
                }
                if (hasMax) {
                    value = preaggregatedValuesArrayPerMeasure[maxOrdinal][subjectID - 1];
                    if (!Double.isNaN(value)) {
                        if (Double.isNaN(retrievedPreaggregatedValuesByMeasure[MAX_ORDINAL][groupID])) {
                            retrievedPreaggregatedValuesByMeasure[MAX_ORDINAL][groupID] = value;
                        }
                        else {
                            if (retrievedPreaggregatedValuesByMeasure[MAX_ORDINAL][groupID] < value) {
                                retrievedPreaggregatedValuesByMeasure[MAX_ORDINAL][groupID] = value;
                            }
                        }
                    }
                }

                someValuesWereRetrievedInLastIteration = true;
            }
        }

        if (hasSumOrAverage && !Double.isNaN(retrievedPreaggregatedValuesByMeasure[COUNT_ORDINAL][groupID])) {
            retrievedPreaggregatedValuesByMeasure[AVERAGE_ORDINAL][groupID] =
                    retrievedPreaggregatedValuesByMeasure[SUM_ORDINAL][groupID]
                    / retrievedPreaggregatedValuesByMeasure[COUNT_ORDINAL][groupID];

            retrievedPreaggregatedValuesByMeasure[VARIANCE_ORDINAL][groupID] =
                    EfficientComputations.varianceWithKnownMean(retrievedRawValuesByMeasureByGroup, retrievedPreaggregatedValuesByMeasure[AVERAGE_ORDINAL][groupID]);

            // estimated total group size for the measure
            retrievedPreaggregatedValuesByMeasure[ESTIMATED_TOTAL_GROUP_SIZE][groupID] =
                    (int) Math.round(totalGroupSize * measure.getSupport());

            // extrapolated sum
            retrievedPreaggregatedValuesByMeasure[EXTRAPOLATED_SUM_ORDINAL][groupID] =
                    retrievedPreaggregatedValuesByMeasure[ESTIMATED_TOTAL_GROUP_SIZE][groupID]
                    * retrievedPreaggregatedValuesByMeasure[AVERAGE_ORDINAL][groupID];
        }
    }

    public void retrieveBatchFromEachGroup(int iteration) {
        if (lastIteration == iteration) {
            return;
        }
        lastIteration = iteration;
        someValuesWereRetrievedInLastIteration = false;

        AnalyzedAttribute measure;
        Estimator[] estimatorsByAggregationFunction;

        boolean hasSumOrAverage;
        boolean hasMin;
        boolean hasMax;

        double[][] preaggregatedValuesArrayPerMeasure;
        double[][] retrievedPreaggregatedValuesByMeasure;
        ArrayList<ArrayList<Double>> retrievedRawValuesByMeasure;

        int groupID;
        Group group;
        int[] subjectIDs;
        int totalGroupSize;
        int groupSampleSize;
        int minSizeOfGroupInSample;

        int start = iteration * GlobalSettings.EARLY_STOP_RETRIEVAL_BATCH_SIZE;
        int end = start + GlobalSettings.EARLY_STOP_RETRIEVAL_BATCH_SIZE;

        for (Map.Entry<AnalyzedAttribute, Estimator[]> entry: estimators.entrySet()) {
            measure = entry.getKey();
            estimatorsByAggregationFunction = entry.getValue();

            hasSumOrAverage = estimatorsByAggregationFunction[SUM_ORDINAL] != null || estimatorsByAggregationFunction[AVERAGE_ORDINAL] != null;
            hasMin = estimatorsByAggregationFunction[MIN_ORDINAL] != null;
            hasMax = estimatorsByAggregationFunction[MAX_ORDINAL] != null;

            preaggregatedValuesArrayPerMeasure = measures.getPreaggregatedValuesArrayPerMeasure(measure);
            retrievedPreaggregatedValuesByMeasure = retrievedPreaggregatedValues.get(measure);
            retrievedRawValuesByMeasure = retrievedRawValues.get(measure);

            /*
                We do not use entry.getKey(), instead we renumber groupID on the
                fly: it can be done because sampleGroups is a LinkedHashMap,
                thus the entrySet order is guaranteed to be the same every time.
            */
            groupID = 0;
            for (Map.Entry<Long, Group> entry2: sampleGroups.entrySet()) {
                group = entry2.getValue();
                subjectIDs = group.subjectIDsArray;
                totalGroupSize = group.totalGroupSize;

                if (start < totalGroupSize) {
                    updateGroup(measure, preaggregatedValuesArrayPerMeasure, retrievedPreaggregatedValuesByMeasure, retrievedRawValuesByMeasure, groupID, subjectIDs, totalGroupSize, start, end, hasSumOrAverage, hasMin, hasMax);
                }

                groupID++;
            }

            minSizeOfGroupInSample = Integer.MAX_VALUE;
            for (ArrayList<Double> groupSample: retrievedRawValues.get(measure)) {
                groupSampleSize = groupSample.size();
                if (groupSampleSize > 0 && minSizeOfGroupInSample > groupSampleSize) {
                    minSizeOfGroupInSample = groupSampleSize;
                }
            }
            sizeOfGroupInSample.get(measure)[0] = minSizeOfGroupInSample;
        }
    }

    public boolean someValuesWereRetrievedInLastIteration() {
        return someValuesWereRetrievedInLastIteration;
    }
}
