//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures.estimation;

import fr.inria.cedar.spade.molapForRDF.AggregateInLattice;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.roaringbitmap.RoaringBitmap;

public class EarlyStopSampleUsingNaiveSampling extends EarlyStopSample {
    private static final Logger LOGGER = Logger.getLogger(EarlyStopSampleUsingNaiveSampling.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public EarlyStopSampleUsingNaiveSampling(
            AggregateInLattice root,
            int numberOfSubjectsToSampleInEachGroup) {
        super(root, numberOfSubjectsToSampleInEachGroup);
    }

    @Override
    public void addTupleToSample(long groupID, int subjectID) {
        Group group;
        if (!groups.containsKey(groupID)) {
            group = new Group();
            groups.put(groupID, group); // sets counter to 1

            group.subjectIDsArray = new int[numberOfSubjectsToSampleInEachGroup];
            group.subjectIDsArray[0] = subjectID;

            group.subjectIDsBitmap = new RoaringBitmap();
        }
        else {
            group = groups.get(groupID);

            int position = group.totalGroupSize++; // post-incrementation: position - 1
            if (position < numberOfSubjectsToSampleInEachGroup) {
                group.subjectIDsArray[position] = subjectID;
            }

            // bitmap updated in postprocessing
        }
    }

    @Override
    @SuppressWarnings("ArraysAsListPrimitiveArray")
    public void postprocess() {
        // only for debugging
        //sample.printSample();

        Group group;
        int totalGroupSize;
        int[] subjectIDs;
        RoaringBitmap bitmap;
        for (Map.Entry<Long, Group> entry : groups.entrySet()) {
            group = entry.getValue();
            totalGroupSize = group.totalGroupSize;
            subjectIDs = group.subjectIDsArray;
            bitmap = group.subjectIDsBitmap;

            // put the subjectIDs in a bitmap (for propagation)
            bitmap.addN(subjectIDs, 0, totalGroupSize < numberOfSubjectsToSampleInEachGroup ? totalGroupSize : numberOfSubjectsToSampleInEachGroup);

            // shuffle the subjectIDs in place
            Collections.shuffle(Arrays.asList(subjectIDs));
        }
    }
}
