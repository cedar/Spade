//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures.estimation;

import fr.inria.cedar.spade.datastructures.Pair;
import fr.inria.cedar.spade.molapForRDF.AggregateInLattice;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.roaringbitmap.RoaringBitmap;

public class EarlyStopSampleUsingReservoirSamplingWithRandomSort extends EarlyStopSample {
    private static final Logger LOGGER = Logger.getLogger(EarlyStopSampleUsingReservoirSamplingWithRandomSort.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected ThreadLocalRandom random;

    // groupID -> priority queue of (subjectID, randomNumber)
    protected HashMap<Long, PriorityQueue<Pair<Integer, Double>>> queues;

    public EarlyStopSampleUsingReservoirSamplingWithRandomSort(
            AggregateInLattice root,
            int numberOfSubjectsToSampleInEachGroup) {
        super(root, numberOfSubjectsToSampleInEachGroup);

        random = ThreadLocalRandom.current();
        queues = new HashMap<>();
    }

    private static class SubjectIDRandomNumberComparator implements
            Comparator<Pair<Integer, Double>> {
        @Override
        public int compare(Pair<Integer, Double> pair1,
                           Pair<Integer, Double> pair2) {
            Double value1 = pair1.getValue();
            Double value2 = pair2.getValue();

            if (value1 == null && value2 == null) {
                return 0;
            }
            if (value1 == null) {
                return 1;
            }
            if (value2 == null) {
                return -1;
            }

            return value1.compareTo(value2);
        }
    }

    @Override
    public void addTupleToSample(long groupID, int subjectID) {
        if (!groups.containsKey(groupID)) {
            Group group = new Group();
            groups.put(groupID, group);

            // comparator ensures that the subjects with the lowest random number will appear in the front of the priority queue
            PriorityQueue<Pair<Integer, Double>> newQueue = new PriorityQueue<>(numberOfSubjectsToSampleInEachGroup, new SubjectIDRandomNumberComparator());
            queues.put(groupID, newQueue);
        }
        else {
            float randomNumber = random.nextFloat();
            PriorityQueue<Pair<Integer, Double>> queue = queues.get(groupID);
            if (queue.size() >= numberOfSubjectsToSampleInEachGroup) {
                double firstRandomNumberInQueue = queue.peek().getValue();
                if (randomNumber > firstRandomNumberInQueue) {
                    // remove the first pair from queue
                    queue.poll();
                    // add current subject with its randomNumber to queue
                    queue.add(new Pair(subjectID, randomNumber));
                }
            }
            else {
                // add to queue
                queue.add(new Pair(subjectID, randomNumber));
            }
        }
    }

    @Override
    @SuppressWarnings("ArraysAsListPrimitiveArray")
    public void postprocess() {
        Long groupID;
        Group group;
        int subjectID;
        int position;
        for (Map.Entry<Long, PriorityQueue<Pair<Integer, Double>>> entry: queues.entrySet()) {
            groupID = entry.getKey();
            group = groups.get(groupID);
            group.subjectIDsArray = new int[numberOfSubjectsToSampleInEachGroup];
            group.subjectIDsBitmap = new RoaringBitmap();
            position = 0;

            PriorityQueue<Pair<Integer, Double>> queue = entry.getValue();
            for (Pair<Integer, Double> pair: queue) {
                subjectID = pair.getKey();
                group.subjectIDsBitmap.add(subjectID);

                group.subjectIDsArray[position++] = subjectID;
            }

            // shuffle the subjectIDs in place
            Collections.shuffle(Arrays.asList(group.subjectIDsArray));
        }
    }
}
