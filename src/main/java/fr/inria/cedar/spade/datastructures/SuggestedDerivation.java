//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures;

/**
 * This is the set of possible ways in which we can derive properties.
 *
 */
public enum SuggestedDerivation {
    COUNT_ATTR("COUNT_ATTR"), // number of times a fact has the attribute attr
    PATH("PATH"), // path derivation
    KWD_IN_TEXT("KWD_IN_TEXT"), // keyword extraction from text
    LANG_DET("LANG_DET"), // language detection from text
    ;
    private final String text;

    SuggestedDerivation(String text, Object... args) {
        this.text = format(text, args);
    }

    private static String format(String text, Object... args) {
        return String.format(text, args);
    }

    public static SuggestedDerivation convert(String enumString) {
        switch (enumString) {
            case "COUNT_ATTR":
                return COUNT_ATTR;
            case "PATH":
                return PATH;
            case "KWD_IN_TEXT":
                return KWD_IN_TEXT;
            case "LANG_DET":
                return LANG_DET;
            default:
                throw new IllegalStateException("No value corresponding to " + enumString);
        }
    }

    @Override
    public String toString() {
        return text;
    }
}
