//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures.estimation;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.molapForRDF.MOLAPMeasures;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class InterestingnessFunctionOverSumEstimator extends DeltaMethodEstimator {
    private static final Logger LOGGER = Logger.getLogger(InterestingnessFunctionOverSumEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    public InterestingnessFunctionOverSumEstimator(
            DatabaseHandler handler,
            MultidimensionalAggregateEstimate aggregateEstimate,
            AnalyzedAttribute measure,
            AggregationFunction aggregationFunction,
            InterestingnessFunction interestingnessFunction,
            double confidenceLevel,
            double[][] retrievedPreaggregatedValuesByMeasure,
            int numberOfGroups,
            int[] sizeOfGroupInSample) {
        super(handler, aggregateEstimate, measure, aggregationFunction, interestingnessFunction, confidenceLevel, retrievedPreaggregatedValuesByMeasure, numberOfGroups, sizeOfGroupInSample);
    }

    @Override
    protected void updateEstimatesForFeatureScalingNormalizedVariance(
            int iteration) {
        double[] extrapolatedSums = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.EXTRAPOLATED_SUM_ORDINAL];

        /*fr.inria.cedar.DataProfiler.utils.Pair minMax = minAndMaxSimultaneously(extrapolatedSums);
        if (minMax == null) {
            throw new IllegalStateException("Couldn't find min and max in extrapolatedSums array");
        }
        double min = minMax.getMin();
        double max = minMax.getMax();*/
        double min = EfficientComputations.minNonNaN(extrapolatedSums);
        double max = EfficientComputations.maxNonNaN(extrapolatedSums);

        double[] normalizedValues = EfficientComputations.normalizeByFeatureScaling(extrapolatedSums, min, max);
        pointEstimate = EfficientComputations.varianceNonNaN(normalizedValues);
        double scalingFactor = max - min;

        double error = computeDeltaMethodConfidenceIntervalForVariance(false);
        double scaledError = error / scalingFactor;

        lowerBoundEstimate = pointEstimate - scaledError;
        upperBoundEstimate = pointEstimate + scaledError;

        lastIteration = iteration;
    }

    @Override
    protected void updateEstimatesForSumNormalizedVariance(int iteration) {
        double[] extrapolatedSums = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.EXTRAPOLATED_SUM_ORDINAL];

        double sum = EfficientComputations.sumNonNaN(extrapolatedSums);
        double variance = EfficientComputations.varianceNonNaN(extrapolatedSums);
        double scalingFactor = 1;
        if (sum != 0) {
            scalingFactor = sum * sum;
        }

        pointEstimate = variance / scalingFactor;

        double error = computeDeltaMethodConfidenceIntervalForVariance(false);
        double scaledError = error / scalingFactor;

        lowerBoundEstimate = pointEstimate - scaledError;
        upperBoundEstimate = pointEstimate + scaledError;

        lastIteration = iteration;
    }

    @Override
    protected void updateEstimatesForMeanNormalizedVariance(int iteration) {
        double[] extrapolatedSums = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.EXTRAPOLATED_SUM_ORDINAL];

        double mean = EfficientComputations.meanNonNaN(extrapolatedSums);
        double variance = EfficientComputations.varianceNonNaN(extrapolatedSums);
        double scalingFactor = 1;
        if (mean != 0) {
            scalingFactor = mean * mean;
        }

        pointEstimate = variance / scalingFactor;

        double error = computeDeltaMethodConfidenceIntervalForVariance(false);
        double scaledError = error / scalingFactor;

        lowerBoundEstimate = pointEstimate - scaledError;
        upperBoundEstimate = pointEstimate + scaledError;

        lastIteration = iteration;
    }

    @Override
    protected void updateEstimatesForSkewness(int iteration) {
        double[] extrapolatedSums = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.EXTRAPOLATED_SUM_ORDINAL];

        // TODO: add a method to compute skewness for non-NaN values
        pointEstimate = EfficientComputations.skewness(extrapolatedSums);

        computeDeltaMethodConfidenceIntervalForSkewness(false);

        lastIteration = iteration;
    }

    @Override
    protected void updateEstimatesForKurtosis(int iteration) {
        double[] extrapolatedSums = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.EXTRAPOLATED_SUM_ORDINAL];

        // TODO: add a method to compute kurtosis for non-NaN values
        pointEstimate = EfficientComputations.kurtosis(extrapolatedSums);

        computeDeltaMethodConfidenceIntervalForKurtosis(false);

        lastIteration = iteration;
    }
}
