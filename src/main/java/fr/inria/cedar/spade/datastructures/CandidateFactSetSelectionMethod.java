//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures;

public enum CandidateFactSetSelectionMethod {
    TYPED_BASED_CANDIDATE_FACT_SET_SELECTION,
    SUMMARY_BASED_CANDIDATE_FACT_SET_SELECTION,
    ATTRIBUTE_BASED_CANDIDATE_FACT_SET_SELECTION,
    SAMPLE_BASED_CANDIDATE_FACT_SET_SELECTION;

    public static CandidateFactSetSelectionMethod convert(String enumString) {
        switch (enumString) {
            case "TYPED_BASED_CANDIDATE_FACT_SET_SELECTION":
                return TYPED_BASED_CANDIDATE_FACT_SET_SELECTION;
            case "SUMMARY_BASED_CANDIDATE_FACT_SET_SELECTION":
                return SUMMARY_BASED_CANDIDATE_FACT_SET_SELECTION;
            case "ATTRIBUTE_BASED_CANDIDATE_FACT_SET_SELECTION":
                return ATTRIBUTE_BASED_CANDIDATE_FACT_SET_SELECTION;
            case "SAMPLE_BASED_CANDIDATE_FACT_SET_SELECTION":
                return SAMPLE_BASED_CANDIDATE_FACT_SET_SELECTION;
            default:
                throw new IllegalStateException("No value corresponding to " + enumString);
        }
    }
}
