//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Pair<KeyType, ValueType> {
    private static final Logger LOGGER = Logger.getLogger(Pair.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected KeyType key;
    protected ValueType value;

    public Pair(KeyType key, ValueType value) {
        this.key = key;
        this.value = value;
    }

    public void setKey(KeyType key) {
        this.key = key;
    }

    public KeyType getKey() {
        return key;
    }

    public void setValue(ValueType value) {
        this.value = value;
    }

    public ValueType getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "(" + key + ", " + value + ")";
    }

    @Override
    public int hashCode() {
        return key.hashCode() * 13 + (value == null ? 0 : value.hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof Pair) {
            Pair pair = (Pair) obj;
            if (key != null ? !key.equals(pair.key) : pair.key != null) {
                return false;
            }
            return !(value != null ? !value.equals(pair.value) : pair.value != null);
        }

        return false;
    }
}
