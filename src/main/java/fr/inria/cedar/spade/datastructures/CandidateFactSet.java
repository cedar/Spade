//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import fr.inria.cedar.spade.database.DatabaseHandler;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class CandidateFactSet {
    private static final Logger LOGGER = Logger.getLogger(CandidateFactSet.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected DatabaseHandler handler;
    protected String definingSQLQuery; // the SQL query that originally tells us how to get these facts from the database
    protected String tableName; // stores the name of the table (from the original storage or subsequently created) where the facts are
    protected String encoding; // encoding of the candidate fact set (id)
    protected int size;

    public CandidateFactSet(DatabaseHandler handler, String query,
                            String tableName) {
        this.handler = handler;
        this.definingSQLQuery = query;
        StringBuilder sqlQuery = new StringBuilder();
        if (tableName != null) {
            this.tableName = tableName;
            encoding = tableName.substring(2);
        }
        else {
            StringBuilder candidateFactSetName = new StringBuilder("generated_cfs_");
            candidateFactSetName.append(Integer.toString(definingSQLQuery.hashCode()));
            try {
                sqlQuery.append("insert into dictionary(value,cleanvalue) values ($dagger$").append(candidateFactSetName).append("$dagger$,'").append(candidateFactSetName).append("') on conflict do nothing");
                handler.executeUpdate(sqlQuery.toString());

                sqlQuery.setLength(0);
                sqlQuery.append("select key from dictionary where value = $dagger$").append(candidateFactSetName).append("$dagger$");
                try (ResultSet results = handler.getResultSet(sqlQuery.toString())) {
                    results.next();
                    encoding = Integer.toString(results.getInt("key"));
                }

                sqlQuery.setLength(0);
                this.tableName = "t_" + encoding;
                sqlQuery.append("create table if not exists ").append(this.tableName).append("(s) as (").append(definingSQLQuery).append(")");
                handler.executeUpdate(sqlQuery.toString());

                // add an index on s column
                sqlQuery.setLength(0);
                sqlQuery.append("create unique index if not exists ").append(this.tableName).append("_i_s on ").append(this.tableName).append(" using btree(s) tablespace pg_default");
                handler.executeUpdate(sqlQuery.toString());
            }
            catch (SQLException ex) {
                throw new IllegalStateException(ex + "; query: " + sqlQuery);
            }
        }

        // add subject_id to number the subjects in the candidate fact set
        try {
            sqlQuery.setLength(0);
            sqlQuery.append("alter table ").append(this.tableName).append(" add subject_id serial");
            handler.executeUpdate(sqlQuery.toString());
        }
        catch (SQLException ex) {
            try {
                if (!GlobalSettings.DATABASE_AUTOCOMMIT) {
                    handler.rollback();
                }
                // it's ok, Postgres dosn't support IF NOT EXIST for SERIAL
                // reference: https://stackoverflow.com/questions/50553558/postgresql-add-serial-column-if-not-exists-still-creating-sequences

                // Before, we were ignoring the error silently due to problems with the language of the error message at the cluster (French)
                /*if (!ex.getMessage().contains("already exists")) {
				throw new IllegalStateException(ex + "; query: " + sqlQuery);
				}*/
            }
            catch (SQLException ex1) {
                throw new IllegalStateException(ex + "\n" + ex1);
            }
        }

        // instead of asking one of the following queries:
        // sqlQuery = "select count(*) from " + tableName;
        // sqlQuery = "select max(subject_id) from " + tableName;
        // we can check the last value in the sequence
        sqlQuery.setLength(0);
        sqlQuery.append("select currval(pg_get_serial_sequence('").append(tableName).append("', 'subject_id'))");
        try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
            while (rs.next()) {
                size = rs.getInt(1);
            }
        }
        catch (SQLException | IllegalStateException e) {
            // if select currvalue fails we fall back on a different query
            if (!GlobalSettings.DATABASE_AUTOCOMMIT) {
                try {
                    handler.rollback();
                }
                catch (SQLException ex1) {
                    throw new IllegalStateException("Unable to compute the size of the candidate fact set. " + ex1);
                }
            }
            sqlQuery.setLength(0);
            sqlQuery.append("select count(*) from ").append(tableName);
            try (ResultSet rs = handler.getResultSet(sqlQuery.toString())) {
                while (rs.next()) {
                    size = rs.getInt(1);
                }
            }
            catch (SQLException ex1) {
                throw new IllegalStateException("Unable to compute the size of the candidate fact set. " + ex1);
            }
        }
    }

    public String getTableName() {
        return tableName;
    }

    public String getDefiningSQLQuery() {
        return definingSQLQuery;
    }

    public String getEncoding() {
        return encoding;
    }

    public String getDecodedName() {
        String decodedCFS;
        try {
            decodedCFS = Utils.getShorterAttributeName(handler.getValue(encoding));
        }
        catch (IllegalStateException ex) {
            throw new IllegalStateException("Decoding of CFS failed " + ex);
        }

        return decodedCFS;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "CFS in table: " + tableName + ", defined by query: " + definingSQLQuery;
    }
}
