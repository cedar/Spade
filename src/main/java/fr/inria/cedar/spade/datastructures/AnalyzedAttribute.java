//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures;

import fr.inria.cedar.spade.configuration.GlobalSettings;
import java.util.LinkedList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AnalyzedAttribute {
    private static final Logger LOGGER = Logger.getLogger(AnalyzedAttribute.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    // For a given triple (subject, attribute, object) in the input RDF graph
    // an attribute is either a property or a derived property,
    // the object is a value of an attribute for a given subject (fact).

    protected long encoding; // the attribute encoding
    protected LinkedList<AggregationFunction> aggregationFunctions;
    protected LinkedList<SuggestedDerivation> suggestedDerivations; // having analyzed the attribute, we know if we want to do derivations

    // The value of a statistic for an attribute can be impacted by constraing
    // its set of subjects, e.g., by choosing the attribute values within
    // a scope of a candidate fact set.

    // Statistics independent from Candidate Fact Set choice
    protected Integer derivationLevel; // the level of derivation: 0 for original properties
    protected AnalyzedAttribute derivedFrom; // the attribute from which this attribute was derived
    protected AnalyzedAttribute derivedFromRoot; // the root attribute in the hierarchy of derivations
    protected SuggestedDerivation derivationType; // the type of derivation
    protected Integer pathLength; // the length of the path if it is a path derived attribute

    // Statistics somewhat dependent on Candidate Fact Set choice
    protected Boolean isNumerical; // whether this attribute is numerical
    protected Boolean hasOnlyLiteralStringValues; // whether this attribute has only string values that are not IRIs nor blank nodes
    protected Double minValue; // the minimum value of the attribute
    protected Double maxValue; // the maximum value of the attribute
    protected Double meanValueLength; // the mean length of the attribute values

    // Statistics fully dependent on Candidate Fact Set choice
    protected Integer numberOfDistinctSubjects; // the number of distinct subjects; needed for support and almost-oneness, at-most-oneness
    protected Integer numberOfDistinctObjects; // the number of distinct objects; needed for the ratio, and on its own
    protected Integer numberOfSubjects; // the number of subjects; needed for the ratio
    //protected Long numberOfObjects; // the number of objects
    protected Double distinctObjectsToSubjectsRatio; // the number of distinct objects to the number of subjects ratio
    protected Double support; // the attribute's support: the number of distinct subjects within a candidate fact set that have a value for this attribute
    protected Double almostOneRatio; // the ratio of the number of distinct subjects that have more than one distinct value to the total number of distinct subjects

    public AnalyzedAttribute(long encoding) {
        this.encoding = encoding;
        aggregationFunctions = new LinkedList<>();
        suggestedDerivations = new LinkedList<>();
    }

    public long getEncoding() {
        return encoding;
    }

    public void addAggregationFunction(AggregationFunction aggregationFunction) {
        aggregationFunctions.add(aggregationFunction);
    }

    public LinkedList<AggregationFunction> getAggregationFunctions() {
        return aggregationFunctions;
    }

    public void addSuggestedDerivation(SuggestedDerivation suggestedDerivation) {
        suggestedDerivations.add(suggestedDerivation);
    }

    public LinkedList<SuggestedDerivation> getSuggestedDerivations() {
        return suggestedDerivations;
    }

    public void setDerivationLevel(int derivationLevel) {
        this.derivationLevel = derivationLevel;
    }

    public Integer getDerivationLevel() {
        return derivationLevel;
    }

    public void setDerivedFrom(AnalyzedAttribute derivedFrom) {
        this.derivedFrom = derivedFrom;
    }

    public AnalyzedAttribute getDerivedFrom() {
        return derivedFrom;
    }

    public void setDerivedFromRoot(AnalyzedAttribute derivedFromRoot) {
        this.derivedFromRoot = derivedFromRoot;
    }

    public AnalyzedAttribute getDerivedFromRoot() {
        return derivedFromRoot;
    }

    public void setDerivationType(SuggestedDerivation derivationType) {
        this.derivationType = derivationType;
    }

    public SuggestedDerivation getDerivationType() {
        return derivationType;
    }

    public void setPathLength(Integer pathLength) {
        this.pathLength = pathLength;
    }

    public Integer getPathLength() {
        return pathLength;
    }

    public void setIsNumerical(Boolean isNumerical) {
        this.isNumerical = isNumerical;
    }

    public Boolean isNumerical() {
        return isNumerical;
    }

    public void setHasOnlyLiteralStringValues(Boolean hasOnlyLiteralStringValues) {
        this.hasOnlyLiteralStringValues = hasOnlyLiteralStringValues;
    }

    public Boolean hasOnlyLiteralStringValues() {
        return hasOnlyLiteralStringValues;
    }

    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    public Double getMinValue() {
        return minValue;
    }

    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

    public Double getMaxValue() {
        return maxValue;
    }

    public void setMeanValueLength(Double meanValueLength) {
        this.meanValueLength = meanValueLength;
    }

    public Double getMeanValueLength() {
        return meanValueLength;
    }

    public void setNumberOfDistinctSubjects(Integer numberOfDistinctSubjects) {
        this.numberOfDistinctSubjects = numberOfDistinctSubjects;
    }

    public Integer getNumberOfDistinctSubjects() {
        return numberOfDistinctSubjects;
    }

    public void setNumberOfDistinctObjects(Integer numberOfDistinctObjects) {
        this.numberOfDistinctObjects = numberOfDistinctObjects;
    }

    public Integer getNumberOfDistinctObjects() {
        return numberOfDistinctObjects;
    }

    public void setNumberOfSubjects(Integer numberOfSubjects) {
        this.numberOfSubjects = numberOfSubjects;
    }

    public Integer getNumberOfSubjects() {
        return numberOfSubjects;
    }

    public void setDistinctObjectsToSubjectsRatio(
            Double distinctObjectsToSubjectsRatio) {
        this.distinctObjectsToSubjectsRatio = distinctObjectsToSubjectsRatio;
    }

    public Double getDistinctObjectsToSubjectsRatio() {
        return distinctObjectsToSubjectsRatio;
    }

    public void setSupport(Double support) {
        this.support = support;
    }

    public Double getSupport() {
        return support;
    }

    public void setAlmostOneRatio(Double almostOneRatio) {
        this.almostOneRatio = almostOneRatio;
    }

    public Double getAlmostOneRatio() {
        return almostOneRatio;
    }

    // whether this is an almost-one attribute: an attribute such that ALMOST_ONE_RATIO is below ALMOST_ONE_THRESHOLD
    public boolean isAlmostOne() {
        return almostOneRatio <= GlobalSettings.ALMOST_ONE_THRESHOLD;
    }

    // whether this is an at-most-one attribute: an attribute such that every subject has at most one distinct value"
    public boolean isAtMostOne() {
        return almostOneRatio == 0;
    }

    @Override
    public String toString() {
        StringBuilder strb = new StringBuilder("Attribute ");
        strb.append(encoding).append(" statistics:");
        if (derivationLevel > 0) {
            strb.append("\n\tderivation level: ").append(derivationLevel)
                .append("; derived from: ").append(derivedFrom.getEncoding())
                .append("; derived from (root): ").append(derivedFromRoot.getEncoding())
                .append("; derivation type: ").append(derivationType);
            if (derivationType == SuggestedDerivation.PATH) {
                strb.append("; path length: ").append(pathLength);
            }
        }
        else {
            strb.append("\n\tderivation level: ").append(derivationLevel).append(" (original property)");
        }
        strb.append("\n\tis numerical: ").append(isNumerical)
            .append("; has only literal string values: ").append(hasOnlyLiteralStringValues)
            .append("\n\tmin value: ").append(minValue)
            .append("; max value: ").append(maxValue)
            .append("; mean value length: ").append(meanValueLength)
            .append("\n\tnumber of distinct subjects: ").append(numberOfDistinctSubjects)
            .append("; number of distinct objects: ").append(numberOfDistinctObjects)
            .append("; number of subjects: ").append(numberOfSubjects)
            .append("; distinct objects to subjects ratio: ").append(distinctObjectsToSubjectsRatio)
            .append("\n\tsupport: ").append(support)
            .append("; almost-one ratio: ").append(almostOneRatio);
        if (almostOneRatio != null) {
            strb.append("; is almost-one: ").append(isAlmostOne())
                .append("; is at-most-one: ").append(isAtMostOne());
        }

        return strb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (!(that instanceof AnalyzedAttribute)) {
            return false;
        }
        AnalyzedAttribute thatCube = (AnalyzedAttribute) that;
        return this.encoding == thatCube.encoding;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 31 * hash + Long.valueOf(encoding).hashCode();
        return hash;
    }
}
