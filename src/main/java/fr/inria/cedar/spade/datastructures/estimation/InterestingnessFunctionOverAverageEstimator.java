//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures.estimation;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.molapForRDF.MOLAPMeasures;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class InterestingnessFunctionOverAverageEstimator extends DeltaMethodEstimator {
    private static final Logger LOGGER = Logger.getLogger(InterestingnessFunctionOverAverageEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected InterestingnessFunctionOverSumEstimator sumEstimator;

    public InterestingnessFunctionOverAverageEstimator(
            DatabaseHandler handler,
            MultidimensionalAggregateEstimate aggregateEstimate,
            AnalyzedAttribute measure,
            AggregationFunction aggregationFunction,
            InterestingnessFunction interestingnessFunction,
            double confidenceLevel,
            double[][] retrievedPreaggregatedValuesByMeasure,
            int numberOfGroups,
            int[] sizeOfGroupInSample) {
        super(handler, aggregateEstimate, measure, aggregationFunction, interestingnessFunction, confidenceLevel, retrievedPreaggregatedValuesByMeasure, numberOfGroups, sizeOfGroupInSample);
    }

    @Override
    protected void updateEstimatesForFeatureScalingNormalizedVariance(
            int iteration) {
        double[] averages = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.AVERAGE_ORDINAL];

        /*fr.inria.cedar.DataProfiler.utils.Pair minMax = minAndMaxSimultaneously(averages);
        if (minMax == null) {
            throw new IllegalStateException("Couldn't find min and max in averages array");
        }
        double min = minMax.getMin();
        double max = minMax.getMax();*/
        double min = EfficientComputations.minNonNaN(averages);
        double max = EfficientComputations.maxNonNaN(averages);

        double[] normalizedValues = EfficientComputations.normalizeByFeatureScaling(averages, min, max);
        pointEstimate = EfficientComputations.varianceNonNaN(normalizedValues);
        double scalingFactor = max - min;

        double error = computeDeltaMethodConfidenceIntervalForVariance(true);
        double scaledError = error / scalingFactor;

        lowerBoundEstimate = pointEstimate - scaledError;
        upperBoundEstimate = pointEstimate + scaledError;

        lastIteration = iteration;
    }

    @Override
    protected void updateEstimatesForSumNormalizedVariance(int iteration) {
        double[] averages = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.AVERAGE_ORDINAL];

        double sum = EfficientComputations.sumNonNaN(averages);
        double variance = EfficientComputations.varianceNonNaN(averages);
        double scalingFactor = 1;
        if (sum != 0) {
            scalingFactor = sum * sum;
        }

        pointEstimate = variance / scalingFactor;

        double error = computeDeltaMethodConfidenceIntervalForVariance(true);
        double scaledError = error / scalingFactor;

        lowerBoundEstimate = pointEstimate - scaledError;
        upperBoundEstimate = pointEstimate + scaledError;

        lastIteration = iteration;
    }

    @Override
    protected void updateEstimatesForMeanNormalizedVariance(int iteration) {
        double[] averages = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.AVERAGE_ORDINAL];

        double mean = EfficientComputations.meanNonNaN(averages);
        double variance = EfficientComputations.varianceNonNaN(averages);
        double scalingFactor = 1;
        if (mean != 0) {
            scalingFactor = mean * mean;
        }

        pointEstimate = variance / scalingFactor;

        double error = computeDeltaMethodConfidenceIntervalForVariance(true);
        double scaledError = error / scalingFactor;

        lowerBoundEstimate = pointEstimate - scaledError;
        upperBoundEstimate = pointEstimate + scaledError;

        lastIteration = iteration;

        //LOGGER.info("Variance: " + variance + ", error: " + error + ", scaling factor: " + scalingFactor + ", estimate: " + pointEstimate + ", [" + lowerBoundEstimate + ", " + upperBoundEstimate + "]");
    }

    @Override
    protected void updateEstimatesForSkewness(int iteration) {
        double[] averages = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.AVERAGE_ORDINAL];

        // TODO: add a method to compute skewness for non-NaN values
        pointEstimate = EfficientComputations.skewness(averages);

        computeDeltaMethodConfidenceIntervalForSkewness(true);

        lastIteration = iteration;
    }

    @Override
    protected void updateEstimatesForKurtosis(int iteration) {
        double[] averages = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.AVERAGE_ORDINAL];

        // TODO: add a method to compute kurtosis for non-NaN values
        pointEstimate = EfficientComputations.kurtosis(averages);

        computeDeltaMethodConfidenceIntervalForKurtosis(true);

        lastIteration = iteration;
    }
}
