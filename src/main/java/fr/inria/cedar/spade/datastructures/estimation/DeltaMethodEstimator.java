//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures.estimation;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.molapForRDF.MOLAPMeasures;
import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.NormalDistributionImpl;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class DeltaMethodEstimator extends Estimator {
    private static final Logger LOGGER = Logger.getLogger(DeltaMethodEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected double quantile;
    protected double constant1;
    protected double constant2;

    public DeltaMethodEstimator(
            DatabaseHandler handler,
            MultidimensionalAggregateEstimate aggregateEstimate,
            AnalyzedAttribute measure,
            AggregationFunction aggregationFunction,
            InterestingnessFunction interestingnessFunction,
            double confidenceLevel,
            double[][] retrievedPreaggregatedValuesByMeasure,
            int numberOfGroups,
            int[] sizeOfGroupInSample) {
        super(handler, aggregateEstimate, measure, aggregationFunction, interestingnessFunction, confidenceLevel, retrievedPreaggregatedValuesByMeasure, numberOfGroups, sizeOfGroupInSample);

        try {
            quantile = (new NormalDistributionImpl(0, 1)).inverseCumulativeProbability((confidenceLevel + 1.0) / 2.0);
        }
        catch (MathException ex) {
            throw new IllegalStateException(ex);
        }

        constant1 = 2.0 / (numberOfGroups - 1.0);
        constant2 = 1.0 / numberOfGroups;
    }

    protected double computeDeltaMethodConfidenceIntervalForVariance(boolean averageAsAggregatationFunction) {
        double[] averages = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.AVERAGE_ORDINAL];
        double[] variances = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.VARIANCE_ORDINAL];

        double averagesSum = EfficientComputations.sumNonNaN(averages);
        double t;
        double groupSize;
        double tau = 0;
        if (averageAsAggregatationFunction) { // the case for average estimator ~ N(mu, sigma^2 / sampleSize)
            for (int s = 0; s < numberOfGroups; s++) {
                if (Double.isNaN(averages[s])) {
                    continue;
                }
                t = constant1 * (averages[s] - constant2 * averagesSum);
                // division by sizeOfGroupInSample later (since we can factor it out)
                tau += variances[s] * t * t;
            }
        }
        else { // the case for sum estimator ~ N(sampleSize * mu, groupSize^2 / sampleSize * sigma^2)
            double[] estimatedTotalGroupSizes = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.ESTIMATED_TOTAL_GROUP_SIZE];
            for (int s = 0; s < numberOfGroups; s++) {
                if (Double.isNaN(averages[s])) {
                    continue;
                }
                t = constant1 * (averages[s] - constant2 * averagesSum);
                groupSize = estimatedTotalGroupSizes[s];
                // multiplication by estimatedTotalGroupSizes, division by sizeOfGroupInSample later (since we can factor it out)
                tau += variances[s] * t * t * groupSize * groupSize;
            }
        }

        // in the formula for tau we have division by sizeOfGroupInSample
        // we factor out this division and combine it with the division by sizeOfGroupInSample under sqrt
        return quantile * Math.sqrt(tau) / sizeOfGroupInSample[0]; // error
    }

    protected void computeDeltaMethodConfidenceIntervalForSkewness(boolean averageAsAggregatationFunction) {
        double[] averages = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.AVERAGE_ORDINAL];
        double[] variances = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.VARIANCE_ORDINAL];

        double averagesSum = EfficientComputations.sumNonNaN(averages);
        double x, y, t, groupSize;

        double fx = EfficientComputations.varianceNonNaN(averages);
        double a = Math.pow(fx, (2.0 / 3));

        double c = 0;
        for (int i = 0; i < numberOfGroups; i++) {
            if (Double.isNaN(averages[i])) {
                continue;
            }
            t = averages[i] - (1.0 / numberOfGroups) * averagesSum;
            c += t * t * t;
        }
        c *= 2.0 / (3 * numberOfGroups);
        double b = c * Math.pow(fx, (-1.0 / 3));

        double d = 0;
        for (int i = 0; i < numberOfGroups; i++) {
            if (Double.isNaN(averages[i])) {
                continue;
            }
            d += averages[i] * averages[i] - (2.0 * averages[i] / numberOfGroups) * averagesSum;
        }
        d /= numberOfGroups;

        double tau = 0;
        if (averageAsAggregatationFunction) {
            for (int s = 0; s < averages.length; s++) {
                x = (3.0 / numberOfGroups) * (averages[s] * averages[s] - d + 2.0 * averages[s] * averagesSum);
                y = (2.0 / (numberOfGroups - 1.0)) * (averages[s] - (1.0 / numberOfGroups) * averagesSum);
                t = a * x + b * y;
                tau += variances[s] * t * t;
            }
        }
        else { // the case for sum
            double[] estimatedTotalGroupSizes = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.ESTIMATED_TOTAL_GROUP_SIZE];
            for (int s = 0; s < averages.length; s++) {
                x = (3.0 / numberOfGroups) * (averages[s] * averages[s] - d + 2.0 * averages[s] * averagesSum);
                y = (2.0 / (numberOfGroups - 1.0)) * (averages[s] - (1.0 / numberOfGroups) * averagesSum);
                t = a * x + b * y;
                groupSize = estimatedTotalGroupSizes[s];
                tau += variances[s] * t * t * groupSize * groupSize;
            }
        }

        double error = quantile * Math.sqrt(tau) / sizeOfGroupInSample[0];
        lowerBoundEstimate = pointEstimate - error;
        upperBoundEstimate = pointEstimate + error;
    }

    protected void computeDeltaMethodConfidenceIntervalForKurtosis(boolean averageAsAggregatationFunction) {
        double[] averages = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.AVERAGE_ORDINAL];
        double[] variances = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.VARIANCE_ORDINAL];

        double averagesSum = EfficientComputations.sumNonNaN(averages);
        double x, y, t, groupSize;
        double fx = EfficientComputations.varianceNonNaN(averages);
        double a = Math.pow(((numberOfGroups - 1.0) / numberOfGroups) * fx, -2.0);

        double c = 0;
        for (int i = 0; i < numberOfGroups; i++) {
            if (Double.isNaN(averages[i])) {
                continue;
            }
            t = averages[i] - (1.0 / numberOfGroups) * averagesSum;
            c += t * t * t * t;
        }
        c *= (2.0 * (numberOfGroups - 1.0) * (numberOfGroups - 1.0)) / (numberOfGroups * numberOfGroups * numberOfGroups * fx * fx * fx);
        double b = c * Math.pow(fx, (-1.0 / 3.0));

        double d = 0;
        for (int i = 0; i < numberOfGroups; i++) {
            if (Double.isNaN(averages[i])) {
                continue;
            }
            d += averages[i] * averages[i] * averages[i] - ((3.0 * averages[i] * averages[i]) / numberOfGroups) + (3.0 * averages[i] / (numberOfGroups * numberOfGroups)) * averagesSum * averagesSum;
        }
        d /= numberOfGroups;

        double tau = 0;
        if (averageAsAggregatationFunction) {
            for (int s = 0; s < averages.length; s++) {
                x = (4.0 / numberOfGroups) * (averages[s] * averages[s] * averages[s] - d + 3.0 * averages[s] * averages[s] * averagesSum + ((3.0 * averages[s]) / numberOfGroups) * averagesSum * averagesSum);
                y = (2.0 / (numberOfGroups - 1.0)) * (averages[s] - (1.0 / numberOfGroups) * averagesSum);
                t = a * x + b * y;
                tau += variances[s] * t * t;
            }
        }
        else { // the case for sum
            double[] estimatedTotalGroupSizes = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.ESTIMATED_TOTAL_GROUP_SIZE];
            for (int s = 0; s < averages.length; s++) {
                x = (4.0 / numberOfGroups) * (averages[s] * averages[s] * averages[s] - d + 3.0 * averages[s] * averages[s] * averagesSum + ((3.0 * averages[s]) / numberOfGroups) * averagesSum * averagesSum);
                y = (2.0 / (numberOfGroups - 1.0)) * (averages[s] - (1.0 / numberOfGroups) * averagesSum);
                t = a * x + b * y;
                groupSize = estimatedTotalGroupSizes[s];
                tau += variances[s] * t * t * groupSize * groupSize;
            }
        }

        double error = quantile * Math.sqrt(tau) / sizeOfGroupInSample[0];
        lowerBoundEstimate = pointEstimate - error;
        upperBoundEstimate = pointEstimate + error;
    }
}
