//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures;

public enum AggregationFunction {
    COUNT_STAR("COUNT(*)"),
    COUNT("COUNT(Measure)"),
    COUNT_DISTINCT("COUNT(DISTINCT_MEASURE)"),
    SUM("SUM(Measure)"),
    AVERAGE("AVG(Measure)"),
    MIN("MIN(Measure)"),
    MAX("MAX(Measure)"),

    // only for early-stop
    RAW_VALUES_LIST("RAW_VALUES(Measure)"),
    VARIANCE("VARIANCE(Measure)"),
    TOTAL_GROUP_SIZE("TOTAL_GROUP_SIZE(Measure)");

    private final String text;

    AggregationFunction(String text, Object... args) {
        this.text = format(text, args);
    }

    public static AggregationFunction getAggregate(String text) {
        switch (text) {
            case "count_star":
                return COUNT_STAR;
            case "count":
                return COUNT;
            case "count_distinct":
                return COUNT_DISTINCT;
            case "sum":
                return SUM;
            case "average":
                return AVERAGE;
            case "min":
                return MIN;
            case "max":
                return MAX;
            case "raw_values_list":
                return RAW_VALUES_LIST;
            case "variance":
                return VARIANCE;
            case "total_group_size":
                return TOTAL_GROUP_SIZE;
            default:
                return null;
        }
    }

    private static String format(String text, Object... args) {
        return String.format(text, args);
    }

    @Override
    public String toString() {
        return text;
    }
}
