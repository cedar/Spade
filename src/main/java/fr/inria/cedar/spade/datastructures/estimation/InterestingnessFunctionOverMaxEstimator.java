//Initial software, [Diao, Guzewicz, Manolescu-Goujot, Mazuran], Copyright C Inria and Ecole polytechnique, see the license available at https://gitlab.inria.fr/cedar/Spade/blob/master/LICENSE.txt

package fr.inria.cedar.spade.datastructures.estimation;

import fr.inria.cedar.DataProfiler.utils.EfficientComputations;
import fr.inria.cedar.spade.database.DatabaseHandler;
import fr.inria.cedar.spade.datastructures.AggregationFunction;
import fr.inria.cedar.spade.datastructures.AnalyzedAttribute;
import fr.inria.cedar.spade.datastructures.InterestingnessFunction;
import fr.inria.cedar.spade.molapForRDF.MOLAPMeasures;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class InterestingnessFunctionOverMaxEstimator extends Estimator {
    private static final Logger LOGGER = Logger.getLogger(InterestingnessFunctionOverMaxEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.INFO);
    }

    protected double globalMax;

    public InterestingnessFunctionOverMaxEstimator(
            DatabaseHandler handler,
            MultidimensionalAggregateEstimate aggregateEstimate,
            AnalyzedAttribute measure,
            AggregationFunction aggregationFunction,
            InterestingnessFunction interestingnessFunction,
            double confidenceLevel,
            double[][] retrievedPreaggregatedValuesByMeasure,
            int numberOfGroups,
            int[] sizeOfGroupInSample) {
        super(handler, aggregateEstimate, measure, aggregationFunction, interestingnessFunction, confidenceLevel, retrievedPreaggregatedValuesByMeasure, numberOfGroups, sizeOfGroupInSample);
        globalMax = measure.getMaxValue();
    }

    protected void computeIntervalForVariance(double[] values,
                                              double globalMaxNormalized) {
        double min = EfficientComputations.minNonNaN(values);
        double diff = globalMaxNormalized - min;
        double diffSquared = diff * diff;

        // Popoviciu's inequality on variances
        upperBoundEstimate = diffSquared / 4.0;

        // von Szokefalvi Nagy's inequality
        lowerBoundEstimate = diffSquared / (2.0 * numberOfGroups);
    }

    @Override
    protected void updateEstimatesForFeatureScalingNormalizedVariance(
            int iteration) {
        double[] maxs = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.MAX_ORDINAL];

        double[] normalizedValues = EfficientComputations.normalizeByFeatureScalingNonNaN(maxs);
        pointEstimate = EfficientComputations.varianceNonNaN(normalizedValues);

        double globalMaxNormalized = 1;
        computeIntervalForVariance(normalizedValues, globalMaxNormalized);

        lastIteration = iteration;
    }

    @Override
    protected void updateEstimatesForSumNormalizedVariance(int iteration) {
        double[] maxs = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.MAX_ORDINAL];
        double[] normalizedValues;
        double sum = EfficientComputations.sumNonNaN(maxs);
        double scalingFactor = 1;
        if (sum != 0) {
            scalingFactor = sum;
            normalizedValues = EfficientComputations.normalizeByDivisionBySumWithKnownSum(maxs, sum);
        }
        else {
            normalizedValues = maxs;
        }

        pointEstimate = EfficientComputations.varianceNonNaN(normalizedValues);

        double globalMaxNormalized = globalMax / scalingFactor;
        computeIntervalForVariance(normalizedValues, globalMaxNormalized);

        lastIteration = iteration;
    }

    @Override
    protected void updateEstimatesForMeanNormalizedVariance(int iteration) {
        double[] maxs = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.MAX_ORDINAL];
        double[] normalizedValues;
        double mean = EfficientComputations.meanNonNaN(maxs);
        double scalingFactor = 1;
        if (mean != 0) {
            scalingFactor = mean;
            normalizedValues = EfficientComputations.normalizeByDivisionByMeanWithKnownMean(maxs, scalingFactor);
        }
        else {
            normalizedValues = maxs;
        }

        pointEstimate = EfficientComputations.varianceNonNaN(normalizedValues);

        double globalMaxNormalized = globalMax / scalingFactor;
        computeIntervalForVariance(normalizedValues, globalMaxNormalized);

        lastIteration = iteration;
    }

    @Override
    protected void updateEstimatesForSkewness(int iteration) {
        double[] maxs = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.MAX_ORDINAL];

        // TODO: add a method to compute skewness for non-NaN values
        pointEstimate = EfficientComputations.skewness(maxs);

        lastIteration = iteration;

        // TODO: compute interval
        throw new IllegalStateException("Not implemented yet");
    }

    @Override
    protected void updateEstimatesForKurtosis(int iteration) {
        double[] maxs = retrievedPreaggregatedValuesByMeasure[MOLAPMeasures.MAX_ORDINAL];

        // TODO: add a method to compute kurtosis for non-NaN values
        pointEstimate = EfficientComputations.kurtosis(maxs);

        lastIteration = iteration;

        // TODO: compute interval
        throw new IllegalStateException("Not implemented yet");
    }
}
