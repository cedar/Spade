FROM maven:3.8.1-jdk-8-openj9

RUN apt-get update && \
    apt-get install -y -qq \
      git graphviz nano curl \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /code

RUN git clone https://gitlab.inria.fr/cedar/Spade.git

RUN curl http://files.inria.fr/cedar/RDFQuotient/RDFQuotient-2.0-with-dependencies.jar \
        --output rdfquotient.jar \
    && mvn -q install:install-file -Dfile=/code/rdfquotient.jar \
         -DgroupId=fr.inria.cedar \
         -DartifactId=RDFQuotient \
         -Dversion=2.0 \
         -Dpackaging=jar

RUN curl http://files.inria.fr/cedar/Spade/DataProfiler-1.1-SNAPSHOT-with-dependencies.jar \
        --output dataprofiler.jar \
    && mvn -q install:install-file -Dfile=/code/dataprofiler.jar \
         -DgroupId=fr.inria.cedar \
         -DartifactId=DataProfiler \
         -Dversion=1.1-SNAPSHOT \
         -Dpackaging=jar

RUN curl http://files.inria.fr/cedar/Spade/spmf.jar \
        --output spmf.jar  \
    && mvn -q install:install-file -Dfile=/code/spmf.jar \
         -DgroupId=ca.pfv.spmf \
         -DartifactId=spmf \
         -Dversion=1.0 \
         -Dpackaging=jar

RUN curl http://files.inria.fr/cedar/Spade/je-7.5.11.jar \
        --output je.jar  \
    && mvn -q install:install-file -Dfile=/code/je.jar \
         -DgroupId=com.sleepycat.je  \
         -DartifactId=je \
         -Dversion=7.5.11 \
         -Dpackaging=jar

WORKDIR /code/Spade

RUN mvn clean install -DskipTests -q -B -ntp

ENTRYPOINT [ "java", "-XX:+UseConcMarkSweepGC", "-Xmx90g", "-Djava.util.secureRandomSeed=true", "-jar", "./target/Spade-1.0-with-dependencies.jar" ]
CMD [ "--help" ]
