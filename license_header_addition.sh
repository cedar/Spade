#!/bin/bash

LICENSE=$(cat license_header.txt)
find "$PWD" -name "*.java" -exec sed -i.bak -e '1i\'$'\n'\\ '{}' \; -exec sed -i.bak -e '1i\'$'\n'"$LICENSE" '{}' \;
