#!/bin/bash

SPADE_REPOSITORY_MAIN_DIRECTORY='/home/cedar/pguzewic/Spade/'
LOADING_DIRECTORY='/local/pguzewic/Spade_experiments/loading/'
DATABASE_PORT=5433
SUMMARY_TYPE=typedstrong

./scripts/load_real_datasets.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT $SUMMARY_TYPE

./scripts/load_synthetic_datasets.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT $SUMMARY_TYPE
