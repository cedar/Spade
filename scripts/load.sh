#!/bin/bash

VERSION="1.0"
LOG_FILE=$1

java -Xmx90g -jar target/Spade-$VERSION-with-dependencies.jar offline 2>&1 | tee -a "$LOG_FILE"
