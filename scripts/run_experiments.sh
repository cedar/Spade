#!/bin/bash

SPADE_REPOSITORY_MAIN_DIRECTORY='/home/cedar/pguzewic/Spade/'
OUTPUT_DIRECTORY='/local/pguzewic/Spade_experiments/output/'
DATABASE_PORT=5433

# old
#./scripts/run_airlinedelays_experiments.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATABASE_PORT
#./scripts/run_nasa_experiments.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATABASE_PORT
#./scripts/run_foodista_experiments.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATABASE_PORT
#./scripts/run_nobelprize_org_data_uniq_experiments.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATABASE_PORT
#./scripts/run_dblp_experiments.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATABASE_PORT

./scripts/run_real_datasets_experiments.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATABASE_PORT

./scripts/run_synthetic_datasets_experiments.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATABASE_PORT
