-- generic: PATH/pgbench OPTIONS -d testing --file=Spade_queries_test.sql --no-vacuum --client=1 --transactions=1 --report-latencies
-- local machine: /Library/PostgreSQL/12/bin/pgbench -h 127.0.0.1 -p 5433 -U postgres -d testing --file=Spade_queries_test.sql --no-vacuum --client=1 --transactions=1 --report-latencies
-- LOG_FILE=psql_benchmarking.log
-- cedar002: /usr/pgsql-12/bin/pgbench -h 127.0.0.1 -p 5433 -U postgres -d testing --file=Spade_queries_test.sql --no-vacuum --client=1 --transactions=1 --report-latencies 2>&1 | tee -a $LOG_FILE
-- cedar005: /usr/pgsql-9.6/bin/pgbench -h 127.0.0.1 -p 5432 -U postgres -d testing --file=Spade_queries_test.sql --no-vacuum --client=1 --transactions=1 --report-latencies 2>&1 | tee -a $LOG_FILE

-- drop indexes
drop index if exists dictionary_i_key_value;
drop index if exists dictionary_i_value_key;
drop index if exists encoded_triples_i_s_p_o;
drop index if exists encoded_triples_i_s_o_p;
drop index if exists encoded_triples_i_p_s_o;
drop index if exists encoded_triples_i_p_o_s;
drop index if exists encoded_triples_i_o_s_p;
drop index if exists encoded_triples_i_o_p_s;
drop index if exists d1_i_s_o;
drop index if exists d1_i_o_s;
drop index if exists d2_i_s_o;
drop index if exists d2_i_o_s;
drop index if exists d3_i_s_o;
drop index if exists d3_i_o_s;
drop index if exists m_i_s_o;
drop index if exists m_i_o_s;
drop index if exists fs_i_s;

-- drop tables
drop table if exists triples;
drop table if exists dictionary;
drop table if exists encoded_triples;
drop table if exists d1;
drop table if exists d2;
drop table if exists d3;
drop table if exists m;
drop table if exists fs;

-- create dataset tables
create table triples(s text, p text, o text, d text);

-- load the dataset into a table
-- local machine: copy triples from '/Users/pawello/PhD studies/Work/Spade/datasets/micro_benchmark_1M_5dim_4mea.nt' delimiter ' ' csv;
-- cedar002, cedar005:
copy triples from '/data/datasets/Spade_benchmark/micro_benchmark_10M_5dim_4mea.nt' delimiter ' ' csv;

-- drop the column of dots
alter table triples drop column d;

-- analyze
analyze triples;

-- selection
select count(*) from triples where o = '<75>';

-- encode triples
create table dictionary as
select row_number() over (order by value asc) as key, value
from (
	select distinct value
	from (
		select s as value from triples
		union all
		select p as value from triples
		union all
		select o as value from triples
	) as q1
) as q2;

-- index the dictionary
create unique index dictionary_i_key_value on dictionary using btree(key, md5(value)) tablespace pg_default;
create unique index dictionary_i_value_key on dictionary using btree(md5(value), key) tablespace pg_default;

-- analyze
analyze dictionary;

-- create encoded triples table
create table encoded_triples as
select dict1.key as s, dict2.key as p, dict3.key as o
from triples t
	join dictionary dict1 on t.s = dict1.value
	join dictionary dict2 on t.p = dict2.value
	join dictionary dict3 on t.o = dict3.value;

-- index encoded triples table
create unique index encoded_triples_i_s_p_o on encoded_triples using btree(s, p, o) tablespace pg_default;
create unique index encoded_triples_i_s_o_p on encoded_triples using btree(s, o, p) tablespace pg_default;
create unique index encoded_triples_i_p_s_o on encoded_triples using btree(p, s, o) tablespace pg_default;
create unique index encoded_triples_i_p_o_s on encoded_triples using btree(p, o, s) tablespace pg_default;
create unique index encoded_triples_i_o_s_p on encoded_triples using btree(o, s, p) tablespace pg_default;
create unique index encoded_triples_i_o_p_s on encoded_triples using btree(o, p, s) tablespace pg_default;

-- analyze
analyze encoded_triples;

-- selection
select count(*) from encoded_triples where o = (select key from dictionary where value = '<75>');

-- create dimension tables
create table d1 as select s, o from encoded_triples where p = (select key from dictionary where value = '<http://bm.org/d_1>');
create table d2 as select s, o from encoded_triples where p = (select key from dictionary where value = '<http://bm.org/d_2>');
create table d3 as select s, o from encoded_triples where p = (select key from dictionary where value = '<http://bm.org/d_3>');

-- create a measure table
create table m as select s, o from encoded_triples where p = (select key from dictionary where value = '<http://bm.org/m_1>');

-- create a fact set table
create table fs as
select s
from encoded_triples
where p = (select key from dictionary where value = '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>')
and o = (select key from dictionary where value = '<http://bm.org/fs_1>');

-- index the dimensions, the measure and the fact set
create unique index d1_i_s_o on d1 using btree(s, o) tablespace pg_default;
create unique index d1_i_o_s on d1 using btree(o, s) tablespace pg_default;

create unique index d2_i_s_o on d2 using btree(s, o) tablespace pg_default;
create unique index d2_i_o_s on d2 using btree(o, s) tablespace pg_default;

create unique index d3_i_s_o on d3 using btree(s, o) tablespace pg_default;
create unique index d3_i_o_s on d3 using btree(o, s) tablespace pg_default;

create unique index m_i_s_o on m using btree(s, o) tablespace pg_default;
create unique index m_i_o_s on m using btree(o, s) tablespace pg_default;

create unique index fs_i_s on fs using btree(s) tablespace pg_default;

-- analyze all the tables
analyze;

-- find the number of distict values in dimension d1
select count(distinct o) from d1;

-- join query 2 dimensions
select d1.s as fact, d1.o as d1, d2.o as d2
from d1 join d2 on d1.s = d2.s;

-- join query 3 dimensions
select d1.s as fact, d1.o as d1, d2.o as d2, d3.o as d3
from d1
	full outer join d2 on d1.s = d2.s
	full outer join d3 on d1.s = d3.s;

-- join query 3 dimensions + fact set
select fs.s as fact, d1.o as d1, d2.o as d2, d3.o as d3
from fs
	left join d1 on fs.s = d1.s
	left join d2 on fs.s = d2.s
	left join d3 on fs.s = d3.s;

-- join query 3 dimensions + fact set + join with dictionary
select fs.s as fact, dict1.value as d1, dict2.value as d2, dict3.value as d3
from fs
	left join d1 on fs.s = d1.s
	left join d2 on fs.s = d2.s
	left join d3 on fs.s = d3.s
	join dictionary dict1 on d1.o = dict1.key
	join dictionary dict2 on d2.o = dict2.key
	join dictionary dict3 on d3.o = dict3.key;

-- join query 1 measure, pre-aggregation
select s as fact, count(o), sum(cast(dict.value as numeric)), min(cast(dict.value as numeric)), max(cast(dict.value as numeric))
from m join dictionary dict on m.o = dict.key
group by s;
