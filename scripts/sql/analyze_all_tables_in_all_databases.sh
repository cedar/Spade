#!/bin/bash

for table in $(psql12 -U postgres -c "\l" | grep micro | cut -d' ' -f2); do
	psql12 -U postgres -d ${table} -c "analyze verbose"
done
