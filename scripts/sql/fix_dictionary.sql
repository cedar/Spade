drop index dictionary_i_value;

alter table dictionary add column cleanvalue text;

--alter table dictionary alter column cleanvalue not null;

update dictionary set cleanvalue = value;

update dictionary set value = case when cleanvalue not like '<%' then concat('"', cleanvalue, '"') else cleanvalue end;

create unique index dictionary_i_value on dictionary using btree(md5(value), key) tablespace pg_default;

create unique index dictionary_i_cleanvalue on dictionary using btree(md5(cleanvalue), key) tablespace pg_default;

analyze dictionary;



drop index dictionary_i_value; alter table dictionary add column cleanvalue text; update dictionary set cleanvalue = value; update dictionary set value = case when cleanvalue not like '<%' then concat('"', cleanvalue, '"') else cleanvalue end; create unique index dictionary_i_value on dictionary using btree(md5(value), key) tablespace pg_default; create unique index dictionary_i_cleanvalue on dictionary using btree(md5(cleanvalue), key) tablespace pg_default; analyze dictionary;
