#!/bin/bash

SPADE_REPOSITORY_MAIN_DIRECTORY="$1"
LOADING_DIRECTORY="$2"
DATABASE_PORT="$3"
SUMMARY_TYPE="$4"

# loading 22 synthetic datasets with the following parameters:
# support = 1.0 by design (skipping)
# no derivations
# saturation disabled because it is not necessary (no ontology)
SATURATE=false

for DATASET in 'micro_benchmark_10M_3dim_1_1_1ratio_0.1sparsity_10mea_5uniform_5zipf.nt' 'micro_benchmark_10M_3dim_1_1_1ratio_0.5sparsity_10mea_5uniform_5zipf.nt' 'micro_benchmark_1M_3dim_1_1_1ratio_0.1sparsity_10mea_5uniform_5zipf.nt' 'micro_benchmark_1M_3dim_1_1_1ratio_0.1sparsity_3mea_1uniform_2zipf.nt' 'micro_benchmark_1M_3dim_1_1_1ratio_0.1sparsity_5mea_2uniform_3zipf.nt' 'micro_benchmark_1M_3dim_1_1_1ratio_0.5sparsity_10mea_5uniform_5zipf.nt' 'micro_benchmark_1M_3dim_1_1_1ratio_0.5sparsity_3mea_1uniform_2zipf.nt' 'micro_benchmark_1M_3dim_1_1_1ratio_0.5sparsity_5mea_2uniform_3zipf.nt' 'micro_benchmark_1M_3dim_1_20_50ratio_0.1sparsity_10mea_5uniform_5zipf.nt' 'micro_benchmark_1M_3dim_1_20_50ratio_0.1sparsity_3mea_1uniform_2zipf.nt' 'micro_benchmark_1M_3dim_1_20_50ratio_0.1sparsity_5mea_2uniform_3zipf.nt' 'micro_benchmark_1M_3dim_1_20_50ratio_0.5sparsity_10mea_5uniform_5zipf.nt' 'micro_benchmark_1M_3dim_1_20_50ratio_0.5sparsity_3mea_1uniform_2zipf.nt' 'micro_benchmark_1M_3dim_1_20_50ratio_0.5sparsity_5mea_2uniform_3zipf.nt' 'micro_benchmark_5M_3dim_1_1_1ratio_0.1sparsity_10mea_5uniform_5zipf.nt' 'micro_benchmark_5M_3dim_1_1_1ratio_0.5sparsity_10mea_5uniform_5zipf.nt'; do
	./scripts/load_dataset.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT "/data/datasets/Spade_benchmark/${DATASET}" $SATURATE $SUMMARY_TYPE ENABLE_COUNT_DERIVATION=false ENABLE_PATH_DERIVATION=false ENABLE_KWD_EXTRACTION=false ENABLE_LANGUAGE_DETECTION=false
done

#SATURATE=true

#for DATASET in 'bsbm1m.nt' 'bsbm10m.nt' 'bsbm100m.nt'; do
#	./scripts/load_dataset.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT "/data/datasets/bsbm/${DATASET}" $SATURATE $SUMMARY_TYPE ENABLE_COUNT_DERIVATION=true ENABLE_PATH_DERIVATION=true ENABLE_KWD_EXTRACTION=true ENABLE_LANGUAGE_DETECTION=true
#done

#for DATASET in 'lubm1m_uniq_onto.nt' 'lubm10m_uniq_onto.nt' 'lubm100m_uniq_onto.nt'; do
#	./scripts/load_dataset.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT "/data/datasets/lubm/${DATASET}" $SATURATE $SUMMARY_TYPE ENABLE_COUNT_DERIVATION=true ENABLE_PATH_DERIVATION=true ENABLE_KWD_EXTRACTION=true ENABLE_LANGUAGE_DETECTION=true
#done
