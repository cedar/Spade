#!/bin/bash

# Usage
# ./scripts/run_experiment.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATASET [KEY=VALUE options for conf/configuration.properties file]
# Note: directories must end with /
# Example
# ./scripts/run_experiment.sh /home/cedar/pguzewic/Spade/ /local/pguzewic/Spade_experiments/output/ nobelprize_org_data_uniq DATABASE=nobelprize_org_data_uniq MAX_NUMBER_OF_DIMENSIONS=3 NUMBER_OF_TUPLES_TO_SAMPLE_FROM_EACH_GROUP=0

SPADE_REPOSITORY_MAIN_DIRECTORY=$1
shift # remove $1 from the arguments list
cd "$SPADE_REPOSITORY_MAIN_DIRECTORY"

OUTPUT_DIRECTORY=$1
shift # remove $1 from the arguments list

DATASET=$1
shift # remove $1 from the arguments list

DATETIME=$(date +'%Y-%m-%d_%H-%M-%S')

OUTPUT_DIRECTORY="${OUTPUT_DIRECTORY}$DATASET/$DATETIME"
mkdir -p "$OUTPUT_DIRECTORY"

#MOLAP_DIRECTORY="${OUTPUT_DIRECTORY}/molap"
RESULTS_DIRECTORY="${OUTPUT_DIRECTORY}/results"

CONFIGURATION_FILE=conf/configuration.properties
git checkout -- "$CONFIGURATION_FILE" # set the file to the default values
echo "Reverting changes to the configuration file '$CONFIGURATION_FILE'"
echo "Altering the configuration file '$CONFIGURATION_FILE' by:"

ARGS=("$@")
#ARGS+=("MOLAP_FOLDER_PATH=$MOLAP_DIRECTORY/")
ARGS+=("FLASK_FOLDER_PATH=$RESULTS_DIRECTORY/")
for PROPERTY in "${ARGS[@]}"; do
	KEY="${PROPERTY%=*}"
	NEW_VALUE="${PROPERTY##*=}"
	NEW_VALUE="${NEW_VALUE//\//\\/}"

	if ! grep -R "^[#]*\s*${KEY}=.*" "$CONFIGURATION_FILE" > /dev/null; then
		echo "    appending a new key '$KEY' with value '$NEW_VALUE'"
		echo "$KEY=$NEW_VALUE" >> "$CONFIGURATION_FILE"
	else
		echo "    setting key '$KEY' to value '$NEW_VALUE'"
		sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$NEW_VALUE/" "$CONFIGURATION_FILE"
	fi

	shift # remove $1 from the arguments list
done

rm "${CONFIGURATION_FILE}.bak"

cp $CONFIGURATION_FILE "$OUTPUT_DIRECTORY/"

echo 'Dropping online tables'

./scripts/drop_online_tables.sh

echo 'The online tables dropped'

echo 'Starting the run'

LOG_FILE="${OUTPUT_DIRECTORY}/run.log"

./scripts/run_Spade.sh "$LOG_FILE"

echo 'The run finished'

mv top_k.csv performance_measurements.json "$OUTPUT_DIRECTORY/"
#rmdir $MOLAP_DIRECTORY
