#!/bin/bash

SPADE_REPOSITORY_MAIN_DIRECTORY='/home/cedar/pguzewic/Spade/'
LOADING_DIRECTORY='/local/pguzewic/Spade_experiments/loading/'
DATABASE_PORT=5433
SATURATE=false
SUMMARY_TYPE='typedstrong'

for DATASET in 'micro_benchmark_1Mfac_3dim_15mea_13uniform_2zipf.nt' 'micro_benchmark_2.5Mfac_3dim_15mea_13uniform_2zipf.nt'; do
	./scripts/load_dataset.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT "/data/datasets/Spade_benchmark/scalability_study/${DATASET}" $SATURATE $SUMMARY_TYPE ENABLE_COUNT_DERIVATION=false ENABLE_PATH_DERIVATION=false ENABLE_KWD_EXTRACTION=false ENABLE_LANGUAGE_DETECTION=false
done

SUMMARY_TYPE='2ptypedstrong'
for DATASET in 'micro_benchmark_5Mfac_1dim_15mea_13uniform_2zipf.nt' 'micro_benchmark_5Mfac_2dim_15mea_13uniform_2zipf.nt' 'micro_benchmark_5Mfac_3dim_10mea_9uniform_1zipf.nt' 'micro_benchmark_5Mfac_3dim_15mea_13uniform_2zipf.nt' 'micro_benchmark_5Mfac_3dim_20mea_18uniform_2zipf.nt' 'micro_benchmark_5Mfac_3dim_25mea_22uniform_3zipf.nt' 'micro_benchmark_5Mfac_3dim_30mea_27uniform_3zipf.nt' 'micro_benchmark_5Mfac_3dim_5mea_4uniform_1zipf.nt' 'micro_benchmark_5Mfac_4dim_15mea_13uniform_2zipf.nt' 'micro_benchmark_5Mfac_5dim_15mea_13uniform_2zipf.nt' 'micro_benchmark_7.5Mfac_3dim_15mea_13uniform_2zipf.nt' 'micro_benchmark_10Mfac_3dim_15mea_13uniform_2zipf.nt'; do
	./scripts/load_dataset.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT "/data/datasets/Spade_benchmark/scalability_study/${DATASET}" $SATURATE $SUMMARY_TYPE ENABLE_COUNT_DERIVATION=false ENABLE_PATH_DERIVATION=false ENABLE_KWD_EXTRACTION=false ENABLE_LANGUAGE_DETECTION=false
done

