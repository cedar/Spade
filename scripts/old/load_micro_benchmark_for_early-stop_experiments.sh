#!/bin/bash

SPADE_REPOSITORY_MAIN_DIRECTORY='/home/cedar/pguzewic/Spade/'
LOADING_DIRECTORY='/local/pguzewic/Spade_experiments/loading/'
DATABASE_PORT=5433
SATURATE=false


SUMMARY_TYPE='typedstrong'
# 'micro_benchmark_100000fac_1dim_100mea_90uniform_10zipf.nt'
# 'micro_benchmark_100000fac_3dim_100mea_90uniform_10zipf.nt'
# 'micro_benchmark_1Mfac_3dim_100mea_90uniform_10zipf.nt'
for DATASET in 'micro_benchmark_100000fac_3dim_30mea_28uniform_2zipf.nt' 'micro_benchmark_1Mfac_3dim_30mea_28uniform_2zipf.nt'; do
	./scripts/load_dataset.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT "/data/datasets/Spade_benchmark/${DATASET}" $SATURATE $SUMMARY_TYPE ENABLE_COUNT_DERIVATION=false ENABLE_PATH_DERIVATION=false ENABLE_KWD_EXTRACTION=false ENABLE_LANGUAGE_DETECTION=false
done


SUMMARY_TYPE='2ptypedstrong'
# 'micro_benchmark_10Mfac_3dim_30mea_28uniform_2zipf.nt'
for DATASET in 'micro_benchmark_5Mfac_3dim_30mea_28uniform_2zipf.nt'; do
	./scripts/load_dataset.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT "/data/datasets/Spade_benchmark/${DATASET}" $SATURATE ${SUMMARY_TYPE} ENABLE_COUNT_DERIVATION=false ENABLE_PATH_DERIVATION=false ENABLE_KWD_EXTRACTION=false ENABLE_LANGUAGE_DETECTION=false
done
