#!/bin/bash

SPADE_REPOSITORY_MAIN_DIRECTORY="$1"
OUTPUT_DIRECTORY="$2"
DATABASE_PORT="$3"

DATASET='nobelprize_org_data_uniq'
./scripts/run_experiment.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATASET DATABASE_PORT=$DATABASE_PORT DATABASE_NAME=$DATASET ENABLE_COUNT_DERIVATION=true ENABLE_PATH_DERIVATION=true ENABLE_KWD_EXTRACTION=true ENABLE_LANGUAGE_DETECTION=true MAX_NUMBER_OF_DIMENSIONS=3 NUMBER_OF_TUPLES_TO_SAMPLE_FROM_EACH_GROUP=0
./scripts/run_experiment.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATASET DATABASE_PORT=$DATABASE_PORT DATABASE_NAME=$DATASET ENABLE_COUNT_DERIVATION=true ENABLE_PATH_DERIVATION=true ENABLE_KWD_EXTRACTION=true ENABLE_LANGUAGE_DETECTION=true MAX_NUMBER_OF_DIMENSIONS=3 NUMBER_OF_TUPLES_TO_SAMPLE_FROM_EACH_GROUP=30 EARLY_STOP_RETRIEVAL_BATCH_SIZE=30 EARLY_STOP_MAX_NUMBER_OF_ITERATIONS_WITHOUT_STOPPAGE=2
./scripts/run_experiment.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATASET DATABASE_PORT=$DATABASE_PORT DATABASE_NAME=$DATASET ENABLE_COUNT_DERIVATION=true ENABLE_PATH_DERIVATION=true ENABLE_KWD_EXTRACTION=true ENABLE_LANGUAGE_DETECTION=true MAX_NUMBER_OF_DIMENSIONS=3 AGGREGATE_EVALUATION_MODULE=cube
