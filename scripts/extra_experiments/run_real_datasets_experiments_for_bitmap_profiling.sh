#!/bin/bash

SPADE_REPOSITORY_MAIN_DIRECTORY='/home/cedar/mmazuran/Spade/'
LOADING_DIRECTORY='/local/mmazuran/Spade_experiments/loading/'
OUTPUT_DIRECTORY='/local/mmazuran/Spade_BitmapProfiles/output/'
DATABASE_PORT=5433

# real data profiles (done on cedar002)
for DATASET in 'CEOsWithAllTheirData_Plus2hopsFIXED' 'nasa' 'nobelprize_org_data_uniq' 'foodista' 'dblp2017_articles' 'airlinedelays'; do
	./scripts/run_experiment.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $OUTPUT_DIRECTORY $DATASET DATABASE_PORT=$DATABASE_PORT DATABASE_NAME=$DATASET SUPPORT_THRESHOLD=0.6 K_FOR_TOP_K=10 MAX_NUMBER_OF_DIMENSIONS=3 NUMBER_OF_TUPLES_TO_SAMPLE_FROM_EACH_GROUP=0 ENABLE_COUNT_DERIVATION=true ENABLE_PATH_DERIVATION=true ENABLE_KWD_EXTRACTION=true ENABLE_LANGUAGE_DETECTION=true AGGREGATE_EVALUATION_MODULE=molap PROFILE_BITMAP_SIZE=true PROFILE_BITMAP_SIZE_FILE_NAME="${DATASET}_bitmapsprofiles.csv"
done

