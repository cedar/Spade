import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math


def compute_bins(top_k, k_for_top_k):
    left = np.histogram_bin_edges(top_k[k_for_top_k:], bins=5)
    right = np.histogram_bin_edges(top_k[:k_for_top_k], bins=5)

    left_list = list(left)
    right_list = list(right)

    # if left_list[0] != 0.0:
    #    left_list.insert(0, 0.0)

    return left_list + right_list


if __name__ == "__main__":
    dataset_name = sys.argv[1]
    top_k_filename = sys.argv[2]
    k_for_top_k = int(sys.argv[3])
    output_directory = sys.argv[4]

    top_k = pd.read_csv(
        top_k_filename,
        header=0,
        names=["aggregate", "interestingness"],
        delimiter=",",
    )["interestingness"]

    # pd.set_option('display.max_rows', None)
    # pd.set_option('display.max_columns', None)
    # pd.set_option('display.width', None)
    # pd.set_option('display.max_colwidth', -1)
    # print(top_k)

    if k_for_top_k > len(top_k.index):
        k_for_top_k = len(top_k.index)
    if k_for_top_k == 0:
        sys.exit(0)

    kth_best_score = top_k.loc[k_for_top_k - 1]

    plt.grid(axis="y", zorder=0)
    y, x, _ = plt.hist(
        top_k,
        bins=compute_bins(top_k, k_for_top_k),
        log=True,
        color="mediumorchid",
        edgecolor="black",
        zorder=2,
    )
    # plt.xscale("log")

    title = dataset_name + " interestingness profile"
    plt.title(title)

    plt.xlabel("interestingness score", fontsize=12)
    plt.ylabel("number of aggregates", fontsize=12)

    plt.axvline(kth_best_score, color="black", linestyle="dashed", linewidth=3)
    middle_y = 10 ** (math.log(y.max(), 10) / 2)
    # print(str(dataset_name) + ": " + str(y.max()) + ", " + str(middle_y))
    t = plt.text(
        kth_best_score,
        middle_y,
        "top " + str(k_for_top_k),
        fontsize="20",
        color="black",
        rotation=270,
        verticalalignment="center",
    )
    t.set_bbox(dict(facecolor="white", alpha=0.7, edgecolor="white"))
    plt.savefig(output_directory + title.replace(" ", "_") + ".png")
