#!/bin/bash

# Example:
# ./scripts/draw_dataset_interestingness_profiles.sh /local/pguzewic/Spade_experiments/interestingness_profiles/ /local/mmazuran/Spade_experiments/output/CEOsWithAllTheirData_Plus2hops/2020-09-02_09-01-09/top_k.csv

OUTPUT_DIRECTORY="$1"
shift

for TOP_K_FILE in "$@"; do
	DATASET=$(echo $TOP_K_FILE | rev | cut -d'/' -f2-3 | rev | tr '/' '_')
	python3 scripts/dataset_interestingness_profile.py ${DATASET} ${TOP_K_FILE} 10 $OUTPUT_DIRECTORY
done;
