#!/bin/bash

# Usage
# ./scripts/load_dataset.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT $DATASET $SATURATE $SUMMARY_TYPE [KEY=VALUE options for conf/configuration.properties file]
# Note: directories must end with /
# Example
# ./scripts/load_dataset.sh /home/cedar/pguzewic/Spade/ /local/pguzewic/Spade_experiments/loading/ 5433 "/data/datasets/Spade_benchmark/micro_benchmark_10M_3dim_1_1_1ratio_0.1sparsity_10mea_5uniform_5zipf.nt" false typedstrong ENABLE_COUNT_DERIVATION=false ENABLE_PATH_DERIVATION=false ENABLE_KWD_EXTRACTION=false ENABLE_LANGUAGE_DETECTION=false

SPADE_REPOSITORY_MAIN_DIRECTORY="$1"
shift # remove $1 from the arguments list
cd "$SPADE_REPOSITORY_MAIN_DIRECTORY"

LOADING_DIRECTORY="$1"
shift

mkdir -p "$LOADING_DIRECTORY"

DATABASE_PORT=$1
shift

DATASET="$1"
shift

SATURATE=$1
shift

SUMMARY_TYPE=$1
shift

DATETIME=$(date +'%Y-%m-%d_%H-%M-%S')

LOADING_PROPERTIES_FILE=conf/loading.properties
SUMMARIZATION_PROPERTIES_FILE=conf/summarization.properties
CONFIGURATION_FILE=conf/configuration.properties
# set the files to the default values
git checkout -- "$LOADING_PROPERTIES_FILE"
git checkout -- "$SUMMARIZATION_PROPERTIES_FILE"
git checkout -- "$CONFIGURATION_FILE"

KEY='database.port'
sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$DATABASE_PORT/" "$LOADING_PROPERTIES_FILE"
sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$DATABASE_PORT/" "$SUMMARIZATION_PROPERTIES_FILE"

KEY='DATABASE_PORT'
sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$DATABASE_PORT/" "$CONFIGURATION_FILE"

KEY='dataset.filename'
DATASET=${DATASET//\//\\/}
sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$DATASET/" "$LOADING_PROPERTIES_FILE"

KEY='database.name'
DATASET=${DATASET%.*}
DATASET=${DATASET##*/}
sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$DATASET/" "$SUMMARIZATION_PROPERTIES_FILE"

KEY='DATABASE_NAME'
sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$DATASET/" "$CONFIGURATION_FILE"

KEY='saturation.enable'
sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$SATURATE/" "$LOADING_PROPERTIES_FILE"
KEY='saturation.type'
if [ "$SATURATE" = 'true' ]; then
	sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=ASSERTION_SAT/" "$LOADING_PROPERTIES_FILE"
else
	sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=NONE/" "$LOADING_PROPERTIES_FILE"
fi
KEY='summary.summarize_saturated_graph'
sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$SATURATE/" "$SUMMARIZATION_PROPERTIES_FILE"

KEY='summary.type'
sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$SUMMARY_TYPE/" "$SUMMARIZATION_PROPERTIES_FILE"

echo "Reverting changes to the configuration file '$CONFIGURATION_FILE'"
echo "Altering the configuration file '$CONFIGURATION_FILE' by:"

ARGS=("$@")
for PROPERTY in "${ARGS[@]}"; do
	KEY="${PROPERTY%=*}"
	NEW_VALUE="${PROPERTY##*=}"
	NEW_VALUE="${NEW_VALUE//\//\\/}"

	if ! grep -R "^[#]*\s*${KEY}=.*" "$CONFIGURATION_FILE" > /dev/null; then
		echo "    appending a new key '$KEY' with value '$NEW_VALUE'"
		echo "$KEY=$NEW_VALUE" >> "$CONFIGURATION_FILE"
	else
		echo "    setting key '$KEY' to value '$NEW_VALUE'"
		sed -i'.bak' "s/^[#]*\s*${KEY}=.*/$KEY=$NEW_VALUE/" "$CONFIGURATION_FILE"
	fi

	shift # remove $1 from the arguments list
done

LOG_FILE="${LOADING_DIRECTORY}${DATASET}_${DATETIME}.log"

rm "${LOADING_PROPERTIES_FILE}.bak" "${SUMMARIZATION_PROPERTIES_FILE}.bak" "${CONFIGURATION_FILE}.bak"

./scripts/load.sh "$LOG_FILE"
