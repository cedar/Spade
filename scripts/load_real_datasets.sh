#!/bin/bash

SPADE_REPOSITORY_MAIN_DIRECTORY="$1"
LOADING_DIRECTORY="$2"
DATABASE_PORT="$3"
SUMMARY_TYPE="$4"

# loading the 6 real datasets with the following parameters:
# support = 0.6
# all derivations enabled 
# saturation enabled
SATURATE=true

for DATASET in 'CEOs/CEOsWithAllTheirData_Plus2hops.nt' 'nasa/nasa.nt' 'nobelprize_org/nobelprize_org_data_uniq.nt' 'foodista/foodista.nt' 'dblp/dblp2017_articles.nt' 'airlinedelays/airlinedelays.nt'; do
	./scripts/load_dataset.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT "/data/datasets/${DATASET}" $SATURATE $SUMMARY_TYPE SUPPORT_THRESHOLD=0.6 ENABLE_COUNT_DERIVATION=true ENABLE_PATH_DERIVATION=true ENABLE_KWD_EXTRACTION=true ENABLE_LANGUAGE_DETECTION=true
done

#./scripts/load_dataset.sh $SPADE_REPOSITORY_MAIN_DIRECTORY $LOADING_DIRECTORY $DATABASE_PORT "/data/datasets/dblp/dblp2017.nt" $SATURATE $SUMMARY_TYPE ENABLE_COUNT_DERIVATION=true ENABLE_PATH_DERIVATION=false ENABLE_KWD_EXTRACTION=false ENABLE_LANGUAGE_DETECTION=true