#!/bin/bash

VERSION="1.0"
LOG_FILE=$1

# -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps
# -Dlog4j.configuration=/src/main/resources/log4j.xml
java -XX:+UseConcMarkSweepGC -Xmx90g -Djava.util.secureRandomSeed=true -jar target/Spade-$VERSION-with-dependencies.jar 2>&1 | tee -a $LOG_FILE

